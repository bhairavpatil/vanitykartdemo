{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !isset($content_only) || !$content_only}
                    </div><!-- #center_column -->
                    </div><!-- .row -->
                </div><!-- #columns -->
            </div><!-- .columns-container -->
                <!-- Footer -->
                <section class="subscribe-container">
                          <div class="container" align="center">
                            <div class="row">
                              <div class="col-md-12">   
                                <img src="http://vanitykart.com/demo/images/subscibes.jpg" alt="subscribe">        
                              </div>
                            </div>
                          </div>
                        </section>

                        <div class="main-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <div class="footer-widget">
                            <h3 class="widget-title">About us</h3>
                            <ul>
                                <li><a href="#">Who we are</a></li>
                                <li><a href="#">Brands A-Z</a></li>
                                <li><a href="#">Careers</a></li>
                            </ul>
                        </div> 
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2">
                        <div class="footer-widget">
                            <h3 class="widget-title">support</h3>
                            <ul>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">Online Returns</a></li>
                                <li><a href="#">Shipping &amp; Delivery</a></li>
                            </ul>

                        </div> 
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-2">
                        <div class="footer-widget">
                            <h3 class="widget-title">Legal</h3>
                            <ul>
                                <li><a href="#">Price Match</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Terms and Conditions</a></li>
                            </ul>
                        </div> 
                    </div> <!-- /.col-md-2 -->
                    <div class="col-md-3">
                        <div class="footer-widget">
                            <h3 class="widget-title">TOP BRANDS</h3>
                            
                             
                                <ul class="col-three">
                                    <li><a href="#">Ginger</a></li>
                                    <li><a href="#">Mango</a></li>
                                    <li><a href="#">Seventy Five</a></li>                                    
                                </ul>
                             
                             
                                <ul class="col-three">
                                    <li><a href="#">Calvin Klein</a></li>
                                    <li><a href="#">Bareminerals</a></li>
                                    <li><a href="#">Calvin Klein </a></li>
                                   
                                </ul>
                             
                             
                                <ul class="col-three">
                                    <li><a href="#">Dior</a></li>
                                    <li><a href="#">COCO</a></li>
                                    <li><a href="#">Floris</a></li>
                                   
                                </ul>
                            
                        </div> 
                    </div>
                    <div class="col-md-2">
                        <div class="footer-widget">
                            <h3 class="widget-title"><div align="center">find us on</div></h3>
                            <ul>
                                <li>
                                  <img src="http://vanitykart.com/demo/images/social-icons.png" alt="social-icons">
                                </li>
                            </ul>
                        </div> 
                    </div>
                </div> <!-- /.row -->
              <div class="bottom-footer">
                <div class="container">
                  <div class="row">
                    <div class="col-md-3">
                     
                      
                       <div class="phone">
                        <img src="http://vanitykart.com/demo/images/icon6.png" alt="icons - phone">
                      </div>
                      <div class="contact-details">
                        <span>Vanity kart </span><span class="black"> 800 444 222</span><br>
                        <div class="mail"><a href="mailto:service@vanitykart.com">service@vanitykart.com</a></div>
                     </div>
                      

                    </div>
                    <div class="col-md-6" align="center">
                     <div class="copyright"> © 2017 VanityKart. All Rights Reserved</div>
                    </div>
                    <div class="col-md-3">
                      <img src="http://vanitykart.com/demo/images/paypal.png" alt="paypal">
                    </div>
                  </div>
                  </div>
                  
                </div>

            </div> <!-- /.container -->
        </div>
                <!-- #footer -->
        </div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
    </body>
</html>