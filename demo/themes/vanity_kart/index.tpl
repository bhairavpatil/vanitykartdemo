{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if isset($HOOK_HOME_TAB_CONTENT) && $HOOK_HOME_TAB_CONTENT|trim}
    {if isset($HOOK_HOME_TAB) && $HOOK_HOME_TAB|trim}
        <ul id="home-page-tabs" class="nav nav-tabs clearfix">
			{$HOOK_HOME_TAB}
		</ul>
	{/if}
	<div class="tab-content">{$HOOK_HOME_TAB_CONTENT}</div>
{/if}
{if isset($HOOK_HOME) && $HOOK_HOME|trim}
	<div class="clearfix">{$HOOK_HOME}</div>
{/if}




<!--Page Loading Overlay Start-->
<div class="page_loading_overlay">
	<div class="page_loading_overlay_content">
		<img src="{$img_dir}logo.png" alt="{l s='Vanitykart'}"/>

		<div id="ballsWaveG">
			<div id="ballsWaveG_1" class="ballsWaveG"></div>
			<div id="ballsWaveG_2" class="ballsWaveG"></div>
			<div id="ballsWaveG_3" class="ballsWaveG"></div>
			<div id="ballsWaveG_4" class="ballsWaveG"></div>
			<div id="ballsWaveG_5" class="ballsWaveG"></div>
			<div id="ballsWaveG_6" class="ballsWaveG"></div>
			<div id="ballsWaveG_7" class="ballsWaveG"></div>
			<div id="ballsWaveG_8" class="ballsWaveG"></div>
		</div>

	</div> <!--/page_loading_overlay_content-->
</div> <!--/page_loading_overlay-->

<!--Page Loading Overlay End-->


<!-- Beauty Section Start -->

	<!-- Beauty Popup -->
	<div class="beauty_popup">
		
		<!-- Beauty Banner Section Start -->
		<section class="beauty_banner">
			<img src="{$img_dir}beauty_banner.jpg" alt="beauty_banner"/>
			<div class="sliderBtn text-center">
				<button class="btn btn-cust hvr-shutter-out-horizontal">Shop the Collection</button>
			</div> <!--/sliderBtn-->
		</section> <!--/beauty_banner-->
		<!-- Beauty Banner Section End -->
		
		<!-- Mood Board Section Start -->
		<section class="mood_board_sec">
			<div class="container">
				<div class="row">	
					<div class="col-lg-9 col-md-9 col-sm-9">
						<!-- Moodboard Section Code Here -->
						<h3 class="temp-title">Moodboard Section Code Here <span>Moodboard Section Code Here</span></h3>
					</div> <!--/col-md-9-->
					
					<div class="col-lg-3 col-md-3 col-sm-3">
						<!-- Related Article Section Code Here-->	
						<h3 class="temp-title">Related Article Section Code Here</h3>
					</div> <!--/col-md-3-->
					
				</div> <!--/row-->
			</div> <!--/container-->
		</section> <!--/mood_board_sec-->
		<!-- Mood Board Section End -->
		
		<!-- Showcase Section Start -->
		<section class="showcase_sec">
			<div class="container">
				<div class="row">	
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- Showcase Section Code Here -->	
						<h3 class="temp-title">Showcase Section Code Here</h3>
					</div> <!--/col-md-12-->							
				</div> <!--/row-->
			</div> <!--/container-->
		</section> <!--/showcase_sec-->
		<!-- Showcase Section End -->
		
		<!-- Hot Product Section Start -->
		<section class="hot_product_sec">
			<div class="container">
				<div class="row">	
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- Hot Product Section Code Here -->
						<h3 class="temp-title">Hot Product Section Code Here</h3>
					</div> <!--/col-md-12-->							
				</div> <!--/row-->
			</div> <!--/container-->
		</section> <!--/hot_product_sec-->
		<!-- Hot Product Section End -->
		
		<!-- Video Image Section Start -->
		<section class="video_img_sec">
			<div class="container">
				<div class="row">	
					<div class="col-lg-12 col-md-12 col-sm-12">
						<!-- Video Image Section Code Here -->
						<h3 class="temp-title">Video-Image Section Code Here</h3>
					</div> <!--/col-md-12-->							
				</div> <!--/row-->
			</div> <!--/container-->
		</section> <!--/video_img_sec-->
		<!-- Video Image Section End -->

	</div> <!--/beauty_popup-->
	
	<!-- Beauty Breadcrumb Section Start -->
	<div class="beauty_breadcrumb">
		<span><img src="{$img_dir}flower6.png" alt="{l s='Flower'}"/></span>
		<a href="#" class="active-breadcrumb" title="index-home">BEAUTY</a>
	</div> <!--/beauty_breadcrumb-->
	<!-- Beauty Breadcrumb Section End -->

<!-- Beauty Section End -->


