<div class="header-top-menu">
	<div class="container">

		{if $MENU != ''}
			<!-- Menu -->
			<div id="block_top_menu" class="sf-contener clearfix col-lg-12">
				<div class="cat-title">{l s="Menu" mod="blocktopmenu"}</div>
				<ul class="sf-menu clearfix menu-content">
					{$MENU}
					
					{if $MENU_SEARCH}
						<li class="sf-search noBack" style="float:right">
							<form id="searchbox" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" method="get">
								<p>
									<input type="hidden" name="controller" value="search" />
									<input type="hidden" value="position" name="orderby"/>
									<input type="hidden" value="desc" name="orderway"/>
									<input type="text" name="search_query" value="{if isset($smarty.get.search_query)}{$smarty.get.search_query|escape:'html':'UTF-8'}{/if}" />
								</p>
							</form>
						</li>
					{/if}
				</ul>
				
				
				
				<ul class="menu-right-menu">
					<!--<li class="" title="Sign In"><a href="#"><img src="http://localhost/vanity_cart/themes/default-bootstrap/img/sign-in.png"/></a></li>
					<li class="" title="Wishlist"><a href="#"><img src="{$img_dir}wishlist.png"/></a></li>-->
				</ul>
				
				
			</div>
			<!--/ Menu -->
		{/if}
		
	</div> <!--/container-->
</div> <!--/header-top-menu-->