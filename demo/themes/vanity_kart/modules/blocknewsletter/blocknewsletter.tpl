{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}





<!-- Block Newsletter module-->
<div id="newsletter_block_left" class="newsletterSec">
	<div class="container">

		<div class="row">
			<div class="col-lg-5 col-md-5 hidden-sm">
				<div class="row policy_section_outer">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="policy_section policy_section1">							
							<img src="{$img_dir}policy/policy1.png" alt="{l s=''}"/>
							Free 2 hrs <span>Delivery</span>
						</div> <!--/policy_section-->
					</div> <!--/col-md-4-->
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="policy_section policy_section2">							
							<img src="{$img_dir}policy/policy2.png" alt="{l s=''}"/>
							30-DayS <span>Returns</span>
						</div> <!--/policy_section-->
					</div> <!--/col-md-4-->
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="policy_section policy_section3">							
							<img src="{$img_dir}policy/policy3.png" alt="{l s=''}"/>
							100% Secure <span>Checkout</span>
						</div> <!--/policy_section-->
					</div> <!--/col-md-4-->
				</div> <!--/row-->
			</div> <!--/col-md-6-->


			<div class="col-lg-7 col-md-7 col-sm-12">				
				<div class="row newsletter_section_outer">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="newsletter_signup_txt">							
							<h3>Signup for our Newsletter<h3>
							<p>Get access to latest arrivals <span>& fashion inspiration</span></p>
						</div> <!--/newsletter_signup_txt-->
					</div> <!--/col-md-4-->
					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="newsletter-ip">							
							
							<div class="block_content">
								<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
									<div class="form-group{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
									<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{else}{l s='Email address' mod='blocknewsletter'}{/if}" />
										<button type="submit" name="submitNewsletter" class="btn btn-default button button-small hvr-sweep-to-right">
											<span>{l s='Subscribe' mod='blocknewsletter'}</span>
										</button>
										<input type="hidden" name="action" value="0" />
									</div>
								</form>
							</div>
							{hook h="displayBlockNewsletterBottom" from='blocknewsletter'}				
						</div> 


						</div> <!--/newsletter_signup_form-->
					</div> <!--/col-md-8-->
				</div> <!--/row-->
			</div> <!--/col-md-6-->

		</div> <!--/row-->









		<!-- <div class="row">
			<div class="col-md-4 col-sm-4">				
				<h4> <img src="{$img_dir}envelope.png">   {l s='[1]Keep up to date with the[/1] latest arrivals & fashion inspiration' tags=['<span>'] mod='blocknewsletter'}</h4>
			</div> 
			
			<div class="col-md-8 col-sm-8 newsletter-ip">				
				
				<div class="block_content">
					<form action="{$link->getPageLink('index', null, null, null, false, null, true)|escape:'html':'UTF-8'}" method="post">
						<div class="form-group{if isset($msg) && $msg } {if $nw_error}form-error{else}form-ok{/if}{/if}" >
							<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="{if isset($msg) && $msg}{$msg}{elseif isset($value) && $value}{$value}{else}{l s='Enter your e-mail' mod='blocknewsletter'}{/if}" />
							<button type="submit" name="submitNewsletter" class="btn btn-default button button-small hvr-sweep-to-right">
								<span>{l s='Subscribe' mod='blocknewsletter'}</span>
							</button>
							<input type="hidden" name="action" value="0" />
						</div>
					</form>
				</div>
				{hook h="displayBlockNewsletterBottom" from='blocknewsletter'}				
			</div> 
		</div>  -->




	</div> <!--/container-->
</div> <!--/newsletterSec-->

<div class="clearfix"></div>

<!-- Footer Start -->

<div class="footer-sec">
	<div class="container">
		<div class="row">

<!-- /Block Newsletter module-->
{strip}
{if isset($msg) && $msg}
{addJsDef msg_newsl=$msg|@addcslashes:'\''}
{/if}
{if isset($nw_error)}
{addJsDef nw_error=$nw_error}
{/if}
{addJsDefL name=placeholder_blocknewsletter}{l s='Enter your email address' mod='blocknewsletter' js=1}{/addJsDefL}
{if isset($msg) && $msg}
	{addJsDefL name=alert_blocknewsletter}{l s='Newsletter : %1$s' sprintf=$msg js=1 mod="blocknewsletter"}{/addJsDefL}
{/if}
{/strip}