{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="footer-btm">
	<div class="container">
		<div class="row">
			<!-- MODULE Block contact infos -->
			<div id="block_contact_infos" class="footer-block col-md-4 col-sm-4 col-xs-12">
				<div>
					
					<ul class="toggle-footer contact-info-sec">
						{if $blockcontactinfos_company != ''}
							<li>
							<!--<i class="icon-phone"></i>-->
							    <img src="{$img_dir}phone.png"> {l s='' mod='blockcontactinfos'} 
								{$blockcontactinfos_company|escape:'html':'UTF-8'}{if $blockcontactinfos_address != ''}, {$blockcontactinfos_address|escape:'html':'UTF-8'}{/if}
								{if $blockcontactinfos_phone != ''}
								<span>{$blockcontactinfos_phone|escape:'html':'UTF-8'}</span>
								{/if}
							</li>
						{/if}
						{if $blockcontactinfos_phone != ''}
							<!-- <li>
								<i class="icon-phone"></i>{l s='' mod='blockcontactinfos'} 
								<span></span>
							</li> -->
						{/if}
						{if $blockcontactinfos_email != ''}
							<li>
								{l s='' mod='blockcontactinfos'} 
								<span>{mailto address=$blockcontactinfos_email|escape:'html':'UTF-8' encode="hex"}</span>
							</li>
						{/if}
					</ul>
				</div>
			</div> 
			<!-- /MODULE Block contact infos -->

			<div class="col-md-4 col-sm-4 col-xs-12 copyrightSec">
				{l s=' %3$s %2$s - [1]VanityKart.[/1] All Rights Reserved' mod='blockcms' sprintf=['PrestaShop™', 'Y'|date, '©'] tags=['<a class="_blank">'] nocache}</a>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 paymentSec">
				<img src="{$img_dir}payment.png">
			</div>
			
		</div> <!--/row-->
	</div> <!--/container-->
</div> <!--/footer-btm-->
</div> <!--/footer-sec-->



