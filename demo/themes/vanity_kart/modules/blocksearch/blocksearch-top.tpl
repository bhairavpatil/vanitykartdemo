{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="header-top-middle">
	<div class="">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 clearfix header_middle_sec">
				<div class="header_middle_sec_inner">
					<!-- Block search module TOP Start -->
					<div id="search_block_top" class="">
						<form id="searchbox" method="get" action="{$link->getPageLink('search', null, null, null, false, null, true)|escape:'html':'UTF-8'}" >
							<input type="hidden" name="controller" value="search" />
							<input type="hidden" name="orderby" value="position" />
							<input type="hidden" name="orderway" value="desc" />
							<div style="position:relative;" ><input class="search_query form-control" type="text" id="search_query_top_new" name="search_query" placeholder="{l s='Search for Fashion, fashion, News & Events' mod='blocksearch'}" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" autocomplete="off" />
							<button type="submit" name="submit_search" class="btn btn-default button-search">
								<span>{l s='Search' mod='blocksearch'}</span>
							</button>
							<div id="divclose">X</div>
							</div>
						</form>
					</div>
					<!-- Block search module TOP End -->

			<div id="divautosearchresult" class="search-autocomplete"></div>
			<div class="clearfix"></div>
			</div> <!--/header_middle_sec_inner-->
			</div> <!--/col-md-4-->

			<!-- Header Logo TOP -->
			<div id="header_logo">
				<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
					<img class="logo img-responsive zoomIn animated" src="{$logo_url}" alt="{$shop_name|escape:'html':'UTF-8'}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if}/>
				</a>
				<div class="clearfix"></div>
			</div> <!--/header_logo-->
