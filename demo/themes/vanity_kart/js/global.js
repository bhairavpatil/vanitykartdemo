/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/
//global variables
var responsiveflag = false;

$(document).ready(function(){
	highdpiInit();
	responsiveResize();
	$(window).resize(responsiveResize);
	if (navigator.userAgent.match(/Android/i))
	{
		var viewport = document.querySelector('meta[name="viewport"]');
		viewport.setAttribute('content', 'initial-scale=1.0,maximum-scale=1.0,user-scalable=0,width=device-width,height=device-height');
		window.scrollTo(0, 1);
	}
	if (typeof quickView !== 'undefined' && quickView)
		quick_view();
	dropDown();

	if (typeof page_name != 'undefined' && !in_array(page_name, ['index', 'product']))
	{
		bindGrid();

		$(document).on('change', '.selectProductSort', function(e){
			if (typeof request != 'undefined' && request)
				var requestSortProducts = request;
			var splitData = $(this).val().split(':');
			var url = '';
			if (typeof requestSortProducts != 'undefined' && requestSortProducts)
			{
				url += requestSortProducts ;
				if (typeof splitData[0] !== 'undefined' && splitData[0])
				{
					url += ( requestSortProducts.indexOf('?') < 0 ? '?' : '&') + 'orderby=' + splitData[0];
					if (typeof splitData[1] !== 'undefined' && splitData[1])
						url += '&orderway=' + splitData[1];
				}
				document.location.href = url;
			}
		});

		$(document).on('change', 'select[name="n"]', function(){
			$(this.form).submit();
		});

		$(document).on('change', 'select[name="currency_payment"]', function(){
			setCurrency($(this).val());
		});
	}

	$(document).on('change', 'select[name="manufacturer_list"], select[name="supplier_list"]', function(){
		if (this.value != '')
			location.href = this.value;
	});

	$(document).on('click', '.back', function(e){
		e.preventDefault();
		history.back();
	});

	jQuery.curCSS = jQuery.css;
	if (!!$.prototype.cluetip)
		$('a.cluetip').cluetip({
			local:true,
			cursor: 'pointer',
			dropShadow: false,
			dropShadowSteps: 0,
			showTitle: false,
			tracking: true,
			sticky: false,
			mouseOutClose: true,
			fx: {
				open:       'fadeIn',
				openSpeed:  'fast'
			}
		}).css('opacity', 0.8);

	if (typeof(FancyboxI18nClose) !== 'undefined' && typeof(FancyboxI18nNext) !== 'undefined' && typeof(FancyboxI18nPrev) !== 'undefined' && !!$.prototype.fancybox)
		$.extend($.fancybox.defaults.tpl, {
			closeBtn : '<a title="' + FancyboxI18nClose + '" class="fancybox-item fancybox-close" href="javascript:;"></a>',
			next     : '<a title="' + FancyboxI18nNext + '" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
			prev     : '<a title="' + FancyboxI18nPrev + '" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
		});

	// Close Alert messages
	$(".alert.alert-danger").on('click', this, function(e){
		if (e.offsetX >= 16 && e.offsetX <= 39 && e.offsetY >= 16 && e.offsetY <= 34)
			$(this).fadeOut();
	});
});

function highdpiInit()
{
	if (typeof highDPI === 'undefined')
		return;
	if(highDPI && $('.replace-2x').css('font-size') == "1px")
	{
		var els = $("img.replace-2x").get();
		for(var i = 0; i < els.length; i++)
		{
			src = els[i].src;
			extension = src.substr( (src.lastIndexOf('.') +1) );
			src = src.replace("." + extension, "2x." + extension);

			var img = new Image();
			img.src = src;
			img.height != 0 ? els[i].src = src : els[i].src = els[i].src;
		}
	}
}


// Used to compensante Chrome/Safari bug (they don't care about scroll bar for width)
function scrollCompensate()
{
	var inner = document.createElement('p');
	inner.style.width = "100%";
	inner.style.height = "200px";

	var outer = document.createElement('div');
	outer.style.position = "absolute";
	outer.style.top = "0px";
	outer.style.left = "0px";
	outer.style.visibility = "hidden";
	outer.style.width = "200px";
	outer.style.height = "150px";
	outer.style.overflow = "hidden";
	outer.appendChild(inner);

	document.body.appendChild(outer);
	var w1 = inner.offsetWidth;
	outer.style.overflow = 'scroll';
	var w2 = inner.offsetWidth;
	if (w1 == w2) w2 = outer.clientWidth;

	document.body.removeChild(outer);

	return (w1 - w2);
}

function responsiveResize()
{
	compensante = scrollCompensate();
	if (($(window).width()+scrollCompensate()) <= 767 && responsiveflag == false)
	{
		accordion('enable');
		accordionFooter('enable');
		responsiveflag = true;
	}
	else if (($(window).width()+scrollCompensate()) >= 768)
	{
		accordion('disable');
		accordionFooter('disable');
		responsiveflag = false;
		if (typeof bindUniform !=='undefined')
			bindUniform();
	}
	blockHover();
}

function blockHover(status)
{
	var screenLg = $('body').find('.container').width() == 1170;

	if ($('.product_list').is('.grid'))
		if (screenLg)
			$('.product_list .button-container').hide();
		else
			$('.product_list .button-container').show();

	$(document).off('mouseenter').on('mouseenter', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{
			var pcHeight = $(this).parent().outerHeight();
			var pcPHeight = $(this).parent().find('.button-container').outerHeight() + $(this).parent().find('.comments_note').outerHeight() + $(this).parent().find('.functional-buttons').outerHeight();
			//$(this).parent().addClass('hovered').css({'height':pcHeight + pcPHeight, 'margin-bottom':pcPHeight * (-1)});
			$(this).find('.button-container').show();
		}
	});

	$(document).off('mouseleave').on('mouseleave', '.product_list.grid li.ajax_block_product .product-container', function(e){
		if (screenLg)
		{
			//$(this).parent().removeClass('hovered').css({'height':'auto', 'margin-bottom':'0'});
			$(this).find('.button-container').hide();
		}
	});
}

function quick_view()
{
	$(document).on('click', '.quick-view:visible, .quick-view-mobile:visible', function(e){
		e.preventDefault();
		var url = this.rel;
		var anchor = '';

		if (url.indexOf('#') != -1)
		{
			anchor = url.substring(url.indexOf('#'), url.length);
			url = url.substring(0, url.indexOf('#'));
		}

		if (url.indexOf('?') != -1)
			url += '&';
		else
			url += '?';

		if (!!$.prototype.fancybox)
			$.fancybox({
				'padding':  0,
				'width':    1087,
				'height':   610,
				'type':     'iframe',
				'href':     url + 'content_only=1' + anchor
			});
	});
}

function bindGrid()
{
	var storage = false;
	if (typeof(getStorageAvailable) !== 'undefined') {
		storage = getStorageAvailable();
	}
	if (!storage) {
		return;
	}

	var view = $.totalStorage('display');

	if (!view && (typeof displayList != 'undefined') && displayList)
		view = 'list';

	if (view && view != 'grid')
		display(view);
	else
		$('.display').find('li#grid').addClass('selected');

	$(document).on('click', '#grid', function(e){
		e.preventDefault();
		display('grid');
	});

	$(document).on('click', '#list', function(e){
		e.preventDefault();
		display('list');
	});
}

function display(view)
{
	if (view == 'list')
	{
		$('ul.product_list').removeClass('grid').addClass('list row');
		$('.product_list > li').removeClass('col-xs-12 col-sm-6 col-md-4').addClass('col-xs-12');
		$('.product_list > li').each(function(index, element) {
			var html = '';
			html = '<div class="product-container"><div class="row">';
			html += '<div class="left-block col-xs-4 col-sm-5 col-md-4">' + $(element).find('.left-block').html() + '</div>';
			html += '<div class="center-block col-xs-4 col-sm-7 col-md-4">';
			html += '<div class="product-flags">'+ $(element).find('.product-flags').html() + '</div>';
			html += '<h5 itemprop="name">'+ $(element).find('h5').html() + '</h5>';
			var hookReviews = $(element).find('.hook-reviews');
			if (hookReviews.length) {
				html += hookReviews.clone().wrap('<div>').parent().html();
			}
			html += '<p class="product-desc">'+ $(element).find('.product-desc').html() + '</p>';
			var colorList = $(element).find('.color-list-container').html();
			if (colorList != null) {
				html += '<div class="color-list-container">'+ colorList +'</div>';
			}
			var availability = $(element).find('.availability').html();	// check : catalog mode is enabled
			if (availability != null) {
				html += '<span class="availability">'+ availability +'</span>';
			}
			html += '</div>';
			html += '<div class="right-block col-xs-4 col-sm-12 col-md-4"><div class="right-block-content row">';
			var price = $(element).find('.content_price').html();       // check : catalog mode is enabled
			if (price != null) {
				html += '<div class="content_price col-xs-5 col-md-12">'+ price + '</div>';
			}
			html += '<div class="button-container col-xs-7 col-md-12">'+ $(element).find('.button-container').html() +'</div>';
			html += '<div class="functional-buttons clearfix col-sm-12">' + $(element).find('.functional-buttons').html() + '</div>';
			html += '</div>';
			html += '</div></div>';
			$(element).html(html);
		});
		$('.display').find('li#list').addClass('selected');
		$('.display').find('li#grid').removeAttr('class');
		$.totalStorage('display', 'list');
	}
	else
	{
		$('ul.product_list').removeClass('list').addClass('grid row');
		$('.product_list > li').removeClass('col-xs-12').addClass('col-xs-12 col-sm-6 col-md-4');
		$('.product_list > li').each(function(index, element) {
			var html = '';
			html += '<div class="product-container">';
			html += '<div class="left-block">' + $(element).find('.left-block').html() + '</div>';
			html += '<div class="right-block">';
			html += '<div class="product-flags">'+ $(element).find('.product-flags').html() + '</div>';
			html += '<h5 itemprop="name">'+ $(element).find('h5').html() + '</h5>';
			var hookReviews = $(element).find('.hook-reviews');
			if (hookReviews.length) {
				html += hookReviews.clone().wrap('<div>').parent().html();
			}
			html += '<p itemprop="description" class="product-desc">'+ $(element).find('.product-desc').html() + '</p>';
			var price = $(element).find('.content_price').html(); // check : catalog mode is enabled
			if (price != null) {
				html += '<div class="content_price">'+ price + '</div>';
			}
			html += '<div itemprop="offers" itemscope itemtype="https://schema.org/Offer" class="button-container">'+ $(element).find('.button-container').html() +'</div>';
			var colorList = $(element).find('.color-list-container').html();
			if (colorList != null) {
				html += '<div class="color-list-container">'+ colorList +'</div>';
			}
			var availability = $(element).find('.availability').html(); // check : catalog mode is enabled
			if (availability != null) {
				html += '<span class="availability">'+ availability +'</span>';
			}
			html += '</div>';
			html += '<div class="functional-buttons clearfix">' + $(element).find('.functional-buttons').html() + '</div>';
			html += '</div>';
			$(element).html(html);
		});
		$('.display').find('li#grid').addClass('selected');
		$('.display').find('li#list').removeAttr('class');
		$.totalStorage('display', 'grid');
	}
}

function dropDown()
{
	elementClick = '#header .current';
	elementSlide =  'ul.toogle_content';
	activeClass = 'active';

	$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
			subUl.slideDown();
			$(this).addClass(activeClass);
		}
		else
		{
			subUl.slideUp();
			$(this).removeClass(activeClass);
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass(activeClass);
		e.preventDefault();
	});

	$(elementSlide).on('click', function(e){
		e.stopPropagation();
	});

	$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
	});
}

function accordionFooter(status)
{
	if(status == 'enable')
	{
		$('#footer .footer-block h4').on('click', function(e){
			$(this).toggleClass('active').parent().find('.toggle-footer').stop().slideToggle('medium');
			e.preventDefault();
		})
		$('#footer').addClass('accordion').find('.toggle-footer').slideUp('fast');
	}
	else
	{
		$('.footer-block h4').removeClass('active').off().parent().find('.toggle-footer').removeAttr('style').slideDown('fast');
		$('#footer').removeClass('accordion');
	}
}

function accordion(status)
{
	if(status == 'enable')
	{
		var accordion_selector = '#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4,' +
								'#left_column .shopping_cart > a:first-child, #right_column .shopping_cart > a:first-child';

		$(accordion_selector).on('click', function(e){
			$(this).toggleClass('active').parent().find('.block_content').stop().slideToggle('medium');
		});
		$('#right_column, #left_column').addClass('accordion').find('.block .block_content').slideUp('fast');
		if (typeof(ajaxCart) !== 'undefined')
			ajaxCart.collapse();
	}
	else
	{
		$('#right_column .block .title_block, #left_column .block .title_block, #left_column #newsletter_block_left h4').removeClass('active').off().parent().find('.block_content').removeAttr('style').slideDown('fast');
		$('#left_column, #right_column').removeClass('accordion');
	}
}

function bindUniform()
{
	if (!!$.prototype.uniform)
		$("select.form-control,input[type='radio'],input[type='checkbox']").not(".not_uniform").uniform();
}

// By Prasanna

// Fixed Header

$(document).ready(function(){
	
	// Set Dynamic Height to Beauty Popup
	//var ht = ( ($(document).height()) - ($('#footer').height()) - ($('#header').height()));	
	//$('.beauty_popup').css({'height':(ht)+'px'});	
	//alert(ht);	
	
	// For Beauty Popup Active Nav ( When Beauty Menu is Clicked )
	$(".beauty-nav").click(function(e) {
		$(this).toggleClass('active');
        $(".beauty_popup").toggleClass('open');
        e.stopPropagation();
    });
	
	// Hide Target Div on Body Click
    $(document).click(function(e) {
        if (!$(e.target).is('.beauty_popup, .beauty_popup *')) {
            $(".beauty_popup").removeClass("open");
        }
		
		if (!$(e.target).is('.beauty-nav, .beauty-nav *')) {
            $(".beauty-nav").removeClass("active");
        }
		
		if (!$(e.target).is('.beauty_breadcrumb, .beauty_breadcrumb *')) {
            $(".beauty_breadcrumb").fadeOut('fast');
        }
		
    });
	
	// For Beauty Breadcrumb Click
	
	$(".beauty_breadcrumb").click(function(){
		$(this).fadeOut('fast');
	});
	
	
	// For Fixed header add class
	
	$(window).scroll(function(){
        if ($(window).scrollTop() > 0) {
				$('#header').addClass('scroll');
				$('.leftSidebarSec').css({"left":"-90px"});
				//$('.beauty_breadcrumb').fadeIn('fast');
				
				if($('.beauty-nav').is('.active')){
					$('.beauty_breadcrumb').fadeIn('fast');
				}
				
			}
        else {
		    $('#header').removeClass('scroll');	
			$('.leftSidebarSec').css({"left":"0px"});
			$('.beauty_breadcrumb').fadeOut('fast');
	    }	
		
	
    }); 
	
	// Add class to menu
	$('.sf-menu > li > a').addClass('hvr-underline-from-center');
	
	// For Article Page Content
	$('.article-btm-content').appendTo('.article-btm-data');
	
	// For Slider right block animation
	//$('#htmlcontent_top ul li.htmlcontent-item-1, #htmlcontent_top ul li.htmlcontent-item-2').addClass('wow fadeInUp animated');
	$('#htmlcontent_top ul li.htmlcontent-item-1').attr("data-wow-duration","1s");
	$('#htmlcontent_top ul li.htmlcontent-item-1').attr("data-wow-delay","0.5s");
	$('#htmlcontent_top ul li.htmlcontent-item-2').attr("data-wow-duration","1s");
	$('#htmlcontent_top ul li.htmlcontent-item-2').attr("data-wow-delay","0.1s");
	
	$('.homeslider-description h2').addClass('wow fadeInDown animated').attr({'data-wow-duration':'2s', 'data-wow-delay':'0.3s'});
	$('.homeslider-description h1').addClass('wow fadeInRight animated').attr({'data-wow-duration':'2s', 'data-wow-delay':'0.4s'});
	$('.homeslider-description p').addClass('wow fadeInLeft animated').attr({'data-wow-duration':'2s', 'data-wow-delay':'0.5s'});
	
	// ScrollTop
	$('.mypresta_scrollup').css({"bottom":"10px"});
	
	// For Tooltip
	
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip()
	});
	
	// For top Brand in Header Section
	
	// Apply id to each menu li
	$('.sf-menu > li').each(function(i) {
		$(this).attr('id', 'menu'+(i+1));
	});
	
	// Insert div into last li
	$("<div class='brand_menu'></div>").insertAfter(".sf-menu > li > ul > li:last-child > a");
	$('.fashion_brand_list').prependTo('.sf-menu > li#menu1 > ul > li:last-child > .brand_menu');
	$('.beauty_brand_list').prependTo('.sf-menu > li#menu2 > ul > li:last-child > .brand_menu');
	$('.fragrance_brand_list').prependTo('.sf-menu > li#menu3 > ul > li:last-child > .brand_menu');
	$('.fitness_brand_list').prependTo('.sf-menu > li#menu4 > ul > li:last-child > .brand_menu');
	
	// For top brands
	$('.footer-top-brand-list').prependTo('.footer-top-brands');

	// For Social Icons on Product page

	 $(".share-block-sec").click(function(){
        $(".social-link-block").fadeToggle();
    });

	//var container = $(".social-link-block");
	
    // For View Block on Product Page

    /* $('#thumbs_list_frame li:nth-child(2)').addClass('first_img');
    $('#thumbs_list_frame').prepend('<li class="360deg view_360"><a><img class="360-thumb" src=""/><span class="overlay"></span></a></li>');

    var loc = $('#thumbs_list_frame li:nth-child(2) > a > img').attr("src");
    $('.360deg > a > img.360-thumb').attr("src", loc ); */

	// For Add Active Class to Size Attribute
	
	$('.size-list > div.radio > span > input[type="radio"]').removeAttr('checked');
	$('.size-list > div.radio > span > input[type="radio"]').click(function() {
        if($(this).parent().hasClass('checked')) {
            $(this).parent().parent().parent().addClass('selected');
            $(this).parent().parent().parent().siblings().removeClass('selected');
        }

        else { 
            $(this).parent().parent().parent().removeClass('selected');
        }
    });
	
	// mCustomScrollbar
	
	$(".event-block").mCustomScrollbar({
		theme:"my-theme"
	});
	
	
	// text-limit for article title
	
	var txt= $('.product-content h2').text();
		
	if(txt.length > 20){
		$('.product-content h2').text(txt.substring(0,30));
	}
	
	// text limitation for Moodboard 1 Description

	$(".product-content h5, p.tagline").dotdotdot({
		ellipsis	: '... '
	});
	
	// For Price Ascending - Descending
    $('.acending-price a').click(function(){
		$('.visited').removeClass('visited');
		$(this).addClass('visited');
	});
	$('.decending-price a').click(function(){
		$('.visited').removeClass('visited');
		$(this).addClass('visited');
	});
	
	/* Menu */
	
	
	
	
	
/* Custom Responsive Tab Start */

$('.cust_tab_sec h4').hover(function(event) {
	event.preventDefault();
	$(this).addClass('active');
	$(this).siblings().removeClass('active');
  
	$(this).parent().removeAttr('style');

    var ph = $(this).parent().height();
	var ch = $(this).next().height();
  

	if (ch > ph) {
		$(this).parent().css({
		  'height': ch + 'px'
		});
	} 
	else {
		$(this).parent().css({
		  'height': ph + 'px'
		});
	} 

});

function tabParentHeight() {
	
	$('li.open').find('.cust_tab_sec').removeAttr('style');
	
	var ph = $('li.open').find('.cust_tab_sec').height();
	var ch = $('li.open').find('.cust_tab_sec ul:first').height();
	
	/* console.log('ch' + ch);
	console.log('ph' + ph); */
  
	if (ch > ph) {
		$('.cust_tab_sec').css({
		  'height': ch + 'px'
		});
	} 
	else {
		$('.cust_tab_sec').css({
		  'height': 'auto'
		});
	}
}

$(window).resize(function() {
  tabParentHeight();
});

$(document).resize(function() {
  tabParentHeight();
});

tabParentHeight();
	
/* Custom Responsive Tab End */


/* New Script Start */
	
	if ($(window).width() > 991) {
	$(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
            $(this).toggleClass('open');   
			$('.cust_tab_sec h4').removeClass('active');
			$('.cust_tab_sec > h4:first-child').addClass('active');		
				console.log("Dropdown hidden..");
				tabParentHeight();
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
            $(this).toggleClass('open');  
			$('.cust_tab_sec h4').removeClass('active');
			$('.cust_tab_sec > h4:first-child').addClass('active');
        }
    );
	
	$('.main-navbar ul.navbar-nav > li > a').click(function(){
		$(this).parent().removeClass('open');
	});
	
	}
	else {
	   //alert('More than 960');
	}
	
	
	
	
	// For Tab Hover

	/*  $('.nav-tabs > li > a').hover(function() {
	  $(this).tab('show');
	});  */

	// For Responsive Tabs
	 /* (function($) {
		  fakewaffle.responsiveTabs(['xs', 'sm']);
	  })(jQuery); */
	
	/* New Script End */

/*  $('.dropdown').on('shown.bs.dropdown', function () {
            alert('hi');
        }); */
		
/* $('.cust_tab_sec h4').on('click', function (event) {
	alert('clicked');
    $(this).parent().parent().parent().addClass('open-duplicate');
	$(this).parent().parent().parent().addClass('open');
}); */ 

// For menu Click remove class open	

$('.main-navbar ul.navbar-nav > li > a').click(function(){
	//$(this).parent().removeClass('open');
	$(this).parent().removeClass('open-duplicate');
});






$('.logo-right-sec li .header_user_info .dropdown a.account').click(function(){
	$(this).parent().removeClass('open');
});

// For Custom Popup

/* $('#wishlistData img').click(function(){
	$('.custPopup').fadeIn();
	$('body').css({"overflow-y":"hidden"});
	$('#wishlistInfo').prependTo('.custPopup-content');
});

$('.custPopup-close').click(function(){
	$('.custPopup').fadeOut();
	$('body').css({"overflow-y":"auto"});
}); */


$('.mob-search-icon img').click(function(){
	//$('.mobile_search_sec').stop().slideToggle();
	$('.header_middle_sec').stop().slideToggle();	
});

// For thumbnail list
	
	if ($(window).width() > 767) {
		$('#thumbs_list_frame').css({"width":"auto"});
	}
		
		
		
		$('.main-navbar ul.navbar-nav > li > a').click(function(){
			$(this).parent().toggleClass('menu-open');
			$(this).prev().togglClass('menu-minus');
			//$(this).parent().find('.menu-show').togglClass('menu-open');
			//$(this).next().next().slideToggle();
		});
		
		
		
		
		
		

//$(".logo-right-sec").append('<li class="mob-search-icon"><img src="{$img_dir}search.png" alt="{l s='Search'}"/></li>');

$(window).on("load resize scroll",function(e){
	
     
	if ($(window).width() < 992) {
		// For Wishlist & Shopping cart
		//$('.header_middle_sec').prependTo('.mobile_search_sec');
		$('#header_logo a img').prependTo('.navbar-brand');
		$('.logo-right-sec').prependTo('.wishlist_cart_sec');
		$('.mob-search-icon').css({"display":"inline-block"});
		
		$('.main-navbar ul.navbar-nav > li').removeClass('dropdown');
		$('.main-navbar ul.navbar-nav > li > a').removeClass("dropdown-toggle");
		$('.main-navbar ul.navbar-nav > li > a').removeAttr("data-toggle");
		$('.menu-show').removeClass('dropdown-menu');
		$('.main-navbar ul.navbar-nav > li > a').removeAttr("href");
		$('.cust_tab_sec > h4 > a').removeAttr("href");		
		$('.cust_tab_sec ul.third_menu').prev().addClass('hassubmenu');
		$('.main-navbar ul.navbar-nav > li').addClass('mob-li');
		$('.cust_tab_sec h4 span').show();
		$('.cust_tab_sec > h4:first-child').removeClass('active');
		// For Menu Plus + Minus
		$('.main-navbar ul.navbar-nav > li.mob-li').each(function(){
			if($(this).find('div.menu-show').length !== 0){
				$(this).find('.menu-plus').show();
			}
			else{
				$(this).find('.menu-plus').hide();
			}
		});
		
	}
	else {	   
		//$('.header_middle_sec').prependTo('.header-top-middle .row');
		$('.navbar-brand img').prependTo('#header_logo a');
		$('.logo-right-sec').appendTo('.logo-right-sec-outer');
		$('.mob-search-icon').css({"display":"none"});
		
		$('.main-navbar ul.navbar-nav > li').addClass('dropdown');
		$('.main-navbar ul.navbar-nav > li > a').addClass("dropdown-toggle");
		$('.main-navbar ul.navbar-nav > li > a').attr("data-toggle");
		$('.menu-show').addClass('dropdown-menu');
		$('.main-navbar ul.navbar-nav > li > a').attr("href");
		$('.cust_tab_sec > h4 > a').attr("href");		
		$('.cust_tab_sec ul.third_menu').prev().removeClass('hassubmenu');
		$('.main-navbar ul.navbar-nav > li').removeClass('mob-li');
		$('.cust_tab_sec h4 span').hide();
		$('.cust_tab_sec > h4:first-child').addClass('active');
	}
	
	if ($(window).width() > 767) {
		// For thumbnail list
		$('#thumbs_list_frame').css({"width":"auto"});
	}
	
	if ($(window).width() < 768) {
		// For Wishlist 
		$('#wishlistData img').addClass('wishlist-open');
		
		
	}
	else{
		// For Wishlist 
		$('#wishlistData img').removeClass('wishlist-open');
		
		
	}
	
	
	
	
	
	
	// On Scroll 
	
	// For Fixed header add class
	
	/* $(window).scroll(function(){
        if ($(window).scrollTop() > 0) {
				$('#header').addClass('scroll');
				$('.leftSidebarSec').css({"left":"-90px"});
				//$('.beauty_breadcrumb').fadeIn('fast');
				
				if($('.beauty-nav').is('.active')){
					$('.beauty_breadcrumb').fadeIn('fast');
				}
				
			}
        else {
		    $('#header').removeClass('scroll');	
			$('.leftSidebarSec').css({"left":"0px"});
			$('.beauty_breadcrumb').fadeOut('fast');
	    }	
		
	
    });  */
	
	
   
});


/* $('#wishlistData img').click(function(){
	alert('hi');
	$('#wishlist-modal').fadeIn();
}); */



// For Responsive Tabs
 $('.responsive-tabs').responsiveTabs({
  accordionOn: ['xs', 'sm'] // xs, sm, md, lg 
});

// For Welcome Screen

$(window).load(function() {
  $('.page_loading_overlay').fadeOut();
});

// For Login Form

$('#login_form .form-group input').focus(function(){
	$(this).prev().addClass('label-top');
});


$('#login_form .form-group input').blur(function()
{
    if( $(this).val().length === 0 ) {
        $(this).prev().removeClass('label-top');
    }
	else{
		$(this).prev().addClass('label-top');
	}
});

// For Register Form

$('#create-account_form .form-group input').focus(function(){
	$(this).prev().addClass('label-top');
});


$('#create-account_form .form-group input').blur(function()
{
    if( $(this).val().length === 0 ) {
        $(this).prev().removeClass('label-top');
    }
	else{
		$(this).prev().addClass('label-top');
	}
});

$('#signupnow span').click(function(){
	$('.login_block').slideUp();
	$('.register_block').slideDown();
});
$('#loginnow span').click(function(){
	$('.login_block').slideDown();
	$('.register_block').slideUp();
});


}); // document ready end






