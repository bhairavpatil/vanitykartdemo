(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');
		
		// Animation
		$(jcarousel).jcarousel({
            animation: 1000,
			transitions: true,
			easing:   'linear'			
        });
	
		// Show Items & For Responsive
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
				
				if (width > 1366) {
                    width = width / 6;
                }
				else if (width > 991) {
                    width = width / 6;
                }
				else if (width > 768) {
                    width = width / 4;
                }
				else if (width >= 600) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            });
			
		$('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })

        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
			
		$('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);

// For jcarousel

// For Related Product Slider
$(function() {
    $('#related-products .jcarousel')
        .jcarousel()
        .jcarouselAutoscroll({
            interval: 3000,
            target: '+=6',
            autostart: false
        });
		
		$('#related-products .jcarousel-control-prev')
            .jcarouselControl({
                target: '-=6'
            });

        $('#related-products .jcarousel-control-next')
            .jcarouselControl({
                target: '+=6'
            });
			
	});
	
// For Recommended Product Slider
$(function() {
    $('#recommended-products .jcarousel')
        .jcarousel()
        .jcarouselAutoscroll({
            interval: 3000,
            target: '+=6',
            autostart: false
        });
		
		$('#recommended-products .jcarousel-control-prev')
            .jcarouselControl({
                target: '-=6'
            });

        $('#recommended-products .jcarousel-control-next')
            .jcarouselControl({
                target: '+=6'
            });
			
	});
	

	
	
$(document).ready(function(){

/* Show & Hide Navigation Arrows if slider items less than expected & show them center */

// For Related Slider
if( $("#related-products .jcarousel ul li").length < 6 ){
	$('#related-products .jcarousel').addClass('jcarousel-centered');
}
else{
	$('#related-products .jcarousel').removeClass('jcarousel-centered');
} 

if( $("#related-products .jcarousel ul li").length > 6 ){
	$('#related-products .jcarousel-control-prev, #related-products .jcarousel-control-next').show();
	//$('.jcarousel-pagination').show();
}
else{
	$('#related-products .jcarousel-control-prev, #related-products .jcarousel-control-next').hide();
	//$('.jcarousel-pagination').hide();
}


// For Recommended Slider
if( $("#recommended-products .jcarousel ul li").length < 6 ){
	$('#recommended-products .jcarousel').addClass('jcarousel-centered');
}
else{
	$('#recommended-products .jcarousel').removeClass('jcarousel-centered');
} 

if( $("#recommended-products .jcarousel ul li").length > 6 ){
	$('#recommended-products .jcarousel-control-prev, #recommended-products .jcarousel-control-next').show();
	//$('.jcarousel-pagination').show();
}
else{
	$('#recommended-products .jcarousel-control-prev, #recommended-products .jcarousel-control-next').hide();
	//$('.jcarousel-pagination').hide();
}




/* For Related Slider
if( $("#related-products .jcarousel ul li").length < 6 ){
	$('#related-products .jcarousel').addClass('jcarousel-centered');
	$('#related-products .jcarousel-control-prev, #related-products .jcarousel-control-next').hide();
	$('#related-products .jcarousel-pagination').hide();
}
else{
	$('#related-products .jcarousel').removeClass('jcarousel-centered');
	$('#related-products .jcarousel-control-prev, #related-products .jcarousel-control-next').show();
	$('#related-products .jcarousel-pagination').show();
} 


For Recommended Slider
if( $("#recommended-products .jcarousel ul li").length < 6 ){
	$('#recommended-products .jcarousel').addClass('jcarousel-centered');
	$('#recommended-products .jcarousel-control-prev, #recommended-products .jcarousel-control-next').hide();
	$('#recommended-products .jcarousel-pagination').hide();
}
else{
	$('#recommended-products .jcarousel').removeClass('jcarousel-centered');
	$('#recommended-products .jcarousel-control-prev, #recommended-products .jcarousel-control-next').show();
	$('#recommended-products .jcarousel-pagination').show();
} */

});