{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{include file="$tpl_dir./errors.tpl"}
{if isset($category)}
	{if $category->id AND $category->active}

    <div class="catalog_top_bar">
        <div class="row catalog_top_border">
            <div class="col-md-4 col-sm-6 product_count_sec">
                <div class="product_result_count">
                    {include file="$tpl_dir./category-count.tpl"} 
                    <span class="product_name_result page-heading{if (isset($subcategories) && !$products) || (isset($subcategories) && $products) || !isset($subcategories) && $products} product-listing{/if}"><span class="cat-name">"{$category->name|escape:'html':'UTF-8'}{if isset($categoryNameComplement)}&nbsp;{$categoryNameComplement|escape:'html':'UTF-8'}{/if}"</span></span>
                </div> <!--/product_result_count-->
            </div> <!--/col-md-4-->

            <div class="col-md-4 hidden-sm hidden-xs">&nbsp;</div>

             <div class="col-md-2 col-sm-3 col-xs-6">
                <div class="sort-by-sec sort-by-sec-in-stock">
                    <span class="sort-txt">IN STOCK</span> 
                    <input type="checkbox" id="stock_switch" />
                    <label class="stock_label" for="stock_switch" title="Show only in stock"></label>
                </div> 
            </div> <!--/col-md-2-->

            <div class="col-md-2 col-sm-3 col-xs-6">
                <div class="sort-by-sec">
                    <span class="sort-txt">price</span> 
                    <div class="price-order">
                        <div class="acending-price" rel="price:desc&">
                            <a href="javascript:void(0);" title="Price highest first"></a>
                        </div>
                        <div class="decending-price" rel="price:asc&">
                            <a href="javascript:void(0);" title="Price lowest first"></a>
                        </div>
                    </div>
                </div> 
            </div>

             <div class="col-md-4 col-ms-4 product_sort_sec" style="display:none;">

                <div class="sort-by-sec">
                    <span class="sort-txt">Sort By</span> 
                    <select class="selectpicker">
                        <option>New Arrivals</option>
                        <option>Popular</option>
                        <option>Top Rated</option>
                    </select>
                </div> <!--/sort-by-sec-->

                <div style="display:none;">
                {if $products}
                    <div class="sortPagiBar clearfix">
                        {include file="./product-sort.tpl"}
                    </div> <!--/sortPagiBar-->
                {/if}
                </div>

            </div> <!--/col-md-4-->
        </div> <!--/row-->
    </div> <!--/catalog_top_bar-->

        

    	{if $scenes || $category->description || $category->id_image}
			<div class="content_scene_cat">
            	 {if $scenes}
                 	<div class="content_scene">
                        <!-- Scenes -->
                        {include file="$tpl_dir./scenes.tpl" scenes=$scenes}
                        {if $category->description}
                            <div class="cat_desc rte">
                            {if Tools::strlen($category->description) > 350}
                                <div id="category_description_short">{$description_short}</div>
                                <div id="category_description_full" class="unvisible">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                            {else}
                                <div>{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                    </div>
				{else}
                    <!-- Category image -->
                    <div class="content_scene_cat_bg"{if $category->id_image} style="background:url({$link->getCatImageLink($category->link_rewrite, $category->id_image, 'category_default')|escape:'html':'UTF-8'}) right center no-repeat; background-size:cover; min-height:{$categorySize.height}px;"{/if}>
                        {if $category->description}
                            <div class="cat_desc">
                            <span class="category-name">
                                {strip}
                                    {$category->name|escape:'html':'UTF-8'}
                                    {if isset($categoryNameComplement)}
                                        {$categoryNameComplement|escape:'html':'UTF-8'}
                                    {/if}
                                {/strip}
                            </span>
                            {if Tools::strlen($category->description) > 350}
                                <div id="category_description_short" class="rte">{$description_short}</div>
                                <div id="category_description_full" class="unvisible rte">{$category->description}</div>
                                <a href="{$link->getCategoryLink($category->id_category, $category->link_rewrite)|escape:'html':'UTF-8'}" class="lnk_more">{l s='More'}</a>
                            {else}
                                <div class="rte">{$category->description}</div>
                            {/if}
                            </div>
                        {/if}
                     </div>
                  {/if}
            </div> <!--/content_scene_cat-->
		{/if}

		
		{if isset($subcategories)}
        {if (isset($display_subcategories) && $display_subcategories eq 1) || !isset($display_subcategories) }
		<!-- Subcategories -->
		<div id="subcategories" style="display:none;">
			<p class="subcategory-heading">{l s='Subcategories'}</p>
			<ul class="clearfix">
			{foreach from=$subcategories item=subcategory}
				<li>
                	<div class="subcategory-image">
						<a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="{$subcategory.name|escape:'html':'UTF-8'}" class="img">
						{if $subcategory.id_image}
							<img class="replace-2x" src="{$link->getCatImageLink($subcategory.link_rewrite, $subcategory.id_image, 'medium_default')|escape:'html':'UTF-8'}" alt="{$subcategory.name|escape:'html':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" />
						{else}
							<img class="replace-2x" src="{$img_cat_dir}{$lang_iso}-default-medium_default.jpg" alt="{$subcategory.name|escape:'html':'UTF-8'}" width="{$mediumSize.width}" height="{$mediumSize.height}" />
						{/if}
					</a>
                   	</div>
					<h5><a class="subcategory-name" href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}">{$subcategory.name|truncate:25:'...'|escape:'html':'UTF-8'}</a></h5>
					{if $subcategory.description}
						<div class="cat_desc">{$subcategory.description}</div>
					{/if}
				</li>
			{/foreach}
			</ul>
		</div>
        {/if}
		{/if}

		{if $products}
            <!-- This Section displayed on Top Section -->
			<div class="content_sortPagiBar clearfix" style="display:none;">
            	<div class="sortPagiBar clearfix">
            		{include file="./product-sort.tpl"}
                	{include file="./nbr-product-page.tpl"}
				</div>
                <div class="top-pagination-content clearfix">
                	{include file="./product-compare.tpl"}
					{include file="$tpl_dir./pagination.tpl"}
                </div>
			</div>
             <!-- This Section displayed on Top Section -->

			{include file="./product-list.tpl" products=$products}
			<div class="content_sortPagiBar" style="display:none;">
				<div class="bottom-pagination-content clearfix">
					{include file="./product-compare.tpl" paginationId='bottom'}
                    {include file="./pagination.tpl" paginationId='bottom'}
				</div>
			</div>
		{/if}

	{elseif $category->id}
		<p class="alert alert-warning">{l s='This category is currently unavailable.'}</p>
	{/if}
{/if}

{literal}
<script>
var hideStockFil = function(){

    var hideStock = setInterval(function(){

    if($('#layered_quantity_1').length){
        $('#layered_quantity_1').parents('.layered_filter').hide();
        clearInterval(hideStock);

        // SHOW ACTIVE STOCK IF USED AS FILTER AFTER RELOAD.
        checkStockFil();
        
    }

},1000);
}
$(window).load(function(){
    hideStockFil();
})

hideStockFil();

var checkStockFil = function(){

    if($('#layered_quantity_1').is(':checked')){
        $('#stock_switch').attr('checked',true);
            $('.stock_label').addClass('stock_active');
    }
    else
    {
        $('#stock_switch').attr('checked',false);
            $('.stock_label').removeClass('stock_active');
    }

}

$(document).on('click','.price-order div',function(){

    var setPriceOrder = $(this).attr('rel');
    $('#selectProductSort').val(setPriceOrder);
    $('#selectProductSort').change();

})
$(document).on('change','#stock_switch',function(){
        if(!$(this).is(':checked')){
            $('.stock_label').toggleClass('stock_active');
        }
        else{
            $('.stock_label').toggleClass('stock_active');
        }
        
        $('#layered_quantity_1').click();

        //$('.stock_label:after').css('background','#bada55');
    })
</script>
<style type="text/css">
#uniform-stock_switch{display: none;}
#stock_switch{
    height: 0;
    width: 0;
    visibility: hidden;
}

.stock_label{
    cursor: pointer;
    text-indent: -9999px;
    width: 70px;
    height: 29px;
    background: grey;
    display: inline-block;
    border-radius: 100px;
    position: relative;
    left:5px;
}

.stock_label:after {
    content: '';
    position: absolute;
    top: 2px;
    left: 5px;
    width: 25px;
    height: 25px;
    background: #fff;
    border-radius: 90px;
    transition: 0.3s;
    color: #000;
}

.stock_active{
    background:#bada55;
}

.stock_active:after {
    left: calc(100% - 5px);
    transform: translateX(-100%);
}

#stock_switch:checked + .stock_label:after {
    left: calc(100% - 5px);
    transform: translateX(-100%);
}

#stock_switch + .stock_label:active:after {
    width: 130px;
}

</style>
{/literal}