<?php include 'header.php';?>

<body ><!-- class="wow fadeInDown animated" data-wow-duration=".8s" data-wow-delay="0s" -->
	
	<?php include 'navigation.php';?>
	
    <section class="categorySec">
		<div class="container">
			
				<ol class="breadcrumb">
					<li class="breadcrumb-item"><a href="index.php">Home</a></li>
					<li class="breadcrumb-item">Beauty</li>
					<li class="breadcrumb-item">Electronics</li>
          <li class="breadcrumb-item active">Hair straightners</li>
				</ol>

			
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3" style="padding-right:0px;">
					<div class="catalogue-leftsidebar">
						
						
						<div class="filter-block">
							<div class="title">Filter By</div>
							
							<p>Hair Straighteners</p>
							
							<a href="#" class="see-all">See all Electronics</a>
							
						</div> <!--/filter-block-->
						
						<!-- <div class="filter-block">
							<div>
								<button class="btn clearBtn"><img src="images/filter.png"/> Clear All</button>	
								<div class="clearfix"></div>
							</div>
							
							<div class="filter-added-row">
							
								<div class="filter-added">	
								   chin
								  <span><img src="images/filter-close.png"/></span> 
								</div> 
								
								<div class="filter-added">	
								   upper lip
								  <span><img src="images/filter-close.png"/></span> 
								</div> 
								
								<div class="filter-added">	
								   forehead
								  <span><img src="images/filter-close.png"/></span> 
								</div> 
								
								<div class="filter-added">	
								   eyebrows
								  <span><img src="images/filter-close.png"/></span> 
								</div> 
								
								
								<div class="clearfix"></div>
							</div> 
							
						</div> --> <!--/filter-block-->


						<div class="filter-block">
						  <div class="title">Price Range</div>
							<div class='slider-example'>
								<div>
								<input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> 
								</div>
								<div class="slider-value">
									<span class="start-val">10 AED</span> 
									<span class="end-val">1000 AED</span>
								<div class="cleafix"></div>
								</div>
							</div>					  
						</div> <!--/filter-block-->

						
						<div class="filter-block filter-by-brand">
							<div class="filter-block-inner">
							
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">								
								
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Brand
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											
											<div class="searchSec">
												<div class="input-group">
												  <input type="text" class="form-control" placeholder="SEARCH">
												  <span class="input-group-addon" id="basic-addon1"><img src="images/brand-search.png"/></span>
												</div>
											</div> <!--/searchSec-->
											
											<ul class="brand-list">
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox1" class="styled" type="checkbox">
															<label for="checkbox1">Philips</label>
														</div>
													</a>
												</li>
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox2" class="styled" type="checkbox">
															<label for="checkbox2">Nova</label>
														</div>
													</a>
												</li>
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox3" class="styled" type="checkbox">
															<label for="checkbox3">Vega</label>
														</div>
													</a>
												</li>
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox4" class="styled" type="checkbox">
															<label for="checkbox4">Remington</label>
														</div>
													</a>
												</li>
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox5" class="styled" type="checkbox">
															<label for="checkbox5">Kemei</label>
														</div>
													</a>
												</li>
												<li><a>
														<div class="checkbox checkbox-default">
															<input id="checkbox6" class="styled" type="checkbox">
															<label for="checkbox6">Ikonic</label>
														</div>
													</a>
												</li>
											</ul>
										</div> <!--/panel-body-->
									</div> <!--/panel-collapse-->
								</div> <!--/panel-->
								
								
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Discount
											</a>
										</h4>
									</div>
									<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
										  <ul class="brand-list">
                        <li><a>
                            <div class="checkbox checkbox-default">
                              <input id="checkbox1" class="styled" type="checkbox">
                              <label for="checkbox1">50% or more</label>
                            </div>
                          </a>
                        </li>
                        <li><a>
                            <div class="checkbox checkbox-default">
                              <input id="checkbox2" class="styled" type="checkbox">
                              <label for="checkbox2">40% or more</label>
                            </div>
                          </a>
                        </li>
                        <li><a>
                            <div class="checkbox checkbox-default">
                              <input id="checkbox3" class="styled" type="checkbox">
                              <label for="checkbox3">30% or more</label>
                            </div>
                          </a>
                        </li>
                        <li><a>
                            <div class="checkbox checkbox-default">
                              <input id="checkbox4" class="styled" type="checkbox">
                              <label for="checkbox4">20% or more</label>
                            </div>
                          </a>
                        </li>
                        <li><a>
                            <div class="checkbox checkbox-default">
                              <input id="checkbox5" class="styled" type="checkbox">
                              <label for="checkbox5">10% or more</label>
                            </div>
                          </a>
                        </li>
                        </ul>
										</div> <!--/panel-body-->
									</div> <!--/panel-collapse-->
								</div> <!--/panel-->
								
								

							</div><!-- panel-group -->
							
							</div> <!--/filter-block-inner-->
						</div> <!--/filter-block-->
					</div> <!--/catalogue-leftsidebar-->
				</div> <!--/col-md-3-->
				
				<div class="col-lg-9 col-md-9 col-sm-9 catalogue-rightsidebar">
					<div class="">
				
					<div class="row">
						<div class="col-md-9 col-sm-9">
							<div class="search-result-count">
								Showing 1- 06
							</div> <!--/search-result-count-->
						</div> <!--/col-md-9-->
						
						<div class="col-md-3 col-sm-3">
							<div class="sort-by-sec">
								<span class="sort-txt">Sort By</span> 
								<select class="selectpicker">
									<option>New Arrivals</option>
                  <option>Popular</option>
              	</select>
							</div> <!--/sort-by-sec-->
						</div> <!--/col-md-3-->
					</div> <!--/row-->
				
				  <div class="">
					<img class="img-responsive" src="images/hair-straightners/hair-straightner-banner.png" align="banner-catalog">
				  </div>
				 
				<p>&nbsp;</p>
					<div class="catalogue-rightSec">

						<div class="row">
							<div class="col-md-4" style="cursor: pointer;">	
								
									<div class="thumb-bg">
                  <a href="product-detail-hair-straightner.php">
						<div class="img-container">
							<img src="images/hair-straightners/h1.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
										
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST780 SensoCare Hair
Straightener with automatic temperature..</h4>
											<h5>Braun H. Straightener  ES 4 /ST 780 Sensocare - Automatic temperature adaption, Nanoglide ceramic floating plates</h5>
											<p>549 AED</p>                
										</div>
                    </a>
										<div class="overlay-icon">
											<span class="show-icon1">
                       <a href="javascript:void(0)"></a>
                       </span>
											<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
											</span>
										</div>
									</div>
								
							</div> <!--/col-md-4-->
							
							<div class="col-md-4">	
              <div class="thumb-bg">
               <a href="product-detail-hair-straightner.php">
								<div class="img-container">
							<img src="images/hair-straightners/h2.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
                </a>
                 <a href="product-detail-hair-straightner.php">
								<div class="card-content">
									<h4>Braun Satin Hair 7 ST750 Hair Straightener
with color saver and IONTEC technology</h4>
                  <h5>Braun H. Straightener  ES 3 /ST 750 Moisturising, anti colour fading / dry out / damage Ceramic.LCD - Cord</h5>
									<p>429 AED</p>                
								</div>
                </a>
								<div class="overlay-icon">
									<span class="show-icon1">
									<a href="javascript:void(0)"></a>
									</span>
									<span class="show-icon2">
									<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
									</span>
								</div>
                </div>
							</div> <!--/col-md-4-->
							
							<div class="col-md-4">
                <div class="thumb-bg">
              <a href="product-detail-hair-straightner.php">  
                <div class="img-container">
							<img src="images/hair-straightners/h3.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
                </a>
                 <a href="product-detail-hair-straightner.php">
                <div class="card-content">
                  <h4>Braun Satin Hair 7 ST710 Hair Straightener
with IONTEC technology</h4>
                  <h5>Braun H. STYLER ES 2/ ST 710 Satinliner Ceramic Straightener with LCD - Cord</h5>
                  <p>299 AED</p>                
                </div>
                </a>

								<div class="overlay-icon">
									<span class="show-icon1">
									<a href="javascript:void(0)"></a>
									</span>
									<span class="show-icon2">
									<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
									</span>
								</div>
                </div>
							</div> <!--/col-md-4-->
						</div> <!--/row-->
						
 						<div class="row">
              <div class="col-md-4">
                <div class="thumb-bg">
                <a href="product-detail-hair-straightner.php">  
                <div class="img-container">
							<img src="images/hair-straightners/h4.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
                </a>
                 <a href="product-detail-hair-straightner.php">
                <div class="card-content">
                <h4>Braun Satin Hair 5 ST570 Hair Straightener
& Multistyler with IONTEC technology</h4> 
                  <h5>Braun H.Straightener ST 570, 4 in 1 Multistyler with curl shaper for easy curling. Ceramic eloxal plates, multiple temperaturs settings
</h5>
                  
                  <p>269 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                </div>
                </div>
              </div> <!--/col-md-4-->
              
              <div class="col-md-4"> 
                <div class="thumb-bg"> 
                 <a href="product-detail-hair-straightner.php">
                  <div class="img-container">
							<img src="images/hair-straightners/h5.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
                </a>
                 <a href="product-detail-hair-straightner.php">
                <div class="card-content">
                  <h4>Braun Satin Hair 5 ST510 Hair Straightener</h4>
                  <h5>Braun H. Straightner ST 510 / ESS Straight  Professional Precision Ceramic Straightener & Styler</h5>
                  <p>219 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                </div>
                </div>
              </div> <!--/col-md-4-->
              
              <div class="col-md-4"> 
                <div class="thumb-bg"> 
                 <a href="product-detail-hair-straightner.php">
                <div class="img-container">
							<img src="images/hair-straightners/h6.jpg" alt="thumbnails" class="img-card">
						</div> <!--/img-container-->
                <div class="card-content">
                  <h4>Braun Satin Hair 3 ST310 Hair Straightener
with wide plates</h4>
                  <h5>Braun H. Straightner ES1/ ST 310 Professional Ceramic Straightener - Cord</h5>
                  <p>169 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                </div>
                </div>
              </div> <!--/col-md-4-->
              
            </div> <!--/row-->

            
			
			
			<div class="load-more-sec">
				<span>Load More</span>
			</div> <!--/load-more-sec-->
			
						
					</div> <!--/catalogue-rightSec-->
					</div>
				</div> <!--/col-md-9-->
			  
			</div> <!--/row-->
		</div> <!--/container-->
    </section>

<?php include 'footer.php';?>

<script>

$("#ex2").slider({});

// Without JQuery
var slider = new Slider('#ex2', {});

</script>