<?php include 'header.php';?>

<body><!-- class="wow fadeInDown animated" data-wow-duration=".8s" data-wow-delay="0s" -->
	
	<?php include 'navigation.php';?>

    <section>
      <div class="container">
        <div class="row">
          <div class="col-md-7">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                <li class="breadcrumb-item">Beauty</li>
                <li class="breadcrumb-item">Electronics</li>
                <li class="breadcrumb-item active">Hair Stylers</li>
                <!-- <li class="breadcrumb-item active">Face Color</li> -->
              </ol>
            <div class="gallary_360">
              <!-- <div class="zoom-360">
                <ul class="list">
                      <li><img src="images/view-360/image-01.jpg" /></li>
                      <li><img src="images/view-360/image-02.jpg" /></li>
                      <li><img src="images/view-360/image-03.jpg" /></li>
                      <li><img src="images/view-360/image-04.jpg" /></li>
                      <li><img src="images/view-360/image-05.jpg" /></li>
                      <li><img src="images/view-360/image-06.jpg" /></li>
                      <li><img src="images/view-360/image-07.jpg" /></li>
                      <li><img src="images/view-360/image-08.jpg" /></li>
                </ul>
            </div> -->

          <section id="gallery" class="simplegallery">
            <div class="content">
                <img src="images/hair-clippers/1/19_4210201649618_Braun_SatinHair7_AS720_01.jpg" class="image_1" alt="" />
                <img src="images/hair-clippers/1/19_4210201649618_Braun_SatinHair7_AS720_03.jpg" class="image_2" style="display:none" alt="thumb" />
                <img src="images/hair-clippers/1/19_4210201649618_Braun_SatinHair7_AS720_04.jpg" class="image_3" style="display:none" alt="thumb" />
                <img src="images/hair-clippers/1/19_4210201649618_Braun_SatinHair7_AS720_05.jpg" class="image_4" style="display:none" alt="thumb" />
				<img src="images/hair-clippers/1/19_4210201649618_Braun_SatinHair7_AS720_06.jpg" class="image_5" style="display:none" alt="thumb" />
            </div>
            <div class="thumbnail">

               <!-- <div class="thumb-selected thumb360">
                    <a href="#">
                        <img src="images/thumbs/view-360.png"  alt="" class="view_360" />
                    </a>
                </div> -->

                <div class="thumb thumb-selected selected">
                    <a href="#" rel="1">
                        <img src="images/hair-clippers/thumbs/thumb-1.jpg" id="thumb_1" alt="big img" />
                    </a>
                </div>
                <div class="thumb thumb-selected">
                    <a href="#" rel="2">
                        <img src="images/hair-clippers/thumbs/thumb-2.jpg" id="thumb_2" alt="big img" />
                    </a>
                </div>
                <div class="thumb thumb-selected">
                    <a href="#" rel="3">
                        <img src="images/hair-clippers/thumbs/thumb-3.jpg" id="thumb_3" alt="big img" />
                    </a>
                </div>
                <div class="thumb thumb-selected">
                    <a href="#" rel="4">
                        <img src="images/hair-clippers/thumbs/thumb-4.jpg" id="thumb_4" alt="big img" />
                    </a>
                </div>
				<div class="thumb last thumb-selected">
                    <a href="#" rel="5">
                        <img src="images/hair-clippers/thumbs/thumb-5.jpg" id="thumb_5" alt="big img" />
                    </a>
                </div>
            </div>
        </section>
        <div class="clear"></div>
      </div>
    </div>
          <!-- <div class="col-md-1">&nbsp;</div>  -->
          <div class="col-md-4">
            <div class="product-details">
             <div class="container">
                <h6>Braun</h6>
                <h2>Braun Satin Hair 7 AS 720 Airstyler With IONTEC Technology And Comb Attachment</h2>
               
                 <div class="price-details"><b>199</b> <span class="normal">AED</span></div>
                 <div class="clear"></div>
                                            <!-- <div class="get_alert">
                <img src="images/tick.png" alt="tick">&nbsp;&nbsp;GET SALE ALERT
                </div>
                <img src="images/info.png" alt="info" class="info-icon">
                  -->
                    <div class="clear"></div>
                    <div class="review-box review-anchor">
                      <div><a id="link1" class="nav-section1" href="#section1">REVIEWS (4)</a></div>
                      <div>4 <i class="fa fa-star" aria-hidden="true"></i></div>
                    </div>

               </div>
			   
			    <div class="container">
                  <h6 class="text-uppercase">color</h6>
                  <span class="blue red-color"><a href="#"></a></span>
                  <span class="blue green-color"><a href="#" class="active"></a></span>
                  <span class="blue black-color"><a href="#"></a></span>
                </div>
               
                <!-- <div class="container">
                  <h6 class="text-uppercase">color</h6>
                  <span class="blue"><a href="#" class="active"></a></span>
                  <span class="blue beige"><a href="#"></a></span>
                  <span class="blue pinklight"><a href="#"></a></span>
                  <span class="blue greylight"><a href="#"></a></span>
                  <span class="blue purplelight"><a href="#"></a></span>
                </div>
                <div class="container">
                  <h6 class="text-uppercase size-text">size</h6>
                  <span class="size xs"><a href="#">xs</a></span>
                  <span class="size x"><a href="#" class="active">s</a></span>
                  <span class="size m"><a href="#">m</a></span>
                  <span class="size l"><a href="#">l</a></span>
                  <span class="size xl"><a href="#">xl</a></span>
                  <div class="clearfix"></div>
                </div>   -->                                   
                <button type="button" class="btn black buyBtn2"><a href="cart.php">buy now</a></button>
                 <div class="container">
                  <ul class="services-details">
                    <li>
                        <ul>
                          <li><img src="images/delivery-icon.gif" alt="Free Delivery"></li>
                          <li>free 2 hrs <br>Delivery</li>
                        </ul>
                    </li>
                    <li>
                      <ul>
                          <li><img src="images/returns-icon.gif" alt="Returns"></li>
                          <li>30-DayS <br>Returns</li>
                      </ul>
                    </li>
                    <li>
                      <ul>
                        <li><img src="images/checkout-icon.gif" alt="Secure Checkout"></li>
                        <li>100% Secure<br> Checkout</li>
                        </ul>
                    </li>
                  </ul>
                </div>
                 <div class="container">
                    <ul class="share-details">
                      <li><img src="images/share-icon.gif" alt="share"></li>
                      <li><img src="images/heart-icon.gif" alt="heart-icon"></li>
                    </ul>
                 </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section> 
    <div id="exTab2"> 
<ul class="nav nav-tabs">
      <li class="active">
        <a  href="#1" data-toggle="tab">DESCRIPTION</a>
      </li>
      <!--<li><a href="#2" data-toggle="tab">SPECIFICATIONS</a>
      </li>-->
   </ul>

      <div class="tab-content">
        <div class="tab-pane active" id="1">
            <div class="container">
              <div class="col-md-8">
                <!--<div class="tab-title">PRODUCT CODE:  4210201649618</div>-->
                <div class="tab-text">
                  "Braun Satin Hair 7 AS 720 Airstyler with IONTEC technology - drying and versatile styling in one.
The Braun Satin Hair 7 IONTEC airstyler styles your hair gently and effectively, leaving it shiny and beautiful every day. Whatever your cut or type of hair, you can enjoy a wide range of styles. Change your look to match your mood, and with Braun’s ultimate hair protection you can be sure that your hair looks as healthy as it feels. This Braun airstyler comes with two attachments so you can achieve salon styles at home. The large round brush helps to create curls and natural looking waves, while the unique comb attachment adds volume to your roots. The Braun Satin Hair 7 IONETC airstyler also infuses your hair with millions of active ions. When released, these ions envelop each hair strand to immediately eliminate the static that can build up with conventional styling tools. You’ll get more even, smooth hair for visibly more shine every day."
                </div>
              </div>
              <!--<div class="col-md-4">
                <div class="tab-title2">Sizing</div>
                <div>
                  <ul class="details-care">
                    <li>
                      We recommend purchasing your normal size
                    </li>
                    <li>
                      Cut to a super-skinny fit
                    </li>
                    <li>
                     Lightweight fabric
                    </li>
                    <li>
                      Model is 177cm/ 5'10", Bust 77cm, Waist 63cm, Hips 88cm <br>and wearing a size S
                    </li>
                  </ul>
                </div>        
              </div>
              <div class="col-md-4">
                <div class="tab-title2">Details & care</div>
                  <ul class="details-care">
                    <li>
                      Straight neckline
                    </li>
                    <li>
                      Off-the-shoulder style
                    </li>
                    <li>
                      Skinny shoulder straps
                    </li>
                    <li>
                      Ribbed knit throughout
                    </li>
                     <li>
                      Black stripes
                    </li>
                  </ul>
              </div>-->
            </div>

        </div>
        <!--<div class="tab-pane" id="2">
          <div class="container">
              <div class="col-md-5">
                <div class="tab-title">SPECIFICATIONS</div>
                <div class="tab-text">
                  A modern, smooth leather bucket bag in nude from Building Block. Features a leather drawstring closure, leather shoulder strap, leather tassels and a leather-lined base. 
                </div>
              </div>
              </div>
        </div>-->
      </div>
  </div>
</section>
   <div class="box-wrapper removetoppad">      
        <div class="container">
            <div class="row">             
                <div class="row">
                <div class="container">
                <div class="row">
                <div class="content-section margin-btm0 margin-top0">
                <div class="container">            
                <div class="row">
                <div class="slider-title">           
                <!-- <div class="h5"> -->
                <div class="slider-title2">  
                <div class="h5">
                <span class="slide-text-f black" id="section1"><img src="images/flower5.png" alt="flowers">related products<img src="images/flower5.png" alt="flowers"></span>
                </div>
                </div>
                </div> 
                </div>
                </div>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                <section class="slider">
                <div class="flexslider flexslider-min flexslider-related carousel clearfix" style="height: 370px">
                <ul class="slides slides-min">
                
                <li>
				<a href="product-detail-hair-styler.php">
				<div class="img-container img-container-related-imgs">
					<img src="images/hair-clippers/2/18_4210201631644_Braun_SatinHair5_AS530_01.png" alt="thumbnails" class="img-card">
				</div> <!--/img-container-->
                <div class="card-content">
                <h4>Braun H.STYLER AS 530</h4>
                <h5>Steam & Style Pro, 2 Styler & Volumeriser attachments</h5>
                <p>159 AED</p>                
                </div>
				</a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
				<a href="product-detail-hair-styler.php">
                <div class="img-container img-container-related-imgs">
					<img src="images/hair-clippers/3/17_4210201631606_Braun_SatinHair3_AS330_01.png" alt="thumbnails" class="img-card">
				</div> <!--/img-container-->
                <div class="card-content">
                <h4>Braun Satin Airstyler AS 330</h4>
                <h5>Big & Small Brush,Volumizer, 400 watt  Dry & Style, 2 temperature settings</h5>
                <p>109 AED</p>                
                </div>
				</a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
				
				<li>
				<a href="product-detail-hair-styler.php">
                <div class="img-container img-container-related-imgs">
					<img src="images/hair-clippers/4/12_4210201644323_EC2_CU750_01.jpg" alt="thumbnails" class="img-card">
				</div> <!--/img-container-->
                <div class="card-content">
                <h4>Braun SatinStyler EC 2/ CU750</h4>
                <h5>anti colour fading,anti -dry out & anti damage  Curling (2200)</h5>
                <p>329 AED</p>                
                </div>
				</a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
				
				<li>
				<a href="product-detail-hair-styler.php">
                <div class="img-container img-container-related-imgs">
					<img src="images/hair-clippers/5/20_4210201644408_Braun_SatinHair7_EC1_CU710_03.jpg" alt="thumbnails" class="img-card">
				</div> <!--/img-container-->
                <div class="card-content">
                <h4>Braun SatinStyler EC 1/ CU </h4>
                <h5> Unique satin ion conditioning, Curling, large LCD display, fast heat- up time </h5>
                <p>289 AED</p>                
                </div>
				</a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                </ul>
                </div>
                </section>
                <!--/.Carousel Wrapper-->
                </div>
                </div> <!-- /.container -->
                </div>
                </div>
                </div>
            </div>                
        </div>

<div class="clearfix">&nbsp;</div>

<section class="section-review">
<div class="container">
    <div class="row">
    <div class="col-md-6 border-right">
      <div class="slider-title2">  
      <div class="h5">
      <span class="slide-text-f black" id="section1"><img src="images/flower5.png" alt="flowers">customer reviews<img src="images/flower5.png" alt="flowers"></span>
      </div>
      </div> 
      <div class="clearfix">&nbsp;</div>
      <div align="left" class="rank">
      <span><i class="fa fa-star" aria-hidden="true"></i></span>
      <span><i class="fa fa-star" aria-hidden="true"></i></span>
      <span><i class="fa fa-star" aria-hidden="true"></i></span>
      <span><i class="fa fa-star" aria-hidden="true"></i></span>
      <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
      <span class="rating"><b>4.2</b>  Rating</span>
      <span class="divider">|</span>
      <span class="rating"><b>4</b> reviews</span>
      <span class="leave-review"><a href="#"> leave a review </a></span>
      </div>  
       <div class="load-review">
          <ul id="myList">
          <li>
        <div class="row bg-odd topbdr">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by Sera </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
        <div class="row bg-even">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by Salman </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
        <div class="row bg-odd">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by John </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div> 
        </li>
        <li>
            <div class="row bg-even">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by Salman </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
        <div class="row bg-odd">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by John </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
            <div class="row bg-even">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by Salman </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
        <div class="row bg-odd">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by John </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
            <div class="row bg-even">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by Salman </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        <li>
        <div class="row bg-odd">
        <div class="rank2">
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star" aria-hidden="true"></i></span>
           <span><i class="fa fa-star" aria-hidden="true"></i></span>
            <span><i class="fa fa-star grey" aria-hidden="true"></i></span>
            <span class="review-date">15 April 2017</span>
        </div>
        <div class="review-header">Reviewed by John </div>
        <div class="review-content">Love this product.</div>
        <div class="review-content">Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me.
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        Like how simple and classy the soft tote is and the smell of the full grain vegetable-tanned leather. The size is just perfect for me
        </div>        
        </div>
        </li>
        </ul>       
        </div>
        <div class="row-loadmore">
        <div align="center">
           <div id="loadMore" class="loadmore">Load more </div>
        </div>        
        </div> 
    </div>



    <div class="col-md-6">
      <div class="slider-title2">  
      <div class="h5">
      <span class="slide-text-f black" id="section1"><img src="images/flower5.png" alt="flowers">Also Bought <img src="images/flower5.png" alt="flowers"></span>
      </div>
      </div>
      <div class="clearfix">&nbsp;</div>
           <div class="like-thumb  carousel clearfix">
                <ul class="slides">
                <li>
                <img src="images/products-thumb/8.jpg" />
                <div class="card-content">
                <h4>Maxi Black Dress</h4>
                <h5>High Neck Midi</h5>
                <p>200 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                <img src="images/products-thumb/9.jpg" />
                <div class="card-content">
                <h4>Jacquard Mess..</h4>
                <h5>The flattering</h5>
                <p>142 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                <img src="images/products-thumb/10.jpg" />
                <div class="card-content">
                <h4>Laura Mercier</h4>
                <h5>4 Secret.</h5>
                <p>108 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <div class="clearfix"></div>
                <li>
                <img src="images/products-thumb/11.jpg" />
                <div class="card-content">
                <h4>LANCÔME</h4>
                <h5>La Vie Est Belle Eau</h5>
                <p>200 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                <img src="images/products-thumb/12.jpg" />
                <div class="card-content">
                <h4>BROW GAL</h4>
                <h5>Light Hair Convertib </h5>
                <p>200 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                <img src="images/products-thumb/13.jpg" />
                <div class="card-content">
                <h4>ILLAMASQUA</h4>
                <h5>Quarts  Antimatter</h5>
                <p>128 AED</p>                
                </div>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                </ul>
                </div>
            </div>
          </div>
    </div>
</section>



        <div class="container">
                <div class="row">
                <div class="container">
                <div class="row">
                <div class="content-section margin-btm0 margin-top0">
                <div class="container">            
                <div class="row">
                <div class="slider-title">
                <div class="slider-title2">  
                <div class="h5">
                <span class="slide-text-f black" id="section1"><img src="images/flower5.png" alt="flowers">Recommended products<img src="images/flower5.png" alt="flowers"></span>
                </div>
                </div>
                </div>    
                </div>
                </div>
                <div class="clearfix">&nbsp;</div>

                <div class="row">
                <section class="slider">
                <div class="flexslider flexslider-related carousel clearfix" style="height: 370px;">
                <ul class="slides">
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
					<img src="images/recommanded/1.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Silkfinish</h4>
                <h5>Precision Hair Remover...</h5>
                <p>89 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/2.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Silk epil</h4>
                <h5>Soft Perfection epilator...</h5>
                <p>129 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
              
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/4.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Epilator</h4>
                <h5>Legs and body, with...</h5>
                <p>299 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>

                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/3.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Silk epil</h4>
                <h5>Massaging Rollers Head...</h5>
                <p>169 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>

                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/5.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Epilator</h4>
                <h5>body and face...</h5>
                <p>349 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>

                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/6.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Face epilator</h4>
                <h5>for chin, upper lip...</h5>
                <p>399 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/7.jpg" />
				</div>
                <div class="card-content">
                <h4>Face Color</h4>
                <h5>exfoliation and...</h5>
                <p>399 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/5.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Epilator</h4>
                <h5>body and face</h5>
                <p>349 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/6.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Face epilator</h4>
                <h5>for chin, upper lip</h5>
                <p>399 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/4.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Epilator</h4>
                <h5>Legs and body, with...</h5>
                <p>299 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>

                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/3.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Silk epil</h4>
                <h5>Massaging Rollers Head...</h5>
                <p>169 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                <li>
                 <a href="product-detail.php">
				 <div class="img-container img-container-related-imgs">
                <img src="images/recommanded/5.jpg" />
				</div>
                <div class="card-content">
                <h4>Braun Epilator</h4>
                <h5>body and face</h5>
                <p>349 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
                </li>
                </ul>
                </div>
                </section>
                <!--/.Carousel Wrapper-->
                </div>
                </div> <!-- /.container -->
                </div>
                </div>
                </div>
              
        </div>
    </div>
   
<div class="clearfix">&nbsp;</div>
   
<?php include 'footer.php';?>
          
<script type="text/javascript" src="js/simplegallery.min.js"></script>
  <script type="text/javascript">
      $(document).ready(function(){

          $('#gallery').simplegallery({
              galltime : 400,
              gallcontent: '.content',
              gallthumbnail: '.thumbnail',
              gallthumb: '.thumb'
          });

          $('a.goback').click(function(){
              window.history.back();
              return false;
          });

      });



$(".thumb-selected").click(function() {
    $('.thumb-selected.selected').not(this).removeClass("selected")
    $(this).addClass("selected");
})

$(document).ready(function () {
    size_li = $("#myList li").size();
    x=4;
    $('#myList li:lt('+x+')').show();
    /*$('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('#myList li:lt('+x+')').show();
    });
    $('#showLess').click(function () {
        x=(x-5<0) ? 3 : x-5;
        $('#myList li').not(':lt('+x+')').hide();
    });*/
});

//Flexslider
// var flagslide=0;
$(window).load(function(){
	$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 192,
		itemMargin: 10,
		pausePlay: false,
		start: function(slider){
		$('body').removeClass('loading');
		}
	});
});

</script>