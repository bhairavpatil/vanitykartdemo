<?php include 'header.php';?>

<body><!-- class="wow fadeInDown animated" data-wow-duration=".8s" data-wow-delay="0s" -->
	
	<?php include 'navigation.php';?>

    <div class="box-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="tagline">health & wellness</div>
                    <h2 class="h2-article">5 ECO-FRIENDLY WOMEN'S<br> ROLL-ON DEOS</h2>
                    <p class="mid-grey">Believe it or not but anthropologists believe that body odour is  our oldest defence mechanism! </p>
                    <div class="social-icons-wrap clearfix">
                    <div class="pull-left">
                    <div class="small-title">by RALEIGH BURGAN </div>
                    <div class="date-title">March 21 2017</div>
                    </div>
                    <div class="pull-right"><img src="images/social-icons-sprites.png" alt="sprites social-icons"></div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">
                    <img src="images/grid-img15.jpg" alt="img" class="img-responsive img-margin">
                    </div>
                    </div>
                    <div class="normal-content">
                    Natural deodorants have come a long way since those cloudy crystals that purported to ward off smell. While the aluminum-based options tend to be more effective for heavy sweaters, if you prefer to keep your beauty routine green, these seven botanical options will keep you relatively dry and smelling delightful.
                    </div>
                    <div class="fancy-title">
                    The mess-free long lasting way to keep
                    smelling nice without harming the environment.
                    </div>
                    <div class="normal-content">
                    If the thought of wearing a natural deodorant makes you sweat, we understand. The all-natural versions of the past were stinky and awful and always made us go to the back of our cabinet for the scary antiperspirant spray that could actually stop a speeding bullet. But a new crop of natural deodorants have made converts of even the most dubious among us: And we would know—we tested them all to, um, varying degrees of success. These are our favorites.
                    </div><div class="normal-content">
                    The all-natural versions of the past were stinky and awful and always made us go to the back of our cabinet for the scary antiperspirant spray that could actually stop a speeding bullet. But a new crop of natural deodorants have made converts of even the most dubious among us: And we would know—we tested them all to, um, varying degrees of success. These are our favorites.
                    </div>


                    <div class="row">                
                        <div class="group-container">
                          <div class="col-md-6">          
                            <h2>1</h2>
                            <h3>FRESH AS FAWN</h3>
                            <h4>Natural Anti-bacterial Deodorant</h4>
                            <p>Fresh As Fawn is our uplifting SUPER STRONG deodorant and will keep you feeling and smelling fresh as a frolickin' fawn all day long. Built with powerful anti-bacterial agents - hallelujah, it works! </p>
                            <button type="button" class="btn black">Shop now</button>
                          </div>
                          <div class="col-md-6">
                            <img src="images/grid-img18.jpg" alt="img" class="img-responsive">
                          </div>
                        </div>                
                    </div>

                    <div class="row">                
                        <div class="group-container">
                          <div class="col-md-6">
                          <img src="images/grid-img19.jpg" alt="img" class="img-responsive">
                          </div>
                          <div class="col-md-6">          
                          <h2>2</h2>
                          <h3>ELIXIRIUM</h3>
                          <h4>Natural Anti-bacterial Deodorant</h4>
                          <p>This is my all natural, organic, herbal dedorant which really works and dissapears any foul odor from your skin for 36 hours. 
                          </p>
                          <button type="button" class="btn black">Shop now</button>
                          </div>    
                        </div>                
                    </div>

                    <div class="row">                
                        <div class="group-container">
                          <div class="col-md-6">          
                            <h2>3</h2>
                            <h3>FRESH AS FAWN</h3>
                            <h4>Natural Anti-bacterial Deodorant</h4>
                            <p>Fresh As Fawn is our uplifting SUPER STRONG deodorant and will keep you feeling and smelling fresh as a frolickin' fawn all day long. Built with powerful anti-bacterial agents - hallelujah, it works!</p>
                            <button type="button" class="btn black">Shop now</button>
                          </div>
                          <div class="col-md-6">
                            <img src="images/grid-img20.jpg" alt="img" class="img-responsive">
                          </div>
                        </div>                
                    </div>

                    <div class="row">                
                        <div class="group-container">
                          <div class="col-md-6">
                            <img src="images/grid-img21.jpg" alt="img" class="img-responsive">
                          </div>
                          <div class="col-md-6">          
                            <h2>4</h2>
                            <h3>BeauTeaStudio</h3>
                            <h4>Natural Anti-bacterial Deodorant</h4>
                            <p>This is my all natural, organic, herbal dedorant which really works and dissapears any foul odor from your skin for 36 hours. 
                            </p>
                            <button type="button" class="btn black">Shop now</button>
                          </div>   
                        </div>                
                    </div>

                    <div class="row">                
                        <div class="group-container">
                          <div class="col-md-6">          
                            <h2>5</h2>
                            <h3>EmilysHomestead</h3>
                            <h4>Natural Anti-bacterial Deodorant</h4>
                            <p>Fresh As Fawn is our uplifting SUPER STRONG deodorant and will keep you feeling and smelling fresh as a frolickin' fawn all day long. Built with powerful anti-bacterial agents - hallelujah, it works! </p>
                            <button type="button" class="btn black">Shop now</button>
                          </div>
                          <div class="col-md-6">
                            <img src="images/grid-img22.jpg" alt="img" class="img-responsive">
                          </div>
                        </div>                
                    </div>
                </div>

                <div class="col-md-1"></div>
                <div class="col-md-3">
                    <div class="">
                    <img src="images/grid-img16.jpg" alt="img" class="img-responsive">
                    </div>          
                    <div class="news-title-static">                  
                    <span class="news-title-inside"><img src="images/flower5.png" alt="flowers"> related Articles</span>    
                    </div>      

                    <div class="news-section">
                    <div class="product-content nonebdr">                    
                    <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                    </div>
                    </div>         
                    <hr>
                    <div class="news-section">
                    <div class="product-content nonebdr">                    
                    <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                    </div>
                    </div>         
                    <hr>
                    <div class="news-section">
                    <div class="product-content nonebdr">                    
                    <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                    </div>
                    </div>         
                    <hr>
                    <div class="news-section">
                    <div class="product-content nonebdr">                    
                    <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                    </div>
                    </div>         
                    <hr>
                    <a href="#" class="btn btn-primary">DISCOVER MORE</a>
                    <br> <br>
                    <div>
                    <img src="images/grid-img6.jpg" alt="img" class="img-responsive">
                    </div>
                    <br>
                    <div>
                    <img src="images/grid-img17.jpg" alt="img" class="img-responsive">
                    </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="row">
         <div class="container">
          <div class="row group-container2 clearfix">
          <div class="col-md-1"></div>
            <div class="box-comment clearfix">
            <div class="top">
            <div class="col-md-3" >          
            <img src="images/comment-btn.jpg" alt="img">
            </div>
            <div class="col-md-4 pull-left">
            <img src="images/social-icons-sprites.png" alt="img">
            </div>
            </div>
            <div class="clearfix">&nbsp;</div>
            <div class="bottom-comments">
            <div class="row">
            <div class="col-md-12" align="center">
            <img src="images/comments-block.jpg" alt="img">
            </div>
            </div>
            </div>
            </div>
        </div>
      </div>
     
<p class="">&nbsp;</p>
  <div class="container">
    <div class="row">
      <div class="content-section margin-btm0 margin-top0">
         <div class="container">            
            <div class="row">
            <div class="col-md-12 slider-title">
            <div class="col-md-12"> 
            <div class="h5">
            <!-- <span class="f1"><img src="images/flower7.png"></span> -->
            <span class="slide-text-f">Hot Products</span>
            <!-- <span class="f2"><img src="images/flower8.png"></span> -->
            <div class="col-md-12 text-animated-row">
            <ul class="">
            <li data-rel="0"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Fashion</span><span class="f2"><img src="images/flower8.png"></span></li>
            <li data-rel="1" class="deactive"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Health & wellness</span><span class="f2"><img src="images/flower8.png"></span></li>
            <li data-rel="2" class="deactive"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Makeup</span><span class="f2"><img src="images/flower8.png"></span></li>
            </ul>
            </div>
            </div>
            </div>
            </div>    
            </div>
        </div>

  <div class="row">
      <section class="slider">
        <div class="flexslider flexslider-related carousel clearfix">
          <ul class="slides">
            <li onclick="return mylink();" style="cursor: pointer;">
				<div class="img-container">
					<img src="images/carousel-thumb1.jpg" />
				</div> <!--/img-container-->
              <div class="card-content">
                <h4>ADEAM</h4>
                <h5>Monochrome Geometric Patterned Tiered..</h5>
                <p>6700 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
           <li onclick="return mylink();" style="cursor: pointer;">
				<div class="img-container">
					<img src="images/carousel-thumb2.jpg" />
				</div> <!--/img-container-->
              <div class="card-content">
                <h4>NICHOLAS</h4>
                <h5>Black Peony Floral Halter Dress</h5>
                <p>3700 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
           <li onclick="return mylink();" style="cursor: pointer;">
				<div class="img-container">
					<img src="images/carousel-thumb3.jpg"/>
				</div> <!--/img-container-->              
              <div class="card-content">
                <h4>PHILOSOPHY DI LORENZO SERAFINI</h4>
                <h5>Monochrome Geometric Patterned Tiered..</h5>
                <p>2700 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li onclick="return mylink();" style="cursor: pointer;">
				<div class="img-container">
					<img src="images/carousel-thumb4.jpg"/>
				</div> <!--/img-container-->  
             <div class="card-content">
                <h4>ZIMMERMANN</h4>
                <h5>Floral Print Oleander Jumpsuit</h5>
                <p>1550 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb8.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>INDIE LEE</h4>
                <h5>Rosehip Cleanser</h5>
                <p>550 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb7.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>MAKE</h4>
                <h5>Universal Stick</h5>
                <p>30 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb5.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>AGENT NATEUR</h4>
                <h5>AGENT NATEUR</h5>
                <p>110 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb6.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>Vintner's Daughter</h4>
                <h5>Active Botanical Serum</h5>
                <p>185 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb9.jpg"/>
				</div> <!--/img-container--> 
              <div class="card-content">
                <h4>laura mercier</h4>
                <h5>Secret Brightening Powder Shade 1</h5>
                <p>105 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb10.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>BY TERRY</h4>
                <h5>Terrybleu Mascara Terrybly</h5>
                <p>186 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb11.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>LAURA MERCIER</h4>
                <h5>Addiction Face Illuminator</h5>
                <p>202 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb12.jpg"/>
				</div> <!--/img-container-->  
              <div class="card-content">
                <h4>ILLAMASQUA</h4>
                <h5>Midnight Antimatter Lipstick</h5>
                <p>120 AED</p>                
              </div>
              <div class="overlay-icon">
                <span class="show-icon1">
                   <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
            </div>
            </li>
          </ul>
        </div>
   
      </section>
<!--/.Carousel Wrapper-->
 </div>
 </div> <!-- /.container -->

 
    </div>
    </div>
    </div>

            </div>
                
        </div>
    </div>

  
 <?php include 'footer.php';?>
 
 <script type="text/javascript">
// var flagslide=0;
$(window).load(function(){
	$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 270,
		itemMargin: 30,
		pausePlay: false,
		start: function(slider){
		$('body').removeClass('loading');
		}
	});
});
</script>
 