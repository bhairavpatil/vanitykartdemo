<?php

require_once(dirname(__FILE__).'/config/config.inc.php');
require_once(dirname(__FILE__).'/init.php');
$context = Context::getContext();
$lang = (int)$context->language->id;
//$result = Category::getNestedCategories(2,(int)$context->language->id);
//$context = new Context();
//die(ddd($result));
$resultArr = array();


if(!Tools::getValue('cat'))
{
$sql = "SELECT a.id_category as id,b.name FROM "._DB_PREFIX_."category a, "._DB_PREFIX_."category_lang b WHERE a.id_category = b.id_category AND b.id_lang = 1 AND a.id_category != 2 AND a.id_category != 1 AND a.id_parent = 2";

$result = Db::getinstance()->ExecuteS($sql);

die(json_encode($result));
}

if(Tools::getValue('cat'))
{
	$resultArrRes = array();

	$cat = Tools::getValue('cat');

	$category = new Category((int)$cat,(int)$lang);

	$products_count = $category->getProducts($lang, 1, 5, null, null, true); 
	
	$resultArrRes = $category->getProducts($lang, 1, $products_count); 

	//ddd($resultArrRes);
	

	$temp = array();

	$sql = "SELECT a.*,b.name FROM "._DB_PREFIX_."category a, "._DB_PREFIX_."category_lang b WHERE a.id_category = b.id_category AND b.id_lang = ".$lang." AND a.level_depth >  0 ORDER BY a.level_depth DESC";
	$result = Db::getinstance()->ExecuteS($sql);

	foreach ($result as $key => $value) {
		// # code...
		//$temp[$value['id_parent']][$value['id_category']] = $value['name'];
		$resultArr[$value['id_category']]['name'] = $value['name'];
		$resultArr[$value['id_category']]['parent'] = $value['id_parent'];
	}


	if(count($resultArrRes)){

	$tempCatArr = array();

		foreach ($resultArrRes as $key => $value) {
			// # code...
			$catName = getCategoryHeirarchy($value['id_category_default']);
			$resultArrRes[$key]['title'] = $resultArrRes[$key]['name'];
			$resultArrRes[$key]['name'] = $resultArrRes[$key]['name'].' ( '.$catName.' )';
			$resultArrRes[$key]['description_short'] = strip_tags($resultArrRes[$key]['description_short'],'a');

			$id_product = $value['id_product'];
			$image = Image::getCover($id_product);
			//$product = new Product($id_product, false, $context->language->id);
			
			$imagePath = $link->getImageLink($value['link_rewrite'], $image['id_image'], 'home_default');
			$resultArrRes[$key]['image_thumb'] = $image['id_image'].'-home_default/'.basename($imagePath);
			$resultArrRes[$key]['image'] = $image['id_image'].'-large_default/'.basename($imagePath);
		}

	die(json_encode($resultArrRes));
	
	}
}

function getCategoryHeirarchy($catid){
	
	global $resultArr;
	$catName = array();
	$curCat = $catid;
	$catName[] = $resultArr[$curCat]['name'];
	$parent = $resultArr[$curCat]['parent'];

	while($parent!=2)
	{
		$curCat = $resultArr[$curCat]['parent'];
		$catName[] = $resultArr[$curCat]['name'];
		$parent = $resultArr[$curCat]['parent'];
	}
	
	rsort($catName);
	return implode(", ", $catName);

}
