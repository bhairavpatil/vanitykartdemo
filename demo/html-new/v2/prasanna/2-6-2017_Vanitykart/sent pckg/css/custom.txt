/* catalogue-page */

.categorySec .breadcrumb{padding:25px 15px !important;}
.catalogue-leftsidebar{border-right:1px solid #e9e9e9;}
.filter-block{padding:15px 15px;border-bottom:1px solid #e9e9e9;}
.filter-block.filter-by-brand{padding:0px;}
.filter-block.filter-by-brand .filter-block-inner{}
.filter-block .title{font-family: 'Oswald', sans-serif;color:#1a1a1a;font-size:13px;font-weight:500;text-transform:uppercase;margin:0px 0px 15px;}
.btn.clearBtn{border:1px solid #dddddd;color:#1a1a1a;font-family: 'Oxygen', sans-serif;font-weight:400;font-size:11px;border-radius:0px;background:none;}
.clearBtn img{margin-right:5px;}
.filter-added{
	border: 1px solid #dddddd;
	color: #f43b67;
	font-family: 'Oxygen', sans-serif;
	font-size: 11px;
	font-weight: 400;
	padding: 6px 12px 6px 12px;
	width: 70%;
	position:relative;
	margin-top:5px;
}
.filter-added:after{
	content: url('../images/filter-close.png');
	position: absolute;
	top: 0px;
	right: 0px;
	height: 100%;
	border-left: 1px solid #ddd;
	padding: 0px 10px;
	line-height: 30px;
	cursor:pointer;
}
.foundationSec label{font-family: 'Oxygen', sans-serif; color:#f43b67;font-weight:400;font-size:11px;margin:0px 0px 10px;display:block;}
.foundationSec a{font-family: 'Oswald', sans-serif;color:#1a1a1a !important;font-size:13px;font-weight:500;border-bottom:1px solid #a7a7a7;text-transform:uppercase;text-decoration:none !important;}

/* collapse */

.filter-block .panel-title > a {
        display: block;
        text-decoration: none;
		font-family: 'Oswald', sans-serif;
		color:#1a1a1a;
		font-size:13px;
		font-weight:500;
		text-transform:uppercase;
		outline:none;
    }
.filter-block .more-less {
        float: right;
        color: #212121;
    }
.filter-block .panel{
	border: none;
	background: none;
	border-radius: 0px;
	border-bottom:1px solid #ddd;
}
.filter-block .panel .panel-heading{padding:15px 15px;}
.filter-block .panel-group{width:100%;}
.filter-block .more-less:before{content:"";}
.filter-block .more-less.glyphicon-plus:before{content: url('../images/collapse-plus.png');}
.filter-block .more-less.glyphicon-minus:before{content: url('../images/collapse-minus.png');}

/* custom search */
.searchSec .input-group{border:none;width:auto;padding:0px;}
.searchSec .input-group input{border-radius: 0px;background:#fff8f0;}
.searchSec .input-group input:focus{border:1px solid #ccc;}
.searchSec .input-group-addon{background: #a7a7a7;border-radius: 0px;}

.brand-list{list-style-type:none;margin:10px 0px 0px;height:92px;}
.brand-list li a{padding:2px;position:relative;font-family: 'Oxygen', sans-serif; color:#1a1a1a;font-weight:400;font-size:11px;display:block;text-decoration:none;cursor:pointer;text-transform:capitalize;}
.brand-list li a:hover{color:#f43d69 !important;}
.brand-list li a .checkbox{margin:0px;outline:none;}
/*.brand-list li a input{position:absolute;top:2px;left:0px;}*/

/* placeholder */

.searchSec .input-group input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
  font-size:9px;
}
.searchSec .input-group input::-moz-placeholder { /* Firefox 19+ */
  font-size:9px;
}
.searchSec .input-group input:-ms-input-placeholder { /* IE 10+ */
  font-size:9px;
}
.searchSec .input-group input:-moz-placeholder { /* Firefox 18- */
  font-size:9px;
}