(function() {
	var container = document.querySelector( 'div.container' ),
		triggerBttn = document.getElementById( 'trigger-overlay' ),
		overlay = document.querySelector( 'div.overlay-page' ),
		//closeBttn = overlay.querySelector( 'button.overlay-close' );
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {
		if( classie.has( overlay, 'open' ) ) {
			classie.remove( overlay, 'open' );
			$(triggerBttn).removeClass('active'); // CUSTOM REMOVE ACTIVE TO TAB
			$('#footer-wrapper').removeAttr('style');
			$('.box-wrapper:first').show();
			classie.remove( container, 'overlay-open' );
			classie.add( overlay, 'close' );
			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.removeEventListener( transEndEventName, onEndTransitionFn );
				}
				classie.remove( overlay, 'close' );
			};
			if( support.transitions ) {
				overlay.addEventListener( transEndEventName, onEndTransitionFn );
				
			}
			else {
				onEndTransitionFn();
			}
		}
		else if( !classie.has( overlay, 'close' ) ) {

			// CUSTOM ADD ACTIVE TO TAB AND STRACH HEIGHT UPTO SUBSCRIPTION ELEMENT
			$(triggerBttn).addClass('active'); 
			//var t = $('.sectionbg1').position();
			//var myHeight = parseInt(t.top-110);
			//$('.overlay-page').css('height',(myHeight)+'px');
			$('.box-wrapper:first').hide();
			var t = $(overlay).find('.video-section').position();
			//var t = $('#footer-wrapper').height();
			var myHeight = parseInt((t.top/2)+200);
			console.log(myHeight);
			$('#footer-wrapper').css({'position':'relative','top':myHeight+'px'});
			//$('#footer-wrapper').css({'top':myHeight+'px'});
			//$(overlay).css({'margin-bottom':myHeight+'px'});
			//$('.box-wrapper').css({'margin-bottom':myHeight+'px'});
			// END CUSTOM CODE

			classie.add( overlay, 'open' );
			classie.add( container, 'overlay-open' );

		}
	}

	triggerBttn.addEventListener( 'click', toggleOverlay );
	//closeBttn.addEventListener( 'click', toggleOverlay );
})();