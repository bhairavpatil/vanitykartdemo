<div id="footer-wrapper">
        <section class="subscribe-container">
            <div class="container" align="center">
                <div class="row">
                    <div class="col-md-12">   
                        <img src="images/subscribe.png" alt="subscribe" class="img-responsive">      
                    </div>
                </div>
            </div>  
        </section>

     <div class="main-footer">
        <div class="container">
           <div class="row">
                <div class="col-md-2">
                    <div class="footer-widget">
                      <h3 class="widget-title">About us</h3>
                        <ul>
                          <li><a href="javascript:void(0);">Who we are</a></li>
                          <li><a href="#">Brands A-Z</a></li>
                          <li><a href="#">Careers</a></li>
                        </ul>
                    </div> 
                </div> <!-- /.col-md-2 -->
                  <div class="col-md-2">
                      <div class="footer-widget">
                          <h3 class="widget-title">support</h3>
                          <ul>
                              <li><a href="#">FAQs</a></li>
                              <li><a href="#">Online Returns</a></li>
                              <li><a href="#">Shipping & Delivery</a></li>
                          </ul>
                      </div> 
                  </div> <!-- /.col-md-2 -->

            <div class="col-md-2">
                <div class="footer-widget">
                    <h3 class="widget-title">Legal</h3>
                    <ul>
                      <li><a href="#">Price Match</a></li>
                      <li><a href="#">Privacy Policy</a></li>
                      <li><a href="#">Terms and Conditions</a></li>
                    </ul>
                </div> 
            </div> <!-- /.col-md-2 -->

            <div class="col-md-4">
                <div class="footer-widget">
                  <h3 class="widget-title">TOP BRANDS</h3>
                  <ul class="col-three">
                    <li><a href="#">Ginger</a></li>
                    <li><a href="#">Mango</a></li>
                    <li><a href="#">Seventy Five</a></li>                                  
                  </ul>
                  <ul class="col-three">
                    <li><a href="#">Calvin Klein</a></li>
                    <li><a href="#">Bareminerals</a></li>
                    <li><a href="#">Calvin Klein </a></li>
                  </ul>
                  <ul class="col-three">
                    <li><a href="#">Dior</a></li>
                    <li><a href="#">COCO</a></li>
                    <li><a href="#">Floris</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-md-2">
                <div class="footer-widget">
                    <h3 class="widget-title"><div align="center">find us on</div></h3>
                    <ul>
                      <li>
                        <img src="images/social-icons.png" alt="social-icons">
                      </li>
                    </ul>
                </div> 
            </div>
        </div> <!-- /.row -->


        <div class="bottom-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                      <div class="phone">
                          <img src="images/icon6.png" alt="icons - phone">
                      </div>
                      <div class="contact-details">
                        <span class="black">+971 -3225 7555</span>&nbsp;
                        <span class="mail"><a href="mailto:service@vanitykart.com">service@vanitykart.com</a></span>
                      </div>
                    </div>
                  <div class="col-md-6" align="center">
                     <div class="copyright"> © 2017 VanityKart. All Rights Reserved</div>
                  </div>
                  <div class="col-md-3 paymentSec">
                      <img src="images/paypal.png" alt="paypal">
                  </div>
                </div>
            </div>
        </div>
      </div> <!-- /.container -->
   </div>
</div>

<div id="pop-quick-view-wrapper" style="display:none;">
    <div id="pop-quick-view">
      <div id="pop-close"><img src="images/close.png" alt="close"></div>
      <div id="pop-content">
      <img src="images/popup-screen.jpg" alt="popup" class="img-responsive">
      </div>
    </div>
</div>


<a href="javascript:void(0);" id="scroll-top" title="Scroll to Top" style="display: none;">Top<span></span></a>

<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-select.min.js"></script>
<script src="js/bootstrap-slider.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery.min.js"><\/script>')</script>
<script src="js/megamenu.js"></script>
<script src="js/ie10-viewport-bug-workaround.js"></script>
<script src="js/ie-emulation-modes-warning.js"></script>
<script src="js/plugins.js"></script> 
<script defer src="js/jquery.flexslider.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/mCustomScrollbar.min.js"></script>
 <script src="js/vendor/modernizr.js"></script>
<script src="js/custom-popup.js"></script>
<script src="js/classie.js"></script>
<script type="text/javascript" src="fancybox/source/jquery.fancybox.js"></script>
<script type="text/javascript" src="magnific-popup/js/magnific-popup.js"></script>  
<script type="text/javascript" src="dist/xzoom.min.js"></script>
<script src="js/foundation.min.js"></script>
<script src="js/setup.js"></script>
<script src="js/jquery_002.js" type="text/javascript"></script>
<script type="text/javascript">jQuery(function(){var SelfLocation=window.location.href.split('?');switch(SelfLocation[1]){case"justify_right":jQuery(".megamenu").megamenu({'justify':'right'});break;case"justify_left":default:jQuery(".megamenu").megamenu();}});</script>

<script>

  window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>
<script type='text/javascript'>
$(function(){
$("#search-button, #search-icon").click(function(e){
e.preventDefault();
$("#search-button, #search-form").toggle();
});
})  

     function change_Image(element) {
         var right = "images/login-details.png";
         var left = "images/user-logebg.png";
         element.src = element.bln ? right : left;
         element.bln = !element.bln;
     }
     function change_Image2(element) {
         var right = "images/login-details.png";
         var left = "images/user-logebg.png";
         element.src = element.bln ? right : left;
         element.bln = !element.bln;
     }
     function onHover()
{
    $("#menuImg").attr('src', 'images/login-details.png');
}

function offHover()
{
    $("#menuImg").attr('src', 'images/user-logebg.png');
}

</script>

<script src="js/wow.min.js"></script>
<script>
new WOW().init();

</script>
<script type="text/javascript">
$('div.overlay-contentscale.open').css({'height':(($(document).height()))+'px'});
$('.video-section video').prop('loop',false);

$(document).ready(function(){ 
  $(window).scroll(function(){ 
  if ($(this).scrollTop() > 100) { 
  $('#scroll-top').fadeIn(); 
  } else { 
  $('#scroll-top').fadeOut(); 
  } 
  }); 
  $('#scroll-top').click(function(){ 
  $("html, body").animate({ scrollTop: 0 }, 600); 
  return false; 
  }); 
});


$(window).scroll(function() {
if ($(this).scrollTop()>0)
{
    $('.a').fadeOut("slow",function(){
        if($('li#trigger-overlay').is('.active'))
        $('.bottomMenu').fadeIn("slow");
    });
}
else
{
$('.bottomMenu').fadeOut(0);
$('.a').fadeIn("fast");
}
});

$("#trigger-overlay.active").click(function(e) {
{
}
});

$(document).click(function(e) {
var me = e.target;
    if(($('li#trigger-overlay').is('.active')) && !($(me).is('.hvr-underline-from-left')))
    {
        if(!e.target.closest('.box-shadow-overlay'))
        {
            var $container = $(".overlay-page.overlay-contentscale.clearfix");
            var $temp = $(".overlay-contentscale");
            $container.removeClass("active");
            $("#trigger-overlay").removeClass("active");
            $(".box-wrapper").css("display","block");
            $("#footer-wrapper").css("top","0");
            $temp.removeClass("open");
            $('.bottomMenu').fadeOut("slow");
        }
    }
});

// Comment Box
$('.top').on('click', function() {
  $parent_box = $(this).closest('.box-comment');
  $parent_box.siblings().find('.bottom-comments').slideUp();
  $parent_box.find('.bottom-comments').slideToggle(500, 'swing');
});

$(document).ready(function() {
    $("div.section-tab-menu>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        var t = $(this).parents('.section-tab-menu');
        $('.section-tab-content').removeClass('active');
        $(t).siblings('.section-tab').find('.section-tab-content:eq('+index+')').addClass("active");
    });
});
</script>
<script type="text/javascript">
function toggleIcon(e) {
$(e.target)
  .prev('.panel-heading')
  .find(".more-less")
  .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

 $('.mm-item').mouseenter(function(){
      var mymenu = $(this).find("div.mm-item-content");
        $(mymenu).find('.list-group-item').removeClass('active');
        $(mymenu).find('.section-tab-content').removeClass('active');
        $(mymenu).find('.list-group-item:eq(0)').addClass('active');
        $(mymenu).find('.section-tab-content:eq(0)').addClass('active');
    });
</script>
<script src="js/wow.min.js"></script>
<script>
  new WOW().init();
</script>

<script type="text/javascript">
function toggleIcon(e) {
    $(e.target)
    .prev('.panel-heading')
    .find(".more-less")
    .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);
// mScustomScroll
$(".brand-list").mCustomScrollbar();
$("#ex2").slider({});

// Without JQuery
var slider = new Slider('#ex2', {});

$(document).ready(function() {
    $("div.section-tab-menu>div.list-group>a").hover(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        var t = $(this).parents('.section-tab-menu');
        $('.section-tab-content').removeClass('active');
        $(t).siblings('.section-tab').find('.section-tab-content:eq('+index+')').addClass("active");
        // $("div.section-tab>div.section-tab-content").removeClass("active");
        // $("div.section-tab>div.section-tab-content").eq(index).addClass("active");
    });
});

$('.acending-price a').click(function(){
    $('.visited').removeClass('visited');
    $(this).addClass('visited');
});
$('.decending-price a').click(function(){
    $('.visited').removeClass('visited');
    $(this).addClass('visited');
});
</script>
<script src="js/demo7.js"></script>
</body>
</html>

