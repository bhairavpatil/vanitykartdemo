<?php include 'header.php';?>
<body>
<?php include 'navigation.php';?>
	<section class="categorySec body-margin">
		<div class="container">			
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="index.php">Home</a></li>
				<li class="breadcrumb-item">Beauty</li>
				<li class="breadcrumb-item">Electronics</li>
				<li class="breadcrumb-item active">Hair Stylers</li>
			</ol>			
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-3" style="padding-right:0px;">
					<div class="catalogue-leftsidebar">
						<div class="filter-block">
							<div class="title">Filter By</div>
							<p>Hair Stylers</p>
							<a href="#" class="see-all">See all Electronics</a>
						</div> 
					<div class="filter-block">
						<div class="title">Price Range</div>
						<div class='slider-example'>
							<div>
								<input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> 
							</div>
							<div class="slider-value">
								<span class="start-val">10 AED</span> 
								<span class="end-val">1000 AED</span>
								<div class="cleafix"></div>
							</div>
						</div>					  
					</div> 						
						<div class="filter-block filter-by-brand">
							<div class="filter-block-inner">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">	
									<div class="panel panel-default"  style="display:none;">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Color
												</a>
											</h4>
										</div>
										<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<ul class="brand-list">
													<li>
													<a>
													<div class="checkbox checkbox-default">
													<input id="checkbox1" class="styled" type="checkbox">
													<label for="checkbox1"><span class="color-sec color-sec1"></span> Red</label>
													</div>
													</a>
													</li>
												<li>
													<a>
													<div class="checkbox checkbox-default">
													<input id="checkbox3" class="styled" type="checkbox">
													<label for="checkbox3"><span class="color-sec color-sec3"></span> Green</label>
													</div>
													</a>
												</li>
													<li>
													<a>
													<div class="checkbox checkbox-default">
													<input id="checkbox5" class="styled" type="checkbox">
													<label for="checkbox5"><span class="color-sec color-sec5"></span> Black</label>
													</div>
													</a>
													</li>
												</ul>
											</div>
										</div>
									</div>
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<i class="more-less glyphicon glyphicon-plus"></i>
											Brand
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="searchSec" style="display:none;">
												<div class="input-group">
												<input type="text" class="form-control" placeholder="SEARCH">
												<span class="input-group-addon" id="basic-addon1"><img src="images/brand-search.png"/></span>
												</div>
											</div>
											<ul class="brand-list" style="height:auto;">
												<li>
													<a>
													<div class="checkbox checkbox-default">
													<input id="checkbox1" class="styled" type="checkbox" checked>
													<label for="checkbox1">Braun</label>
													</div>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
									<div class="panel panel-default" style="display:none;">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Discount
												</a>
											</h4>
										</div>
										<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<ul class="brand-list">
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox1" class="styled" type="checkbox">
														<label for="checkbox1">50% or more</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox2" class="styled" type="checkbox">
														<label for="checkbox2">40% or more</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox3" class="styled" type="checkbox">
														<label for="checkbox3">30% or more</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox4" class="styled" type="checkbox">
														<label for="checkbox4">20% or more</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox5" class="styled" type="checkbox">
														<label for="checkbox5">10% or more</label>
														</div>
														</a>
													</li>
												</ul>
											</div> 
										</div> 
									</div> 
								</div>
							</div>
						</div> 
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 catalogue-rightsidebar">
					<div class="">				
						<div class="row">
						<div class="col-md-5 col-sm-5">
							<div class="search-result-count">
							 	Showing 1- 09
							</div> 
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="sort-by-sec">
								<span class="sort-txt">IN STOCK</span> 
								<label class="switch">
									<input type="checkbox">
									<span class="slider-switch round"></span>
								</label>
							</div> 
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="sort-by-sec">
								<span class="sort-txt">price</span> 
								<div class="price-order">
									<div class="acending-price">
										<a href="javascript:void(0);"></a>
									</div>
									<div class="decending-price">
										<a href="javascript:void(0);"></a>
									</div>
								</div>
							</div> 
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="sort-by-sec">
								<span class="sort-txt">Sort By</span> 
								<select class="selectpicker">
									<option>New Arrivals</option>
									<option>Popular</option>
									<option>Top Rated</option>
								</select>
							</div> 
						</div> 
					</div> 				
					<div class="">
						<img class="img-responsive" src="images/hair-stylers/catalogue_hairstylers.jpg" align="banner-catalog">
					</div>				 
					<p>&nbsp;</p>
						<div class="catalogue-rightSec">
							<div class="row">
								<div class="col-md-4" style="cursor: pointer;">
									<div class="thumb-bg">
										<a href="product-detail-hair-styler.php">
											<div class="img-container">
												<img src="images/hair-stylers/1/19_4210201649618_Braun_SatinHair7_AS720_01.jpg" alt="thumbnails" class="img-card">
											</div> 									
											<div class="card-content">
												<h4>Braun Creation H.STYLER AS 720</h4>
												<h5>Moisture balance  Styling attachments</h5>
												<p>199 AED</p>                
											</div>
										</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)">
													<img src="images/icon2.png" alt="icons">
												</a>
											</span>
										</div>
									</div>								
								</div> <!--/col-md-4-->
								<div class="col-md-4">	
									<div class="thumb-bg">
										<a href="product-detail-hair-styler.php">
											<div class="img-container">
												<img src="images/hair-stylers/2/18_4210201631644_Braun_SatinHair5_AS530_01.jpg" alt="thumbnails" class="img-card">
											</div>
										</a>
									<a href="product-detail-hair-styler.php">
										<div class="card-content">
											<h4>Braun H.STYLER AS 530</h4>
											<h5>Steam & Style Pro, 2 Styler & Volumeriser attachments</h5>
											<p>159 AED</p>                
										</div>
									</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)">
													<img src="images/icon2.png" alt="icons">
												</a>
											</span>
										</div>
									</div>
								</div> <!--/col-md-4-->
								<div class="col-md-4">
									<div class="thumb-bg">
										<a href="product-detail-hair-styler.php">  
											<div class="img-container">
												<img src="images/hair-stylers/3/17_4210201631606_Braun_SatinHair3_AS330_01.jpg" alt="thumbnails" class="img-card">
											</div> 
										</a>
									<a href="product-detail-hair-styler.php">
										<div class="card-content">
											<h4>Braun Satin Airstyler AS 330</h4>
											<h5>Big & Small Brush,Volumizer, 400 watt  Dry & Style, 2 temperature settings</h5>
											<p>109 AED</p>                
										</div>
									</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)">
													<img src="images/icon2.png" alt="icons">
												</a>
											</span>
										</div>
									</div>
								</div> <!--/col-md-4-->
							</div> <!--/row-->
							<div class="row">
								<div class="col-md-4" style="cursor: pointer;">	
									<div class="thumb-bg">
										<a href="product-detail-hair-styler.php">
											<div class="img-container">
												<img src="images/hair-stylers/4/12_4210201644323_EC2_CU750_01.jpg" alt="thumbnails" class="img-card">
											</div>
											<div class="card-content">
												<h4>Braun SatinStyler EC 2/ CU750</h4>
												<h5>
													anti colour fading,anti -dry out & anti damage  Curling (2200)
												</h5>
												<p>329 AED</p>                
											</div>
										</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)">
													<img src="images/icon2.png" alt="icons">
												</a>
											</span>
										</div>
									</div>
								</div> <!--/col-md-4-->
								<div class="col-md-4">	
									<div class="thumb-bg">
										<a href="product-detail-hair-styler.php">
											<div class="img-container">
												<img src="images/hair-stylers/5/20_4210201644408_Braun_SatinHair7_EC1_CU710_03.jpg" alt="thumbnails" class="img-card">
											</div> 
										</a>
									<a href="product-detail-hair-styler.php">
										<div class="card-content">
											<h4>Braun SatinStyler EC 1/ CU 710 </h4>
											<h5> Unique satin ion conditioning, Curling, large LCD display, fast heat- up time </h5>
											<p>289 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon1">
											<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
										</span>
									</div>
									</div>
								</div> <!--/col-md-4-->
							</div> <!--/row-->
							<div class="load-more-sec">
								<span>Load More</span>
							</div> <!--/load-more-sec-->
						</div> <!--/catalogue-rightSec-->
					</div>
				</div> <!--/col-md-9-->
			</div> <!--/row-->
		</div> <!--/container-->
	</section>
<?php include 'footer.php';?>
