$(document).on('click','.show-icon2',function(){
$("#pop-quick-view-wrapper").fadeIn();
});

$(document).on('click','#pop-close',function(){
$("#pop-quick-view-wrapper").fadeOut(function(){
$(this).next('#pop-content').empty();
});
});

function animateMyTitle(target,direction)
{
	var move = 'top';
	if(direction=='next')
	move = 'top';

	//console.log('custom:'+target+'direction:'+direction);

	$('.slider-title li').each(function(){
		if($(this).attr('data-rel')==target)
			//$(this).show('fadeIn',{direction:move},500);
		$(this).fadeIn();
		else
			$(this).fadeOut();
	});
	
}
//on('click'

//alert( $(".flexslider.flexslider-related .slides-min > li").length )

if( $(".flexslider.flexslider-related .slides-min > li").length < 6 ){
	$('.flexslider-min').addClass('flex-centered');
}
else{
	$('.flexslider-min').removeClass('flex-centered');
} 

// PRODUCT GALLERY START HANDLE NAVIGATION ARROWS ON CLICK AND MOUSEOVER

var firstImg = $('.sp:first').index();
var lastImg = $('.sp:last').index();

$('.sp').hover(function(){

	if($(this).index()>firstImg)
		togglePreviousNav(false);
	else
		togglePreviousNav(true);
	if($(this).index()<lastImg)
		toggleNextNav(false);
	else
		toggleNextNav(true);

})

$(document).on('click','#button-previous',function(){

  		var curImg = $('img.xactive');
  		var nextImg = $(curImg).parents('.sp').prev('.sp').length;
  		if(nextImg){

  			var showImg = $(curImg).parents('.sp').prev('.sp');

  			if($(showImg).index()<lastImg)
  				toggleNextNav(false);

  			if($(showImg).prev('.sp').length<=0)
  				togglePreviousNav(true);
  			else
  				togglePreviousNav(false);

  			$(showImg).trigger('hover');
  		}
  		else
  		{
  			togglePreviousNav(true);
  		}
  	})

$(document).on('click','#button-next',function(){
  		
  		var curImg = $('img.xactive');
  		var nextImg = $(curImg).parents('.sp').next('.sp').length;
  		if(nextImg){

  			var showImg = $(curImg).parents('.sp').next('.sp');

  			if($(showImg).index()>firstImg)
  				togglePreviousNav(false);

  			if($(showImg).next('.sp').length<=0)
  				toggleNextNav(true);
  			else
  				toggleNextNav(false);

  			$(showImg).trigger('hover');
  		}
  		else
  		{
  			toggleNextNav(true);
  		}
  	})

function togglePreviousNav(hide){

	if(hide)
		$('#button-previous').fadeOut();
	else
		$('#button-previous').fadeIn();
}

function toggleNextNav(hide){

	if(hide)
		$('#button-next').fadeOut();
	else
		$('#button-next').fadeIn();
}

// PRODUCT GALLERY END HANDLE NAVIGATION ARROWS ON CLICK AND MOUSEOVER