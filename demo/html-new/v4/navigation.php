<header id="header-stick" class="navbar navbar-default navbar-fixed-top">
        <div class="top-header">
          <div class="hdr-patch">
          <div class="row">
            <div class="col-md-8 col-sm-6 hdr-patch-left"><span class="sale">SALE</span> <span>Up to 40% off clothing, accessories and more.</span> <a>Shop Sale</a></div>
            <!-- <div class="col-md-4 text-center">STOCK UP FOR SUMMER!  SHOP AT 40% OFF NOW. </div>-->
            <div class="col-md-4 col-sm-6">
              <ul class="pull-right">
                <li><img src="images/icon6.png" alt="phone-icons"><span>+971 -3225 7555</span></li>
                <li class="track-o"><img src="images/icon7.png" alt="phone-icons">Track order</li>
              </ul>
            </div>
          </div>
        </div> <!--/hdr-patch-->
          <div>
          <div class="mid-hdr">
            <div class="row">
                <div class="col-sm-4">
                  <form class="navbar-form" role="search">
                      <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search For fashion, News & Events" name="q">
                        <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                        </button>
                        </div>       
                      </div>
                  </form>
                </div>
                <div class="col-sm-4">
                  <div align="center">
                    <a href="index.php" title="vanity kart site logo"><img src="images/logo.png" alt="site-logo" class="sitelogo"></a>
                  </div>
                </div>
              <div class="col-sm-4">
                <ul class="account_details pull-right"> 
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="color: #98055e!important">Sara smith <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="#">My Orders</a></li>
                          <li><a href="#">My Profile</a></li>
                          <li><a href="#">Wishlist</a></li>
                          <li><a href="#">Sale alert</a></li>
                          <li><a href="#" class="nonebdr">Logout</a></li>
                        </ul>
                    </li>
                  <li class="dropdown navbdr-none" style="padding: 5px 8px 7px 12px!important;   margin: 0px 0px 0px 0px!important;">
                    <a href="#" class="dropdown-toggle user" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/wishlist-ico.png" alt="wishlist-ico"></a>
                    <ul class="dropdown-menu adjest-menu2">
                      <li>
                        <img src="images/wishlist-dropbg.jpg" alt="bg">
                      </li>
                    </ul>
                  </li>
                  <li class="dropdown navbdr-none" style="padding: 5px 8px 7px 12px!important;    margin: 0px 10px 0px 0px!important;">
                    <a href="#" class="dropdown-toggle user" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="images/login-cart.png" alt="cart" style="position: relative;top: -3px;"></a>
                    <ul class="dropdown-menu adjest-menu">
                      <li><img src="images/cart-dropbg.jpg" alt="bg"></li>
                    </ul>
                  </li>

                  
                  </ul>
                </div>
              </div>
            </div> <!--/mid-hdr-->
              <div class="navbar-header">
                <button type="button" data-toggle="collapse" data-target="#navbar-collapse-grid" class="navbar-toggle"><span class="icon-bar"></span>
                <span class="icon-bar"></span><span class="icon-bar"></span></button><a href="#" class="navbar-brand"></a>
              </div>
<!-- class="navbar navbar-default navbar-fixed-top -->
<div class="" align="center">
<ul class="megamenu" style="display: block;">
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link mm-item-link">Fashion</a>
        <div style="width: 100%; display: none;" class="mm-item-content mm-show">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
							  
									<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
							  
                                  <!--<div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div> -->
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
									
									<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
								
                                  <!--<div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>-->
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
									<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
                                  <!--<div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>-->
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
									<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
                                  <!--<div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>-->
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
										<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
                                      <!--<div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>-->
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
											<div class="col-md-2">
										<div class="listbox-col listbox-beauty menu_last_hover">
										  <ul>
										  <li class="colhead">Arabic Wear</li>
										  <li class="colhead">Western Wear</li>
										  <li class="colhead">Indian Wear</li>
										  <li class="colhead">Sports/Active Wear</li>
										  <li class="colhead">Swim/Beach Wear</li>
										  <li class="colhead">Lingerie</li>
										  <li class="colhead">Plus Size</li>
										  <li class="colhead">Petite Wear</li>
										  <li class="colhead">Maternity Wear</li>
										  </ul>                                                                          
										</div> <!--/listbox-col-->
									</div> <!--/col-md-2-->
									
									<div class="col-md-8 pull-left">
										<img src="images/fashion_thumb.jpg" alt="beauty" class="img-responsive">
									</div> <!--/col-md-8-->
									
									<div class="col-md-2">
										<ul>
											<li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
											  <ul>
												  <li><img src="images/accessories-icons.png" alt="accessories"></li>
											  </ul>
											</li>
										 </ul>
									</div> <!--/col-md-2-->
                                          <!--<div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>-->
                                        </div>
										
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>     
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Beauty</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item active text-center">
                                      makeup
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Skin care
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      hair care
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      bath & body
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      herbal
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      electronics
                                  </a>
                              </div>
                          </div>

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                  <div class="section-tab-content active">
                    <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-2">
                                  <div class="listbox-col listbox-beauty">
                                      <ul>
                                      <li class="colhead">Face</li>
                                      <li class="colhead">Eye</li>
                                      <li class="colhead">Nails</li>
                                      <li class="colhead">Lips</li>
                                      <li class="colhead">Accessories</li>
                                      <li class="colhead">Value Sets</li>
                                      <li class="colhead">VK Style Guide</li>
                                      <li class="colhead">Top Brands</li>
                                      <li class="colhead">Beauty Tips</li>
                                      </ul>                                                                          
                                  </div>
                              </div>
                            <div class="col-md-8 pull-left">
                                <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                            </div>
                              <div class="col-md-2">
                                  <ul>
                                  <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                      <ul>
                                          <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                      </ul>
                                  </li>
                                   </ul>
                              </div>
                          </div>
                      </div>
                  </div>



               <div class="section-tab-content">
                    <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-2">
                                  <div class="listbox-col listbox-beauty">
                                      <ul>
                                        <li class="colhead">Cleanse</li>
                                        <li class="colhead">Other Needs</li>
                                        <li class="colhead">Shop by Concern</li>
                                        <li class="colhead">Care </li>
                                        <li class="colhead">Derma Cosmetics </li>
                                        <li class="colhead">Value Sets </li>
                                        <li class="colhead">VK Style Guides</li>
                                        <li class="colhead">Top Brands</li>
                                      </ul>                                                                          
                                  </div>
                              </div>
                            <div class="col-md-8 pull-left">
                                <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                            </div>
                              <div class="col-md-2">
                                  <ul>
                                  <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                      <ul>
                                          <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                      </ul>
                                  </li>
                                   </ul>
                              </div>
                          </div>
                      </div>
                  </div>
               <div class="section-tab-content">
                    <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-2">
                                  <div class="listbox-col listbox-beauty">
                                      <ul>
                                          <li class="colhead">Shampoo & Conditioner </li>
                                          <li class="colhead">Nourishment</li>
                                          <li class="colhead">Styling </li>
                                          <li class="colhead">Best For </li>
                                          <li class="colhead">Accessories </li>
                                          <li class="colhead">Expert Hair Care </li>
                                          <li class="colhead">Top Concerns</li>
                                          <li class="colhead">Value Sets </li>
                                          <li class="colhead">VK Style Guides </li>
                                          <li class="colhead">Top Brands </li>
                                      </ul>                                                                          
                                  </div>
                              </div>
                            <div class="col-md-8 pull-left">
                                <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                            </div>
                              <div class="col-md-2">
                                 <ul>
                                  <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                      <ul>
                                          <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                      </ul>
                                  </li>
                                   </ul>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="section-tab-content">
                    <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-2">
                                  <div class="listbox-col listbox-beauty">
                                      <ul>
                                          <li class="colhead">Bath  </li>
                                          <li class="colhead">Hand & Foot Care </li>
                                          <li class="colhead">Body Care </li>
                                          <li class="colhead">Nourishment </li>
                                          <li class="colhead">Value Sets & Accessories </li>
                                          <li class="colhead">Top Concerns</li>
                                          <li class="colhead">VK Style Guides </li>
                                          <li class="colhead">VK Styles </li>
                                          <li class="colhead">Top Brands</li>
                                      </ul>                                                                          
                                  </div>
                              </div>
                            <div class="col-md-8 pull-left">
                                <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                            </div>
                              <div class="col-md-2">
                                 <ul>
                                  <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                      <ul>
                                          <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                      </ul>
                                  </li>
                                   </ul>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="section-tab-content">
                    <div class="row">
                          <div class="col-md-12">
                              <div class="col-md-2">
                                  <div class="listbox-col listbox-beauty">
                                      <ul>
                                          <li class="colhead">Lip Balm </li>
                                          <li class="colhead">Kajal</li>
                                          <li class="colhead">Face Wash</li>
                                          <li class="colhead">Face Cream</li>
                                          <li class="colhead">Cleanser</li>
                                          <li class="colhead">Toner</li>
                                          <li class="colhead">Moisturizer</li>
                                          <li class="colhead">Sunscreen</li>
                                          <li class="colhead">Day Cream</li>
                                          <li class="colhead">Night Cream</li>
                                      </ul>                                                                          
                                  </div>
                              </div>
                            <div class="col-md-8 pull-left">
                                <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                            </div>
                              <div class="col-md-2">
                                  <ul>
                                  <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                      <ul>
                                          <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                      </ul>
                                  </li>
                                   </ul>
                              </div>
                          </div>
                      </div>
                  </div>
              <div class="section-tab-content">
                <div class="row">
                  <div class="col-md-12">
                      <div class="col-md-2">
                          <div class="listbox-col listbox-beauty">
                              <ul>
                                  <li class="colhead">
                                  <a href="catalogue-page-hair-dryers.php">Hair Dryers</a> 
                                  </li>
                                  <li class="colhead">
                                  <a href="catalogue-page-hair-stylers.php">Hair Stylers</a>
                                  </li>
                                  <li class="colhead">
                                  <a href="catalogue-page-hair-straightners.php">Hair Straightners</a>
                                  </li>
                                  <li class="colhead"><a href="catalogue-page-epilators.php">Epilators</a>
                                  </li>
                                  <li class="colhead">Massagers</li>
                                  <li class="colhead">Gifts &amp; Value Sets</li>
                                  <li class="colhead">VK Style Guides</li>
                                  <li class="colhead">Top Brands</li>
                              </ul>        
                          </div>
                      </div>
                    <div class="col-md-8 pull-left">
                        <img src="images/beauty-thumb1.jpg" alt="beauty" class="img-responsive">
                    </div>
                      <div class="col-md-2">
                        <ul>
                          <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                              <ul>
                                  <li><img src="images/accessories-icons.png" alt="accessories"></li>
                              </ul>
                          </li>
                        </ul>
                    </div>
                </div>
              </div>
            </div>
        </div> 
          
        </div>
        <!-- <div class="mm-js-shadow"></div> -->
        </div>
      </li>
       <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Fragrance</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
           <div class="section-tab-container">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item active text-center">
                                      Type
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Family
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Formulation
                                  </a>
                                  
                              </div>
                          </div>
                          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Perfumes</li>
                                                      <li class="colhead">Deodorants</li>
                                                      <li class="colhead">Roll Ons</li>
                                                      <li class="colhead">Body Mists and Spray</li>
                                                      <li class="colhead">Gifts and Value Sets</li>
                                                      <li class="colhead">VK Style Guide</li>
                                                      <li class="colhead">Top Brands</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fragrance_img.jpg" alt="fragrance_img" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>


                               <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Celebrity  </li>
                                                      <li class="colhead">Woody </li>
                                                      <li class="colhead">Spicy </li>
                                                      <li class="colhead">Floral  </li>
                                                      <li class="colhead">Fruity  </li>
                                                      <li class="colhead">Citrus </li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fragrance_img.jpg" alt="beauty" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                                  </div>

                                   <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Oil</li>
                                                      <li class="colhead">Spray</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fragrance_img.jpg" alt="beauty" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                              </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>  
           </div>
        </div>
        </div>
      </li>
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">fitness</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         <ul>
                        <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item active text-center">
                                      Sports wear
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Gym Apparels
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shoes 
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Yoga 
                                  </a>
                                  
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                    <li class="colhead">T Shirts  </li>
                                                    <li class="colhead">Bottoms</li>
                                                    <li class="colhead">Sports Bra</li>
                                                    <li class="colhead">Hoodies &amp; Sweatshirts</li>
                                                    <li class="colhead">Shorts </li>
                                                    <li class="colhead">Sets </li>
                                                    <li class="colhead">Jackets</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fitness_banner.jpg" alt="fitness_banner" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>



                           <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Dumbbells</li>
                                                      <li class="colhead">Medicine Ball</li>
                                                      <li class="colhead">Multi Function Benc</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fitness_banner.jpg" alt="fitness_banner" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                           <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Running</li>
                                                      <li class="colhead">Training </li>
                                                      <li class="colhead">Non Marking Souls </li>
                                                      <li class="colhead">Cycling</li>
                                                      <li class="colhead">Yoga </li>
                                                      <li class="colhead">Dance</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/fitness_banner.jpg" alt="fitness_banner" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                                  <div class="section-tab-content">
                                    <div class="row">
                                          <div class="col-md-12">
                                              <div class="col-md-2">
                                                  <div class="listbox-col listbox-beauty">
                                                      <ul>
                                                          <li class="colhead">Mats  </a></li>
                                                          <li class="colhead">Blocks</a></li>
                                                          <li class="colhead">Straps</a></li>
                                                      </ul>                                                                          
                                                  </div>
                                              </div>
                                            <div class="col-md-8 pull-left">
                                                <img src="images/fitness_banner.jpg" alt="fitness_banner" class="img-responsive">
                                            </div>
                                              <div class="col-md-2">
                                                  <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                           </div>       
                         </ul> 
        </div>
        </div>
      </li>

      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Health & Wellness</a>
          <div style="width: 100%; display: none;" class="mm-item-content">
              <div class="mm-content-base">         
                <ul>
                        <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item active text-center">
                                      Monitoring Devices 
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      DIET & NUTRITION
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      HEALTH FOOD & DRINKS 
                                  </a>
                                  
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">BP Monitors </li>
                                                      <li class="colhead">Diabetic Care </li>
                                                      <li class="colhead">Respiratory Care &amp; Heart Care</li>
                                                      <li class="colhead">Hemoglobin &amp; Uric Acid Monitors </li>
                                                      <li class="colhead">Thermometers </li>
                                                      <li class="colhead">Body Fat Analyzers </li>
                                                      <li class="colhead">Weight Management</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/health-wellness-banner.jpg" alt="beauty" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>



                           <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                      <li class="colhead">Sports Supplements </li>
                                                      <li class="colhead">Vitamins &amp; Supplements</li>
                                                      <li class="colhead">Weight Management Products</li>
                                                      <li class="colhead">Family Nutrition </li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/health-wellness-banner.jpg" alt="beauty" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                           <div class="section-tab-content">
                                <div class="row">
                                      <div class="col-md-12">
                                          <div class="col-md-2">
                                              <div class="listbox-col listbox-beauty">
                                                  <ul>
                                                     <li class="colhead">Herbal Tea</li>
                                                     <li class="colhead">Slimming Tea</li>
                                                  </ul>                                                                          
                                              </div>
                                          </div>
                                        <div class="col-md-8 pull-left">
                                            <img src="images/health-wellness-banner.jpg" alt="beauty" class="img-responsive">
                                        </div>
                                          <div class="col-md-2">
                                              <ul>
                                              <li class="colhead" style="text-align: left!important;"><a href="#">Shop By Brands</a>
                                                  <ul>
                                                      <li><img src="images/accessories-icons.png" alt="accessories"></li>
                                                  </ul>
                                              </li>
                                               </ul>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                           </div>       
                         </ul> 
              </div>
          </div>
      </li>

      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Accessories</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
                                      <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
                                          <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>
                                        </div>
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Services</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
                                      <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
                                          <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>
                                        </div>
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">New Arrivals</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
                                      <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
                                          <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>
                                        </div>
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">value</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
                                      <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
                                          <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>
                                        </div>
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>
      <li class="mm-item">
        <a href="javascript:void(0)" class="mm-item-link">Pre-Owned</a>
        <div style="width: 100%; display: none;" class="mm-item-content">
        <div class="mm-content-base">
         
          <div class="section-tab-container">
                          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 section-tab-menu">
                              <div class="list-group">
                                  <a href="#" class="list-group-item text-center active">
                                      clothing
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Shop by
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      accessories
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      Designers
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      sale
                                  </a>
                                  <a href="#" class="list-group-item text-center">
                                      services
                                  </a>
                              </div>
                          </div>

                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 section-tab">
                              <div class="section-tab-content active">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">arabic wear</li>
                                       <li><a href="catalogue-page.html" class="list-link">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                   
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>                         

                         <!--Shop By-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Shop By</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Kaftans</a></li>
                                      <li><a href="#">Jalabiyas</a></li>
                                      <li><a href="#">Abayas</a></li>
                                      <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                      <li class="colhead">western wear</li>
                                      <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                      <li><a href="#">Jumpsuits &amp; Playsuits&lt;</a></li>
                                      <li><a href="#">Tops, Shirts &amp; T-shirts&lt;</a></li>
                                      <li><a href="#">Jeans &amp; Leggings&lt;</a></li>
                                      <li><a href="#">Capris &amp; Trousers&lt;</a></li>
                                      <li><a href="#">Skirts &amp; Shorts&lt;</a></li>
                                      <li><a href="#">Sweatshirts &amp; Sweaters&lt;</a></li>
                                      <li><a href="#">Blazers, Coats &amp; Jackets&lt;</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                      <li><a href="#">Dress materials &amp; Suit</a></li>
                                      <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                      <li><a href="#">Anarkali &amp; Gowns</a></li>
                                      <li><a href="#">Dupatta &amp; Shawls</a></li>
                                      <li><a href="#">Sarees &amp; Blouses</a></li>
                                      <li><a href="#">Lehenga Choli</a></li>
                                      <li><a href="#">Skirts &amp; Palazzos</a></li>
                                      <li><a href="#">Leggings</a></li>
                                      <li><a href="#">Salwars &amp; Chudidars</a></li>
                                     <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                      <li><a href="#">T-shirts &amp; Tops</a></li>
                                      <li><a href="#">Bodysuits</a></li>
                                      <li><a href="#">Tights, Leggings</a></li>
                                      <li><a href="#">Track pants</a></li>
                                      <li><a href="#">Shorts</a></li>
                                      <li><a href="#">Sweatshirts</a></li>
                                      <li><a href="#">Socks</a></li>
                                      <li><a href="#">Sports Bras</a></li>
                                      <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Accessories-->
                                <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Accessories</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                      <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                </div>
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                      <li><a href="#">Bras</a></li>
                                      <li><a href="#">Panties</a></li>
                                      <li><a href="#">Lingerie Sets</a></li>
                                      <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                      <li><a href="#">Babydoll &amp; Chemise</a></li>
                                      <li><a href="#">Camisoles &amp; Thermals</a></li>
                                      <li><a href="#">Loungwear</a></li>
                                      <li><a href="#">Pajamas, Tights</a></li>
                                      <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                      <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                               </div>
                                 <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                      <li><a href="#">Bikini</a></li>
                                      <li><a href="#">Coverups &amp; Sarongs</a></li>
                                      <li><a href="#">One piece &amp; Monokini</a></li>
                                      <li><a href="#">Beach maxi Kaftans</a></li>
                                      <li><a href="#">Beach Tops</a></li>
                                      <li><a href="#">Beach Shorts</a></li>
                                      <li><a href="#">Swim suits</a></li>
                                      <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                               
                              <!--Designers-->
                               <div class="section-tab-content">
                                  <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Designers</li>
                                       <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Kaftans</a></li>
                                       <li><a href="#">Jalabiyas</a></li>
                                       <li><a href="#">Abayas</a></li>
                                       <li><a href="#">Prayer wear</a></li>
                                     </ul>
                                      <ul>
                                       <li class="colhead">western wear</li>
                                       <li><a href="catalogue-page.html">Dresses</a></li>
                                       <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                       <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                       <li><a href="#">Jeans &amp; Leggings</a></li>
                                       <li><a href="#">Capris &amp; Trousers</a></li>
                                       <li><a href="#">Skirts &amp; Shorts</a></li>
                                       <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                       <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                     </ul>                                     
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">indian wear</li>
                                       <li><a href="#">Dress materials &amp; Suit</a></li>
                                       <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                       <li><a href="#">Anarkali &amp; Gowns</a></li>
                                       <li><a href="#">Dupatta &amp; Shawls</a></li>
                                       <li><a href="#">Sarees &amp; Blouses</a></li>
                                       <li><a href="#">Lehenga Choli</a></li>
                                       <li><a href="#">Skirts &amp; Palazzos</a></li>
                                       <li><a href="#">Leggings</a></li>
                                       <li><a href="#">Salwars &amp; Chudidars</a></li>
                                       <li><a href="#">Jackets</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                    <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">sports/active wear</li>
                                       <li><a href="#">T-shirts &amp; Tops</a></li>
                                       <li><a href="#">Bodysuits</a></li>
                                       <li><a href="#">Tights, Leggings</a></li>
                                       <li><a href="#">Track pants</a></li>
                                       <li><a href="#">Shorts</a></li>
                                       <li><a href="#">Sweatshirts</a></li>
                                       <li><a href="#">Socks</a></li>
                                       <li><a href="#">Sports Bras</a></li>
                                       <li><a href="#">Sets </a></li>
                                     </ul>                                 
                                   </div>
                                   </div>

                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Lingerie</li>
                                       <li><a href="#">Bras</a></li>
                                       <li><a href="#">Panties</a></li>
                                       <li><a href="#">Lingerie Sets</a></li>
                                       <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                       <li><a href="#">Babydoll &amp; Chemise</a></li>
                                       <li><a href="#">Camisoles &amp; Thermals</a></li>
                                       <li><a href="#">Loungwear</a></li>
                                       <li><a href="#">Pajamas, Tights</a></li>
                                       <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                       <li><a href="#">Robes</a></li>                    
                                   </ul></div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">Swim/Beach wear</li>
                                       <li><a href="#">Bikini</a></li>
                                       <li><a href="#">Coverups &amp; Sarongs</a></li>
                                       <li><a href="#">One piece &amp; Monokini</a></li>
                                       <li><a href="#">Beach maxi Kaftans</a></li>
                                       <li><a href="#">Beach Tops</a></li>
                                       <li><a href="#">Beach Shorts</a></li>
                                       <li><a href="#">Swim suits</a></li>
                                       <li><a href="#">Rash Guards</a></li>
                                     </ul>                                 
                                   </div>
                                   </div>
                                    <div class="col-md-2">
                                   <div class="listbox-col">
                                     <ul>
                                       <li class="colhead">plus size</li>
                                       <li class="colhead">petite wear</li>
                                       <li class="colhead">maternity wear</li>
                                       <li><a href="#">&nbsp;</a></li>
                                       <li>
                                         <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                       </li>
                                     </ul>                                 
                                   </div>
                                </div>
                              </div>

                              <!--Sale-->
                                    <div class="section-tab-content">
                                      <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Sale</li>
                                             <li><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Kaftans</a></li>
                                             <li><a href="#">Jalabiyas</a></li>
                                             <li><a href="#">Abayas</a></li>
                                             <li><a href="#">Prayer wear</a></li>
                                           </ul>
                                            <ul>
                                             <li class="colhead">western wear</li>
                                             <li><a href="#"></a><a href="catalogue-page.html">Dresses</a></li>
                                             <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                             <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                             <li><a href="#">Jeans &amp; Leggings</a></li>
                                             <li><a href="#">Capris &amp; Trousers</a></li>
                                             <li><a href="#">Skirts &amp; Shorts</a></li>
                                             <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                             <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                           </ul>                                     
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">indian wear</li>
                                             <li><a href="#">Dress materials &amp; Suit</a></li>
                                             <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                             <li><a href="#">Anarkali &amp; Gowns</a></li>
                                             <li><a href="#">Dupatta &amp; Shawls</a></li>
                                             <li><a href="#">Sarees &amp; Blouses</a></li>
                                             <li><a href="#">Lehenga Choli</a></li>
                                             <li><a href="#">Skirts &amp; Palazzos</a></li>
                                             <li><a href="#">Leggings</a></li>
                                             <li><a href="#">Salwars &amp; Chudidars</a></li>
                                             <li><a href="#">Jackets</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                          <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">sports/active wear</li>
                                             <li><a href="#">T-shirts &amp; Tops</a></li>
                                             <li><a href="#">Bodysuits</a></li>
                                             <li><a href="#">Tights, Leggings</a></li>
                                             <li><a href="#">Track pants</a></li>
                                             <li><a href="#">Shorts</a></li>
                                             <li><a href="#">Sweatshirts</a></li>
                                             <li><a href="#">Socks</a></li>
                                             <li><a href="#">Sports Bras</a></li>
                                             <li><a href="#">Sets </a></li>
                                           </ul>                                 
                                         </div>
                                         </div>

                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Lingerie</li>
                                             <li><a href="#">Bras</a></li>
                                             <li><a href="#">Panties</a></li>
                                             <li><a href="#">Lingerie Sets</a></li>
                                             <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                             <li><a href="#">Babydoll &amp; Chemise</a></li>
                                             <li><a href="#">Camisoles &amp; Thermals</a></li>
                                             <li><a href="#">Loungwear</a></li>
                                             <li><a href="#">Pajamas, Tights</a></li>
                                             <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                             <li><a href="#">Robes</a></li>                    
                                         </ul></div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">Swim/Beach wear</li>
                                             <li><a href="#">Bikini</a></li>
                                             <li><a href="#">Coverups &amp; Sarongs</a></li>
                                             <li><a href="#">One piece &amp; Monokini</a></li>
                                             <li><a href="#">Beach maxi Kaftans</a></li>
                                             <li><a href="#">Beach Tops</a></li>
                                             <li><a href="#">Beach Shorts</a></li>
                                             <li><a href="#">Swim suits</a></li>
                                             <li><a href="#">Rash Guards</a></li>
                                           </ul>                                 
                                         </div>
                                         </div>
                                          <div class="col-md-2">
                                         <div class="listbox-col">
                                           <ul>
                                             <li class="colhead">plus size</li>
                                             <li class="colhead">petite wear</li>
                                             <li class="colhead">maternity wear</li>
                                             <li><a href="#">&nbsp;</a></li>
                                             <li>
                                               <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                             </li>
                                           </ul>                                 
                                         </div>
                                      </div>
                                    </div>
                             
                                    <!--Services-->
                                       <div class="section-tab-content">
                                          <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Kaftans</a></li>
                                                 <li><a href="#">Jalabiyas</a></li>
                                                 <li><a href="#">Abayas</a></li>
                                                 <li><a href="#">Prayer wear</a></li>
                                               </ul>
                                                <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="catalogue-page.html">Dresses</a></li>
                                                 <li><a href="#">Jumpsuits &amp; Playsuits</a></li>
                                                 <li><a href="#">Tops, Shirts &amp; T-shirts</a></li>
                                                 <li><a href="#">Jeans &amp; Leggings</a></li>
                                                 <li><a href="#">Capris &amp; Trousers</a></li>
                                                 <li><a href="#">Skirts &amp; Shorts</a></li>
                                                 <li><a href="#">Sweatshirts &amp; Sweaters</a></li>
                                                 <li><a href="#">Blazers, Coats &amp; Jackets</a></li>
                                               </ul>                                     
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Dress materials &amp; Suit</a></li>
                                                 <li><a href="#">Kurtas,Tunics &amp; Tops</a></li>
                                                 <li><a href="#">Anarkali &amp; Gowns</a></li>
                                                 <li><a href="#">Dupatta &amp; Shawls</a></li>
                                                 <li><a href="#">Sarees &amp; Blouses</a></li>
                                                 <li><a href="#">Lehenga Choli</a></li>
                                                 <li><a href="#">Skirts &amp; Palazzos</a></li>
                                                 <li><a href="#">Leggings</a></li>
                                                 <li><a href="#">Salwars &amp; Chudidars</a></li>
                                                <li><a href="#">Jackets</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                              <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                <li><a href="#">T-shirts &amp; Tops</a></li>
                                                <li><a href="#">Bodysuits</a></li>
                                                <li><a href="#">Tights, Leggings</a></li>
                                                <li><a href="#">Track pants</a></li>
                                                <li><a href="#">Shorts</a></li>
                                                <li><a href="#">Sweatshirts</a></li>
                                                <li><a href="#">Socks</a></li>
                                                <li><a href="#">Sports Bras</a></li>
                                                <li><a href="#">Sets </a></li>
                                               </ul>                                 
                                             </div>
                                             </div>

                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bras</a></li>
                                                 <li><a href="#">Panties</a></li>
                                                 <li><a href="#">Lingerie Sets</a></li>
                                                 <li><a href="#">Shapewear, Bodies &amp; Corset</a></li>
                                                 <li><a href="#">Babydoll &amp; Chemise</a></li>
                                                 <li><a href="#">Camisoles &amp; Thermals</a></li>
                                                 <li><a href="#">Loungwear</a></li>
                                                 <li><a href="#">Pajamas, Tights</a></li>
                                                 <li><a href="#">Nighties &amp; Sleep shirts</a></li>
                                                 <li><a href="#">Robes</a></li>                    
                                             </ul></div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">Services</li>
                                                 <li><a href="#">Bikini</a></li>
                                                 <li><a href="#">Coverups &amp; Sarongs</a></li>
                                                 <li><a href="#">One piece &amp; Monokini</a></li>
                                                 <li><a href="#">Beach maxi Kaftans</a></li>
                                                 <li><a href="#">Beach Tops</a></li>
                                                 <li><a href="#">Beach Shorts</a></li>
                                                 <li><a href="#">Swim suits</a></li>
                                                 <li><a href="#">Rash Guards</a></li>
                                               </ul>                                 
                                             </div>
                                             </div>
                                              <div class="col-md-2">
                                             <div class="listbox-col">
                                               <ul>
                                                 <li class="colhead">plus size</li>
                                                 <li class="colhead">petite wear</li>
                                                 <li class="colhead">maternity wear</li>
                                                 <li><a href="#">&nbsp;</a></li>
                                                 <li>
                                                   <img src="images/book-style.jpg" alt="img" class="img-responsive">
                                                 </li>
                                               </ul>                                 
                                             </div>
                                          </div>
                                        </div>
                                  </div>
                                </div>
        </div><!-- <div class="mm-js-shadow"></div> --></div>
      </li>
      <li class="clear-fix"></li>
    </ul>
          
                                        
                  </div>
              


             </div>
        </div>
     </header>