<?php include 'header.php';?>
<body>
<?php include 'navigation.php';?>
<section class="categorySec body-margin">
	<div class="container">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="index.php">Home</a></li>
			<li class="breadcrumb-item">Beauty</li>
			<li class="breadcrumb-item">Electronics</li>
			<li class="breadcrumb-item active">Epilators</li>
		</ol>
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3" style="padding-right:0px;">
				<div class="catalogue-leftsidebar">
					<div class="filter-block">
						<div class="title">Filter By</div>
						<p>EPILATORS</p>
						<a href="#" class="see-all">See all Electronics</a>
					</div>
					<div class="filter-block" style="display:none">
						<div>
							<button class="btn clearBtn">
							<img src="images/filter.png"/> Clear All</button>	
							<div class="clearfix"></div>
						</div>
						<div class="filter-added-row">
							<div class="filter-added">
							chin
							<span><img src="images/filter-close.png"/></span> 
							</div>
							<div class="filter-added">	
							upper lip
							<span><img src="images/filter-close.png"/></span> 
							</div> 
							<div class="filter-added">	
							forehead
							<span><img src="images/filter-close.png"/></span> 
							</div>
							<div class="filter-added">	
							eyebrows
							<span><img src="images/filter-close.png"/></span> 
							</div>
							<div class="clearfix"></div>
						</div>
					</div> 
					<div class="filter-block">
						<div class="title">Price Range</div>
						<div class='slider-example'>
							<div>
								<input id="ex2" type="text" class="span2" value="" data-slider-min="10" data-slider-max="1000" data-slider-step="5" data-slider-value="[250,450]"/> 
							</div>
							<div class="slider-value">
								<span class="start-val">10 AED</span> 
								<span class="end-val">1000 AED</span>
								<div class="cleafix"></div>
							</div>
						</div>					  
					</div>
						<div class="filter-block filter-by-brand">
							<div class="filter-block-inner">
								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									<div class="panel panel-default" style="display:none;">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Size
												</a>
											</h4>
										</div>
										<div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<ul class="brand-list">
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox1" class="styled" type="checkbox">
														<label for="checkbox1">S</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox2" class="styled" type="checkbox">
														<label for="checkbox2">M</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox3" class="styled" type="checkbox">
														<label for="checkbox3">L</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox4" class="styled" type="checkbox" checked="">
														<label for="checkbox4">XL</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox5" class="styled" type="checkbox">
														<label for="checkbox5">XXL</label>
														</div>
														</a>
													</li>
												</ul>
											</div> 
										</div> 
									</div>
								<div class="panel panel-default" style="display:none;">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseOne">
											<i class="more-less glyphicon glyphicon-plus"></i>
											Color
											</a>
										</h4>
									</div>
									<div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<ul class="brand-list">
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox1" class="styled" type="checkbox">
														<label for="checkbox1"><span class="color-sec color-sec1"></span> Red</label>
														</div>
													</a>
												</li>
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox2" class="styled" type="checkbox">
														<label for="checkbox2"><span class="color-sec color-sec2"></span> Blue</label>
														</div>
													</a>
												</li>
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox3" class="styled" type="checkbox">
														<label for="checkbox3"><span class="color-sec color-sec3"></span> Green</label>
														</div>
													</a>
												</li>
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox4" class="styled" type="checkbox">
														<label for="checkbox4"><span class="color-sec color-sec4"></span> Pink</label>
														</div>
													</a>
												</li>
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox5" class="styled" type="checkbox">
														<label for="checkbox5"><span class="color-sec color-sec5"></span> Black</label>
														</div>
													</a>
												</li>
											</ul>
										</div> 
									</div> 
								</div>
									<div class="panel panel-default" style="display:none;">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												occasion
												</a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<ul class="brand-list">
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox1" class="styled" type="checkbox" checked="">
															<label for="checkbox1">Casual</label>
															</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox2" class="styled" type="checkbox">
														<label for="checkbox2">Formal</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox3" class="styled" type="checkbox">
														<label for="checkbox3">Party</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox4" class="styled" type="checkbox">
														<label for="checkbox4">Sports</label>
														</div>
														</a>
													</li>
													<li>
														<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox5" class="styled" type="checkbox">
														<label for="checkbox5">Wedding</label>
														</div>
														</a>
													</li>
												</ul>
											</div> 
										</div> 
									</div> 
								<div class="panel panel-default">
									<div class="panel-heading" role="tab" id="headingOne">
										<h4 class="panel-title">
											<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
											<i class="more-less glyphicon glyphicon-plus"></i>
											Brand
											</a>
										</h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
										<div class="panel-body">
											<div class="searchSec" style="display:none;">
												<div class="input-group">
													<input type="text" class="form-control" placeholder="SEARCH">
													<span class="input-group-addon" id="basic-addon1"><img src="images/brand-search.png"/></span>
												</div>
											</div> 
											<ul class="brand-list" style="height:auto">
												<li>
													<a>
														<div class="checkbox checkbox-default">
														<input id="checkbox1" class="styled" type="checkbox" checked>
														<label for="checkbox1">Braun</label>
														</div>
													</a>
												</li>
											</ul>
										</div> 
									</div> 
								</div>
									<div class="panel panel-default" style="display:none;">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseOne">
												<i class="more-less glyphicon glyphicon-plus"></i>
												Discount
												</a>
											</h4>
										</div>
										<div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<ul class="brand-list">
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox1" class="styled" type="checkbox">
															<label for="checkbox1">50% or more</label>
															</div>
														</a>
													</li>
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox2" class="styled" type="checkbox">
															<label for="checkbox2">40% or more</label>
															</div>
														</a>
													</li>
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox3" class="styled" type="checkbox">
															<label for="checkbox3">30% or more</label>
															</div>
														</a>
													</li>
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox4" class="styled" type="checkbox">
															<label for="checkbox4">20% or more</label>
															</div>
														</a>
													</li>
													<li>
														<a>
															<div class="checkbox checkbox-default">
															<input id="checkbox5" class="styled" type="checkbox">
															<label for="checkbox5">10% or more</label>
															</div>
														</a>
													</li>
												</ul>
											</div> 
										</div> 
									</div> 
								</div>
							</div> 
						</div> 
				    </div> 
			    </div>
			<div class="col-lg-9 col-md-9 col-sm-9 catalogue-rightsidebar">
				<div class="">
					<div class="row">
						<div class="col-md-5 col-sm-5">
							<div class="search-result-count">
							 	Showing 1- 09
							</div> 
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="sort-by-sec">
								<span class="sort-txt">IN STOCK</span> 
								<label class="switch">
									<input type="checkbox">
									<span class="slider-switch round"></span>
								</label>
							</div> 
						</div>
						<div class="col-md-2 col-sm-2">
							<div class="sort-by-sec">
								<span class="sort-txt">price</span> 
								<div class="price-order">
									<div class="acending-price">
										<a href="javascript:void(0);"></a>
									</div>
									<div class="decending-price">
										<a href="javascript:void(0);"></a>
									</div>
								</div>
							</div> 
						</div>
						<div class="col-md-3 col-sm-3">
							<div class="sort-by-sec">
								<span class="sort-txt">Sort By</span> 
								<select class="selectpicker">
									<option>New Arrivals</option>
									<option>Popular</option>
									<option>Top Rated</option>
								</select>
							</div> 
						</div> 
					</div>
					<div class="">
						<img class="img-responsive" src="images/dresses.png" align="banner-catalog">
					</div>
					<p>&nbsp;</p>
					<div class="catalogue-rightSec">
						<div class="row">
							<div class="col-md-4" style="cursor: pointer;">	
								<div class="thumb-bg">
									<a href="product-detail-epilator.php">
										<div class="img-container">
											<img src="images/Epilators/01_4210201152781_Braun_FaceSpa_SE851_04.jpg" alt="thumbnails" class="img-card">
										</div>
										<div class="card-content">
											<h4>Braun FaceSpa 851 Facial Epilator & Cleanser With 3 Beauty Brushes</h4>
											<h5>exfoliation and regular brush, epilation head and cap, beauty pouch</h5>
											<p>399 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon1">
											<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
										</span>
									</div>
								</div>
							</div>
						<div class="col-md-4">	
							<div class="thumb-bg">
								<a href="product-detail-epilator.php">
									<div class="img-container">
										<img src="images/Epilators/3.jpg" alt="thumbnails" class="img-card">
									</div>
								</a>
							<a href="product-detail-epilator.php">
								<div class="card-content">
									<h4>Face Color</h4>
									<h5>2 regular brushes, epilation, head and cap, beauty pouch</h5>
									<p>399 AED</p>                
								</div>
							</a>
								<div class="overlay-icon">
									<span class="show-icon1">
										<a href="javascript:void(0)"></a>
									</span>
									<span class="show-icon2">
										<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
									</span>
								</div>
							</div>
						</div> 
							<div class="col-md-4">
								<div class="thumb-bg">
									<a href="product-detail-epilator.php">  
										<div class="img-container">
										<img src="images/Epilators/4.jpg" alt="thumbnails" class="img-card">
										</div>
									</a>
									<a href="product-detail-epilator.php">
										<div class="card-content">
											<h4>Face Color</h4>
											<h5>2 regular brushes, epilation, head and cap, beauty pouch</h5>
											<p>399 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon1">
											<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
										</span>
									</div>
								</div>
							</div> 
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="thumb-bg">
									<a href="product-detail-epilator.php"> 
										<div class="img-container">
											<img src="images/Epilators/2.jpg" alt="thumbnails" class="img-card">
										</div> 
									</a>
									<a href="product-detail-epilator.php">
										<div class="card-content">
											<h4>Braun Face epilator</h4> 
											<h5>for chin, upper lip, forehead and eyebrows...</h5>
											<p>399 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon1">
											<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
										</span>
									</div>
								</div>
							</div> 
						<div class="col-md-4"> 
							<div class="thumb-bg"> 
								<a href="product-detail-epilator.php">
									<div class="img-container">
										<img src="images/Epilators/5.jpg" alt="thumbnails" class="img-card">
									</div> 
								</a>
							<a href="product-detail-epilator.php">
								<div class="card-content">
									<h4>Braun Face epilator</h4>
									<h5>for chin, upper lip, forehead and eyebrows,with lighted mirror...</h5>
									<p>349 AED</p>                
								</div>
							</a>
								<div class="overlay-icon">
									<span class="show-icon1">
										<a href="javascript:void(0)"></a>
									</span>
									<span class="show-icon2">
										<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
									</span>
								</div>
							</div>
						</div> 
							<div class="col-md-4"> 
								<div class="thumb-bg"> 
									<a href="product-detail-epilator.php">
										<div class="img-container">
											<img src="images/Epilators/6.jpg" alt="thumbnails" class="img-card">
										</div> 
										<div class="card-content">
											<h4>Braun IPL </h4>
											<h5>hair removal system for use on body and face</h5>
											<p>1599 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon2">
										<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
										</span>
									</div>
								</div>
							</div> 
						</div> 
							<div class="row">
								<div class="col-md-4"> 
									<div class="thumb-bg"> 
										<a href="product-detail-epilator.php"> 
											<div class="img-container">
												<img src="images/Epilators/7.jpg" alt="thumbnails" class="img-card">
											</div> 
										</a>
										<a href="product-detail-epilator.php">
											<div class="card-content">
												<h4>Braun Silkepil 9</h4>
												<h5>wet & dry, shaver head, trimmer cap, facial cap. Cordless, waterproof...</h5>
												<p>749 AED</p>                
											</div>
										</a>
										<div class="overlay-icon">
											<span class="show-icon1">
											<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
											<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
											</span>
										</div>
									</div>
								</div> 
								<div class="col-md-4">
									<div class="thumb-bg"> 
										<a href="product-detail-epilator.php"> 
											<div class="img-container">
												<img src="images/Epilators/8.jpg" alt="thumbnails" class="img-card">
											</div> 
											<div class="card-content">
												<h4>Braun Silkepil 9</h4>
												<h5>wet & dry + facial epliator, massage cap...</h5>
												<p>649 AED</p>                
											</div>
										</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
											</span>
										</div>
									</div>
								</div> 
								<div class="col-md-4"> 
									<div class="thumb-bg"> 
										<a href="product-detail-epilator.php"> 
											<div class="img-container">
												<img src="images/Epilators/9.jpg" alt="thumbnails" class="img-card">
											</div> 
											<div class="card-content">
												<h4>Braun Silkepil 9 series</h4>
												<h5>gifting edtion for legs, body and face, inclused beauty pouch</h5>
												<p>599 AED</p>                
											</div>
										</a>
										<div class="overlay-icon">
											<span class="show-icon1">
												<a href="javascript:void(0)"></a>
											</span>
											<span class="show-icon2">
												<a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
											</span>
										</div>
									</div>
								</div> 
							</div> 
						<div class="load-more-sec">
							<span>Load More</span>
						</div> 
					</div> 
				</div>
			</div> 
		</div> 
	</div>
</section>
<?php include 'footer.php';?>
