<?php include 'header.php';?>

	<body>
	
		<?php include 'navigation.php';?>

      <div class="bottomMenu">
      <span><img src="images/flower6.png" alt="flowers"/></span>
      <a href="#" class="active-breadcrumb" title="index-home">BEAUTY</a>
     </div>

    <div id="carouselFade" class="carousel slide carousel-fade body-margin" data-ride="carousel" data-interval="4000">
        <div class="overlay-box">
            <div class="slidebox-1 wow fadeInUp animated" data-wow-delay="0.5s">
            <img src="images/banner-box1.jpg" alt="overlay-box">
            </div>
            <div class="slidebox-1 wow fadeInUp animated" data-wow-delay="0.1s">
            <img src="images/banner-box2.jpg" alt="overlay-box">
            </div>
        </div>

      <div class="leftSidebarSec a"><!-- id="leftnav-stick" -->
          <ul>
              <li>
                <div class="categoryImg" title="Fashion">
                    <a href="#" class="hvr-underline-from-left">
                    <img src="images/fashion2.png" alt="Fashion" class="leftsidebar-icon1" />
                    <div class="leftnav-btmtitle">Fashion</div>
                    </a>
                </div>
              </li>
              <li id="trigger-overlay" class="beauty-nav">
                  <div class="categoryImg" title="Beauty">
                    <a href="#" class="hvr-underline-from-left">
                    <div class="leftnav-btmtitle">Beauty</div>
                    </a>
                  </div>
              </li>
              <li>
                <div class="categoryImg" title="Fragrance">
                  <a href="#" class="hvr-underline-from-left">
                  <img src="images/fragrance2.png" alt="Fragrance"  class="leftsidebar-icon2"/>
                  <div class="leftnav-btmtitle">Fragrance</div>
                  </a>
                </div>
              </li>
              <li>
                <div class="categoryImg" title="Fitness">
                  <a href="#" class="hvr-underline-from-left">
                  <img src="images/fitness2.png" alt="Fitness" class="leftsidebar-icon3"/>
                  <div class="leftnav-btmtitle">Fitness</div>
                  </a>
                </div>
              </li>
              <li>
                <div class="categoryImg" title="Health & Wellness">
                  <a href="#" class="hvr-underline-from-left">
                  <img src="images/health-wellness2.png" alt="Health & Wellness" class="leftsidebar-icon4" />
                  <div class="leftnav-btmtitle">Health & Wellness</div>
                  </a>
                </div>
              </li>
              <li>
                <div class="categoryImg" title="Accessories">
                  <a href="#" class="hvr-underline-from-left">
                  <img src="images/accessories2.png" alt="Accessories"  class="leftsidebar-icon5"/>
                  <div class="leftnav-btmtitle">Accessories</div>
                  </a>
                </div>
              </li>
          </ul>
        </div>

<!-- home page intro-->
        <div class="carousel-inner" role="listbox">          
            <div class="item active">  
            <div class="carousel-caption">                  
            <p>Featured collection</p>
            <h3>go anywhere</h3>
            <span><a href="#">DISCOVER the collection <img src="images/arrow.png" alt="arrow"></a></span>
            </div>
            </div>
            <div class="item"> 
            <div class="carousel-caption">                  
            <p>Featured collection</p>
            <h3>go anywhere</h3>
            <span><a href="#">DISCOVER the collection <img src="images/arrow.png" alt="arrow"></a></span>
            </div>
            </div>
            <div class="item"> 
            <div class="carousel-caption">                  
            <p>Featured collection</p>
            <h3>go anywhere</h3>
            <span><a href="#">DISCOVER the collection <img src="images/arrow.png" alt="arrow"></a></span>
            </div>
            </div>
        </div>        
      </div>
      <!-- home page intro end-->


<div class="box-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                  <div class="product-thumb">
                      <img src="images/grid-img1.jpg" alt="img">
                  </div>
                  <div class="product-content">
                      <span class="tagline">Fitness</span>                            
                      <h5><a href="#">
                      ROONEY MARA'S ROAD <br>TO THE 2016 OSCARS</a>
                      </h5>                    
                  </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
              <div class="product-item-vote">
                  <div class="product-thumb">
                      <img src="images/grid-img2.jpg" alt="img">
                  </div>
                  <div class="product-content">
                    <span class="tagline">Health & wellness</span> 
                    <h5><a href="#">10 Ideas For A Sugar<br>Detox Diet Plan</a></h5>               
                  </div>
              </div>
              <div class="product-item-vote">
                <div class="product-thumb">
                  <img src="images/grid-img3.jpg" alt="img">
                </div>
                <div class="product-content">
                  <span class="tagline">Beauty</span>
                  <h5><a href="#">contouring magic in<br>90 seconds</a></h5>                
                </div>
              </div>
          </div>

            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote">
                    <div class="product-thumb">
                    <img src="images/grid-img4.jpg" alt="img">
                    </div>
                    <div class="product-content">
                    <span class="tagline">fragrances</span>
                    <h5><a href="#">New Rose-Infused<br>Beauty Products to Try</a></h5>
                    </div>
                </div>
                <div class="product-item-vote">
                    <div class="product-thumb">
                    <img src="images/grid-img5.jpg" alt="img">
                    </div>
                    <div class="product-content">
                    <span class="tagline">Fitness</span>
                    <h5><a href="#">can Jogging Help You<br>Lose Weight?</a></h5>
                    </div>
                </div>
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="product-item-vote news-section">
                  <div class="product-content news-title">
                    <span class="news-title"><img src="images/flower5.png" alt="flowers"> Latest for you</span>                           
                  </div>

                  <div class="product-content">
                  <span class="tagline">Fashion</span>
                  <h5><a href="#">3 expert ways to wear denim shorts at a festival</a></h5>                            
                  </div>
                    <hr>
                  <div class="product-content">
                  <span class="tagline">News</span>
                  <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                  </div>
                   <hr>
                  <div class="product-content">
                  <span class="tagline">Fitness</span>
                  <h5><a href="#">7 Things You May Not Know About Richard Simmons</a></h5>                            
                  </div>
                  <hr>
                  <div class="product-content">
                  <span class="tagline">Health & wellness</span>
                  <h5><a href="news-article.php">Top 5 Eco-friendly Women's roll-on deos</a></h5>                            
                  </div>
                  <hr>
                  <div class="product-thumb">
                  <img src="images/grid-img6.jpg" alt="img">
                  </div> 
                  </div> 
            </div>
        </div><!-- row end-->

         <div class="row">
            <div class="content-section margin-btm0">
              <div class="container">
                  <div class="row">
                      <div class="col-md-4">
                          <div class="product-item-1 parent">
                            <div class="product-thumb child">
                              <img src="images/grid-img8.jpg" alt="img">
                            </div> 
                            <div class="overlay-icon">
                              <span class="show-icon1">
                              <a href="javascript:void(0)"></a>
                              </span>
                              <span class="show-icon2">
                              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                              </span>
                            </div>
                          </div> 

                          <div class="product-item-1 parent">
                              <div class="product-thumb child">
                                  <img src="images/grid-img7.jpg" alt="img">
                              </div> 
                              <div class="overlay-icon">
                                <span class="show-icon1">
                                <a href="javascript:void(0)"></a>
                                </span>
                                <span class="show-icon2">
                                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                                </span>
                              </div>
                          </div> 
                      </div> 


                  <div class="col-md-4">
                      <div class="product-holder">
                      <div class="product-item-2">
                      <div class="product-thumb">
                      <img src="images/grid-img9.jpg" alt="img">
                      </div> 
                      </div> 
                      <div class="product-item-2">
                      <div class="product-content white nonebdr nonepad">
                      <h2>Get Ready <br>for spring</h2>
                      <p class="tagline text-lowercase" align="center">It’s that time again, finally the sun is gradually showing its face and flowers are starting to bloom. Spring is just around the corner.
                      </p>
                      <div align="center"><button type="button" class="btn black">DISCOVER MORE</button></div>                                
                      </div>
                      </div> 
                      <div class="clearfix"></div>
                      </div> 
                  </div> 

                  <div class="col-md-4">
                      <div class="product-item-3 parent">
                          <div class="product-thumb child">
                            <img src="images/grid-img10.jpg" alt="img">
                          </div> 
                          <div class="overlay-icon">
                            <span class="show-icon1">
                            <a href="javascript:void(0)"></a>
                            </span>
                            <span class="show-icon2">
                            <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                            </span>
                          </div>
                      </div>

                      <div class="product-item-3 parent">
                          <div class="product-thumb child">
                            <img src="images/grid-img11.jpg" alt="img">
                          </div> 
                          <div class="overlay-icon">
                            <span class="show-icon1">
                            <a href="javascript:void(0)"></a>
                            </span>
                            <span class="show-icon2">
                            <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                            </span>
                          </div>
                      </div>
                  </div>
              </div> <!-- /.row -->

              <div class="row">
                  <div class="col-md-12 slider-title">
                      <div class="col-md-12"> 
                          <div class="h5">                      
                            <span class="slide-text-f">Hot Products</span>             
                              <div class="col-md-12 text-animated-row">
                                <ul class="">
                                    <li data-rel="0"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Beauty</span><span class="f2"><img src="images/flower8.png"></span></li>
                                    <li data-rel="1" class="deactive"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Beauty</span><span class="f2"><img src="images/flower8.png"></span></li>
                                    <li data-rel="2" class="deactive"><span class="f1"><img src="images/flower7.png"></span><span class="data-text">Makeup</span><span class="f2"><img src="images/flower8.png"></span></li>
                                </ul>
                              </div>
                          </div>
                      </div>
                  </div>    
               </div>  
            </div>
         </div>
      </div> 
    </div><!-- container end-->
</div><!-- box-wrapper end-->


<section class="slider clearfix">
   <div class="container">
     <div class="row">
       <div class="flexslider flexslider-related carousel clearfix">
         <ul class="slides">
            <li style="cursor: pointer;">
                <a href="product-detail-epilator.php">
				<div class="img-container">
					<img src="images/home-thumbs/1.jpg" />
				</div> <!--/img-container-->
                <div class="card-content">
                <h4>Braun Silkepil 7</h4>
                <h5>Skin Spa, removes shortest...</h5>
                <p>549 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
            </li>

            <li style="cursor: pointer;">
                <a href="product-detail-epilator.php">
				<div class="img-container">
					<img src="images/home-thumbs/2.jpg" />
				</div> <!--/img-container-->
                
                <div class="card-content">
                <h4>Braun Silkepil 7</h4>
                <h5>wet & dry, Body & Face... </h5>
                <p>499 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
            </li>

            <li style="cursor: pointer;">
                <a href="product-detail-epilator.php">
				<div class="img-container">
					<img src="images/home-thumbs/3.jpg" />
				</div> <!--/img-container-->
                
                <div class="card-content">
                <h4>Face Color</h4>
                <h5>head and cap, beauty pouch</h5>
                <p>399 AED</p>                
                </div>
                </a>
                <div class="overlay-icon">
                <span class="show-icon1">
                <a href="javascript:void(0)"></a>
                </span>
                <span class="show-icon2">
                <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                </span>
                </div>
            </li>

            <li style="cursor: pointer;">
              <a href="product-detail-epilator.php">
			  <div class="img-container">
					<img src="images/home-thumbs/4.jpg"/>
				</div> <!--/img-container-->
              
              <div class="card-content">
              <h4>Braun Epilator SE5541</h4>
              <h5>Legs and body, with shaver head, trimmer...</h5>
              <p>299 AED</p>                
              </div>
              </a>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>            
			  <a href="product-detail-hair-straightner.php">
				<div class="img-container">
					<img src="images/hair-straightners/h1.jpg" />
				</div> <!--/img-container-->
              <div class="card-content">
			  <h4>Braun Satin Hair 7 ST780 SensoCare Hair
Straightener with automatic temperature..</h4>
			  <h5>Braun H. Straightener  ES 4 /ST 780 Sensocare - Automatic temperature adaption, Nanoglide ceramic floating plates</h5>
              <!--<h4>SensoCare Hair Straightener</h4>
              <h5>Ceramic Straightener & Styler...</h5>-->
              <p>549 AED</p>                
              </div>
			  </a>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>
				<a href="product-detail-hair-straightner.php">
				<div class="img-container">
					<img src="images/hair-straightners/h6.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
			   <h4>Braun Satin Hair 3 ST310 Hair Straightener
with wide plates</h4>
               <h5>Braun H. Straightner ES1/ ST 310 Professional Ceramic Straightener - Cord</h5>
              <!--<h4>Hair Straightener With Wide Plates</h4>
              <h5>Curls,straightens,Waves and Flicks...</h5>-->
              <p>169 AED</p>                
              </div>
			  </a>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>
				<a href="product-detail-hair-straightner.php">
				<div class="img-container">
					<img src="images/hair-straightners/h2.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
			  <h4>Braun Satin Hair 7 ST750 Hair Straightener
with color saver and IONTEC technology</h4>
              <h5>Braun H. Straightener  ES 3 /ST 750 Moisturising, anti colour fading / dry out / damage Ceramic.LCD - Cord</h5>
              <!--<h4>Hair Straightener With Color Saver</h4>
              <h5>Ceramic Straightener & Styler...</h5>-->
              <p>429 AED</p>                
              </div>
			  </a>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>
				<a href="product-detail-hair-straightner.php">
				<div class="img-container">
					<img src="images/hair-straightners/h4.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
			  <h4>Braun Satin Hair 5 ST570 Hair Straightener
& Multistyler with IONTEC technology</h4> 
              <h5>Braun H.Straightener ST 570, 4 in 1 Multistyler with curl shaper for easy curling. Ceramic eloxal plates, multiple temperaturs settings
</h5>
              <!--<h4>Hair Straightener & Multistyler</h4>
              <h5>Curls,straightens,Waves and Flicks...</h5>-->
              <p>269 AED</p>                
              </div>
			  </a>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>				
				<div class="img-container">
					<img src="images/carousel-thumb9.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
              <!-- <div class="price-discount">50%</div> -->
              <h4>laura mercier</h4>
              <h5>Secret Brightening Powder Shade 1</h5>
              <p>105 AED <!-- <span class="over-through-price">1000 AED</span> --></p>                
              </div>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>
				<div class="img-container">
					<img src="images/carousel-thumb10.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
              <h4>by terry</h4>
              <h5>Terrybleu Mascara Terrybly</h5>
              <p>186 AED</p>                
              </div>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>

            <li>
				<div class="img-container">
					<img src="images/carousel-thumb11.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
              <h4>laura mercier</h4>
              <h5>Addiction Face Illuminator</h5>
              <p>202 AED</p>                
              </div>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>
            <li>
				<div class="img-container">
					<img src="images/carousel-thumb12.jpg" />
				</div> <!--/img-container-->
              
              <div class="card-content">
              <h4>illamasqua</h4>
              <h5>Midnight Antimatter Lipstick</h5>
              <p>120 AED</p>                
              </div>
              <div class="overlay-icon">
              <span class="show-icon1">
              <a href="javascript:void(0)"></a>
              </span>
              <span class="show-icon2">
              <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
              </span>
              </div>
            </li>
          </ul>
         </div>
      </div>
      <div align="center" class="row">
          <button type="button" class="btn black margin0">shop the Collection</button>
       </div>
   </div>
  <div class="content-section video-section clearfix">
    <div class="container">
      <div class="row">
          <div class="col-md-7 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0s" >
          <div class="product-item-1">
          <div class="product-thumb">
          <video width="715" height="407" class="img-responsive" poster="images/video-thumb.jpg" onclick="this.play();" controls loop>
          <source src="images/trailer.mp4" type="video/mp4">
          </video> 
          </div> 
          </div> 
          </div> 
          <div class="col-md-3 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.3s" >
          <div class="product-holder">
          <div class="product-item-2">
          <div class="product-thumb">
          <img src="images/grid-img12.jpg" alt="img">
          </div>                            
          </div>                  
          <div class="clearfix"></div>
          </div> 
          </div> 
          <div class="col-md-2 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.5s">
          <div class="product-item-3">
          <div class="product-thumb">
          <img src="images/grid-img13.jpg" alt="img">
          </div> 
          </div>
          <div class="product-item-3 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.7s">
          <div class="product-thumb">
          <img src="images/grid-img14.jpg" alt="img">
          </div> 
          </div>
          </div>
      </div> <!-- /.row end-->            
    </div> <!-- /.container end-->
   </div>
</section>

  <section class="sectionbg1">
      <div class="flower-icon1"><img src="images/flower3.png" align="img" class="img-responsive"></div>
      <div class="flower-icon2"><img src="images/flower2.png" align="img" class="img-responsive"></div>
      <div class="flower-icon3"><img src="images/flower1.png" align="img" class="img-responsive"></div>

      <div class="content-section">
          <div class="container">
              <div class="row">
                  <div class="col-md-12 section-title">
                      <h2>Vanity kart is every women's new online home<br> <span>of luxury fashion, beauty and lifestyle</span></h2>
                  </div>
                  <div class="col-md-12">
                      <div class="col-md-4">
                        <div align="center"><img src="images/icon3.png" alt="icons"></div>
                        <h3 class="h3-title">Free 2 hour delivery in Dubai</h3>
                        <div align="center" class="small">Orders may currently only be<br> delivered to addresses within..
                        </div>
                        </div>
                        <div class="col-md-4">

                        <div align="center"><img src="images/icon4.png" alt="icons"></div>
                        <h3 class="h3-title">custom sale alerts</h3>
                        <div align="center" class="small">At VanityKart we are committed to <br>offering you the best price possible.</div>

                        </div>
                        <div class="col-md-4">                    
                        <div align="center"><img src="images/icon5.png" alt="icons"></div>
                        <h3 class="h3-title">EXCLUSIVE REWARDS </h3>
                        <div align="center" class="small">Simply visit any Participating Store <br>and our staff will be happy to..
                        </div>             
                      </div>
                  </div>

              </div>

          </div>

      </div>
      <div class="clearfix"></div>
                <div align="center" class="row">
                <button type="button" class="btn btn-lg black margin0">become an exclusive member today</button>
                </div>
  </section>





<!-- beauty page on over show here-->

<!-- open/close -->
<div class="overlay-page overlay-contentscale clearfix">
      <div class="box-shadow-overlay right-box">
          <div id="carouselFade" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000">
              <div class="carousel-inner" role="listbox">          
                  <div class="item item4 active">  
                  </div> 
              </div>        
          </div>

            <div class="box-wrapper">
                  <div class="container">
                  <div class="row">
                  <div class="col-md-3 col-sm-6">
                  <div class="product-item-vote">
                  <div class="product-thumb">
                  <img src="images/grid-img23.jpg" alt="img">
                  </div> 
                  <div class="product-content">
                  <span class="tagline">herbal</span>                            
                  <h5><a href="#">
                  Mechanic Soap with <br>ctivated charcoal and..</a>
                  </h5>                    
                  </div>
                  </div> 
                  </div> 
                  <div class="col-md-3 col-sm-6">
                  <div class="product-item-vote">
                  <div class="product-thumb">
                  <img src="images/grid-img24.jpg" alt="img">
                  </div> 
                  <div class="product-content">
                  <span class="tagline">Makeup</span> 
                  <h5><a href="#">11 Eco-Friendly Beauty<br>Products Celebs Love..</a></h5>
                   
                  </div>
                  </div> 
                  <div class="product-item-vote">
                  <div class="product-thumb">
                  <img src="images/grid-img26.jpg" alt="img">
                  </div> 
                  <div class="product-content">
                  <span class="tagline">electronics</span>
                  <h5><a href="#">Here's a Fishtail Braid You<br>Can Actually Do</a></h5>
                    
                  </div>
                  </div> 
                  </div> 
                  <div class="col-md-3 col-sm-6">
                  <div class="product-item-vote">
                  <div class="product-thumb">
                  <img src="images/grid-img25.jpg" alt="img">
                  </div> 
                  <div class="product-content">
                  <span class="tagline">Hair care</span>
                  <h5><a href="#">Shay Mitchell's Braided<br>Top Knot Is Next Level</a></h5>
                  </div>
                  </div> 
                  <div class="product-item-vote">
                  <div class="product-thumb">
                  <img src="images/grid-img27.jpg" alt="img">
                  </div> 
                  <div class="product-content">
                  <span class="tagline">skin care</span>
                  <h5><a href="#">The Secret to Celeb Skin?<br>"Low-Dose, Baby Botox"</a></h5>
                  </div>
                  </div> 
                  </div> 
                  <div class="col-md-3 col-sm-6">
                  <div class="product-item-vote news-section">
                  <div class="product-content news-title">
                  <span class="news-title"><img src="images/flower5.png" alt="flowers"> Latest for you</span>                          
                  </div>

                  <div class="product-content">
                  <span class="tagline">News</span>
                  <h5><a href="#">Emma Roberts' Dotted Eyeliner Trick Only Takes 5 Seconds</a></h5>                            
                  </div>
                  <hr>
                  <div class="product-content">
                  <span class="tagline">Fitness</span>
                  <h5><a href="#">Jennifer Aniston, Miley Cyrus & More: See How Stars Stay Fit!</a></h5>                            
                  </div>
                  <hr>
                  <div class="product-content">
                  <span class="tagline">Health & wellness</span>
                  <h5><a href="#">Charlie Hunnam's Workout Secret: I Make Love As Often as I Can</a></h5>                            
                  </div>
                  <hr>
                  <div class="product-content">
                  <span class="tagline">fashion</span>
                  <h5><a href="news-article.php">3 Expert Ways to Wear Denim Shorts at a Festival</a></h5>                            
                  </div>

                  <hr>
                  <div class="product-thumb">
                  <img src="images/grid-img28.jpg" alt="img">
                  </div> 
                  </div> 
                  </div> 
                  </div>

                  <div class="row">
                  <div class="content-section margin-btm0">
                  <div class="container">
                  <div class="row">
                  <div class="col-md-4">
                  <div class="product-item-1 parent">
                  <div class="product-thumb child">
                  <img src="images/grid-img29.jpg" alt="img">
                  </div> 
                  <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                  </div>
                  </div> 

                  <div class="product-item-1 parent">
                  <div class="product-thumb child">
                  <img src="images/grid-img32.jpg" alt="img">
                  </div> 
                  <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                  </div>
                  </div> 
                  </div> 
                  <div class="col-md-4">
                  <div class="product-holder">
                  <div class="product-item-2">
                  <div class="product-thumb">
                  <img src="images/grid-img30.jpg" alt="img">
                  </div> 
                  </div> <!-- /.product-item-2 -->
                  <div class="product-item-2 bg-beachproof">
                  <div class="product-content nonebdr nonepad" align="center">
                  <h2><!-- beach Proof  --><br><!-- <img src="images/makeup.jpg" alt="img"> --></h2><br>
                  <div class="tagline text-lowercase" align="center" style="text-align: center;">
                  It's that time again, finally the sun is<br>
                  gradually showing its face and flowers are<br>
                  starting to bloom.
                  </div>
                  <div align="center"><button type="button" class="btn black">DISCOVER MORE</button></div>
                  </div>

                  </div> <!-- /.product-item-2 -->
                  <div class="clearfix"></div>
                  </div> <!-- /.product-holder -->
                  </div> <!-- /.col-md-5 -->
                  <div class="col-md-4">
                  <div class="product-item-3 parent">
                  <div class="product-thumb child">
                  <img src="images/grid-img31.jpg" alt="img">
                  </div> 
                  <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                  </div>
                  </div>
                  <div class="product-item-3 parent">
                  <div class="product-thumb child">
                  <img src="images/grid-img33.jpg" alt="img">
                  </div> 
                  <div class="overlay-icon">
                  <span class="show-icon1">
                  <a href="javascript:void(0)"></a>
                  </span>
                  <span class="show-icon2">
                  <a href="javascript:void(0)"><img src="images/icon2.png" alt="icons"></a>
                  </span>
                  </div>
                  </div>
                  </div>
                  </div> 
                  </div>

                  <div class="row">
                  <div class="container" align="center">
                  <div class="clearfix">
                  <ul>
                  <li>
                  <img src="images/gallery-3.jpg" class="img-responsive" />              
                  </li>            
                  </ul>
                  </div>        
                  </div>
                  <!--/.Carousel Wrapper-->
                  </div>
                  </div> <!-- /.container -->

                  <div class="content-section video-section clearfix ">
                  <div class="container">
                  <div class="row">
                  <div class="col-md-7 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0s" >
                  <div class="product-item-1">
                  <div class="product-thumb">
                  <video width="715" height="407" class="img-responsive" poster="images/video-thumb1.jpg" onclick="this.play();" controls loop>
                  <source src="images/trailer.mp4" type="video/mp4">
                  </video> 
                  </div> 
                  </div> 
                  </div> 
                  <div class="col-md-3 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.3s" >
                  <div class="product-holder">
                  <div class="product-item-2">
                  <div class="product-thumb">
                  <img src="images/grid-img34.jpg" alt="img">
                  </div>                            
                  </div> <!-- /.product-item-2 -->                 
                  <div class="clearfix"></div>
                  </div> <!-- /.product-holder -->
                  </div> <!-- /.col-md-5 -->
                  <div class="col-md-2 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div class="product-item-3">
                  <div class="product-thumb">
                  <img src="images/grid-img35.jpg" alt="img">
                  </div> 
                  </div>
                  <div class="product-item-3 wow fadeInDown animated" data-wow-duration="1s" data-wow-delay="0.7s">
                  <div class="product-thumb">
                  <img src="images/grid-img36.jpg" alt="img">
                  </div> 
                  </div>
                  </div>
                  </div> <!-- /.row -->            
                  </div> <!-- /.container -->
                  </div>
                  </div>
                  </div>
            </div>
        </div>
      <div class="clearfix"></div>
</div><!-- beauty page end here-->

<?php include 'footer.php';?>

<script type="text/javascript">
// var flagslide=0;
$(window).load(function(){
	$('.flexslider').flexslider({
		animation: "slide",
		animationLoop: true,
		itemWidth: 270,
		itemMargin: 30,
		pausePlay: false,
		start: function(slider){
		$('body').removeClass('loading');
		}
	});
});
</script>
