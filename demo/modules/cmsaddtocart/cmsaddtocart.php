<?php
if (!defined('_PS_VERSION_'))
  exit;

class CmsAddToCart extends Module 
{
	function __construct()
	{
		$this->name = 'cmsaddtocart';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
    	$this->author = 'uwtech';
    	$this->need_instance = 0;
    	$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    	$this->bootstrap = true;
 
    	parent::__construct();
 
    	$this->displayName = $this->l('Cms Add To Cart');
    	$this->description = $this->l('Module allows product on CMS page to be added to cart.');

	}

	public function install(){

		if(!parent::install() || !$this->registerHook('Header') || !$this->registerHook('actionAdminControllerSetMedia'))
		return false;
			return true;
	}

	public function uninstall(){

		return parent::uninstall();
	}

	public function hookHeader($param){

		$myController = $this->context->controller->php_self;
		if($myController=='cms')
		$this->context->controller->addJS($this->_path.'/js/'.$this->name.'.js');
	}

	public function hookActionAdminControllerSetMedia($param){

			$allval = Tools::getAllValues();
			if(array_key_exists('updatecms', $allval) || array_key_exists('addcms', $allval)){
			//$this->context->controller->addCSS($this->_path.'/css/'.$this->name.'.css');
			$this->context->controller->addJS($this->_path.'/js/'.$this->name.'_admin.js');
		}
	}
}