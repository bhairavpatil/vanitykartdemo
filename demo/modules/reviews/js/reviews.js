$(document).ready(function(){

	 //$("#ratinginput").rating();

$("#submitReviews").click(function(e){
	 //e.preventDefault();

	 $('#title_error_msg').hide();
	 $('#comment_error_msg').hide();
	 $('#rating_error_msg').hide();
	 var errorFlag = 0;

	 //if page is cmspage
	 if(typeof(cmsid) != 'undefined'){
	 	var id = cmsid;
	 	var rating = 0;
	 }
	 else if(typeof(productid) != 'undefined'){ //if page is productpage
	 	var id = productid;
	 	var rating = $('.caption .label').text();

	 	if($("#newcommenttitle").val() == '' || typeof($("#newcommenttitle").val()) == 'undefined'){ 
			$('#title_error_msg').show();
			errorFlag = 1;
		}
	 	if(rating == '' || typeof(rating) == 'undefined' || rating == 0){ 
			$('#rating_error_msg').show();
			errorFlag = 1;
		}

	 }
	 	
	
	if($("#newcommentbox").val() == '' || typeof($("#newcommentbox").val()) == 'undefined'){ 
		$('#comment_error_msg').show();
		errorFlag = 1;
	}
	 		


	if(!errorFlag){
			$.ajax({
					url: reviews_controller_url + '?action=add_comment&pagetype=' +pagetype + '&id=' +id + '&rating=' + rating,
					//url: '/prestashop/module/reviews/addreviews?action=add_comment',
					data: $('#review_form').serialize(),
					type: 'POST',
					headers: { "cache-control": "no-cache" },
					dataType: "json",
					success: function(data){
						$('#new_comment_form_error ul').hide();
						$('#succsess_msg').hide();
						if (data.result)
						{	
							//Blank comment box after succesful comment submission				
							$('#newcommentbox').val('');
							$('#newcommenttitle').val('');
							//Display Success message to user
							$('#succsess_msg').slideDown('slow').show();
						}
						else
						{
							//Display all the errors while submitting the comment	
							$('#new_comment_form_error ul').html('');
							$.each(data.errors, function(index, value) {
								$('#new_comment_form_error ul').append('<li>'+value+'</li>');
							});
							//Display Error div
							$('#new_comment_form_error ul').show();
							$('#new_comment_form_error').slideDown('slow');
						}
					}
				});

	}

});

//Action handling for submit the comments for loggedin and non-loggedin user 
$('#comment_btn').click(function(){
	if(parseInt(loggedin)){
		//Display Comment form for loggedin user		
		$("#review_form").show();
		
		$parent_box = $(this).closest('.box-comment');
		$parent_box.siblings().find('.bottom-comments').slideUp();
		$parent_box.find('.bottom-comments').slideToggle(500, 'swing');
	}
	else{
		//Redirect user to same page where he wants to submit the comment		
		window.location.href="http://vanitykart.com/demo/login?back="+window.location.href;
	}
});


// Load More


$('ul#myList li:nth-child(1) .row').addClass('topbdr');
$('ul#myList li:even .row').addClass('bg-odd');
$('ul#myList li:odd .row').addClass('bg-even');



/* size_li = $("#myList li").size();
    x=4;
    $('#myList li:lt('+x+')').show();
	
    $('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('#myList li:lt('+x+')').show();
         $('.row-loadless').show();
        if(x == size_li){
            $('.row-loadmore').hide();
        }
    });
	
    $('#showLess').click(function () {
        x=(x-5<0) ? 3 : x-5;
        $('#myList li').not(':lt('+x+')').hide();
        $('.row-loadmore').show();
         $('.row-loadless').show();
        if(x == 3){
            $('.row-loadless').hide();
        }
    }); */
	
	
 size_li = $("#myList li").size();
    x=4;
	
	if(x == 4){
		$('.row-loadmore').hide();
	}
	else{
		$('.row-loadmore').show();
	}
	
    $('#myList li:lt('+x+')').show();
    $('#loadMore').click(function () {
        x= (x+4 <= size_li) ? x+4 : size_li;
        $('#myList li:lt('+x+')').show();
         //$('.row-loadless').show();
        if(x == size_li){
            $('.row-loadmore').hide();
        }
    });
    /* $('#showLess').click(function () {
        x=(x-4<0) ? 4 : x-4;
        $('#myList li').not(':lt('+x+')').hide();
        $('.row-loadmore').show();
         $('.row-loadless').show();
        if(x == 4){
            $('.row-loadless').hide();
        }
    }); */
	
	$('.review_close img').click(function(){
		$('#review_form').hide();
	});
	
	$('.no_reviews').prependTo('.if_no_review');




});