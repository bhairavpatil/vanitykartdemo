<?php


include_once(dirname(__FILE__).'/../../reviews.php');
class reviewsDefaultModuleFrontController extends ModuleFrontController
{
	//public $auth = true;
	public function initContent()
	{
		
	}

	public function postProcess()
	{			
		$module_instance = new Reviews();
		$result = true;
		$errors = array();
		if (Tools::isSubmit('action'))
		{
			//Submit comments from cms page for loggedin user only
			if(Tools::getValue('action') == 'add_comment' && $this->context->customer->id)
			{
						$new_comment['from'] = pSQL($this->context->customer->firstname . ' '. $this->context->customer->lastname);
						$new_comment['page_type'] = Tools::getValue('pagetype');	
						$new_comment['id_page'] = (int)Tools::getValue('id');	
						$new_comment['user_id'] = $this->context->customer->id;	
						$new_comment['title'] = strip_tags(Tools::getValue('title'));
						$new_comment['comment'] = strip_tags(Tools::getValue('comment'));
						$new_comment['rating'] = strip_tags(Tools::getValue('rating'));
						$new_comment['date'] = date("Y-m-d H:i:s");      

						if (!$new_comment['comment'] || !Validate::isMessage($new_comment['comment']))
							$errors[] = $module_instance->l('Comment is incorrect', 'default');

						//Check if comment is already given by user
						$comment_exist = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'user_reviews where id_page ='.(int)Tools::getValue('id').' AND page_type="'.Tools::getValue('pagetype').'" AND user_id = '.$this->context->customer->id);

						if(count($comment_exist) > 0)
							$errors[] = $module_instance->l('Comment is already given by you.', 'default');

						if(!count($errors))
						{
							if(!Db::getInstance()->insert($module_instance->table_name,$new_comment)){
								$errors[] = $module_instance->l('Error while adding to the database'. ': ' . $mysql_error(), 'default' );
								$result = false;
							}
						}
						else
						{
							$result = false;
						}
						
			}
			else{
				$errors[] = $module_instance->l('Please Login to enter a comment', 'default');
				$result = false;
			}

			die(Tools::jsonEncode(array(
							'result' => $result,
							'errors' => $errors
						)));	
		}
	}
	
}