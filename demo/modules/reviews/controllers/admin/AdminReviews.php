<?php


require_once(dirname(__FILE__).'/../../classes/ReviewsClass.php');

class AdminReviewsController extends ModuleAdminController
{
	//public $auth = true;

	public function __construct()
	{
		$this->table = 'user_reviews';
		$this->module = 'reviews';
		$this->className = 'ReviewsClass';
		$this->lang = false;
		$this->bootstrap = true;
		$this->identifier = 'id_reviews';
		$this->context = Context::getContext(); 

		$this->fields_list = array(
			'id_reviews' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs'
				),
			'title' => array(
				'title' => $this->l('Title')				
				),
			'comment' => array(
				'title' =>  $this->l('Comment')			
				),
			'from' => array(
				'title' =>  $this->l('From')			
				),
			'page_type' => array(
				'title' =>  $this->l('Page Type')			
				),
			'rating' => array(
				'title' =>  $this->l('Rating')			
				),
			'approved' => array(
				'title' =>  $this->l('Approved'),
				'active' => 'toggle',
				'type' => 'bool'			
				)
			);

		$this->bulk_action = array(
			'delete' => array(
				'text' => $this->l('Delete Selected'),
				'icon' => 'icon-trash',
				'confirm' => $this->l('Delete Selected Items?')
				)
			);

		parent::__construct();
	}

	public function renderList()
	{

		if(isset($this->_filter) && trim($this->_filter) == '')
			$this->_filter = $this->original_filter;

		//$this->addRowAction('edit');		
		$this->addRowAction('delete');
		
		return parent::renderList();
	}

	//Hide Add New Reviews button from admin Toolbar
	public function initToolbar() {
	    parent::initToolbar();
	    unset($this->toolbar_btn['new']);
	}

	public function postProcess()
	{
		if (Tools::isSubmit('toggleuser_reviews'))
		{
			$id_reviews = Tools::getValue('id_reviews');
			if($this->module->toggleReviews($id_reviews))
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminReviews').'&conf=4');
			else
				$this->errors[] = $this->module->errors;

			
		}
		return parent::postProcess();
	}

	public function renderForm()
	{

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('PageID'),
				'name' => 'id_page',
			);

		$inputs[] =  array(
                    'type' => 'radio',
                    'label' => 'Page Type',
                    'name' => 'page_type',
                    'values' => array(
                        array(
                            'id' => 'type_cms',
                            'value' => 'cms',
                            'label' => 'CMS'
                        ),
                        array(
                            'id' => 'type_product',
                            'value' => 'product',
                            'label' => 'Product'
                        )
                    )
                );

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Title'),
				'name' => 'title',
			);

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('From'),
				'name' => 'from',
			);

		$inputs[] = array(
				'type' => 'textarea',
				'label' => $this->l('Comment'),
				'name' => 'comment',
				'desc' => $this->l('Whats in your mind'),
			);

		$inputs[] = array(
				'type' => 'datetime',
				'label' => $this->l('Date'),
				'name' => 'date',
			);

		/*$inputs[] = array(
				'type' => 'hidden',				
				'name' => 'approved',
			);*/

		$this->fields_form = array(				
				'legend' => array(
					'title' => Tools::isSubmit('updateuser_reviews') ?  $this->l('Edit Reviews') : $this->l('Add New Reviews'),
					'icon' => 'icon-cogs'
					),
				'input' => $inputs,
				'submit' => array(
					'title' => Tools::isSubmit('updateuser_reviews') ?  $this->l('Update') : $this->l('Add'),
					'class' => 'btn btn-default pull-right'
					)							
			);	

		return parent::renderForm();

	}

	
}