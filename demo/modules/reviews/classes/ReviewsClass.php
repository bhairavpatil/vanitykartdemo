<?php

class ReviewsClass extends ObjectModel
{
	public $id_reviews;
	public $id_page;
	public $user_id;
	public $page_type;
	public $title;
	public $from;
	public $comment;
	public $approved;
	public $rating;
	public $date;

	public static $definition = array(
			'table' => 'user_reviews',
			'primary' => 'id_reviews',
			'fields' => array(
					'id_page' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt', 'required' => true), 
					'user_id' => array('type' => self::TYPE_INT), 
					'page_type' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true), 
					'title' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true), 
					'from' => array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true), 
					'comment' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true), 
					'approved' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
					'rating' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'), 
					'date' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat', 'required' => true), 
				)
		);
}