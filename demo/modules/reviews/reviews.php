<?php

if(!defined('_PS_VERSION_'))
	exit;

class Reviews extends Module
{

	private $html = '';
	private $errors = '';

	public function __construct()
	{
		$this->name = 'reviews';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->ps_versions_compliancy = array('min' => '1.6' , 'max' => _PS_VERSION_);
		
		$this->bootstrap = true;
		$this->need_instance = 0;

		//$this->controllers = array('reviews','addreviews');

		$this->displayName = $this->l('Customer Reviews and Comments');
		$this->description = $this->l('User can give reviews and comments on product and cms page. and admin can approve the same in back end');

		$this->table_name = 'user_reviews';

		parent::__construct();

	}

	public function install()
	{
		if(!parent::install() OR
			!$this->createTabs() OR
			!$this->installMyTable() OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('reviewsOnCmsPage') OR
			!$this->registerHook('productTabContent') OR
			!$this->registerHook('reviewBlock')
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if(!parent::uninstall() OR
			!$this->eraseTabs() OR
			!$this->removeMyTable()
			)
			return false;
		return true;	
	}

	private function createTabs()
	{
		$tab = new Tab();
		$tab->active = 1;
		$languages = Language::getLanguages(false);
		if(is_array($languages))
		{
			foreach ($languages as $language) {
				$tab->name[$language['id_lang']] = $this->l('Reviews');
			}
		}
		$tab->class_name = 'Admin'.ucfirst($this->name);
		$tab->module = $this->name;
		$tab->id_parent = 142;

		return (bool)$tab->add();
	}

	private function eraseTabs()
	{
		$id_tab = (int)Tab::getIdFromClassName('Admin'.ucfirst($this->name));
		if($id_tab){
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return true;
	}

	private function installMyTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.'user_reviews` (
				`id_reviews` INT(12) NOT NULL AUTO_INCREMENT,
				`id_page` INT(12) NOT NULL,
				`user_id` INT(12) NOT NULL,
				`page_type` VARCHAR(64) NOT NULL,
				`title` VARCHAR(64) NOT NULL,
				`from` VARCHAR(64) NOT NULL,
				`comment` TEXT NOT NULL,
				`approved` TINYINT NOT NULL,
				`rating` TINYINT NOT NULL,
				`date` datetime NOT NULL,
				PRIMARY KEY (`id_reviews`)
				) ENGINE = ' . _MYSQL_ENGINE_;

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;		
	}

	private function removeMyTable()
	{
		$sql = 'DROP TABLE '._DB_PREFIX_.$this->table_name;

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;
	}

	public function getContent()
	{
		$this->postProcess();
		$this->displayForm();
		return $this->html;
	}

	public function postProcess()
	{			
		if(Tools::isSubmit('submitNewReviews'))
		{
			$reviews['id_page'] = Tools::getValue('id_page');
			$reviews['page_type'] = Tools::getValue('page_type');
			$reviews['title'] = Tools::getValue('title');
			$reviews['from'] = Tools::getValue('from');
			$reviews['comment'] = Tools::getValue('comment');			
			$reviews['date'] = Tools::getValue('date');

			foreach ($reviews as $key => $value)
			{
				if(empty($value) || !Validate::isGenericName($value))
					$this->errors[] = Tools::displayError('Invalid Field'). ': ' . $key;
			}

			if(!$this->errors)
			{
				if(Tools::getValue('id_reviews'))
				{
					if(!Db::getInstance()->update($this->table_name,$reviews, 'id_reviews = '. (int)Tools::getValue('id_reviews')))			
						$this->errors[] = Tools::displayError('Error while updating the database'). ': ' . $mysql_error();		
				} else {
					if(!Db::getInstance()->insert($this->table_name,$reviews))
						$this->errors[] = Tools::displayError('Error while adding to the database'). ': ' . $mysql_error();
				}
				
			}

			$confirmation = Tools::getValue('id_reviews') ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');
				
			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else{
				$this->html .= $this->displayConfirmation($confirmation);	
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name);
			}
		}
		else if (Tools::isSubmit('deleteuser_reviews'))
		{			
			if(!Db::getInstance()->delete($this->table_name,'id_reviews = '. (int)Tools::getValue('id_reviews')))
				$this->errors[] = Tools::displayError('Error while deleting from the database'). ': ' . $mysql_error();

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($this->l('Entry Successfully Removed'));	
		}
		else if (Tools::isSubmit('toggleuser_reviews'))
		{
			$id_reviews = Tools::getValue('id_reviews');
			if($this->toggleReviews($id_reviews))
				$this->html .= $this->displayConfirmation($this->l('Entry Status Changed'));
			else
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));		
			
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminReviews').'&conf=4');
		}
		
	}

	public function toggleReviews($id_reviews)
	{		
		if(Db::getInstance()->getValue('SELECT approved FROM '._DB_PREFIX_.$this->table_name.' where id_reviews = '.(int)$id_reviews))
		{ 
			//Disable it
			if(!Db::getInstance()->update($this->table_name, array('approved' => 0) , 'id_reviews = '.(int)$id_reviews))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;

		}
		else //Enable it
		{
			if(!Db::getInstance()->update($this->table_name, array('approved' => 1) , 'id_reviews = '.(int)$id_reviews))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;
		}
	}


	public function generateReviewList()
	{
		$content = $this->getAll();

		$fields_list = array(
			'id_reviews' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs'
				),
			'title' => array(
				'title' => $this->l('Title')				
				),
			'comment' => array(
				'title' =>  $this->l('Comment')			
				),
			'from' => array(
				'title' =>  $this->l('From')			
				),
			'page_type' => array(
				'title' =>  $this->l('Page Type')			
				),
			'rating' => array(
				'title' =>  $this->l('Rating')			
				),
			'approved' => array(
				'title' =>  $this->l('Approved'),
				'active' => 'toggle',
				'type' => 'bool'			
				)
			);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->actions = array('delete');
		//$helper->show_toolbar = true;
		//$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();		
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'id_reviews';
		$helper->title = $this->l('List of Reviews');
		$helper->table = 'user_reviews';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;

		return $helper->generateList($content,$fields_list);
	}

	public function initToolbar()
	{
		$getToken = Tools::getAdminTokenLite('AdminModules');

		$this->toolbar_btn['new'] = array(
				'href' => $this->context->link->getAdminLink('AdminModules',false).'&token='.$getToken.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&addNewReviews',
				'desc' => $this->l('Add New')
			);

		return $this->toolbar_btn;
	}

	//Fetch All reviews from the database
	public function getAll()
	{
		return Db::getInstance()->ExecuteS('
				SELECT * 
				FROM '._DB_PREFIX_.$this->table_name);
	}

	//Display add, edit and listview on the basis of condition
	public function displayForm()
	{
		//ddd(Tools::getAllValues());
		if(Tools::isSubmit('updateuser_reviews') || (Tools::getValue('id_reviews') && !Tools::isSubmit('deleteuser_reviews')))
			$this->html .= $this->generateForm(true);
		else if (Tools::isSubmit('addNewReviews') || Tools::isSubmit('submitNewReviews')) 		
			$this->html .= $this->generateForm();
		else {
			$this->html .= $this->generateReviewList();			
		}
				
	}

	//Create Form for add and edit mode
	public function generateForm($editing = false)
	{
		if($editing)
		{
			$inputs[] =  array(
					'type' => 'hidden',
					'name' => 'id_reviews'
				);
		}

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('PageID'),
				'name' => 'id_page',
			);

		$inputs[] =  array(
                    'type' => 'radio',
                    'label' => 'Page Type',
                    'name' => 'page_type',
                    'values' => array(
                        array(
                            'id' => 'type_cms',
                            'value' => 'cms',
                            'label' => 'CMS'
                        ),
                        array(
                            'id' => 'type_product',
                            'value' => 'product',
                            'label' => 'Product'
                        )
                    )
                );

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Title'),
				'name' => 'title',
			);

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('From'),
				'name' => 'from',
			);

		$inputs[] = array(
				'type' => 'textarea',
				'label' => $this->l('Comment'),
				'name' => 'comment',
				'desc' => $this->l('Whats in your mind'),
			);

		$inputs[] = array(
				'type' => 'datetime',
				'label' => $this->l('Date'),
				'name' => 'date',
			);


		$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $editing ?  $this->l('Edit Reviews') : $this->l('Add New Reviews'),
						'icon' => 'icon-cogs'
						),
					'input' => $inputs,
					'submit' => array(
						'title' => $editing ?  $this->l('Update') : $this->l('Add'),
						'class' => 'btn btn-default pull-right'
						)
					)			
			);

		if(!$editing)
		{
			foreach($inputs as $input)
				$values[$input['name']] = '';
		}
		else
		{
			//ddd(Tools::getValue('id_reviews'));
			$values = $this->getSingle(Tools::getValue('id_reviews'));
		}

		

		$helper = new HelperForm();
		$helper->submit_action = 'submitNewReviews';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $values
			);

		return $helper->generateForm(array($fields_form));

	} 

	//Fetch details to be display in edit mode
	public function getSingle($id_reviews)
	{
		return Db::getInstance()->getRow('
			SELECT *
			FROM '._DB_PREFIX_.'user_reviews 
			where id_reviews ='.(int)$id_reviews
			 );
	}

	//Display comments on cms page
	public function hookReviewsOnCmsPage($params)
	{

		$pagetype = $this->context->controller->php_self;
		$id = $this->context->controller->cms->id;

		$reviews = $this->getAllReviews($pagetype,$id);
		$count = count($reviews);		
		if($reviews)
		{
			$this->context->smarty->assign(array(
				'reviews' => $reviews				
			));
		}		
		
		//send these parameter to js file for comments adding process using controller
		$this->context->smarty->assign(array(           
            'reviews_controller_url' => $this->context->link->getModuleLink('reviews'),           
            'pagetype' => $pagetype,           
            'cmsid' => $id, 
            'count' => $count,
            'loggedin' => $this->context->customer->logged          
       ));

		return $this->display(__FILE__,'CmsCustomerReview.tpl');
	}

	public function hookProductTabContent($params)
	{
		$pagetype = $this->context->controller->php_self;		
		$id = $this->context->controller->getProduct()->id;

		$reviews = $this->getAllReviews($pagetype,$id);	
		$count = count($reviews);	
		if($reviews)
		{
			$this->context->smarty->assign(array(
				'reviews' => $reviews
			));
		}

		//avg rating
		$r2 = 0;
		foreach ($reviews as $key => $value) {
			$r1 = $value['rating'];
			$r2 = $r2 + $r1;		
		}
		
		
		$rating_avg = $count > 0 ? number_format($r2/$count,1) : 0;
		$rating_avg_star = $count > 0 ? number_format($r2/$count,0) : 0;
		

		//send these parameter to js file for comments adding process using controller
		$this->context->smarty->assign(array(           
            'reviews_controller_url' => $this->context->link->getModuleLink('reviews'),           
            'pagetype' => $pagetype,           
            'productid' => $id, 
            'count' => $count,
            'rating_avg' => $rating_avg,
            'rating_avg_star' => $rating_avg_star,
            'loggedin' => $this->context->customer->logged          
       ));

		//return $this->display(__FILE__,'ProductCustomerReview.tpl');
	}

	public function getAllReviews($pagetype,$id)
	{		
		return Db::getInstance()->ExecuteS('
			SELECT *
			FROM '._DB_PREFIX_.'user_reviews 
			where id_page ='.(int)$id.' 
			AND page_type="'.$pagetype.'"
			AND approved = 1');
		
	}

	public function hookDisplayHeader()
	{
		if($this->context->controller->php_self == 'cms' || $this->context->controller->php_self == 'product'){
			$this->context->controller->addJS($this->_path.'js/reviews.js');
			$this->context->controller->addCSS($this->_path.'views/css/reviews.css');				
		}

		if($this->context->controller->php_self == 'product'){
			$this->context->controller->addJS($this->_path.'js/star-rating.js');
			$this->context->controller->addCSS($this->_path.'views/css/bootstrap.min.css');
		}
	}

	public function HookReviewBlock()
	{

		return $this->display(__FILE__,'ProductCustomerReview.tpl');

	}

}