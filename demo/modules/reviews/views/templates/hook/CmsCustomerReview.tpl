<script type="text/javascript">
var reviews_controller_url = '{$reviews_controller_url}';
var pagetype = '{$pagetype}';
var cmsid = '{$cmsid}';
var loggedin = '{$loggedin}';

</script>


<!-- User Comment Section Start -->

<div class="row group-container2 enter_cmt-sec clearfix">
	<div class="col-md-1"></div> <!--/col-md-1-->
<div class="box-comment clearfix">
	<div class="top">
		<div class="row">
			<div class="col-md-3">          
				<div class="enter-cmt-txt" id="comment_btn"><img src="{$img_dir}comment.png" alt="{l s='Comment'}"/> Enter Your Comment <span>{if ($count > 0)} ({$count}) {/if}</span> </div> <!--/enter-cmt-txt-->
			</div> <!--/col-md-3-->
			<div class="col-md-4 socialwidcmt pull-left">
				{hook h="displayRightColumnProduct"}
			</div> <!--/col-md-4-->
		</div> <!--/row-->
	</div> <!--/top-->

	<div class="bottom-comments">
		<div class="row">
			<div class="col-md-12" align="center">
				
				<form id="review_form" class="comment_form">
					<div class="commentsSec">
						<img class="cmt-flower cmt-flower-left" src="{$img_dir}left-flower.png" alt="{l s='Comment'}"/>
						{l s="Comments" mod="reviews"} <span>{if ($count > 0)} ({$count}) {/if}</span>
						<img class="cmt-flower cmt-flower-right" src="{$img_dir}right-flower.png" alt="{l s='Comment'}"/>
					</div> <!--/commentsSec-->
					
					<div class="user_input_comment_sec">
						<div class="user_cmt_box">						
							<input type="text" name="comment" id="newcommentbox" placeholder="Enter your comment" />

							<div class="user_cmt_indi_msg">
								<div id="new_comment_form_error" class="error error_msg exiting_cmt" style="display:none;">
									<ul></ul>
								</div>
								<div id="comment_error_msg" class="error error_msg" style="display:none;">
									{l s="Please Enter Comment" mod="reviews"}
								</div>
								<div id="succsess_msg" class="success success_msg" style="display:none;">
									{l s="Comment Successfully Submitted, will be visible after approval" mod="reviews"}
								</div>
							</div> <!--/user_cmt_indi_msg-->
						</div> <!--/user_cmt_box-->

						<button type="button" name="submitCmsReviews" id="submitReviews" class="btn black">
							{l s="Submit A Comment" mod="reviews"}
						</button>						
						<div class="clearfix"></div>
					</div> <!--/user_input_comment_sec-->
				</form>

				



				{if isset($reviews)}
				<div class="user_comments_sec">
					<ul>
						{foreach from=$reviews item=result}
	                    <li>
	                        <div class="row">
	                            <div class="col-xs-2 col-md-2 user_img_sec">
	                                <img src="{$img_dir}user-img.png" alt="{l s='User Image'}"/>
	                            </div>  <!--/col-md-2-->
	                            <div class="col-xs-10 col-md-10 user_cmt_sec">
	                                <div class="user_name">
	                                	{$result.from}
	                                	<span class="user_cmt_date">{$result.date|date_format:"%B %e %Y, %I:%M %p"}</span> 
	                                	<!-- <span class="user_cmt_ime">1:57 AM</span> --> 
	                                </div> <!--/user_name-->
	                               	<div class="user_cmt">
	                               		{$result.comment}
	                               	</div> <!--/user_cmt-->
	                               	<div class="user_cmt_more">
	                               		<a href="#">More</a>
	                               	</div> <!--/user_cmt_more-->
	                            </div> <!--/col-md-11-->
	                        </div> <!--/row-->
	                    </li>
	                    {/foreach}
                	</ul>
				</div> <!--/user_comments_sec-->
				{/if}
			</div> <!--/col-md-12-->
		</div> <!--/row-->
	</div> <!--/bottom-comments-->
</div> <!--/box-comment-->
</div> <!--/group-container2-->

<!-- User Comment Section End -->
























<!-- <div><span id="comment_btn"><a href="javascript:void(0);">Enter Your Comment {if ($count > 0)} ({$count}) {/if}</a></span></div>
<div id="new_comment_form_error" class="error" style="display:none;padding:15px 25px">
	<ul></ul>
</div>
<div id="comment_error_msg" class="error" style="display:none;padding:15px 25px">
	{l s="Please Enter Comment" mod="reviews"}
</div>
<div id="succsess_msg" class="success" style="display:none;padding:15px 25px">
	{l s="Comment Successfully Submitted, will be visible after approval" mod="reviews"}
</div>
<form id="review_form" class="default-form form-horizontal box" style="display: none;">
	<p class="page-subheading">
		{l s="Comments" mod="reviews"} {if ($count > 0)} ({$count}) {/if}
	</p>
	<p>&nbsp;</p>

	<div class="form-group">
		
		<div class="col-xs-6">
		<input type="text" name="comment" id="newcommentbox" > 
			<textarea name="comment" id="newcommentbox" rows="10" class="form-control"></textarea> 
		</div>
	</div>

	<div class="form-group">
		<div class="col-xs-6 col-sm-offset-3">
			<button type="button" name="submitCmsReviews" id="submitReviews" class="btn btn-default button button-medium">
				<span>{l s="Submit A Comment" mod="reviews"}<i></i></span>

			</button>
		</div>
	</div>

</form>



{if isset($reviews)}
<div id="reviewsblock">
<p class="title_block">{l s="Reviews" mod="reviews"}</p>
<div class="block_content">
	{foreach from=$reviews item=result}
	<div style="border:1px solid red;">
	<p class="review-title">{$result.title}</p>
	<p class="review-comment">{$result.comment}</p>
	<p class="review-from">{$result.from}</p>
	<p class="review-date">{$result.date|date_format:"%B %e %Y, %I:%M %p"}</p>
	</div>
	{/foreach}
</div>
</div>
{/if} -->

<script>
{literal}

// Comment Box
/*$('.enter-cmt-txt').on('click', function() {
	$parent_box = $(this).closest('.box-comment');
	$parent_box.siblings().find('.bottom-comments').slideUp();
	$parent_box.find('.bottom-comments').slideToggle(500, 'swing');
});*/

{/literal}
</script>