
<script type="text/javascript">
var reviews_controller_url = '{$reviews_controller_url}';
var pagetype = '{$pagetype}';
var productid = '{$productid}';
var loggedin = '{$loggedin}';

</script>

<div class="cust-review-section">

	<div class="slider_title">
		<span class="slide_text">
			<img src="{$img_dir}flower5.png" alt="flowers">
				{l s="customer reviews" mod="customerreviews"}
			<img src="{$img_dir}flower5.png" alt="flowers">
		</span>
	</div> <!--/slider_title-->

	<div align="left" class="row rank">

		<div class="col-md-8 col-sm-8">
		<span class="if_no_review"></span>
		
			{if ($count > 0)}
			<div class="cust_reviews">

			{section name="foo" start=0 loop=5 step=1}
					{if $rating_avg_star le $smarty.section.foo.index}
						<span><i class="icon-star empty"></i></span>
					{else}	
						<span><i class="icon-star"></i></span>
					{/if}
			{/section}

		      <span class="star-rating star-rating-left"><b>{$rating_avg}</b> {l s="Rating" mod="reviews"} </span>
		      <span class="star-divider">|</span>
		      <span class="star-rating"><b>{$count}</b> {l s="Reviews" mod="reviews"} </span>
		    </div> <!--/cust_reviews-->
		    {/if}
		
		</div> <!--/col-md-8-->

	    <div class="col-md-4 col-sm-4">
	    	<span class="leave-review" id="comment_btn"><a href="javascript:void(0);"> leave a review </a></span>
	    </div> <!--/col-md-4-->
	    

	    <div class="clearfix"></div>
	</div> <!--/rank-->

	<!-- Comment Review Section Start -->
	<form id="review_form" class="form-horizontal review_form" style="display: none;">
		<div class="review_title">
			{l s="Share your Experience" mod="reviews"} <!-- {if ($count > 0)} ({$count}) {/if} -->
			<span class="review_close"><img src="{$img_dir}review_close.png" alt="{l s='Close'}"/></span>
		</div> <!--/review_title-->

		<div class="review_form_sec">

			<div class="form-group">
			    <label class="col-sm-4 control-label review_form_title">Rate this product</label>
			    <div class="col-sm-8 review_form_ip">
			    	<input id="ratinginput" value="0" type="text" class="form-control rating" />
			    	<div id="rating_error_msg" class="error error_msg rating_error_msg" style="display:none;">
						{l s="Please select Rating" mod="reviews"}
					</div>
			    </div>
			</div> <!--/form-group-->
			<div class="form-group">
			    <label class="col-sm-4 control-label review_form_title">Review Title</label>
			    <div class="col-sm-8 review_form_ip">
			     	<input type="text" name="title" id="newcommenttitle" class="form-control"/>
			     	<div id="title_error_msg" class="error error_msg" style="display:none;">
						{l s="Please enter a Review Title" mod="reviews"}
					</div>
			    </div>
			</div> <!--/form-group-->
			<div class="form-group">
			    <label class="col-sm-4 control-label review_form_title">Description</label>
			    <div class="col-sm-8 review_form_ip">
			    	<textarea name="comment" id="newcommentbox" rows="5" class="form-control"></textarea>
					<div id="comment_error_msg" class="error error_msg" style="display:none;">
						{l s="Please enter a Review Comment" mod="reviews"}
					</div>	
			    </div>
			</div> <!--/form-group-->

			<div class="form-group">
			    <div class="col-sm-8 col-sm-offset-4 review_form_btn">
			      	<button type="button" name="submitCmsReviews" id="submitReviews" class="btn black">
						{l s="Submit" mod="reviews"}
					</button>	
					<div id="succsess_msg" class="success success_msg" style="display:none;">
						{l s="Comment Successfully Submitted, will be visible after approval" mod="reviews"}
					</div>
					<div id="new_comment_form_error" class="error error_msg exiting_cmt" style="display:none;">
						<ul></ul>
					</div>
			    </div>
			</div> <!--/form-group-->

		</div> <!--/review_form_sec--> 
	</form>
	<!-- Comment Review Section End -->
	

	<!-- User Review List Section Start -->
	
	<div class="load-review">
		{if isset($reviews)}
		<ul id="myList">
			{foreach from=$reviews item=result}
			<li>
		        <div class="row">
			        <div class="rank2">
			        	{section name="foo" start=0 loop=5 step=1}
							{if $result.rating le $smarty.section.foo.index}
								<i class="icon-star empty"></i>
							{else}	
								<i class="icon-star"></i>
							{/if}
						{/section}
			            <span class="review-date">{$result.date|date_format:"%e %B %Y"}</span>
			        </div> <!--/rank2-->
		        	<div class="review-header">Reviewed By {$result.from}</div> <!--/review-header-->
		        	<div class="review-content">{$result.title}</div> <!--/review-content-->
		        	<div class="review-content">{$result.comment}</div> <!--/review-content-->      
		        </div> <!--/row-->
		    </li>
			{/foreach}
	    </ul>
	    {else if}
		<span class="no_reviews">{l s="No reviews" mod="reviews"}</span>
	    {/if}
	</div> <!--/load-review-->
	
	<div class="row row-loadmore">
		<div align="center">
		   <div id="loadMore" class="loadmore">Load more </div>
		</div>        
	</div> <!--/row-loadmore-->

	<div class="row row-loadless">
		<div align="center">
		   <div id="showLess" class="loadmore">Load less </div>
		</div>        
	</div> <!--/row-loadmore-->

	<!-- User Review List Section End -->

</div> <!--/cust-review-section-->


 <!-- {if isset($reviews)}
<div id="reviewsblock">
	{foreach from=$reviews item=result}
		{section name="foo" start=0 loop=5 step=1}
			{if $result.rating le $smarty.section.foo.index}
				<i class="glyphicon glyphicon-star-empty"></i>
			{else}	
				<i class="glyphicon glyphicon-star"></i>
			{/if}
		{/section}
		<p class="review-title">{$result.title}</p>
		<p class="review-comment">{$result.comment}</p>
		<p class="review-from">Reviewed By {$result.from}</p>
		<!--p class="review-date">{$result.date|date_format:"%B %e %Y, %I:%M %p"}</p>->
		<p class="review-date">{$result.date|date_format:"%e %B %Y"}</p>
	{/foreach}
</div>
{else if}
{l s="No reviews found" mod="reviews"}
{/if}  -->

<!-- <form id="review_form" class="form-horizontal" style="display: none;">
			<div class="form-group">
			<div class="col-xs-6">
				<label>Title</label>
				<input type="text" name="title" id="newcommenttitle" > 			
			</div>
			<div class="col-xs-6">
				<label>Rating</label>
				<input id="ratinginput" value="0" type="text" class="rating">
			</div>
			<div class="col-xs-6">
				<label>Comment</label>
				<input type="text" name="comment" id="newcommentbox" >
				<textarea name="comment" id="newcommentbox" rows="10" class="form-control"></textarea>	
			</div>
		</div>
		<div class="form-group">
			<div class="col-xs-6 col-sm-offset-3">
				<button type="button" name="submitCmsReviews" id="submitReviews" class="btn btn-default button button-medium">
					<span>{l s="Submit A Comment" mod="reviews"}<i></i></span>

				</button>
			</div>
		</div> 
		</form>-->

	<!--<div class="indication-msg">
		<div id="new_comment_form_error" class="error" style="display:none;padding:15px 25px">
			<ul></ul>
		</div>
		<div id="comment_error_msg" class="error" style="display:none;padding:15px 25px">
			{l s="Please Enter Comment" mod="reviews"}
		</div>
		<div id="title_error_msg" class="error" style="display:none;padding:15px 25px">
			{l s="Please Enter title" mod="reviews"}
		</div>
		<div id="rating_error_msg" class="error" style="display:none;padding:15px 25px">
			{l s="Please Select Rating" mod="reviews"}
		</div>
		<div id="succsess_msg" class="success" style="display:none;padding:15px 25px">
			{l s="Comment Successfully Submitted, will be visible after approval" mod="reviews"}
		</div>
	</div> -->