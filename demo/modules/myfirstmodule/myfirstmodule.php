<?php   
if (!defined('_PS_VERSION_'))
  exit;

class MyFirstModule extends Module
{
	public function __construct()
  {
    $this->name = 'myfirstmodule';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'uwtech';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    $this->bootstrap = true;
 
    parent::__construct();
 
    $this->displayName = $this->l('My first module');
    $this->description = $this->l('Description of my first module.');
 
    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
 
    //if (!Configuration::get('MYFIRSTMODULE_NAME'))      
      //$this->warning = $this->l('No name provided');
  }


	public function install()
	{
	   
	  /*if(!parent::install() OR !$this->registerHook('displayLeftColumn') || $this->registerHook('header'))
	  	return false;*/
	  $success = (parent::install()/*
      && $this->registerHook('header')*/
    );

    return $success;
	}

  public function uninstall()
  {
    return parent::uninstall();
  }

	/*public function hookHeader()
	{
    $this->context->controller->addCSS(($this->_path).'css/my.css', 'all');
    $this->context->controller->addCSS(($this->_path).'css/bootstrap.min.css', 'all');
    $this->context->controller->addCSS(($this->_path).'css/header.css', 'all');
    $this->context->controller->addCSS(($this->_path).'css/animate.css', 'all');
		$this->context->controller->addCSS(($this->_path).'css/font-awesome.min.css', 'all');
	} 
  */

  public function getContent(){

    $newCMS =  new CMS(7,$this->context->language->id);
    ppp($newCMS);
  }

}


