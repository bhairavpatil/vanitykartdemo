<!-- Block mymodule -->
<div id="mymodule_block_home" class="block">
  <h4>Welcome!</h4>
  <div class="block_content">
    <p>Hello,
       {if isset($my_module_name) && $my_module_name}
           {$my_module_name}
       {else}
           World
       {/if}
       !       
    </p>   
    <ul>
      <li><a href="{$my_module_link}" title="Click this link">Click me!</a></li>
    </ul>
    <div id="special_block_right" class="block products_block exclusive blockspecials">
    <h4 class="title_block">Test Popular</h4>
      <div class="block_content">
          <ul class="products clearfix">
               {if isset($myproducts) && $myproducts}
                    <li class="product_image">
                      <a href="{$myproducts->url}"><img src="{$link->getImageLink($myproducts->link_rewrite, $myproducts->id_image, 'medium_default')|escape:'html'}" height="{$mediumSize.height}" width="{$mediumSize.width}" title="{$myproducts->name[1]|escape:html:'UTF-8'}" /></a>
                    </li>
                    <li>

                    <h5 class="s_title_block"><a href="{$myproducts->url}" title="{$myproducts->name[1]|escape:html:'UTF-8'}">{$myproducts->name[1]|escape:html:'UTF-8'}</a></h5>
                    </li>

                    <!-- <li>{$myproducts->name[1]}</li>
                      <a href="{$myproducts->url}">test</a> -->
                <!-- <img src="{$myproducts->image_url}"> -->
               {/if}
          </ul>
      </div>
    </div>
  </div>
</div>
<!-- /Block mymodule