<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/WishList.php');
require_once(dirname(__FILE__).'/blockwishlist.php');

$wishList = array();
$wishList['flag'] = false;
$wishList['products'] = array();
$context = Context::getContext();
/*$action = Tools::getValue('action');
$add = (!strcmp($action, 'add') ? 1 : 0);
$delete = (!strcmp($action, 'delete') ? 1 : 0); */
//$id_wishlist = (int)Tools::getValue('id_wishlist');
/*$id_product = (int)Tools::getValue('id_product');
$quantity = (int)Tools::getValue('quantity');
$id_product_attribute = (int)Tools::getValue('id_product_attribute');*/

// Instance of module class for translations
$module = new BlockWishList();

/*if (Configuration::get('PS_TOKEN_ENABLE') == 1 &&
	strcmp(Tools::getToken(false), Tools::getValue('token')) &&
	$context->customer->isLogged() === true
)
	echo $module->l('Invalid token', 'cart');*/

	//ddd($context->customer->id);

if($context->customer->isLogged())
{

	$custID = (int)$context->customer->id;

	if(Tools::getValue('action') && Tools::getValue('action')=='removeall' && Configuration::get('GETSALE_ALERT'))
		removeSaleAlertSubscriptions($custID);

	//die($context->customer->id);
	$wishlists = Wishlist::getByIdCustomer((int)$context->customer->id);
	



	$products = WishList::getProductByIdCustomer($wishlists[0]['id_wishlist'], $context->customer->id, $context->language->id, null, true);

	$link = new Link();
	$setWishListForSaleAlert = 0;

	foreach ($products as $key => $value) {
		// # code...
		$image = Product::getCover($products[$key]['id_product']);
		$imagePath = $context->link->getImageLink($products[$key]['link_rewrite'], $image['id_image'], ImageType::getFormatedName('home'));
		$products[$key]['prod_img'] = $imagePath;

		$products[$key]['getsalealert'] = false;
		if(Configuration::get('GETSALE_ALERT'))
		{
			$status = checkSaleAlertProdStatus($products[$key]['id_product'],$custID);
			if($status)
			{
				$products[$key]['getsalealert'] = $status;
			}
			else
			{
				$setWishListForSaleAlert += 1;
			}
		}
		// CHECK FOR EXISTANCE OF WISHLIST PRODUCT IN GETSALEALERT

	}

	//ddd($products);

	$count = count($products);
	// get sale alert feature 
	$saleAlert = (Configuration::get('GETSALE_ALERT')) ? Configuration::get('GETSALE_ALERT') : 0;
	$context->smarty->assign(array('products' => $products,'counter' => $count,'sale_alert' => $saleAlert,'wishlist_to_salealert' => $setWishListForSaleAlert));
	//$context->smarty->assign('counter',$count);
	//ddd($products);

	//echo json_encode($context->smarty->fetch(_PS_MODULE_DIR_.'blockwishlist/getwishlist.tpl'));
	$t = $context->smarty->fetch(_PS_MODULE_DIR_.'blockwishlist/getwishlist.tpl');
	
	
	$wishList['flag'] = true;
	$wishList['count'] = $count;
	$wishList['products'] = $t;
		
} 

function checkSaleAlertProdStatus($prodID,$custID){

			$prodExist = false;

			$sql = 'SELECT * FROM '._DB_PREFIX_.'getsalealert_user WHERE user_id = '.$custID. ' AND id_prod = '.$prodID ;

			$alertArr = Db::getInstance()->getRow($sql);

			if($alertArr)
			{
				$prodExist = $alertArr['alert_id'];	
			}

			return $prodExist;
	}

function removeSaleAlertSubscriptions($custID){

	$wishList = array();

	$wishList['flag'] = true;
	$wishList['count'] = 0;
	$wishList['products'] = '';

	$sql = "SELECT * FROM "._DB_PREFIX_."getsalealert_user WHERE user_id = ".$custID;
	if(Db::getInstance()->ExecuteS($sql)){
		Db::getInstance()->delete('getsalealert_user', 'user_id = '.$custID);

		$sql1 = "SELECT * FROM "._DB_PREFIX_."getsalealert_email_pool WHERE user_id = ".$custID;
		if(Db::getInstance()->ExecuteS($sql1))
			Db::getInstance()->delete('getsalealert_email_pool', 'user_id = '.$custID);
	}

	die(json_encode($wishList));

}

	//die(Tools::jsonEncode(array('data' => $wishList)));
	die(json_encode($wishList));
