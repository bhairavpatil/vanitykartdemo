{* included this style .. as an experiment *}
<style type="text/css">
{literal}
/* this is an intersting idea for this section */
.wishlist-info > .icon-bell::before{
color: red;
font-weight: bold;
}
{/literal}
</style>
{if $products}
	<dl class="products" style="{if $products}{/if}">
	<div class="wishlist_scroll">
	{foreach from=$products item=product name=i}
	<div class="wishlist-info">
		<a class="ajax_cart_block_remove_link" href="javascript:;" onclick="javascript:WishlistCart('wishlist_block_list', 'delete', '{$product.id_product}', {$product.id_product_attribute}, '0');" title="{l s='remove this product from my wishlist' mod='blockwishlist'}" rel="nofollow"><i class="remove-icon"></i></a>
	<div class="wishlist-pick-box">
		<input type="checkbox" value="{$product.id_product}" id-attr="{$product.id_product_attribute}" class="wishlist-pick-prod">
	</div>
	 <div class="wishlist-prod-img">
			<a class="cart-images" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html'}" title="{$product.name}"><img src="{$product.prod_img}" alt="{$product.name}"></a>
	 </div> <!--/wishlist-prod-img-->

	 <div class="wishlist-prod-info">

			<dt class="{if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if}">
			
			
			<a class="cart_block_product_name" href="{$link->getProductLink($product.id_product, $product.link_rewrite, $product.category_rewrite)|escape:'html'}" title="{$product.name|escape:'html':'UTF-8'}">{$product.name|truncate:30:'...'|escape:'html':'UTF-8'}</a>

			

			{if isset($product.attributes_small)}
		<dd class="{if $smarty.foreach.i.first}first_item{elseif $smarty.foreach.i.last}last_item{else}item{/if}">
			<a href="{$link->getProductLink($product.id_product, $product.link_rewrite)|escape:'html'}" title="{l s='Product detail' mod='blockwishlist'}">{$product.attributes_small|escape:'html':'UTF-8'}</a>
		</dd>
		{/if}
		<!-- <span class="quantity-formated"><span class="quantity">{$product.quantity|intval}</span></span> -->
		
		{if !$product.getsalealert && $sale_alert}
		 <!-- <button type="button" class="btn btn-group-sm get-sale-alert alert-off-{$product.id_product}" rel="{$product.id_product}">Get Sale Alert</button>
		 
		 <button style="display: none;" type="button" class="btn btn-success alert-on-{$product.id_product}" rel="{$product.id_product}">
			<span class="icon-check"></span> Sale Alert
		</button> -->

		<!-- TOGGLE BUTTON STATUS WHEN USER CLICK ON SALE ALERT -->
		<span style="display: none;" class="icon-bell get-sale-alert alert-on-{$product.id_product}"> Sale Alert</span>

		{/if}
		{if $product.getsalealert && $sale_alert}
		<span class="icon-bell get-sale-alert alert-on-{$product.id_product} active"> Sale Alert</span>
		<span id="rm-alert-{$product.id_product}" onclick="javascript:getsalealert('remove',{$product.getsalealert});" style="visibility: hidden;"></span>
		{/if}
		</dt>
	 </div> <!--/wishlist-prod-info-->
	<div class="clearfix"></div>
	</div> <!--/wishlist-info-->
	{/foreach}
	</div> <!--/wishlist_scroll-->
	</dl>

	{if $sale_alert && $wishlist_to_salealert>0}
	 <button type="button" class="btn btn-white" id="get-sale-alert" style="display: none;">
			<!-- <span class="icon-bell"></span> --> {l s='Get Sale Alert'}
	 </button>
	{/if}
	<button type="button" class="btn btn-cust" id="wishlist-add-to-bag" style="display: none;">
		<a href="#" title="{l s='Add to Bag' mod='blockwishlist'}" rel="nofollow" class="wishlist-add-to-bag">{l s='Add to Bag' mod='getsalealert'}</a>
	</button>


{else}
	<dl class="products whishlist-error-msgs">
	{if isset($error) && $error}
		<dt>{l s='You must create a wishlist before adding products' mod='blockwishlist'}</dt>
	{else}
		<dt>{l s='No products' mod='blockwishlist'}</dt>
	{/if}
	</dl>
{/if}


<script>
$(document).ready(function(){
	$(".wishlist_scroll").mCustomScrollbar({
		theme:"my-theme"
	});	
});

</script>