<?php

class Product extends ProductCore
{
	public $videourl;
	public $product_feature;

	public function __construct($id_product = null, $full = false, $id_lang = null, $id_shop = null, Context $context = null)
	{
		self::$definition['fields']['videourl'] = array(
				'type' => self::TYPE_STRING, 
				'validate' => 'isString',
				'size' => 3999999999999
			);

		self::$definition['fields']['product_feature'] = array(
				'type' => self::TYPE_STRING, 
				'validate' => 'isString',
				'size' => 3999999999999
			);

		parent::__construct($id_product, $full, $id_lang, $id_shop, $context);
	}
}