<?php

if(!defined('_PS_VERSION_'))
	exit;

class ProductVideo extends Module
{
	public function __construct()
	{	
		$this->name = 'productvideo';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->need_instance = 0;

		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = 'Product Extra Info';
		$this->description = 'insert product video embed url of youtube channel and product extra feature';

	}

	public function install()
	{
		if(!parent::install() OR
			!$this->alterProductTable() OR		
			!$this->registerHook('displayAdminProductsExtra') OR
			!$this->registerHook('actionProductUpdate') OR
			!$this->registerHook('displayProductVideo') 
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall() OR
			!$this->alterProductTable('remove'))
			return false;
		return true;
	}

	public function alterProductTable($method = 'add')
	{
		if($method == 'add')
			$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product ADD `videourl` VARCHAR (255) NOT NULL , ADD `product_feature` text NOT NULL AFTER `videourl`';
		else 
			$sql = 'ALTER TABLE ' . _DB_PREFIX_ . 'product DROP COLUMN `videourl`, DROP `product_feature`';

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;
	}

	public function hookActionProductUpdate($params)
	{
		if(Tools::isSubmit('videourl'))
		{
			if(!Db::getInstance()->update('product', array('videourl' => Tools::getValue('videourl')), 'id_product = ' . (int)Tools::getValue('id_product')))
				$this->controller->errors[] = Tools::displayError('Error while updating videourl');
		}	

		if(Tools::isSubmit('product_feature'))
		{
			if(!Db::getInstance()->update('product', array('product_feature' => Tools::getValue('product_feature')), 'id_product = ' . (int)Tools::getValue('id_product')))
				$this->controller->errors[] = Tools::displayError('Error while updating product_feature');
		}	
	}

	public function hookDisplayAdminProductsExtra($params)
	{

		$result = Db::getInstance()->executeS('SELECT videourl, product_feature FROM '._DB_PREFIX_.'product WHERE id_product = ' . (int)Tools::getValue('id_product'));
		$this->context->smarty->assign('result', $result[0]);
		return $this->display(__FILE__, 'adminProductsExtra.tpl'); 
	}

	public function hookDisplayProductVideo($params)
	{		
		return $this->display(__FILE__, 'productvideo.tpl');
	}
}