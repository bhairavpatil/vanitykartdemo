
<div id="product-moduleproductvideo" class="panel product-tab">

	<input type="hidden" name="submitted_tabs[]" value="ModuleProductVideo">
	<h3>Product Extra Information</h3>
	<div class="form-group">
		<label class="control-label col-lg-3">Embeded Video Url</label>
		<div class="col-lg-9">
			<input type="text" name="videourl" value="{$result.videourl}">
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-lg-3">Product Feature</label>
		<div class="col-lg-9">
			<!-- <input type="textarea" name="product_feature" value="{$result.product_feature}"> -->
			<textarea cols="50" rows="4" name="product_feature">
			   {$result.product_feature}
			</textarea>
		</div>
	</div>

	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay'}</button>
	</div>

</div>
