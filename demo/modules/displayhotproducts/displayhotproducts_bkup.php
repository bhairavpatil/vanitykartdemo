<?php

if(!defined('_PS_VERSION_'))
	exit;

class displayHotProducts extends Module
{
	
	private $html = '';
	private $errors = '';

	public function __construct()
	{
		$this->name = 'displayhotproducts';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

		$this->controllers = array('displayhotproducts');

		$this->need_instance = 0;
		$this->bootstrap = true;

		$this->displayName = $this->l('Display Hot Products');
		$this->description = $this->l('Promote hot products from product listing');

		$this->table_name = 'hot_products';

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() OR
			!$this->createTabs() OR
			!$this->registerHook('displayHeader') OR			
			!$this->registerHook('backOfficeHeader') OR			
			!$this->registerHook('displayHotProductsBlock')
			)
			return false;
		return true; 
	}

	public function uninstall()
	{
		if(!parent::uninstall() OR
			!$this->eraseTabs()			
			)
			return false;
		return true;
	}

	public function createTabs()
	{
		$tab = new Tab();
		$tab->active = 1;
		$languages = Language::getLanguages(false);
		if(is_array($languages))
		{
			foreach ($languages as $language)
			{
				$tab->name[$language['id_lang']] = $this->displayName;
			}
		}
		$tab->class_name = 'AdminDisplayHotProducts';
		$tab->module = $this->name;
		$tab->id_parent = 0;

		return (bool)$tab->add();
	}

	public function eraseTabs()
	{
		$id_tab = (int)Tab::getIdFromClassName('Admin'.ucfirst($this->name));
		if($id_tab)
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return true;
	}

	public function getContent()
	{
		$this->displayForm();
		$this->postProcess();
		return $this->html;
	}

	public function displayForm()
	{			
		$hot_products_id = Tools::getValue('hot_products_id');

		if(Tools::getValue('action') == 'addnewhotproducts' || !empty($hot_products_id))
			$this->html .= $this->generateForm();
		
	}

	public function postProcess()
	{			
		//ddd(Tools::getAllValues());
		//ddd(Tools::getValue('submitBulksendedconfiguration'));
		//ddd(Tools::getValue('submitBulkdeleteconfiguration'));
		//ddd($this->context->cookie->main_category);

		

		


	/*	$productListItems = Tools::getValue('inputPackItems');

		if(Tools::isSubmit('submitUpdate') && isset($productListItems) && !empty($productListItems))
		{
			$productList = explode('-', rtrim($productListItems,'-'));
			$productListArr = implode(',', $productList);

			$hotproducts['category_id'] = Tools::getValue('main_category');
			$hotproducts['sub_category_id'] = Tools::getValue('sub_category');
			$hotproducts['product_ids'] = $productListArr;
			
			if(!Db::getInstance()->insert($this->table_name,$hotproducts))
				$this->errors[] = Tools::displayError('Error while adding to the database'). ': ' . $mysql_error();


			$confirmation = Tools::getValue('hot_products_id') ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');
				
			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($confirmation);	

		}*/

		$product_id_list = '';
		$product_ids = Tools::getValue('configurationBox');
		if(is_array($product_ids) && count($product_ids))
			$product_id_list = implode(",", $product_ids);

		//ddd($product_ids);

		$date_from = Tools::getValue('date_from');		
		$date_to = Tools::getValue('date_to');		

		if(isset($date_from) && !empty($date_from))
		{
			if(!isset($date_to) || empty($date_to))
				$this->html .= $this->displayError("please select both date range");
		}

		if(isset($date_to) && !empty($date_to))
		{
			if(!isset($date_from) || empty($date_from))
				$this->html .= $this->displayError("please select both date range");
		}

		
		$brand = Tools::getValue('brand_name');
		$promote_flag = Tools::getValue('promote_flag');
		//$submitFilterconfiguration = Tools::getValue('submitFilterconfiguration');

		/*if(!isset($submitFilterconfiguration) || empty($submitFilterconfiguration))
		{ 
			$main_category = Tools::getValue('type_categories');					
			if(!isset($main_category) || empty($main_category))
				$main_category = 2;	
			$this->context->cookie->main_category = $main_category;
			$this->html .= $this->generateProductList($main_category,$brand,$date_from,$date_to);
		}
		else
		{
			$this->html .= $this->generateProductList($this->context->cookie->main_category,$brand,$date_from,$date_to);
		}*/
	
		$actionflag = Tools::getValue('actionflag');
		if($actionflag == 'new')
			$this->context->cookie->main_category = '';

		
		$main_category = Tools::getValue('type_categories');	
		if(isset($main_category) && !empty($main_category))
		{
			$this->context->cookie->main_category = $main_category;
		}
		else
		{
			if(isset($this->context->cookie->main_category))
				$main_category = $this->context->cookie->main_category;
			else
				$main_category = 2;
		}

		//ddd($main_category);

		$hotproducts['category_id'] = $main_category;
		$hotproducts['product_ids'] = $product_id_list;

		if(isset($main_category) && isset($product_id_list) && !empty($product_id_list))
		{

			$res_exist = Db::getInstance()->ExecuteS('SELECT * FROM `ps_hot_products` where category_id = '.$main_category);

			if(isset($_GET['submitBulksendedconfiguration']))
			{				
				if(count($res_exist))
				{
					if(!Db::getInstance()->update($this->table_name,$hotproducts, 'category_id = '.$main_category ))	
							$this->errors[] = Tools::displayError('Error while updating the database'). ': ' . $mysql_error();	
				}
				else
				{
					//get category name
					$main_category_name = Db::getInstance()->getrow('SELECT name FROM `ps_category_lang` where id_category = '.$main_category);
					$hotproducts['category_name'] = $main_category_name['name'];

					if(!Db::getInstance()->insert($this->table_name,$hotproducts))
							$this->errors[] = Tools::displayError('Error while adding to the database'). ': ' . $mysql_error();
				}

				$confirmation = count($res_exist) ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');				
				
			}

			if(isset($_GET['submitBulkdeleteconfiguration']))
			{
				if(count($res_exist))
				{
					$product_id_arr = explode(',', $res_exist[0]['product_ids']);					
					$del_id_arr = explode(",",$product_id_list);

					$final_arr = implode(",",array_diff($product_id_arr,$del_id_arr));
					$hotproducts['product_ids'] = $final_arr;

					if(!Db::getInstance()->update($this->table_name,$hotproducts, 'category_id = '.$main_category ))	
							$this->errors[] = Tools::displayError('Error while updating the database'). ': ' . $mysql_error();

					$confirmation = $this->l('Entry Successfully Deleted');	
				}
			}

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($confirmation);

				$this->context->cookie->main_category = '';
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name);
			
		}



		/*if(!isset($main_category) || empty($main_category))
		{
			$main_category = $this->context->cookie->main_category =  2;				
		}*/

		$hot_products_id = Tools::getValue('hot_products_id');

		if(Tools::getValue('action') == 'addnewhotproducts' || !empty($hot_products_id))
		{
			$this->context->cookie->action_state =  'addnewhotproducts';

			if(isset($hot_products_id) && !empty($hot_products_id))
			{
				$res = Db::getInstance()->getrow('SELECT category_id FROM `ps_hot_products` where hot_products_id = '.$hot_products_id);
				$main_category = $this->context->cookie->main_category = $res['category_id'];
				$promote_flag = 1;
			}
			/*else
			{ 
				$this->context->cookie->promote_flag = 0;				
			}*/
			$this->html .= $this->generateProductList($main_category,$brand,$date_from,$date_to,$promote_flag);
		}
		else
			$this->html .= $this->generatePromotionList();
		
		//ddd($this->context->cookie);

		//ddd($this->html);
		
		//if(Tools::isSubmit('submitFilterconfiguration'))
			//Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&submitFilterproduct=1');
		
	}

	public function generateForm()
	{
		$inputs = array();

		//get active brands list
		$brands_list = Db::getInstance()->ExecuteS('SELECT * FROM `ps_manufacturer` where active = 1');

		//Fetch Main Category List under the home category
		$category_list = $this->getCategoryList(2);

		//Added home category explicitly in main category array
		$category_list[] = array('id_category' => 2, 'name' => 'Home');
	
		//sort multidimensional array
		usort($category_list, function($a, $b) {
   			return $a['id_category'] - $b['id_category'];
		});

		$actionflag = Tools::getValue('actionflag');
		if($actionflag == 'new')
			$this->context->cookie->main_category = '';

		//if select view mode then fetch main category id
		$hot_products_id = Tools::getValue('hot_products_id');
		if(isset($hot_products_id) && !empty($hot_products_id))
		{
			$res = Db::getInstance()->getrow('SELECT category_id FROM `ps_hot_products` where hot_products_id = '.$hot_products_id);
			$cat_id = $this->context->cookie->main_category = $res['category_id'];			
		}	
		else
			$cat_id = Tools::getValue('type_categories'); 

		if(!isset($cat_id) || empty($cat_id))
		{
			if(isset($this->context->cookie->main_category))
				$cat_id = $this->context->cookie->main_category;
			else
				$cat_id = 2;
		}

		//if(empty($cat_id) || !isset($cat_id))
		//	$cat_id = 2; 
		
	
		//ddd($sub_category_list);

		//Select Main Category		
		/*$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Category',
                    'class' => 'chosen',                    
                    'name' => 'main_category',                                
                    'options' => array(                       
                        'query' => $category_list,
                        'id' => 'id_category',
                        'name' => 'name'
                    ),
                );*/

		$inputs[] =  array(
                    'type' => 'categories',
                    'label' => 'Filter By Categories',
                    'name' => 'type_categories',
                    'tree' => array(
                        'root_category' => 1,
                        'id' => 'id_category',
                        'name' => 'name_category',
                        'selected_categories' => array($cat_id),                        
                    )
                );
	

		$inputs[] =  array(
                    'type' => 'switch',
                    'label' => 'Show Promoted Products',
                    'name' => 'promote_flag',
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0
                        )
                    )
                );
		
           /* if(!empty($sub_category_list))
            {  
		         $inputs[] = array(
		                    'type' => 'select',
		                    'label' => 'Select Sub Category',
		                    'class' => 'chosen',
		                    'name' => 'sub_category',                                
		                    'options' => array(                       
		                        'query' => $sub_category_list,
		                        'id' => 'id_category',
		                        'name' => 'name'
		                    ),
		                );
			}*/

         $inputs[] = array(
                    'type' => 'text',  
                    'label' => 'Filter By Brand',                  
                    'name' => 'brand_name'                       
                                    
                );

         $inputs[] = array(
                    'type' => 'date',  
                    'label' => 'Date From',                  
                    'name' => 'date_from'     
                                    
                );

         $inputs[] = array(
                    'type' => 'date',  
                    'label' => 'Date To',                  
                    'name' => 'date_to'     
                                    
                );

  /*       $inputs[] = array(
                    'type' => 'html',                    
                    'name' => 'divPackItems',                   
                    'html_content' => '<div id="divPackItems"></div>'                  
                );

		
		$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'dynamic_token',
                    'value' => ''           
                );

		$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'dynamic_token2',
                    'value' => ''           
                );

		$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'inputPackItems',
                     'value' => ''             
                );

		$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'namePackItems',
                    'value' => ''           
                );*/
				

		$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'pagination_number',
                    'value' => ''           
                );

	/*	$inputs[] = array(
                    'type' => 'hidden',                    
                    'name' => 'submitFilterconfiguration',
                    'value' => ''           
                );*/
		

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Promote Hot Products in a particular category'),
					'icon' => 'icon-cogs'
					),
				'input' => $inputs,
				'submit' => array(
					'title' => $this->l('Search'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submitUpdate'
					)
				)
			);


		//$this->context->cookie->pagination_number = '';

		$pagination_number = Tools::getValue('pagination_number');	
		if(!isset($pagination_number) || empty($pagination_number))
			$pagination_number = 1;

		//ddd($pagination_number);

		/*if(isset($pagination_number) && !empty($pagination_number))
		{
			$this->context->cookie->pagination_number = $pagination_number;
		}
		else
		{
			if(isset($this->context->cookie->pagination_number))
				$pagination_number = $this->context->cookie->pagination_number;
			else
				$pagination_number = 1;
		}*/

		//ddd(Tools::getValue('submitFilterconfiguration'));
		//ddd(Tools::getAllValues());
//ppp($pagination_number);

		$helper = new HelperForm(); 		
		//$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&token='.Tools::getValue('token').'&submitFilterconfiguration='.$pagination_number;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&token='.Tools::getValue('token').'&action='.$this->context->cookie->action_state;
		//&submitFilterconfiguration=2#configuration
		//$helper->token = Tools::getAdminTokenLite('AdminModules');
		
		$helper->tpl_vars = array(
				'fields_value' => $this->getConfigFieldsValues($inputs),
				);				
		
		return $helper->generateForm(array($fields_form));			
	}

	public function getConfigFieldsValues($inputs)
	{
		$empty_values = array();
		foreach ($inputs as $input)
        {
           /*	if($input['name']=='dynamic_token')
           		$empty_values[$input['name']] = Context::getContext()->link->getAdminLink('AdminDisplayHotProducts').'&ajax=1&action=GetSelectedProductData' ;
           	elseif($input['name']=='dynamic_token2')
           		$empty_values[$input['name']] = Context::getContext()->link->getAdminLink('AdminDisplayHotProducts').'&ajax=1&action=GetProductData' ;*/

           	//if($this->context->cookie->promote_flag)	
           	//	$promote_flag = 1;
           	//else	
           		$hot_products_id = Tools::getValue('hot_products_id');
				if(isset($hot_products_id) && !empty($hot_products_id))
					$promote_flag = 1;						
				else
           			$promote_flag = Tools::getValue('promote_flag');

           	
           	if($input['name']=='type_categories')
           		$empty_values[$input['name']] = Tools::getValue('type_categories') ;
           	elseif($input['name']=='brand_name')
           		$empty_values[$input['name']] = Tools::getValue('brand_name') ;
           	elseif($input['name']=='date_from')
           		$empty_values[$input['name']] = Tools::getValue('date_from') ;
           	elseif($input['name']=='date_to')
           		$empty_values[$input['name']] = Tools::getValue('date_to') ;
           	elseif($input['name']=='promote_flag')
           		$empty_values[$input['name']] = $promote_flag ;
           	else
           	 	$empty_values[$input['name']] = '';                      
        }

		return $empty_values;
	}	

	public function generatePromotionList()
	{
		$resultArr = $this->getAllHotProductsPromotionList();

		$fields_list = array(				
				'hot_products_id' => array(
					'title' => $this->l('ID'),
					'align' => 'center',
					 'orderby' => false,
					 'search' => false,
					'class' => 'fixed-width-xs'
					),				
				'category_name' => array(
					'title' => $this->l('category'),
					'search' => false,
					'orderby' => false,		         
					),
				);


		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->module = $this;
		$helper->actions = array('view');

		$helper->simple_header = false;
		$helper->listTotal = count($resultArr);
		$helper->identifier = 'hot_products_id';
		$helper->title = $this->l('List Of Promotion');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name;
		$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar1();

		return $helper->generateList($resultArr,$fields_list);
	}

	public function initToolbar1()
	{
		$this->toolbar_btn['new'] = array(
				'href' => $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&action=addnewhotproducts&actionflag=new&token='.Tools::getValue('token'),
				'desc' => $this->l('Add new promotion')
			);

		return $this->toolbar_btn;
	}

	public function generateProductList($cat_id,$brand,$date_from,$date_to,$promote_flag)
	{
		$contentArr = $this->getProductListByCategory($cat_id,$promote_flag);
		$content = $contentArr['category_products'];
		$finalArr = array();
		//ddd($content);
		//ddd($contentArr['count']);
		//check brand filter is set or not
		if(isset($brand) && !empty($brand))
			$brandFilterFlag = true;
		else
			$brandFilterFlag = false;

		//check Date filter is set or not
		if( (isset($date_from) && !empty($date_from))  && (isset($date_to) && !empty($date_to)) )
			$dateFilterFlag = true;
		else
			$dateFilterFlag = false;

		$filter_start_date = date('Y-m-d', strtotime($date_from));
    	$filter_end_date = date('Y-m-d', strtotime($date_to));

		if(isset($content) && !empty($content))
		{ 
			foreach ($content as $key => $value)
			{
				//To Display Image in helper list section
				$value['id_image'] = '<img style="width: 40%;" src="'._PS_BASE_URL_.__PS_BASE_URI__.'img/tmp/'.'product_mini_'.$value['id_product'].'_'.$this->context->shop->id.'.jpg'.'"/>';
				$value['promoted'] = 0;
			
				$content[$key] = $value;

				$product_add_date = date('Y-m-d', strtotime($value['date_add']));

				if($brandFilterFlag && $dateFilterFlag)
				{					
					if ((stripos($value['manufacturer_name'], $brand) !== false)
							&& ($product_add_date >= $filter_start_date) && ($product_add_date <= $filter_end_date)
						)				
					    $finalArr[] = $content[$key];					
					else				
						continue;				
				}
				elseif($brandFilterFlag)
				{					
					if (stripos($value['manufacturer_name'], $brand) !== false)				
					    $finalArr[] = $content[$key];					
					else				
						continue;				
				}
				elseif($dateFilterFlag)
				{
					 if (($product_add_date >= $filter_start_date) && ($product_add_date <= $filter_end_date))
					 	 $finalArr[] = $content[$key];					
					else				
						continue;	
				}
				else	
					$finalArr[] = $content[$key];
					
			} 
		}
		
		//ddd($finalArr);
		$fields_list = array(				
				'id_product' => array(
					'title' => $this->l('ID'),
					'align' => 'center',
					 'orderby' => false,
					 'search' => false,
					'class' => 'fixed-width-xs'
					),
				'id_image' => array(
					'title' => $this->l('Image'),
		            'align' => 'center',
		            'orderby' => false,
					'search' => false,
					'float' => true
					),
				'name' => array(
					'title' => $this->l('Name'),
					'search' => false,
					 'orderby' => false,		         
					),
				'manufacturer_name' => array(
					'title' => $this->l('Brand'),
					'search' => false,
					 'orderby' => false,
					/*'filter_key' => 'm!manufacturer_name'	*/	         
					),
				'category_default' => array(
					'title' => $this->l('Category'),
					 'orderby' => false,
					'search' => false,	         
					),
				'price' => array(
					'title' => $this->l('Final Price'),
					 'orderby' => false,
					'search' => false,		         
					),
				'reference' => array(
					'title' => $this->l('Reference Code'),
					 'orderby' => false,
					'search' => false,		         
					),
				'quantity' => array(
					'title' => $this->l('Stock'),
					 'orderby' => false,
					'search' => false,		         
					),
				'date_add' => array(
	           	 	'title' => $this->l('Date'),
	           	 	 'orderby' => false,
	           	 	 'search' => false,
	           	 	'type' => 'date'
	        		),
				'promoted' => array(
					'title' =>  $this->l('Promoted'),
					 'orderby' => false,
					'active' => 'toggle',
					'type' => 'bool'			
					)				
	            
				
			);

				  

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->module = $this;
		//$helper->actions = array('edit','delete');

		//ddd($this->context->cookie->action_state);

		$helper->simple_header = false;
		$helper->listTotal = $contentArr['count'];
		$helper->identifier = 'id_product';
		$helper->title = $this->l('List Of Products');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&action='.$this->context->cookie->action_state;
		$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();

		if($promote_flag)
			$helper->bulk_actions = array('delete' => array('text' => 'Remove Promote Selected', 'confirm' => 'Selected products will remove from hot products promotion.'));
		else
			$helper->bulk_actions = array('sended' => array('text' => 'Promote Selected', 'confirm' => 'Selected products will promote in hot products section.'));

		return $helper->generateList($finalArr,$fields_list);
	}

	public function initToolbar(){
		$this->toolbar_btn['new'] = array(
				'href' => $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name.'&action=addnewhotproducts',
				'desc' => $this->l('Add new')
			);

		return $this->toolbar_btn;
	}


	public function getProductListByCategory($cat_id,$promote_flag)
	{
		//fetch the product listing
		/*$category = new Category(Context::getContext()->shop->getCategory(),(int)Context::getContext()->language->id);
		$nb = 10000;
		$products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 10));	*/
		/*$productObj = new Product();
		$products = $productObj->getProducts((int)Context::getContext()->language->id, 0, 0, 'id_product', 'asc' );
		ddd($products);	*/

		//ddd(Tools::getAllValues());

		$pagenumber = Tools::getValue('pagination_number');
		$products_per_page = Tools::getValue('configuration_pagination');

		if(!isset($pagenumber) || empty($pagenumber))
			$pagenumber = 1;
		if(!isset($products_per_page) || empty($products_per_page))
			$products_per_page = 50;

		//$product_arr = array();

		$category_products = array();

		if($promote_flag)
		{
			$product_list = Db::getInstance()->getrow('SELECT * FROM `ps_hot_products` where category_id = '.$cat_id);
			$product_list_ids = explode(",", $product_list['product_ids']);
			//ddd($product_list_ids);
			$products_count = count($product_list_ids);			
			foreach($product_list_ids as $key => $productID)
			{			 
				$product = new Product($productID, false, '1');			 
				$category_products[$key]['id_product'] = $product->id;
				$category_products[$key]['name'] = $product->name;
				$brand_name = Db::getInstance()->getrow('SELECT name FROM `ps_manufacturer` where id_manufacturer = '.$product->id_manufacturer);
				$category_products[$key]['manufacturer_name'] = $brand_name['name'];
				$category_products[$key]['category_default'] = $product->category;
				$category_products[$key]['price'] = Product::getPriceStatic((int)$product->id, true, null, 2);
				$category_products[$key]['reference'] = $product->reference;
				$category_products[$key]['quantity'] = Product::getQuantity((int)$product->id);
				$category_products[$key]['date_add'] = $product->date_add;
			}

		}
		else
		{
			$category = new Category((int)$cat_id);
			//find total number of products
			$products_count = $category->getProducts((int)Context::getContext()->language->id, 1, 5, null, null, true);	
		
			$category_products = $category->getProducts((int)Context::getContext()->language->id, $pagenumber, $products_per_page,'id_product', 'asc');
		}

		

		//ddd($category_products);

		/*$start = 1;
		$products_per_page = 3;

		if($products_count <= $products_per_page)
			$page_num = 1;
		else 
			$page_num = (int)($products_count / $products_per_page) + 1 ;
		
		//get products list for particular category
		for ($i = $start; $i <= $page_num; $i++)
		{
			$category_products = $category->getProducts((int)Context::getContext()->language->id, $i, $products_per_page,'id_product', 'asc');
			if(is_array($category_products) && count($category_products))
			{
				foreach ($category_products as $key => $value) 
				{				
					$product_arr[] = $category_products[$key];	
				}							
				
			}	
		}	*/	

		if(is_array($category_products) && count($category_products))			
			return array('category_products' => $category_products, 'count' => $products_count);		
		else
			return false;

		

		/*$id_shop = $this->context->shop->id;
		$default_lang = $this->context->language->id;
		return Db::getInstance()->ExecuteS('
				SELECT p.id_product,p.id_shop_default , pl.name as product_title, cl.name as categories, p.price, p.reference, m.name as brand_name FROM ps_product p LEFT JOIN ps_product_lang pl ON (p.id_product = pl.id_product) LEFT JOIN ps_category_product cp ON (p.id_product = cp.id_product) LEFT JOIN ps_category_lang cl ON (p.id_category_default = cl.id_category) LEFT JOIN ps_category c ON (cp.id_category = c.id_category) LEFT JOIN ps_manufacturer m ON (m.id_manufacturer = p.id_manufacturer) WHERE pl.id_lang = '.$default_lang.' AND p.id_shop_default = '.$id_shop.' AND p.active = 1 GROUP BY p.id_product
				');	*/

	}
	

	public function hookDisplayHeader($params)
	{
		 //$this->context->controller->addCSS($this->_path.'/views/css/displayhotproducts.css');
		// $this->context->controller->addJS($this->_path.'views/js/displayhotproducts.js');
	}

	public function hookBackOfficeHeader($params)
	{ 
		 //$this->context->controller->addCSS($this->_path.'/views/css/displayhotproducts.css');
		 $this->context->controller->addJS($this->_path.'views/js/displayhotproducts.js');	
		 /* $this->context->controller->addjQueryPlugin(array(  
                'select2'                
            ));	 */
		// $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');

		
	}

	public function hookDisplayHotProductsBlock($params)
	{
		//ddd($params['category_id']);
		$cat_list = $this->getCategoryList($params['category_id']);
		//ddd($cat_list);     
		$hotproducts_array = array();                                                                                     
		foreach ($cat_list as $key => $value) 
		{			
			$getProductIds = Db::getInstance()->ExecuteS('SELECT * FROM `ps_hot_products` where category_id = '.$value['id_category']); 
			if($getProductIds)
			{
				$product_id_array = explode(',', $getProductIds[0]['product_ids']);
				if($product_id_array)
				{
					$counter = 0;
					$counterkey = 0;
					foreach ($product_id_array as $key => $value)
					{
						$product_info = new Product($value, false, '1');						

						$hotproducts_array[$getProductIds[0]['category_name']][$counter][$counterkey]['id_product'] = $product_info->id;
						$hotproducts_array[$getProductIds[0]['category_name']][$counter][$counterkey]['name'] = $product_info->name;
						$hotproducts_array[$getProductIds[0]['category_name']][$counter][$counterkey]['link_rewrite'] = $product_info->link_rewrite;
						//$hotproducts_array[$getProductIds[0]['category_name']][$counter][$counterkey]['id_image'] = $product_info->id_image;
						$hotproducts_array[$getProductIds[0]['category_name']][$counter][$counterkey]['price'] = Product::getPriceStatic((int)$product_info->id, true, null, 2);

						$getrowcount = count($hotproducts_array[$getProductIds[0]['category_name']][$counter]);
						if($getrowcount == 4)
						{
							$counter++;
							$counterkey = -1;
						}

						$counterkey++;
						
					}
					//ddd($hotproducts_array);
				}
			}
		}

		if(is_array($hotproducts_array) && count($hotproducts_array))
		{
			$this->context->smarty->assign(array(
					'hot_product_arr' => $hotproducts_array
				));
		}
		
		
	}


	public function getCategoryList($cat_id)
	{
		$sql = new DbQuery();
		$sql->select('cl.id_category,cl.name');
		$sql->from('category','c');
		$sql->innerJoin('category_lang','cl','c.id_category = cl.id_category');
		$sql->where('c.id_parent = '.$cat_id);
		$sql->orderBy('c.id_category');
		return Db::getInstance()->executeS($sql);
	}

	public function getAllHotProductsPromotionList()
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM `ps_hot_products`');
	}

}
