<?php

class AdminDisplayHotProductsController extends ModuleAdminController
{
	//fetch Subcategory on  the basis of main category selection
	public function ajaxProcessGetSubCategoryData()
	{
		if(!empty(Tools::getValue('method')) && !empty(Tools::getValue('id_data')) && Tools::getValue('method') == 'getSubCat')
		{
			$cat_id = Tools::getValue('id_data');
			$sub_cat_list = Db::getInstance()->ExecuteS('SELECT cl.id_category,cl.name FROM `ps_category` c, `ps_category_lang` cl where id_parent = '.$cat_id.' and c.id_category = cl.id_category');			
			die(Tools::jsonEncode(array('sub_cat_result'=> $sub_cat_list)));
		}
	}

	//Fetch Product List according to search term
	public function ajaxProcessGetProductData()
	{
		if(!empty(Tools::getValue('action')) && Tools::getValue('action') == 'GetProductData')
		{
			$query = Tools::getValue('q', false);
			if (!$query or $query == '' or strlen($query) < 1) {
			    die();
			}

			/*
			 * In the SQL request the "q" param is used entirely to match result in database.
			 * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
			 * they are no return values just because string:"(ref : #ref_pattern#)"
			 * is not write in the name field of the product.
			 * So the ref pattern will be cut for the search request.
			 */
			if ($pos = strpos($query, ' (ref:')) {
			    $query = substr($query, 0, $pos);
			}

			$excludeIds = Tools::getValue('excludeIds', false);
			if ($excludeIds && $excludeIds != 'NaN') {
			    $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
			} else {
			    $excludeIds = '';
			}

			// Excluding downloadable products from packs because download from pack is not supported
			$excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', true);
			$exclude_packs = (bool)Tools::getValue('exclude_packs', true);

			$context = Context::getContext();

			$sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, image_shop.`id_image` id_image, il.`legend`, p.`cache_default_attribute`
			FROM `'._DB_PREFIX_.'product` p
			'.Shop::addSqlAssociation('product', 'p').'
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)$context->language->id.Shop::addSqlRestrictionOnLang('pl').')
			LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
				ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$context->language->id.')
			WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
	        (!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
	        ($excludeVirtuals ? 'AND NOT EXISTS (SELECT 1 FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))' : '').
	        ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '').
	        ' GROUP BY p.id_product';

			$items = Db::getInstance()->executeS($sql);

			//ddd($items);

			if ($items && ($excludeIds || strpos($_SERVER['HTTP_REFERER'], 'AdminScenes') !== false))
			{
			    foreach ($items as $item) 
			    {
			        echo trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";
			    }
			}
			elseif ($items)
			{
			    // packs
			    $results = array();
			    foreach ($items as $item) 
			    {
			        $product = array(
			                'id' => (int)($item['id_product']),
			                'name' => $item['name'],
			                'ref' => (!empty($item['reference']) ? $item['reference'] : ''),
			                'image' => str_replace('http://', Tools::getShopProtocol(), $context->link->getImageLink($item['link_rewrite'], $item['id_image'], 'home_default')),
			            );
			            array_push($results, $product);
			    }
			    $results = array_values($results);
			    echo Tools::jsonEncode($results);
			} 
			else
			{
			    Tools::jsonEncode(new stdClass);
			}


		}
	}

		
}