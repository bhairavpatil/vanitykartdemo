{if isset($hot_product_arr)}
<section class="related_product_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 cust-review-section hot-product-section">
				
				<div class="slider_title">
					<span class="slide_text hot_prod_text">
						<img class="left_img" src="{$img_dir}left-flower.png" alt="flowers">
							<span class="slider_main_title">{l s="Hot Products" mod="hotproducts"}</span>
							<span class="slider_category"></span>
							<!-- <p class="jcarousel-pagination" style="display:block;"></p> -->
						<img class="right_img" src="{$img_dir}right-flower.png" alt="flowers">
					</span>
				</div> <!--/slider_title-->

				<div id="hot-products" class="jcarousel-wrapper product_slider">
				    <div class="jcarousel">
				        <ul class="hot_products_list">
				        	{foreach name="outer" from=$hot_product_arr  key="keyname"  item=result}	
				        	<!-- <div class="slider_category_inner"><span class="slider_category_name">{$keyname}</span></div> -->
				        	{foreach name="inner1" from=$result item=res}
				        	<li>

				        	
				        		<div class="slider_category_inner"><span class="slider_category_name">{$keyname}</span></div>
								{foreach name="inner2" from=$res item=res1}
				        	
								
				        		
				        			<div class="col-md-3 col-sm-6">			        		
					        		<div class="slider_item_sec">
										<a href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="{$res1.name|htmlspecialchars}">
											<div class="img-container img-container-related-imgs">
												<img src="{$link->getImageLink($res1.link_rewrite, $res1.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$res1.name|htmlspecialchars}" />
											</div> <!--/img-container-->
											<div class="card-content">
												<h4>{$res1.name|truncate:14:'...'|escape:'html':'UTF-8'}</h4>
												<!-- <h5>2 regular brushes, epilation</h5> -->
												<p>{convertPrice price=$res1.price}</p>                
											</div>
										</a>
										<div class="overlay-icon">
										<!-- start wishlist status icon -->
											{hook h='displayWishlistStatus' id_prod="{$res1.id_product}"}
										<!-- end wishlist status icon -->
											<span class="show-icon show-icon2">
												<!-- <a href="javascript:void(0)"></a> -->
												 <a class="quick-view" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="Quick view"></a>
											</span>
										</div> <!--/overlay-icon-->
									</div> <!--/slider_item_sec-->
								</div> <!--/col-md-3-->

									
							{/foreach}
								

								</li>
								{/foreach}	
							{/foreach}
				        </ul>
				    </div> <!--/jcarousel-->

				    <a href="#" class="jcarousel-control-prev"><i class="icon-chevron-left"></i></a>
				    <a href="#" class="jcarousel-control-next"><i class="icon-chevron-right"></i></a>

				    <!-- <p class="jcarousel-pagination" style="display:block;"></p> -->
				</div> <!--/jcarousel-wrapper-->
				





			</div> <!--/col-md-12-->
		</div> <!--/row-->
	</div> <!--/container-->
</section> <!--/related_product_sec-->
{/if}





<script>

(function($) {
    $(function() {
        var jcarousel = $('#hot-products .jcarousel');
		
		// Animation
		$(jcarousel).jcarousel({
            animation: 1000,
			transitions: true,
			easing:   'linear'			
        });
	
		// Show Items & For Responsive
        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
				
				if (width > 1366) {
                    width = width / 1;
                }
				else if (width > 769) {
                    width = width / 1;
                }
				else if (width == 768) {
                    width = width / 1;
                }
				else if (width >= 600) {
                    width = width / 1;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            });
			

     });
})(jQuery);


// For Hot Product Slider
$(function() {
    $('#hot-products .jcarousel')
        .jcarousel()
        .jcarouselAutoscroll({
            interval: 3000,
            target: '+=1',
            autostart: false
        });
		
		$('#hot-products .jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('#hot-products .jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });


            

			
	});






</script>