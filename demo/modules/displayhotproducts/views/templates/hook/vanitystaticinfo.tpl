<!-- Static Section Start -->
<section class="sectionbg1">
	<div class="flower-icon1"><img src="{$img_dir}flower3.png" align="img" class="img-responsive"></div>
	<div class="flower-icon2"><img src="{$img_dir}flower2.png" align="img" class="img-responsive"></div>
	<div class="flower-icon3 desktop_flower"><img src="{$img_dir}flower1.png" align="img" class="img-responsive"></div>
	<div class="flower-icon3 mob_flower"><img src="{$img_dir}flower11.png" align="img" class="img-responsive"></div>
    <div class="content-section">
        <div class="container">
        	<div class="row">
                <div class="col-md-12 section-title">
                    <h2>Vanity kart is every women's new online home<br> <span>of luxury fashion, beauty and lifestyle</span></h2>
                </div> <!--/section-title-->
            </div> <!--/row-->

            <div class="row">
                <div class="col-md-12 vanity_static_block">
					<div class="col-md-4 col-sm-4">
						<div align="center"><img src="{$img_dir}delivery.png" alt="icons"></div>
						<h3 class="h3-title">Free 2 hour delivery in Dubai</h3>
						<div align="center" class="small">Orders may currently only be<br> delivered to addresses within..</div>
					</div> <!--/col-md-4-->
					<div class="col-md-4 col-sm-4">                   
						<div align="center"><img src="{$img_dir}gift.png" alt="icons"></div>
						<h3 class="h3-title">CUSTOM SALE ALERTS</h3>
						<div align="center" class="small">At VanityKart we are committed to <br>offering you the best price possible.</div>
					</div> <!--/col-md-4-->
					<div class="col-md-4 col-sm-4">                    
						<div align="center"><img src="{$img_dir}e-rewards.png" alt="icons"></div>
						<h3 class="h3-title">EXCLUSIVE REWARDS </h3>
						<div align="center" class="small">Simply visit any Participating Store <br>and our staff will be happy to..</div>
					</div> <!--/col-md-4-->
                </div> <!--/col-md-12-->
            </div> <!--/row-->
          </div> <!--/container-->
      </div> <!--/content-section-->
      	<div class="row">
	        <div class="col-md-12 text-center">
	        	<button type="button" class="btn btn-lg black btn-member">become an exclusive member today</button>
	        </div>
	    </div> <!--/row-->
 </section>
<!-- Static Section End -->