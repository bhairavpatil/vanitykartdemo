{if isset($hot_product_arr)}
{l s="Hot Products" mod="hotproducts"}

{foreach name="outer" from=$hot_product_arr  key="keyname"  item=result}	
	<div> {$keyname}
	{foreach name="inner1" from=$result item=res}
	{foreach name="inner2" from=$res item=res1}
	<div>
		<ul>
			<li>{$res1.name|truncate:14:'...'|escape:'html':'UTF-8'}</li>
			<li>
				<a href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="{$res1.name|htmlspecialchars}">
				<img src="{$link->getImageLink($res1.link_rewrite, $res1.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$res1.name|htmlspecialchars}" />
				</a>
			</li>
			<li>
			<p class="price_display">
				<span class="price">{convertPrice price=$res1.price}</span>
			</p>
				
			</li>

			<div class="quick-view-wrapper-mobile">
                <a class="quick-view-mobile" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}">
                    <i class="icon-eye-open"></i>
                </a>
            </div>
            <a class="quick-view" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}">
                <span>Quick view</span>
            </a>

		</ul>	

		
	</div>	

	{/foreach}
	{/foreach}
	</div>
{/foreach}
{/if}