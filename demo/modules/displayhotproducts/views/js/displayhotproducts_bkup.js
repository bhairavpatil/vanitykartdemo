$(window).load(function(){

	//fix dropdown search box alignment and readonly issue
	/*$('#main_category_chosen').removeClass('chosen-container-single-nosearch');
	$('#sub_category_chosen').removeClass('chosen-container-single-nosearch');
	$('input:text').attr('readonly',false);*/

	//Display and align "add this product" button
	/*$('#productlist_select').parents('.col-lg-9').after('<div class="col-lg-2"><button type="button" id="add_pack_item" class="btn btn-default"><i class="icon-plus-sign-alt"></i> Add this product</button></div>');
	$('#productlist_select').parents('.col-lg-9').removeClass('col-lg-9').addClass('col-lg-6');*/


	$('#configuration_form_submit_btn').show();

	$('.pagination-link').on('click',function(e){
				e.preventDefault();				
				$('#submitFilter'+$(this).data("list-id")).val($(this).data("page")); 
				$('#pagination_number').val($(this).data("page"));
				$('#submitFilterconfiguration').val($(this).data("page"));
				//alert($('#pagination_number').val());
				$('#configuration_form_submit_btn').click();
			});

	//on change of category tree submit the form
	/*$('#id_category').change(function(){
		//Explicitly submit form	
		$('#configuration_form_submit_btn').click();

	});*/

	/*$('.id_product_checkbox').click(function(e){ 
		e.stopPropagation();
	});*/

	//$('.pointer').attr('onclick','').unbind('click');

	//on change of sub category submit the form, so that product list can be refresh
	/*$('#sub_category').change(function(){
		//Explicitly submit form	
		$('#configuration_form_submit_btn').click();

	});*/


/*	var main_category =	$('#main_category').val();
	var sub_category = $('#sub_category').val();


	if((typeof main_category != "undefined") && (typeof sub_category != "undefined"))
	{
		var  dynamic_token = $('input[name="dynamic_token"]').val();
		
		$.ajax({
				  type: 'POST',
				  url: dynamic_token,
				  data: 'main_category='+main_category+'&sub_category=' + sub_category,
				  dataType: 'json',
				  success: function(data) {
				  	
				  	console.log(data);		  	

				  }
			});
	}*/


	/*var main_category_id = $('#main_category').val();
	makeAjaxCall(main_category_id);	

	$('#main_category').change(function(){
		main_category_id = this.value;
			makeAjaxCall(main_category_id);	
	});
*/
	


	//function makeAjaxCall(id){	

	/*	var subcategoryhtml = '';
		

		//Default hide sub category		
		$('#main_category').parents('.form-group').next('.form-group').hide();
		$('#configuration_form_submit_btn').show();

		$.ajax({
				  type: 'POST',
				  url: dynamic_token,
				  data: 'method=getSubCat&id_data=' + id,
				  dataType: 'json',
				  success: function(data) {
				  	//if sub category is present then display this section
				  	if(data.sub_cat_result.length){					  

					  	$.each(data.sub_cat_result,function(k,v){
					  		subcategoryhtml += '<option value="'+v['id_category']+'">'+v['name']+'</option>';
					  	})
					  
					  	$('#sub_category').chosen({width: '250px'}).empty().append(subcategoryhtml).trigger("chosen:updated");
					  	$('#sub_category_chosen').removeClass('chosen-container-single-nosearch');
						$('input:text').attr('readonly',false);
						$('#main_category').parents('.form-group').next('.form-group').show(); //If sub category present then show 					  	
				  	}

				  }
			});*/

	//}
	
	/*function productFormatResult(item) {
			itemTemplate = "<div class='media'>";
			itemTemplate += "<div class='pull-left'>";
			itemTemplate += "<img class='media-object' width='40' src='" + item.image + "' alt='" + item.name + "'>";
			itemTemplate += "</div>";
			itemTemplate += "<div class='media-body'>";
			itemTemplate += "<h4 class='media-heading'>" + item.name + "</h4>";
			itemTemplate += "<span>REF: " + item.ref + "</span>";
			itemTemplate += "</div>";
			itemTemplate += "</div>";
			return itemTemplate;
		}

		function productFormatSelection(item) {
			return item.name;
		}

	var selectedProduct;
	var  dynamic_token2 = $('input[name="dynamic_token2"]').val();
		
	$('#productlist_select').select2({
			placeholder: search_product_msg,
			minimumInputLength: 2,
			width: '100%',
			dropdownCssClass: "bootstrap",
			ajax: {
				//url: "ajax_hot_product_list.php",
				url: dynamic_token2,
				dataType: 'json',
				data: function (term) {
					return {
						q: term
					};
				},
				results: function (data) {
					//Get selected product list ids and exlude these ids from main result array 
					var excludeIds = getSelectedIds();
					var returnIds = new Array();
					if (data) {
						for (var i = data.length - 1; i >= 0; i--) {
							var is_in = 0;
							for (var j = 0; j < excludeIds.length; j ++) {
								if (data[i].id == excludeIds[j][0] && (typeof data[i].id_product_attribute == 'undefined' || data[i].id_product_attribute == excludeIds[j][1]))
									is_in = 1;
							}
							if (!is_in)
								returnIds.push(data[i]);
						}
						return {
							results: returnIds
						}
					} else {
						return {
							results: []
						}
					}
				}
			},
			formatResult: productFormatResult,
			formatSelection: productFormatSelection,
		})
		.on("select2-selecting", function(e) {
			selectedProduct = e.object
		});

		function getSelectedIds()
		{
			var reg = new RegExp('-', 'g');
			var regx = new RegExp('x', 'g');

			var input = $('#inputPackItems');

			if (input.val() === undefined)
				return '';

			var inputCut = input.val().split(reg);

			var ints = new Array();

			for (var i = 0; i < inputCut.length; ++i)
			{
				var in_ints = new Array();
				if (inputCut[i]) {
					//var inputQty = inputCut[i].split(regx);
					in_ints[0] = inputCut[i];
					//in_ints[0] = inputQty[0];
					//in_ints[1] = inputQty[1];
				}
				ints[i] = in_ints;
			}

			return ints;
		}


		$('#add_pack_item').on('click', addPackItem);

		function addPackItem() {

			if (selectedProduct) {
				

				if (typeof selectedProduct.id_product_attribute === 'undefined')
					selectedProduct.id_product_attribute = 0;

				var divContent = $('#divPackItems').html();
				divContent += '<li class="product-pack-item media-product-pack" data-product-name="' + selectedProduct.name + '"  data-product-id="' + selectedProduct.id + '" data-product-id-attribute="' + selectedProduct.id_product_attribute + '">';
				divContent += '<img class="media-product-pack-img" src="' + selectedProduct.image +'"/>';
				divContent += '<span class="media-product-pack-title">' + selectedProduct.name + '</span>';
				divContent += '<span class="media-product-pack-ref">REF: ' + selectedProduct.ref + '</span>';
				//divContent += '<span class="media-product-pack-quantity"><span class="text-muted">x</span> ' + selectedProduct.qty + '</span>';
				divContent += '<button type="button" class="btn btn-default delPackItem media-product-pack-action" data-delete="' + selectedProduct.id + '" data-delete-attr="' + selectedProduct.id_product_attribute + '"><i class="icon-trash"></i></button>';
				divContent += '</li>';

				// QTYxID-QTYxID
				// @todo : it should be better to create input for each items and each qty
				// instead of only one separated by x, - and ¤
				//var line =  selectedProduct.id + 'x' + selectedProduct.id_product_attribute;
				var line =  selectedProduct.id;
				var lineDisplay =  selectedProduct.name;

				$('#divPackItems').html(divContent);
				$('#inputPackItems').val($('#inputPackItems').val() + line  + '-');
				$('#namePackItems').val($('#namePackItems').val() + lineDisplay + '¤');

				$('.delPackItem').on('click', function(e){
					e.preventDefault();
					e.stopPropagation();
					delPackItem($(this).data('delete'), $(this).data('delete-attr'));
				})
				selectedProduct = null;
				$('#curPackItemName').select2("val", "");
				$('.pack-empty-warning').hide();
			} else {
				//error_modal(error_heading_msg, msg_select_one);
				return false;
			}
		}

		function delPackItem(id, id_attribute) {

			var reg = new RegExp('-', 'g');
			var regx = new RegExp('x', 'g');

			var input = $('#inputPackItems');
			var name = $('#namePackItems');

			var inputCut = input.val().split(reg);
			var nameCut = name.val().split(new RegExp('¤', 'g'));

			input.val(null);
			name.val(null);
			for (var i = 0; i < inputCut.length; ++i)
				if (inputCut[i]) {
					//var inputQty = inputCut[i].split(regx);
					var inputQty = inputCut[i];
					if (inputQty != id) { 
						input.val( input.val() + inputCut[i] + '-' );
						name.val( name.val() + nameCut[i] + '¤');
					}
				}

			var elem = $('.product-pack-item[data-product-id="' + id + '"][data-product-id-attribute="' + id_attribute + '"]');
			elem.remove();

			if ($('.product-pack-item').length === 0){
				$('.pack-empty-warning').show();
			}
		}
*/

		
});

