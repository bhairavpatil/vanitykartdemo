<?php

if(!defined('_PS_VERSION_'))
	exit;

class displayTopBrands extends Module
{
	
	private $html = '';

	public function __construct()
	{
		$this->name = 'displaytopbrands';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

		$this->need_instance = 0;
		$this->bootstrap = true;

		$this->displayName = $this->l('Display Top Brands');
		$this->description = $this->l('Select Top Brands From List');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() OR
			!$this->createTabs() OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('displayTop') OR
			!$this->registerHook('displayTopBrands') OR
			!$this->registerHook('displayFooter')
			)
			return false;
		return true; 
	}

	public function uninstall()
	{

		$sqlRes = $this->getMainCategory();
		foreach ($sqlRes as $key => $value) {

			//Replace Speical character and space
			$search  = array(' ', '&');
			$replace = array('_', 'AND');
			$value['name'] = str_replace($search, $replace, $value['name']);

			Configuration::deleteByName('HEADER_PROMOTED_BRANDS_FOR_'.$value['name']);
		}

		if(!parent::uninstall() OR
			!$this->eraseTabs() OR
			!Configuration::deleteByName('FOOTER_PROMOTED_BRANDS')
			)
			return false;
		return true;
	}

	public function createTabs()
	{
		$tab = new Tab();
		$tab->active = 1;
		$languages = Language::getLanguages(false);
		if(is_array($languages))
		{
			foreach ($languages as $language)
			{
				$tab->name[$language['id_lang']] = $this->displayName;
			}
		}
		$tab->class_name = 'AdminDisplayTopBrands';
		$tab->module = $this->name;
		$tab->id_parent = 142;

		return (bool)$tab->add();
	}

	public function eraseTabs()
	{
		$id_tab = (int)Tab::getIdFromClassName('AdminDisplayTopBrands');
		if($id_tab)
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return true;
	}

	public function getContent()
	{
		$this->postProcess();
		$this->displayForm();
		return $this->html;
	}

	public function displayForm()
	{
		$this->html .= $this->generateForm();
	}

	public function postProcess()
	{			

		if(Tools::isSubmit('submitUpdate'))
		{
			$suc_msg = 0;	
			//updating list for footer promoted brands		
			$footer_postdata = Tools::getValue('brands_list_for_footer');	
			//if value is present then update the db for footer brands		
			if($footer_postdata)	
			{
				$list_ids = implode(',',$footer_postdata);			
				Configuration::updateValue('FOOTER_PROMOTED_BRANDS', $list_ids);
				$suc_msg = 1;				
			}
			else //if value is blank then update the db to null
			{
				Configuration::updateValue('FOOTER_PROMOTED_BRANDS', NULL);
			}

			//Fetch Main Category List under the home category
			$sqlRes = $this->getMainCategory(); 
			foreach ($sqlRes as $key => $value) {
				//Replace Speical character and space
				$search  = array(' ', '&');
    			$replace = array('_', 'AND');
				$value['name'] = str_replace($search, $replace, $value['name']);

				$header_postdata = Tools::getValue('brands_list_for_header_'.$value['id_category']);
				//if value is present then update the db for header brands
				if($header_postdata)	
				{
					$list_ids = implode(',',$header_postdata);			
					Configuration::updateValue('HEADER_PROMOTED_BRANDS_FOR_'.$value['name'], $list_ids);
					$suc_msg = 1;					
				}
				else //if value is blank then update the db to null
				{
					Configuration::updateValue('HEADER_PROMOTED_BRANDS_FOR_'.$value['name'], NULL);
				}				

			}

			if($suc_msg)
			$this->html .= $this->displayConfirmation($this->l('Settings Updated'));	
			
		}		
	}

	public function generateForm()
	{
		$inputs = array();

		//get active brands list
		$brands_list = Db::getInstance()->ExecuteS('SELECT * FROM `ps_manufacturer` where active = 1');

		//Fetch Main Category List under the home category
		$sqlRes = $this->getMainCategory();
		
		//this is for footer brands inputs
		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Multiple Brands For Footer',
                    'name' => 'brands_list_for_footer[]',
                    'class' => 'chosen',
                    'multiple' => true,
                    'options' => array(                       
                        'query' => $brands_list,
                        'id' => 'id_manufacturer',
                        'name' => 'name'
                    ),
                );

		//this is for header brands inputs
		foreach ($sqlRes as $key => $value) {			
			$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Multiple Brands in Header For '.$value['name'] . ' Category',
                    'name' => 'brands_list_for_header_'.$value['id_category'].'[]',
                    'class' => 'chosen',
                    'multiple' => true,
                    'options' => array(                       
                        'query' => $brands_list,
                        'id' => 'id_manufacturer',
                        'name' => 'name'
                    ),
                );
		}

		

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Promote Top Brands to home page'),
					'icon' => 'icon-cogs'
					),
				'input' => $inputs,
				'submit' => array(
					'title' => $this->l('save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submitUpdate'
					)
				)
			);

		$helper = new HelperForm();
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		
		$helper->tpl_vars = array(
				'fields_value' => $this->getConfigFieldsValues(),
				);				
		
		return $helper->generateForm(array($fields_form));			
	}

	public function getConfigFieldsValues()
	{
		//get selected footer brands list
		$footer_selected_list_ids = explode(',',Configuration::get('FOOTER_PROMOTED_BRANDS'));
		$footerArr =  array(
			'brands_list_for_footer[]' => $footer_selected_list_ids		
			);	

		//get selected header brands list
		$sqlRes = $this->getMainCategory();
		foreach ($sqlRes as $key => $value) {
			//Replace Speical character and space
			$search  = array(' ', '&');
			$replace = array('_', 'AND');
			$value['name'] = str_replace($search, $replace, $value['name']);

			$headerArr['brands_list_for_header_'.$value['id_category'].'[]'] = explode(',',Configuration::get('HEADER_PROMOTED_BRANDS_FOR_'.$value['name']));
		}

		$arrRes = array_merge($headerArr,$footerArr);

		return $arrRes;
	}


	//display Top brands in footer section
	public function hookDisplayFooter($params)
	{		
		//Get all brands list
		$brands_list = Db::getInstance()->ExecuteS('SELECT * FROM `ps_manufacturer` where active = 1 order by name');		
		foreach ($brands_list as $key => $value) {
			$brands_list_arr[$value['id_manufacturer']] = $value['name'];
		}
		
		//Case-1 --> top Brands for footer, promoted in back end
		$promoted_top_brands = array();
		if(Configuration::get('FOOTER_PROMOTED_BRANDS')){
			$promoted_top_brands_list_ids = explode(',',Configuration::get('FOOTER_PROMOTED_BRANDS'));	
			if($promoted_top_brands_list_ids){
				foreach ($promoted_top_brands_list_ids as $key => $value) {
					$promoted_top_brands[] = $brands_list_arr[$value]; 
				}
			}
		}
		
		//case-2 --> show top brands on the basis of sales
		$top_sales_brands = array();
		$top_sales_brands_list = Db::getInstance()->ExecuteS('SELECT id_manufacturer,count(*) as cnt FROM `ps_order_detail`, `ps_product` where ps_product.id_product = ps_order_detail.product_id group by id_manufacturer order by cnt desc');
		foreach ($top_sales_brands_list as $key => $value) {
			$top_sales_brands[] = $brands_list_arr[$value['id_manufacturer']];			
		}
		
		//Combining case 1 and case 2
		$top_brands_list_for_case1_2 = array_unique(array_merge($promoted_top_brands,$top_sales_brands));

		//Case-3 --> show top brands in alphabetical order by combining case 1,2,3
		$top_brands_list_for_case2_3 = array_unique(array_merge($top_brands_list_for_case1_2,$brands_list_arr));

		//assign brand id and urlname in array using brands name 
		foreach ($top_brands_list_for_case2_3 as $key => $value) {	
			$urlname = strtolower(str_replace(' ', '-', $value));			
			$footer_top_brands_list[] = array('id' => array_search($value,$brands_list_arr), 'displayname' => $value, 'urlname' => $urlname);
		}

		if($footer_top_brands_list)
		{
			$this->context->smarty->assign(array(
				'footer_top_brands_list'=> $footer_top_brands_list,
				'base_path'=> __PS_BASE_URI__
			));
			return $this->display(__FILE__, 'displaytopbrandsfooter.tpl');
		}	
		
	}

/*	public function hookDisplayTop($params)
	{
		//Get all brands list
		$brands_list = Db::getInstance()->ExecuteS('SELECT * FROM `ps_manufacturer` where active = 1 order by name');		
		foreach ($brands_list as $key => $value) {
			$brands_list_arr[$value['id_manufacturer']] = $value['name'];
		}

		//Case-1 --> top Brands for Header, promoted in back end
		$promoted_top_brands = array();
		$fetch_main_cat = $this->getMainCategory();
		foreach ($fetch_main_cat as $key => $value) {	

			//Replace Speical character and space
			$search  = array(' ', '&');
			$replace = array('_', 'AND');
			$value['name'] = str_replace($search, $replace, $value['name']);

			if(Configuration::get('HEADER_PROMOTED_BRANDS_FOR_'.$value['name'])){
				$promoted_top_brands_list_ids = explode(',',Configuration::get('HEADER_PROMOTED_BRANDS_FOR_'.$value['name']));	
				if($promoted_top_brands_list_ids){
					foreach ($promoted_top_brands_list_ids as $key1 => $value1) {
						$promoted_top_brands[$value['id_category']][$key1] = $value1; 
					}
				}
			}
		}
		
		//Case-2 --> top Brands for Header, on the basis of sales
		$main_cat_id_arr = array();
		$main_cat_array = array();
		foreach ($fetch_main_cat as $key => $value) {
			$main_cat_id_arr[] = $value['id_category'];
			$main_cat_array[$value['id_category']]  = $value['name'];
		}
		$main_cat_id = implode(',',$main_cat_id_arr);
		//Fetch Top brands on the basis of sales 
		$top_sales_list_arr = array();
		$top_sales_list = Db::getInstance()->ExecuteS('SELECT id_manufacturer,id_category,count(*) as cnt FROM `ps_order_detail`, `ps_product`, `ps_category_product` where ps_product.id_product = ps_order_detail.product_id and ps_product.id_product = ps_category_product.id_product and ps_category_product.id_category in ('.$main_cat_id.') group by product_id');
		$top_sales_list_arr = array();
		foreach ($top_sales_list as $key => $value) {	
					
			if(!isset($top_sales_list_arr[$value['id_category']])){
				$top_sales_list_arr[$value['id_category']] = array();
			}

			//if brand not already exist in array then just push it	
			if(!array_key_exists($value['id_manufacturer'],$top_sales_list_arr[$value['id_category']]))
				$top_sales_list_arr[$value['id_category']][$value['id_manufacturer']] = $value['cnt'];
			else{ //if brand already exist in array then increase the count for particular category

				$top_sales_list_arr[$value['id_category']][$value['id_manufacturer']] = $top_sales_list_arr[$value['id_category']][$value['id_manufacturer']] +  $value['cnt'];
			}
		}
					
		//sort array by highest count
		foreach ($top_sales_list_arr as $key => $value) {
			arsort($top_sales_list_arr[$key]);
		}

		

		//Merge both the arrays of case1 (using value) and case2 (using key)		
		$case1_2_arr = array();
		foreach ($main_cat_id_arr as $key => $value) {
			$temp_arr1 = array();
			$temp_arr2 = array();
			
			if(is_array($promoted_top_brands) && isset($promoted_top_brands[$value]))
				$temp_arr1[$value] = $promoted_top_brands[$value]; 

			if(is_array($top_sales_list_arr) && isset($top_sales_list_arr[$value]))
						$temp_arr2 = array_keys($top_sales_list_arr[$value]);


			if(isset($temp_arr1[$value]))
				$case1_2_arr[$value] = array_unique(array_merge($temp_arr1[$value],$temp_arr2));
			else if(count($temp_arr2))
				$case1_2_arr[$value] = $temp_arr2;				
		}			
		

		//Case-3 --> fetch brands according to alphabetically
		$topbrands_alphabet_list_arr = array();
		$topbrands_alphabet_list = Db::getInstance()->ExecuteS('SELECT ps_product.id_product, ps_product.id_manufacturer, ps_manufacturer.name ,id_category FROM `ps_product`, `ps_category_product`, `ps_manufacturer` where ps_product.id_product = ps_category_product.id_product and ps_product.id_manufacturer = ps_manufacturer.id_manufacturer and ps_category_product.id_category in ('.$main_cat_id.') order by ps_manufacturer.name');

		foreach ($topbrands_alphabet_list as $key => $value) { 
			
			if(is_array($topbrands_alphabet_list_arr) && isset($topbrands_alphabet_list_arr[$value['id_category']])) {			
				if(!in_array($value['id_manufacturer'], $topbrands_alphabet_list_arr[$value['id_category']]))
					$topbrands_alphabet_list_arr[$value['id_category']][] = $value['id_manufacturer'];
			}
			else{				
				$topbrands_alphabet_list_arr[$value['id_category']][] = $value['id_manufacturer'];
			}
		}
		
		//Merge both the arrays of case2 and case3 using values only		
		$case2_3_arr = array();		
		foreach ($main_cat_id_arr as $key => $value) {
			$temp_arr3 = array();
			$temp_arr4 = array();

			if(is_array($case1_2_arr) && isset($case1_2_arr[$value]))
				$temp_arr3[$value] = $case1_2_arr[$value];			

			if(is_array($topbrands_alphabet_list_arr) && isset($topbrands_alphabet_list_arr[$value]))
				$temp_arr4 = $topbrands_alphabet_list_arr[$value]; 

			if(isset($temp_arr3[$value]))
				$case2_3_arr[$value] = array_unique(array_merge($temp_arr3[$value],$temp_arr4));
			else if(count($temp_arr4))
				$case2_3_arr[$value] = array_unique($temp_arr4);
			
		}
		
		//Assign brand name and urlname to array
		foreach ($case2_3_arr as $key => $value) {
			foreach ($value as $key1 => $value1) {							
				$urlname = strtolower(str_replace(' ', '-', $brands_list_arr[$value1]));			
				$header_top_brands_list[$key][$value1] = array('category' => $main_cat_array[$key], 'id' => $value1, 'displayname' => $brands_list_arr[$value1], 'urlname' => $urlname, 'shop_id' => $this->context->shop->id );
			}
		}
			
		if($header_top_brands_list)
		{
			$this->context->smarty->assign(array(
				'header_top_brands_list'=> $header_top_brands_list,
				'base_path'=> __PS_BASE_URI__
			));
			//return $this->display(__FILE__, 'displaytopbrandsheader.tpl');
		}	
		
		
	}*/

	public function hookDisplayTopBrands($params)
	{

		//ddd($params);	

		//Get all brands list
		$brands_list = Db::getInstance()->ExecuteS('SELECT * FROM `ps_manufacturer` where active = 1 order by name');		
		foreach ($brands_list as $key => $value) {
			$brands_list_arr[$value['id_manufacturer']] = $value['name'];
		}

		//Case-1 --> top Brands for Header, promoted in back end
		$promoted_top_brands = array();
		$category_result = $this->getCategoryName($params['category_id']);   
		$category_name = $category_result[0]['name'];
		
		if(isset($category_name))
		{
			//Replace Speical character and space
			$search  = array(' ', '&');
			$replace = array('_', 'AND');
			$category_name = str_replace($search, $replace, $category_name);

			if(Configuration::get('HEADER_PROMOTED_BRANDS_FOR_'.$category_name))
			{
				$promoted_top_brands = explode(',',Configuration::get('HEADER_PROMOTED_BRANDS_FOR_'.$category_name));			
			}

		}

		//Case-2 --> top Brands for Header, on the basis of sales
		$top_sales_list_arr = array();
		$top_sales_list = Db::getInstance()->ExecuteS('SELECT id_manufacturer,id_category,count(*) as cnt FROM `ps_order_detail`, `ps_product`, `ps_category_product` where ps_product.id_product = ps_order_detail.product_id and ps_product.id_product = ps_category_product.id_product and ps_category_product.id_category = '.$params['category_id'].' group by id_manufacturer order by cnt desc');	
		foreach ($top_sales_list as $key => $value) {	
					
			$top_sales_list_arr[] = $value['id_manufacturer'];
		}
					
		
		//Merge both the arrays 
		$case1_2_arr = array_unique(array_merge($promoted_top_brands,$top_sales_list_arr));
		
		//Case-3 --> fetch brands according to alphabetically
		$topbrands_alphabet_list_arr = array();
		$topbrands_alphabet_list = Db::getInstance()->ExecuteS('SELECT  ps_product.id_manufacturer, ps_manufacturer.name ,id_category FROM `ps_product`, `ps_category_product`, `ps_manufacturer` where ps_product.id_product = ps_category_product.id_product and ps_product.id_manufacturer = ps_manufacturer.id_manufacturer and ps_category_product.id_category = '.$params['category_id'].' group by ps_manufacturer.name order by ps_manufacturer.name');

		foreach ($topbrands_alphabet_list as $key => $value) { 			
			$topbrands_alphabet_list_arr[] = $value['id_manufacturer'];
		}
				

		//Merge both the arrays of case2 and case3
		$case2_3_arr = array_values(array_unique(array_merge($case1_2_arr,$topbrands_alphabet_list_arr)));		
				
		//Assign brand name and urlname to array
		$header_top_brands_list = array();
		foreach ($case2_3_arr as $key1 => $value1) {							
			$urlname = strtolower(str_replace(' ', '-', $brands_list_arr[$value1]));			
			$header_top_brands_list[] = array('category' => $category_name, 'id' => $value1, 'displayname' => $brands_list_arr[$value1], 'urlname' => $urlname, 'shop_id' => $this->context->shop->id );
		}
						
		//ddd($header_top_brands_list);
		if($header_top_brands_list)
		{
			$this->context->smarty->assign(array(
				'header_top_brands_list'=> $header_top_brands_list,
				'base_path'=> __PS_BASE_URI__
			));
			return $this->display(__FILE__, 'displaytopbrandsheader1.tpl');
		}	
		
		
	
	}

	public function hookDisplayHeader($params)
	{
		 $this->context->controller->addCSS($this->_path.'/views/css/displaytopbrands.css');
	}

	public function getMainCategory()
	{
		$sql = new DbQuery();
		$sql->select('cl.id_category,cl.name');
		$sql->from('category','c');
		$sql->innerJoin('category_lang','cl','c.id_category = cl.id_category');
		$sql->where('c.id_parent = 2');
		$sql->where('c.active = 1');
		$sql->where('cl.id_lang = '.$this->context->language->id);
		$sql->orderBy('c.id_category');
		return Db::getInstance()->executeS($sql);
	}

	public function getCategoryName($id_category)
	{
		return Db::getInstance()->ExecuteS('SELECT * FROM `ps_category_lang` where id_category = '.$id_category);	
	}

}
