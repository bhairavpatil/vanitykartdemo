{if isset($header_top_brands_list)}
<li class="col-md-3">
	<p class="top_brand_title">Shop By Brands</p>
	<ul class="top_brands_list" id="test">		
		{foreach from=$header_top_brands_list item=list1 key=k1 name=outer}				  	
			<li><a href="{$base_path}{$list1.id}_{$list1.urlname}" ><img src="{$base_path}img/tmp/manufacturer_mini_{$list1.id}_{$list1.shop_id}.jpg" /></a></li>		  	
		{/foreach}		
	</ul>
</li>
{/if}
