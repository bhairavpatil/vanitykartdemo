$(document).ready(function() {
	$('#search_query_top_new').keyup(function() {
		$("#divclose").show();
		var my_current_selection = $('#search_query_top_new').val();
		if(my_current_selection!='')
		{
			$.ajax({
				type: 'POST',
				data: 'chosen_option='+my_current_selection,
				url: baseUri + 'modules/autosearch/ajax.php',
				success: function(data){
					$('.search-autocomplete').html(data);
				}
			})
		}
		else
		{
			$('.search-autocomplete').html('');
			$('#search_query_top_new').val('');
			$("#divclose").hide();
		}

	});	
	$("#divclose").click(function(){
		$('.search-autocomplete').html('');
		$('#search_query_top_new').val('');
		$("#divclose").hide();
	});
});