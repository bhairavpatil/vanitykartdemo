<?php
if(!defined('_PS_VERSION_'))
	exit;
class AutoSearch extends Module
{
	public function __construct()
	{
		$this->name = 'autosearch';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Bhairav';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		$this->need_instance = 0;
		$this->bootstrap = true;
		$this->displayName = $this->l('Auto Search');
		$this->description = $this->l('Auto Search');
		parent::__construct();
	}

	public function install()
	{
		if(
			!parent::install() OR
			!$this->registerHook('displayHeader') OR 
			!$this->registerHook('DisplayAutoSearch') OR 
			!$this->registerHook('displayHome') 
			)
			return false;
		return true;
	}

	public function hookDisplayHome($params)
	{
		return $this->display(__FILE__, 'autosearch.tpl');
	}

	public function hookHeader($params)
	{
		$this->context->controller->addJS($this->_path . '/views/js/autosearch.js');
		$this->context->controller->addCSS($this->_path . '/views/css/autosearch.css');
		$this->context->controller->addCSS($this->_path . '/views/css/bootstrap-select.min.css');
	}

	public function hookDisplayAutoSearch($params)
	{
		$searchdata = $this->getautosearchdata($params);


		$this->context->smarty->assign(array(
			'mydata'=> $searchdata
		));

		$this->smarty->assign('basepath', __PS_BASE_URI__);


	
		return $this->display(__FILE__, 'autosearchresult.tpl');
	}

	public function getautosearchdata($params)
	{
		$dataautosearch1 = Db::getInstance()->ExecuteS('SELECT a.name,TRUNCATE( price, 2 ) as price,a.id_product,id_image,b.ean13 as barcode, d.link_rewrite as catlink , a.link_rewrite as prodlink FROM ps_product_lang a, ps_product b ,  ps_image c , ps_category_lang d where a.id_product = b.id_product and d.id_category = b.id_category_default and c.id_product = b.id_product and a.name like "%'.$params.'%" GROUP BY id_product limit 0,4');
		$dataautosearch2 = Db::getInstance()->ExecuteS('SELECT *,a.meta_title as metatitle FROM ps_cms_lang a, ps_cms b , ps_category_lang c where  a.id_cms = b.id_cms and b.cat_assoc = c.id_category and b.id_cms_category=2 and a.meta_title like "%'.$params.'%" GROUP BY a.id_cms  limit 0,4');

		$dataautosearch4 = Db::getInstance()->ExecuteS('SELECT * FROM ps_category_lang where name like "%'.$params.'%" group by id_category limit 0,4');


		$dataautosearch5 = Db::getInstance()->ExecuteS('SELECT replace(LOWER(name)," ","-") as link,id_manufacturer,name  FROM ps_manufacturer where name like "%'.$params.'%" limit 0,4');
		

		$alldataautosearch[0] = $dataautosearch1;
		$alldataautosearch[1] = $dataautosearch2;
		// $alldataautosearch[2] = $dataautosearch3;
		$alldataautosearch[3] = $dataautosearch4;
		$alldataautosearch[4] = $dataautosearch5;
		// $alldataautosearch[5] = __PS_BASE_URI__;

		return $alldataautosearch;
	}
}