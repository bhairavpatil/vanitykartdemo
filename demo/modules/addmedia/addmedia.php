<?php

if(!defined('_PS_VERSION_'))
	exit;


class AddMedia extends Module
{

	public function __construct()
	{
		$this->name = 'addmedia';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.5' , 'max' => _PS_VERSION_);

		$this->displayName = $this->l('Add Media Js and Css');
		$this->description = $this->l('Add common js and css files for front end ');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('header') ||
			!$this->registerHook('footer')
			)
			return false;
		return true;
	}

	public function unisntall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}

	public function hookHeader($params)
	{	
		//Demo links
		//if($this->context->controller->php_self == 'product')
		//$this->context->controller->addJqueryPlugin(array('scrollTo', 'serialScroll'));
		//$this->context->controller->addCSS(_THEME_CSS_DIR_.'test.css');
		//$this->context->controller->addJS(_THEME_JS_DIR_.'test.js');

		/*if($this->context->controller->php_self == 'category'){
			//$this->context->controller->addCSS(_THEME_CSS_DIR_.'autoload/uniform.default.css');
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'uniform.default.css');
		}*/

		
		// CSS Files
		$this->context->controller->addCSS(_THEME_CSS_DIR_.'animate.css');
		$this->context->controller->addCSS(_THEME_CSS_DIR_.'mCustomScrollbar.css');

		//jCarousel CSS
		$this->context->controller->addCSS(_THEME_CSS_DIR_.'jcarousel.responsive.css');

		//Select CSS
		$this->context->controller->addCSS(_THEME_CSS_DIR_.'bootstrap-select.min.css');

		$this->context->controller->addCSS(_THEME_CSS_DIR_.'bootstrap-responsive-tabs.css');
		
		// JS Files		
		$this->context->controller->addJS(_THEME_JS_DIR_.'wow.js');		
		$this->context->controller->addJS(_THEME_JS_DIR_.'mCustomScrollbar.min.js');
		$this->context->controller->addJS(_THEME_JS_DIR_.'dotdotdot.js');
		$this->context->controller->addJS(_THEME_JS_DIR_.'simplegallery.min.js');

		//jCarousel JS
		$this->context->controller->addJS(_THEME_JS_DIR_.'jquery.jcarousel.min.js');
		$this->context->controller->addJS(_THEME_JS_DIR_.'jcarousel.responsive.js');

		//Select JS
		$this->context->controller->addJS(_THEME_JS_DIR_.'bootstrap-select.min.js');
		
		//Responsive Tabs
		$this->context->controller->addJS(_THEME_JS_DIR_.'jquery.bootstrap-responsive-tabs.min.js');

		$this->context->controller->addJS(_THEME_JS_DIR_.'custom.js');

		
		

	}

	public function hookFooter($params)
	{
		
	}

}

