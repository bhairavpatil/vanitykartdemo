<?php

if(!defined('_PS_VERSION_'))
	exit;


class SocialShare extends Module
{
	public function __construct()
	{
		$this->name = 'socialshare';
		$this->author = 'Sumit Jain';
		$this->tab = 'advertising_marketing';
		$this->need_instance = 0;
		$this->version = '1.0';
		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		parent::__construct();

		$this->displayName = $this->l('Social Share');
		$this->description = $this->l('Display social sharing button on cms and product page');
	}	

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('displaySocialShare') ||
			!$this->registerHook('displayHeader') ||
			!$this->registerHook('displayRightColumnProduct')
			)
			return false;
		return true;
	}

	public function hookDisplaySocialShare()
	{
		$pagetype = $this->context->controller->php_self;

		//if page is product page then display social icon
		if($pagetype == 'product'){
			//get product object
			$product = $this->context->controller->getProduct();
			if (isset($product) && Validate::isLoadedObject($product))
			{
				//get image cover id	
				$image_cover_id = $product->getCover($product->id);
				if (is_array($image_cover_id) && isset($image_cover_id['id_image']))
					$image_cover_id = (int)$image_cover_id['id_image'];
				else
					$image_cover_id = 0;

				Media::addJsDef(
						array(
							'sharing_name' => addcslashes($product->name, "'"),
							'sharing_url' => addcslashes($this->context->link->getProductLink($product), "'"),
							'sharing_img' => addcslashes($this->context->link->getImageLink($product->link_rewrite, $image_cover_id), "'"),
							'pagetype' => $pagetype						
						)
				);
			}
		}


		if($pagetype == 'cms')
		{	
			$cms = $this->context->controller->cms;
			if (isset($cms) && Validate::isLoadedObject($cms))
			{
				Media::addJsDef(
						array(
							'sharing_name' => addcslashes($cms->meta_title, "'"),
							'sharing_url' => addcslashes($this->context->link->getCMSLink($cms->id), "'"),
							'sharing_img' => _PS_BASE_URL_.__PS_BASE_URI__.'img/cms/'.$cms->thumb_image,
							'pagetype' => $pagetype						
						)
					);
			}
		}

		//these parameter will used in socialshare_header.tpl file
		$this->context->smarty->assign(array(
					'pagetype' => $this->context->controller->php_self,
					'imgurl' =>  $pagetype == 'product' ? addcslashes($this->context->link->getImageLink($product->link_rewrite, $image_cover_id), "'") : _PS_BASE_URL_.__PS_BASE_URI__.'img/cms/'.$cms->thumb_image
		));

		return $this->display(__FILE__,'socialshare.tpl');
	}

	public function hookDisplayHeader($params)
	{
		if($this->context->controller->php_self == 'cms' || $this->context->controller->php_self == 'product')
		{
			$this->context->controller->addJS($this->_path.'js/socialshare.js');
			$this->context->controller->addCSS($this->_path.'views/css/socialshare.css');	

			$this->hookDisplaySocialShare();

			return $this->display(__FILE__,'socialshare_header.tpl');			
		}
	}

	public function hookDisplayRightColumnProduct($params)
	{
		return $this->hookDisplaySocialShare();
	}
	
}