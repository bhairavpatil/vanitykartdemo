<meta property="og:url"           content="{$request}" />
<meta property="og:type"          content="{$pagetype}" />
<meta property="og:title"         content="{$meta_title|escape:'html':'UTF-8'}" />
<meta property="og:description"   content="{$meta_description|escape:'html':'UTF-8'}" />
<meta property="og:site_name" content="{$shop_name}" />
<meta property="og:image"         content="{$imgurl}" />

