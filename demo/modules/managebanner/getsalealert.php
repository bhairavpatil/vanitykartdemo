<?php 
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
* This module is extended version of wishlist module and used to manage the sale alert subscription based 
* on users wishlist products
*

*/

if (!defined('_PS_VERSION_'))
	exit;

class GetSaleAlert extends Module {

	private $html = '';


	public function __construct()
	{
		$this->name = 'getsalealert';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'uwtech';
		$this->need_instance = 0;
		$this->dependencies = array('blockwishlist');
		$this->controllers = array('managesalealert');

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Get Sale Alert');
		$this->description = $this->l('Adds a subscription for getting sale alerts containing the customer\'s wishlists.');
		$this->default_wishlist_name = $this->l('Manage Sale Alert');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->html = '';
	}


	public function install(){

		if(!parent::install() || !$this->registerHook('header') || !$this->registerHook('customerAccount') ){
			return false;
		}
		else
		{
			Configuration::updateValue('GETSALE_ALERT', 0);
			return true;

		}

		/*
			CREATE TABLE `ps_getsalealert_email_pool` (
			  `id` int(11) NOT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `email` varchar(256) DEFAULT NULL,
			  `id_prod` int(11) DEFAULT NULL,
			  `previous_price` decimal(20,2) DEFAULT NULL,
			  `current_price` decimal(20,2) DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;

			ALTER TABLE `ps_getsalealert_email_pool`
  			ADD PRIMARY KEY (`id`);

  			ALTER TABLE `ps_getsalealert_email_pool`
  			MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


				  			CREATE TABLE `ps_getsalealert_user` (
				  `alert_id` int(11) NOT NULL,
				  `user_id` int(11) DEFAULT NULL,
				  `email` varchar(256) DEFAULT NULL,
				  `id_prod` int(11) DEFAULT NULL,
				  `previous_price` decimal(20,2) DEFAULT NULL,
				  `current_price` decimal(20,2) DEFAULT NULL,
				  `flag` tinyint(1) DEFAULT '0',
				  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;


				ALTER TABLE `ps_getsalealert_user`
  ADD PRIMARY KEY (`alert_id`);

  ALTER TABLE `ps_getsalealert_user`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT;

		*/
	}

	public function uninstall(){

		if(!parent::uninstall()){
			return false;
		}
		else{

			Configuration::deleteByName('GETSALE_ALERT');
			return true;

		}
	}

	public function hookHeader(){

		//$this->context->controller->addCSS($this->_path.'homeslider.css');
		$this->context->controller->addJS($this->_path.'/js/getsalealert.js');
	}

	public function hookCustomerAccount(){

		$this->smarty->assign(array(
				'getsalealert' => Configuration::get('GETSALE_ALERT'),
			));
		return $this->display(__FILE__, 'my-account.tpl');
	}


	public function getContent(){


		//ddd($this->_path);

		$this->_postProcess();
		$this->displayForm();
		return $this->html;
	}

	private function _postProcess(){

		if(Tools::isSubmit('submitSaleAlert'))
		{
			$curVal = Tools::getValue('status');
			Configuration::updateValue('GETSALE_ALERT',$curVal);
		}
		/*if(Tools::isSubmit('submitGetSubscriber'))
		{
			$curVal = Tools::getAllValues();
			//ddd($curVal);
			if((array_key_exists('id_customer', $curVal) && Tools::getValue('id_customer')) || array_key_exists('id_product', $curVal)){
				$list = $this->_getAlertList($curVal);

				if(count($list))
				$this->html .= $this->_generateAlertList($list);
			}

		}*/
	}

	public function displayForm(){

		$this->html .= $this->generateForm();
		//if(Configuration::get('GETSALE_ALERT'))
		$this->html .= $this->generateForm1();

		if(Tools::isSubmit('submitGetSubscriber'))
		{
			$curVal = Tools::getAllValues();
			//ddd($curVal);
			if((array_key_exists('id_customer', $curVal) && Tools::getValue('id_customer')) || array_key_exists('id_product', $curVal)){
				$list = $this->_getAlertList($curVal);

				if(count($list))
				$this->html .= $this->_generateAlertList($list);
			}

		}
	}

	public function generateForm(){

		$inputs[] = array(
                    'type' => 'switch',
                    'label' => 'Active',
                    'desc' => 'Allow users to subscribe for Sale Alert from Wishlist of product',
                    'name' => 'status',
                    'values' => array(
                        array(
                            'id' => 'status',
                            'value' => 1
                        ),
                        array(
                            'id' => 'status',
                            'value' => 0
                        )
                    )
                );


		$fields_form = array(
			'form' => array(
				'legend' => array(
						'title' => $this->l('Activate Sale Alert'),
						'icon' => 'icon-cogs'
						),
				'input' => $inputs,
				
				'submit' => array(
						'title' => $this->l('Save'),
						'class' => 'btn btn-default pull-right'
						),
					)
				);

		foreach ($inputs as $input)
		{
			$empty_values[$input['name']] = (!Tools::getValue($input['name'])) ? Configuration::get('GETSALE_ALERT') : Tools::getValue($input['name']);
		}

		$action = Configuration::get('GETSALE_ALERT');

		$helper = new HelperForm();
		$helper->submit_action = 'submitSaleAlert';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&conf=4&action='.$action;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $empty_values
			);
		
		//if($_POST)
		//$helper->tpl_vars['fields_value']['promote_to_page[]'] = array(0 => 'cms_1',1 => 'cms_2');

		return $helper->generateForm(array($fields_form));
	}

	public function generateForm1(){


		//if(Configuration::get('GETSALE_ALERT'))
		$inputs[] = array(
						'type' => 'select',
						'label' => $this->l('Customers :'),
						'name' => 'id_customer',
						'options' => array(
							'default' => array('value' => 0, 'label' => $this->l('Choose customer')),
							'query' => $this->getAlertCustomers(),
							'id' => 'id_customer',
							'name' => 'name'
						),
					);

		$inputs[] = array(
						'type' => 'select',
						'label' => $this->l('Products :'),
						'class' => 'chosen',
                    	'multiple' => true, 
						'name' => 'id_product',
						'options' => array(
							'query' => $this->getAlertProducts(),
							'id' => 'id_product',
							'name' => 'name'
						),
					);


		$fields_form = array(
			'form' => array(	
				'legend' => array(
						'title' => $this->l('Get Sale Alert List'),
						'icon' => 'icon-cogs'
						),
				'input' => $inputs,

				'submit' => array(
						'title' => $this->l('Seach'),
						'class' => 'btn btn-default pull-right'
						),
					)
				);

		foreach ($inputs as $input)
		{
			$empty_values[$input['name']] = (!Tools::getValue($input['name'])) ? Configuration::get('GETSALE_ALERT') : Tools::getValue($input['name']);
		}

		$helper = new HelperForm();
		$helper->submit_action = 'submitGetSubscriber';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&action='.$action;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $empty_values
			);
		
		//if($_POST)
		//$helper->tpl_vars['fields_value']['promote_to_page[]'] = array(0 => 'cms_1',1 => 'cms_2');

		return $helper->generateForm(array($fields_form));
	}




	public function getAlertCustomers()
	{
		$customerArr = array();

		$custExists = Db::getInstance()->ExecuteS("SELECT a.*, b.firstname FROM "._DB_PREFIX_."getsalealert_user a, "._DB_PREFIX_."customer b WHERE a.user_id = b.id_customer GROUP BY a.user_id");

		if($custExists)
		{
			foreach ($custExists as $key => $value) {
				// # code...
				$customerArr[] = array('id_customer' => $value['user_id'],'name' => $value['firstname']);
			}
		}

		return $customerArr;
	}

	public function getAlertProducts()
	{
		$productArr = array();

		$prodExists = Db::getInstance()->ExecuteS("SELECT a.id_prod, b.name FROM "._DB_PREFIX_."getsalealert_user a, "._DB_PREFIX_."product_lang b WHERE a.id_prod = b.id_product GROUP BY a.id_prod");

		if($prodExists)
		{
			foreach ($prodExists as $key => $value) {
				// # code...
				$productArr[] = array('id_product' => $value['id_prod'],'name' => $value['name']);
			}
		}

		return $productArr;
	}

	private function _generateAlertList($list){


		$content = $list;
		
		$fields_list = array(
		
			'alert_id' => array(
				'title' => 'ID',
			),
			'cname' => array(
				'title' => $this->l('User'),
			),
			'email' => array(
				'title' => $this->l('Email'),
			),
			'id_prod' => array(
				'title' => $this->l('Product ID')
			),
			'image' => array(
				'title' => $this->l('Image'),
				'float' => true,
			),
			'name' => array(
				'title' => $this->l('Product Name')
			),
			'previous_price' => array(
				'title' => $this->l('Alert Price')
			),
			
		);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'alert_id';
		$helper->simple_header = true;
		$helper->title = $this->l('List of Sale Alerts');
/*		$helper->table = 'getsalealert_user';
*/		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name .'&module_name=' . $this->name;
		$helper->toolbar_scroll = false;
		//$helper->bulk = true;

		return $helper->generateList($content, $fields_list);
	}

	public function _getAlertList($param){

		$listArr = array();

		$link = new Link();

		$context = Context::getContext();

		$prodWhereClause = "";

		if(array_key_exists('id_product', $param))
		{
			$id_prod = "'".implode("','",$param['id_product'])."'";

			if($id_prod)
			$prodWhereClause = " AND a.id_prod IN (".$id_prod.") ";
		}

		if($param['id_customer'])
				$prodWhereClause .= " AND a.user_id = ".$param['id_customer'];
		
			$sql = "SELECT a.*, b.firstname as cname, c.name, c.link_rewrite FROM "._DB_PREFIX_."getsalealert_user a, "._DB_PREFIX_."customer b, "._DB_PREFIX_."product_lang c WHERE a.user_id = b.id_customer AND a.id_prod = c.id_product ".$prodWhereClause;

			$listExists = Db::getInstance()->ExecuteS($sql);

			//ppp($sql);
			//$listArr['sql'] = $sql;

			//ddd($listArr);

			if($listExists){

				foreach ($listExists as $key => $val) {
					// GET SMALL IMAGE TO SHOW IN LIST
					$image = Image::getCover($val['id_prod']);
					$imagePath = $this->context->link->getImageLink($val['link_rewrite'], $image['id_image'], ImageType::getFormatedName('small'));
					$listExists[$key]['image'] = '<img src="'.$imagePath.'">';
				}

				$listArr = $listExists;
			}
			

		return $listArr;
	}


}