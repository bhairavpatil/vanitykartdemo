<!-- Hello -->
<a href="?controller=AdminModules&configure=managebanner&module_name=managebanner&action=showbanners&item={Tools::getValue('item')}&token={Tools::getValue('token')}&submitFilterbanner_promotion"><button class="btn btn-default"><i class="process-icon-back"></i>Back to list</button></a>
<p>&nbsp;</p>
<div class="form-group">
<label class="control-label col-lg-2">
Banner Image
</label>
<div class="row">
    <div class="col-lg-6">
		<img src="{$base}/views/img/{$bannerRes['image']}">
	</div>
</div>
</div>

<div class="form-group">
<label class="control-label col-lg-2">
Item
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['item']}
	</div>
</div>
</div>

{if $bannerRes['section']}
<div class="form-group">
<label class="control-label col-lg-2">
Section
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['section']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['category']}
<div class="form-group">
<label class="control-label col-lg-2">
Category
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['category']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['sub-categories']}
<div class="form-group">
<label class="control-label col-lg-2">
Sub Category
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['sub-categories']}
	</div>
</div>
</div>
{else if $bannerRes['applyforall-sub-categories']}
<div class="form-group">
<label class="control-label col-lg-2">
Applied for Sub Categories
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['applyforall-sub-categories']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['cms']}
<div class="form-group">
<label class="control-label col-lg-2">
CMS
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['cms']}
	</div>
</div>
</div>
{else if $bannerRes['applyforall-cms']}
<div class="form-group">
<label class="control-label col-lg-2">
Applied for CMS
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['applyforall-cms']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['product']}
<div class="form-group">
<label class="control-label col-lg-2">
Product
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['product']}
	</div>
</div>
</div>
{else if $bannerRes['applyforall-product']}
<div class="form-group">
<label class="control-label col-lg-2">
Applied for Product
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['applyforall-product']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['position']}
<div class="form-group">
<label class="control-label col-lg-2">
Position
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['position']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['active_from']}
<div class="form-group">
<label class="control-label col-lg-2">
Active From
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['active_from']}
	</div>
</div>
</div>
{/if}

{if $bannerRes['active_to']}
<div class="form-group">
<label class="control-label col-lg-2">
Active To
</label>
<div class="row">
    <div class="col-lg-6">
		{$bannerRes['active_to']}
	</div>
</div>
</div>
{/if}

<div class="form-group">
<label class="control-label col-lg-2">
Status
</label>
<div class="row">
    <div class="col-lg-6">
		{if $bannerRes['status']}
		<button class="btn btn-success disabled">Active</button>
		{else}
		<button class="btn disabled">Deactive</button>
		{/if}
	</div>
</div>
</div>