{if $banners}
<div class="article_banner_sec">
{foreach from=$banners item=banner name=i}
<div class="product-thumb article_banner_block">
{if $banner['url']}
<a href="{$banner['url']}">
{/if}
<img src="{$base}/views/img/{$banner['image']}">
{if $banner['url']}
</a>
{/if}
</div>
{/foreach}

</div> <!--/article_banner_sec-->
{/if}