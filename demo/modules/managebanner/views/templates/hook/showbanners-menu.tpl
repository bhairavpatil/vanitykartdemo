{if $banners}
<li class="col-md-7">
	<div class="row menu_banner_sec">
		{foreach from=$banners item=banner name=i}
			<div class="col-lg-4 col-md-4 col-sm-4 full_image">
			{if $banner['url']}
			<a href="{$banner['url']}">
			{/if}
			<img src="{$base}views/img/{$banner['image']}">
			{if $banner['url']}
			</a>
			{/if}
			</div> <!--/full_image-->
		{/foreach}
	</div> <!--/menu_banner_sec-->
</li>
{/if}