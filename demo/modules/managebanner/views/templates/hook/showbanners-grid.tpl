{if $banners}
<div class="video_banner_sec">
{foreach from=$banners item=banner name=i}
<div class="full_image" style="float: left; margin-bottom: 10px;">
{if $banner['url']}
<a href="{$banner['url']}">
{/if}
<img src="{$base}/views/img/{$banner['image']}">
{if $banner['url']}
</a>
{/if}
</div>
{/foreach}
</div>
{/if}