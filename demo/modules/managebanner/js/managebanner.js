$(document).on('change','#item,#item-child, #cms-category, input[name="use-cms-content"], input[name="applyforall"]',function(){
 $('#configuration_form').submit();
})

$(document).on('click','#configuration_form_submit_btn',function(){

	$('#submit-flag').attr('value',1);
})

$(document).on('click',':input[name="submitResetbanner_promotion"]',function(){

	$('table.banner_promotion input').attr('value','');
})

/*$(document).on('change',':input[name="applyforall"]',function(){
 	
 	if($(this).val()==1)
 	$('#category-child-2').parents('.form-group').slideUp();
 	else
 	$('#category-child-2').parents('.form-group').slideDown();
})*/

$(document).on('change','.set-guidelines',function(){
	getUploadSize();
})

$(document).on('click','.pos-change',function(e){

	e.preventDefault();
	e.stopPropagation();
	$(this).next('.my-pos').fadeIn();
})


$(window).load(function(){
getUploadSize();
});

var getUploadSize = function(){

var sizeArr = ['menu','latest-news','video-section','related-news'];
var sizeArrObj = new Object;
var hintText = '<b>Note: <i>Please make sure uploaded image diamensions (W X H) is as per suggested guidelines to best fit in the UI.</i></b><p></p>';

$.each(sizeArr,function(index,value){

	var mySize = $('.get-guidelines:last').val();

	//console.log('key:'+value);
	if(mySize=='menu'){
		var tempObj = new Object;
		sizeArrObj.max_h = 350;
		sizeArrObj.max_w = 650;
	}
	else if(mySize=='latest-news'){
		var tempObj = new Object;
		sizeArrObj.max_h = 145;
		sizeArrObj.max_w = 263;
	}
	else if(mySize=='video-section'){
		var tempObj = new Object;
		sizeArrObj.max_h = 360;
		sizeArrObj.max_w = 450;
	}
	else if(mySize=='related-news'){
		var tempObj = new Object;
		sizeArrObj.max_h = 0;
		sizeArrObj.max_w = 263;
	}
})

if(sizeArrObj.max_w)
hintText += '<b>Max Width: '+sizeArrObj.max_w+'px </b>';
if(sizeArrObj.max_h)
hintText += '<b>, Max Height: '+sizeArrObj.max_h+'px </b>';  

$('#banner_img').parents('.col-lg-9').find('.help-block').html(hintText);
console.log(sizeArrObj);

}
