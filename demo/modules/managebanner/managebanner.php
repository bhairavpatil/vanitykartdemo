<?php 
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
* This module is extended version of wishlist module and used to manage the sale alert subscription based 
* on users wishlist products
*

*/

if (!defined('_PS_VERSION_'))
	exit;

class ManageBanner extends Module {

	private $html = '';
	private $bannerFilters = array();
	private $toggleStatus = 0;
	private $updateArr = array();
	

	public function __construct()
	{
		$this->name = 'managebanner';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'uwtech';
		$this->need_instance = 0;
		//$this->controllers = array('managebanner');

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Manage Banner');
		$this->description = $this->l('Upload and promote banner to different places on website.');
		$this->default_wishlist_name = $this->l('Manage Sale Alert');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->html = '';
	}


	public function install(){

		if(!parent::install() || !$this->registerHook('actionAdminControllerSetMedia') || !$this->registerHook('bannerDisplayRight'))
			return false;
				return true;


		/*
			CREATE TABLE `ps_getsalealert_email_pool` (
			  `id` int(11) NOT NULL,
			  `user_id` int(11) DEFAULT NULL,
			  `email` varchar(256) DEFAULT NULL,
			  `id_prod` int(11) DEFAULT NULL,
			  `previous_price` decimal(20,2) DEFAULT NULL,
			  `current_price` decimal(20,2) DEFAULT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1;

			ALTER TABLE `ps_getsalealert_email_pool`
  			ADD PRIMARY KEY (`id`);

  			ALTER TABLE `ps_getsalealert_email_pool`
  			MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


				  			CREATE TABLE `ps_getsalealert_user` (
				  `alert_id` int(11) NOT NULL,
				  `user_id` int(11) DEFAULT NULL,
				  `email` varchar(256) DEFAULT NULL,
				  `id_prod` int(11) DEFAULT NULL,
				  `previous_price` decimal(20,2) DEFAULT NULL,
				  `current_price` decimal(20,2) DEFAULT NULL,
				  `flag` tinyint(1) DEFAULT '0',
				  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
				) ENGINE=InnoDB DEFAULT CHARSET=latin1;


				ALTER TABLE `ps_getsalealert_user`
  ADD PRIMARY KEY (`alert_id`);

  ALTER TABLE `ps_getsalealert_user`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT;

		*/
	}

	public function uninstall(){

		if(!parent::uninstall())
			return false;
				return true;
	}

	/*public function hookDisplayRightColumn($param){

		// BANNERS TO BE DISPLAYED IN RIGHT HAND SIDE LATEST ARTICLES
		$param['section'] = 'related-news';
		return $this->hookBannerDisplayRight($param);
	}*/

	public function hookBannerDisplayRight($param){

		// BANNERS TO BE DISPLAYED IN RIGHT HAND SIDE LATEST ARTICLES
		$context = Context::getContext();
		$myController = $context->controller->php_self;

		$section = false;
		$item = $myController;
		$limit = false;

		$id_cat=false;
		$id_cms=false;
		$id_prod=false;
		$id_sub_cat=false;

		if($myController=='index')
		$item = 'home';

		if($param['section'])
		$section = $param['section'];

		if($section=='menu'){
			$item = 'menu';
			$id_cat = $param['cat_id'];
			$id_sub_cat = $param['sub_cat_id'];
		}

		// PARAMETER USED FOR RELATED-NEWS SECTION WHERE 1ST BANNER IS TO SHOW ABOVE THE ARTICLES & REST BELOW THE ARTICLES
		if($param['showbanner'])
		$limit = $param['showbanner'];

		return $this->_getBanners($item,$section,$limit,$id_cat,$id_cms,$id_prod,$id_sub_cat);
		//ddd($param['section']);
	}

	private function _getBanners($item=false,$section=false,$limit=false,$id_cat=false,$id_cms=false,$id_prod=false,$id_sub_cat=false)
	{
		// $context = Context::getContext();
		// $myController = $context->controller->php_self;
		$curDate = date('Y-m-d',time());
		$whereClause = '';


		if($item=='cms')
		{
			$id_cat = $context->controller->cms->cat_assoc;
			$id_cms = $context->controller->cms->id;
		}

		if($id_cat)
		$whereClause .= ' AND b.id_cat = '.$id_cat;


		// SELECT BANNER EXISTS FOR THIS LOCATION
		if($item!='menu'){
		$sql = "SELECT * FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner AND a.item = '".$item."' and a.section='".$section."' AND a.status = 1 AND a.item != 'menu' AND ((a.active_from IS NULL AND a.active_to IS NULL) OR (a.active_from >= '".$curDate."' OR (a.active_to >= '".$curDate."' OR a.active_to IS NULL))) ".$whereClause." ORDER BY b.position";
		}
		else
		{
			if($id_sub_cat)
			{
				$sql = "SELECT * FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner AND a.item = '".$item."' AND a.status = 1 AND ((a.active_from IS NULL AND a.active_to IS NULL) OR (a.active_from >= '".$curDate."' OR (a.active_to >= '".$curDate."' OR a.active_to IS NULL))) AND b.id_sub_cat = ".$id_sub_cat." ORDER BY b.position";

				if(!Db::getInstance()->ExecuteS($sql)){
				$sql = "SELECT * FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner AND a.item = '".$item."' AND a.status = 1 AND ((a.active_from IS NULL AND a.active_to IS NULL) OR (a.active_from >= '".$curDate."' OR (a.active_to >= '".$curDate."' OR a.active_to IS NULL))) ".$whereClause." AND b.all_flag = 1 ORDER BY b.position";
				}
			}

		}

		$result = Db::getInstance()->ExecuteS($sql);

		//ddd($sql);

		$banners = array();

		if($result)
		{
			
			if(($section && $section=='latest-news') || $limit==1){
				$temp = array();
				$temp['image'] = $result[0]['image'];
				$temp['url'] = $result[0]['url'];
				$banners[] = $temp;
			}
			else
			{
				foreach ($result as $key => $value) {
					// # code...
					$temp = array();
					$temp['image'] = $value['image'];
					$temp['url'] = $value['url'];
					$banners[] = $temp;
				}	

				if($limit>1 && count($banners)>1)
					// if limit is set and more than one banner pop the first element of array
					array_shift($banners);
			}
		}

		$this->context->smarty->assign(array('banners' => $banners,'base' => $this->_path));

		if($section!='video-section' && $section!='menu')
		return $this->display(__FILE__,'showbanners.tpl');
		else if($section!='menu')
		return $this->display(__FILE__,'showbanners-grid.tpl');
		else
		return $this->display(__FILE__,'showbanners-menu.tpl');

		//ddd(__FILE__, 'showbanners.tpl');


	}

	public function hookActionAdminControllerSetMedia(){

		if(Tools::getValue('configure')=='managebanner'){
			$this->context->controller->addCSS($this->_path.'/css/managebanner.css');
			$this->context->controller->addJS($this->_path.'/js/managebanner.js');
		}
	}

	public function getContent(){

		if(Tools::isSubmit('deletebanner_promotion') && Tools::getValue('id_banner')){
		
			$id = Tools::getValue('id_banner');
			$item = Tools::getValue('item');
			$this->_bannerRemove($id,$item);
		}

		if(!Tools::isSubmit('viewbanner_promotion')){
		$this->_postProcess();
		$this->displayForm();
		}

		/*if(Tools::isSubmit('updatebanner_promotion')){
		$this->_postProcess();
		$this->displayForm();
		}*/

		if(Tools::isSubmit('submitFilterbanner_promotion') || Tools::getValue('banner_promotionOrderby'))
		{
			//$allKey = Tools::getAllValues();
			//ddd($allKey);
			
			$this->getFiterBannerResult();
		}

		if(Tools::isSubmit('togglebanner_promotion'))
		{
			$this->_bannerSetStatus();
			$this->getFiterBannerResult();
		}

		if(Tools::isSubmit('viewbanner_promotion'))
		{
			$this->_bannerViewDetail();
			//$this->getFiterBannerResult();
		}

		if(Tools::isSubmit('submitSearchBanner'))
		{

			$item = Tools::getValue('item');
			$cat = (Tools::getValue('category')) ? Tools::getValue('category') : false;
			$section = (Tools::getValue('sub-item')) ? Tools::getValue('sub-item') : false;
			$submitflag = Tools::getValue('submit-flag');

			// SET GLOBAL VARIABLE TO ACCESS LATER ON
			$this->bannerFilters['item'] = $item;
			$this->bannerFilters['category'] = $cat;
			$this->bannerFilters['sub-item'] = $section;

			if($submitflag){
			$content = $this->getBannerList($item,$section,$cat);
			$this->html .= $this->_generateBannerList($content['result'],$content['count']);
			//ddd($content);
			}
		}

		return $this->html;
	}

	private function _bannerRemove($id,$item){

			$myRedirect = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->name.'&module_name='.$this->name;
			$action = '&action=showbanners&item='.$item.'&submitFilterbanner_promotion';

			if($id){

			$imgSql = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."banner_promotion WHERE id = ".$id);
			$imgToRemove = $imgSql[0]['image'];

			if(Db::getInstance()->delete('banner_promotion_section','id_banner = '.$id)){
				if(Db::getInstance()->delete('banner_promotion','id = '.$id)){
					$action .= '&conf=1';

					// REMOVE PHYSICAL FILE
					unlink(dirname(__FILE__) . '/views/img/' . $imgToRemove);
				}
			}

			$this->bannerFilters['item'] = $item;
		
		}

		Tools::redirectAdmin($myRedirect.$action);

	}

	private function _bannerViewDetail(){

		$banner_id = Tools::getValue('id_banner');
		$this->_setBannerView($banner_id);
	}

	private function _bannerSetStatus(){

		$banner_id = Tools::getValue('id_banner');

		if($banner_id)
		$sql = "SELECT status FROM "._DB_PREFIX_."banner_promotion WHERE id = ".$banner_id;

		$curStatus = Db::getInstance()->getValue($sql);
		if($curStatus)
			Db::getInstance()->update('banner_promotion',array('status' => 0), 'id = '.$banner_id);
		else
			Db::getInstance()->update('banner_promotion',array('status' => 1), 'id = '.$banner_id);
		//ddd($banner_id);

		$this->toggleStatus = 1;

	}

	public function getFiterBannerResult()
	{
			$item = Tools::getValue('item');
			$cat = (Tools::getValue('category')) ? Tools::getValue('category') : false;
			$section = (Tools::getValue('sub-item')) ? Tools::getValue('sub-item') : false;

			// SET GLOBAL VARIABLE TO ACCESS LATER ON
			$this->bannerFilters['item'] = $item;
			$this->bannerFilters['category'] = $cat;
			$this->bannerFilters['sub-item'] = $section;

			$content = $this->getBannerList($item,$section,$cat);
			$this->html .= $this->_generateBannerList($content['result'],$content['count']);
	}

	private function _postProcess(){

		//$allKey = Tools::getAllValues();
		
		//$errors = array();
		$curDate = strtotime(date('Y-m-d',time()));
		$bannerImg = array();
		$myRedirect = false;


		if(Tools::isSubmit('submitManageBanner'))
		{
			//$allKey = Tools::getAllValues();

			//ddd($allKey);

			$validImageKey = Tools::getValue('item');
			$startFrom = `NULL`;
			$endTo = `NULL`;
			$bannerFile = Tools::getValue('filename');
			$updateFlag = (Tools::getValue('id_banner')) ? Tools::getValue('id_banner') : false;
			$old_banner_img = (Tools::getValue('db_banner_img')) ? Tools::getValue('db_banner_img') : false ;
			$submitflag = Tools::getValue('submit-flag');

			//if(array_key_exists('banner_file', $allKey) = array('' => , );)
			if(Tools::getValue('submit-flag')==1)
			{
				if(Tools::getValue('item'))
				{
					$validImageKey = $item = Tools::getValue('item');
					if($item=='menu')
					{
						if(Tools::getValue('applyforall')<=0 && !Tools::getValue('sub-category'))
							$this->_errors[] = 'Please select sub-category for banner';
					}
				}

				if(Tools::getValue('sub-item'))
				{
					$validImageKey = Tools::getValue('sub-item');
				}


				if(!$bannerFile && !$updateFlag){
				$this->_errors[] = 'Please upload a file first';
				}
				else if($bannerFile){


					// IF IN EDIT MODE AND BANNER IS UPLOADED
					$helper = new HelperImageUploader('banner_img');
					$files = $helper->process();

					
					if($files)
					{

						foreach ($files as $file)
						{
							$bannerImg = $file;

							if(isset($file['save_path']))
							{
								$sizeToArr = getimagesize($file['save_path']);

								$isValidSize = $this->_verifySize($validImageKey,$sizeToArr);

								if(!$isValidSize)
								{
									$this->_errors[] = 'Files dimensions are not correct, please refer the guidelines';
								}
							}
						}
					}
				}
				
				if(Tools::getValue('active_from'))
				{
					$startFrom = Tools::getValue('active_from');
					$active_from = strtotime($startFrom);

					if($curDate>$active_from)
					{
						$this->_errors[] = 'Please select valid start date';
					}
				}
				if(Tools::getValue('active_to'))
				{
					$endTo = Tools::getValue('active_to');
					$active_to = strtotime($endFrom);

					if($curDate>$active_from)
					{
						$this->_errors[] = 'Please select valid end date';
					}
				}

					if(!$this->_errors)
					{
						if($bannerFile){
						if(!ImageManager::checkImageMemoryLimit($bannerImg['save_path']))
						$this->_errors[] = Tools::displayError('Memory Limit reached');
						}

						if(!$this->_errors)
						{

							if(!ImageManager::resize($bannerImg['save_path'], dirname(__FILE__) . '/views/img/' . $bannerImg['name']) && $bannerFile){
								$this->_errors[] = Tools::DisplayError('An error occurred during the image upload');
							}
							else
							{
								$subItem = '';
								if(Tools::getValue('sub-item'))
								$subItem = Tools::getValue('sub-item');


								// ONCE ALL ERRORS ARE CLEAR DO THE ENTRY IN DATABASE
								$toInsert = array();
								$toInsert['item'] = $item;
								if(Tools::getValue('sub-item'))
								$toInsert['section'] = Tools::getValue('sub-item');
								if($bannerFile)
								$toInsert['image'] = $bannerImg['name'];
								if(Tools::getValue('banner_url'))
								$toInsert['url'] = Tools::getValue('banner_url');
								if(Tools::getValue('active_from'))
								$toInsert['active_from'] = $startFrom;
								if(Tools::getValue('active_to'))
								$toInsert['active_to'] = $endTo;
								$toInsert['status'] = Tools::getValue('status');

								$position = false;

								if(Tools::getValue('position')){
								$position = Tools::getValue('position');
								$oldPosition = Tools::getValue('db_banner_position');
								}
								

								//ddd($position);

								if(!$updateFlag)
								Db::getInstance()->insert('banner_promotion',$toInsert);
								else
								Db::getInstance()->update('banner_promotion',$toInsert,'id = '.$updateFlag);

								if(!$updateFlag)
								$bannerID = Db::getInstance()->Insert_ID(); 
								else
								$bannerID = $updateFlag; 

								// IF UPDATE FLAG IS TRUE REMOVE ENTRIES FROM SECOND TABLE AND RE-INSERT
								Db::getInstance()->delete('banner_promotion_section', 'id_banner = '.$updateFlag);

								// INSERT INTO SECOND TABLE 
								if($bannerID)
								{

									$toInsert = array();
									
									// PREPARE INSERT FOR ELEMENT MENU
									if($item=='menu')
									{
										$catID = Tools::getValue('category');

										if(!Tools::getValue('applyforall')){

											$cmsContent = Tools::getValue('sub-category');

											foreach ($cmsContent as $key => $cmsID) {
												// # code...
												$temp = array();
												$temp['id_banner'] = $bannerID;
												$temp['id_cat'] = $catID;
												$temp['all_flag'] = Tools::getValue('applyforall');
												$temp['id_sub_cat'] = $cmsID;
												$toInsert[] = $temp;
											}

										}
										else{
												// # code...
												$temp = array();
												$temp['id_banner'] = $bannerID;
												$temp['id_cat'] = $catID;
												$temp['all_flag'] = Tools::getValue('applyforall');
												$toInsert[] = $temp;
										}
									}
									// PREPARE INSERT FOR ELEMENT CMS
									else if($item=='cms')
									{
										if(!Tools::getValue('use-cms-content')){

											$cmsContent = Tools::getValue('cms-content');

											foreach ($cmsContent as $key => $cmsID) {
												// # code...
												$temp = array();
												$cmsTempArr = explode('-', $cmsID);
												$cmsID = $cmsTempArr[0];
												$catID = $cmsTempArr[1];
												$temp['id_banner'] = $bannerID;
												$temp['id_cat'] = $catID;
												$temp['all_flag'] = Tools::getValue('use-cms-content');
												$temp['id_cms'] = $cmsID;
												$toInsert[] = $temp;
											}

										}
										else{

											$cmsContent = Tools::getValue('cms-category');

											foreach ($cmsContent as $key => $cmsID) {
												// # code...
												$temp = array();
												$catID = $cmsID;
												$temp['id_banner'] = $bannerID;
												$temp['id_cat'] = $catID;
												$temp['all_flag'] = Tools::getValue('use-cms-content');
												$toInsert[] = $temp;
											}

										}
										
									}
									else
									{
										$temp = array();
										$temp['id_banner'] = $bannerID;
										$temp['all_flag'] = 0;
										$toInsert[] = $temp;
									}

									// INSERT INTO BANNER SECTION TABLE
									if(count($toInsert))
									{
											foreach ($toInsert as $key => $value) {

												$idcat = false;
												$allflag = false;
												$cmsid = false;

												// # code...
												if(array_key_exists('id_cat',$value))
												$idcat = $value['id_cat'];
												if(array_key_exists('all_flag',$value))
												$allflag = $value['all_flag'];
												
												if(!$allflag && array_key_exists('id_cms', $value))
												$cmsid = $value['id_cms'];

												// CHECK FOR EXISTING POSITION FOR BANNER IF EXISTS SAVE FOR NEXT POSITION
												if(!$position){
												$bannerPos = $this->_checkBannerPos($bannerID,$item,$subItem,$idcat,$allflag,$cmsid);
												$value['position'] = $bannerPos+1;
												}
												else if($position==$oldPosition)
												{
													$value['position'] = $position;
												}
												

												Db::getInstance()->insert('banner_promotion_section',$value);
												
										}

												// Swap positions if changed
												if($position!=$oldPosition){

														$bannerPos = $this->_checkBannerPos($bannerID,$item,$subItem,$idcat,$allflag,$cmsid,$position);

														// CHANGE POSITION OF CURRENT BANNER WITH BANNER WHICH HAS REQUESTED POSITION
														//$sectionID = Db::getInstance()->Insert_ID();
														Db::getInstance()->update('banner_promotion_section', array('position' => $position),'id_banner = '.$bannerID);
														// CHANGE POSITION OF BANNER WHICH WAS HOLDING PREVIOUS POSITION
														Db::getInstance()->update('banner_promotion_section', array('position' => $oldPosition),'id_banner = '.$bannerPos['id_banner']);

														//ddd($bannerPos);
												}

												
									}


								}

							}

						}
					}
			}
			

			if(!count($this->_errors) && $submitflag)
			$myRedirect = true;

			//$curVal = Tools::getValue('status');
			//Configuration::updateValue('GETSALE_ALERT',$curVal);
			//ddd(Tools::getAllValues());
		}
		
		// UNLINK TEMPLRATORY DIRECTORY FOR UPLOADED FILE IF ANY
		if(count($bannerImg)){
			   unlink($bannerImg['save_path']);

			   // UNLINK THE OLD FILE FROM DIRECTORY
			   if($updateFlag && !count($this->_errors))
			   	unlink(dirname(__FILE__) . '/views/img/' . $old_banner_img);



		}


		if(count($this->_errors)){
			$this->html .= $this->displayError(implode($this->_errors, '<br />'));
			$myRedirect = false;
		}
		else if($myRedirect)
		{
		// REDIRECT TO LIST VIEW AS PER THE ACTIONS
		$action = '&action=showbanners&conf=3';
		if($updateFlag)
		$action = '&action=showbanners&conf=4&item='.$item.'&submitSearchBanner';

		$myRedirect = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->name.'&tab_module='.$this->name.'&module_name='.$this->name;
		Tools::redirectAdmin($myRedirect.$action);
		}
		//if($banner_id)
	}

	public function displayForm(){

		if((!Tools::isSubmit('togglebanner_promotion')) && (!Tools::getValue('action') || Tools::getValue('id_banner')))
			$this->html .= $this->generateForm();
		else
			$this->html .= $this->generateForm1();
	}

	public function generateForm(){


		$updateFlag = 0;
		$submitClass = 'hide-first-load';

		$banner_id  = (Tools::getValue('id_banner')) ? Tools::getValue('id_banner') : false;

		if(Tools::isSubmit('updatebanner_promotion')){

		$updateFlag = $banner_id;
		//$banner_id = $updateFlag;

		$this->updateArr = $this->_getBannerView($banner_id);

		//ddd($this->updateArr);
		}

		if($banner_id){
			$inputs[] = array(
                    'type' => 'hidden',
                    'name' => 'id_banner',
                );
		}


		// default variables to assign
		$item = (!$updateFlag) ? Tools::getValue('item') : $this->updateArr['item'];
		

		if(!$updateFlag){
		$category = Tools::getValue('category');
		}
		else if($updateFlag && $item=='menu'){
		$category = $this->updateArr['category'][0];
		}
		else{
		$category = $this->updateArr['category'];
		}

		 //: ($updateFlag && $item=='menu') ? $this->updateArr['category'][0] : $this->updateArr['category'];

		$applyforall = (!$updateFlag) ? Tools::getValue('applyforall') : $this->updateArr['applyforall'];

		$section = false;
		if(!$updateFlag && Tools::getValue('sub-item'))
		$section = Tools::getValue('sub-item');
		else
		$section = $this->updateArr['sub-item'];

		$position = (!$updateFlag) ? Tools::getValue('db_banner_position') : $this->updateArr['position'];	

		//ddd($position);

		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Item',
                    'desc' => 'Select Page/Place where banner will be promoted',
                    'name' => 'item',
                    'class' => 'get-guidelines',
                    'id' => 'item',
                    'options' => array(
							'default' => array('value' => '', 'label' => $this->l('Choose one')),
							'query' => $this->getItems(),
							'id' => 'id',
							'name' => 'name'
						),
                );

		if($item && $item=='menu'){
		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Category',
                    'desc' => 'Select in which category banner will be promoted',
                    'name' => 'category',
                    'id' => 'item-child',
                    'options' => array(
							'default' => array('value' => 0, 'label' => $this->l('Choose one')),
							'query' => $this->getCategory(),
							'id' => 'id',
							'name' => 'name'
						),
                );

		if($category && $category!='')
		{
		//$category = Tools::getValue('category');

		//$applyforall = array();
		//$applyforall[] = array('id' => 1,'name' => 'All');
		//ddd('test');

		$inputs[] = array(
                    'type' => 'switch',
                    'label' => 'Apply for all sub-categories',
                    'desc' => 'If checked banner will be applicable for all sub-categories',
                    'name' => 'applyforall',
                    'id' => 'category-child-1',
                    'values' => array(
                        array(
                            'id' => 'on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'off',
                            'value' => 0
                        )
                    )
                );

		if(!$applyforall){
			//ddd('test');
		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Sub Category',
                    'desc' => 'Select in which sub-category/ies banner will be promoted',
                    'name' => 'sub-category',
                    'class' => 'chosen',
                    'multiple' => true,
                    'id' => 'category-child-2',
                    'options' => array(
							'query' => $this->getCategory($category),
							'id' => 'id',
							'name' => 'name'
						),
                );
			}


		// load image if 
		$oldImg = false;
		if(!$updateFlag && Tools::getValue('db_banner_img'))
		$oldImg = Tools::getValue('db_banner_img');
		else
		$oldImg = $this->updateArr['banner_img'];

		if($oldImg){

			$inputs[] = array(
			'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<div class="banner-img-wrap"><img src="'.$this->_path.'views/img/'.$oldImg.'"><input type="hidden" name="db_banner_img" value="'.$oldImg.'"><input type="hidden" name="db_banner_position" value="'.$position.'"></div>',
            );
		}
		// call image input
		$inputs[] = $this->_getImageInput($oldImg);
		$inputs[] = $this->_getUrlInput();
		$inputs[] = $this->_getBannerActiveFrom();
		$inputs[] = $this->_getBannerActiveTo();

		if($banner_id)
		// IF IN EDIT MODE SHOW POSITION DROP-DOWN
		$inputs[] = $this->_getPosition($banner_id,$item,$section,$category);

		$inputs[] = $this->_getBannerStatus();

		$submitClass = '';

		}

	}
	else if($item && $item!='menu')
	{
		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select section',
                    'desc' => 'Select in which section banner will be promoted',
                    'name' => 'sub-item',
                    'id' => 'item-child-2',
                    'class' => 'get-guidelines set-guidelines',
                    'options' => array(
							'query' => $this->getSubItems($item),
							'id' => 'id',
							'name' => 'name'
						),
                );

		if($item=='cms')
		{
			if(!$category)
			$cmsCat = Tools::getValue('cms-category');
			else
			$cmsCat = $category;

			//ddd($cmsCat);

			$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select cateogry',
                    'desc' => 'Select in which category of CMS banner will be promoted',
                    'name' => 'cms-category',
                    'id' => 'cms-category',
                    'class' => 'chosen',
                    'multiple' => true,
                    'options' => array(
							'query' => $this->_getCmsCategory(),
							'id' => 'id_category',
							'name' => 'catprod'
						),
                );

			$inputs[] = array(
                    'type' => 'switch',
                    'label' => 'Apply for all Articles in category',
                    'desc' => 'Choose Yes if banner will be promoted to all Article/News/Events of selected category',
                    'name' => 'use-cms-content',
                    'values' => array(
                    	array(
							'id' => 'cms-content-on',
							'value' => 1,
						),
						array(
							'id' => 'cms-content-off',
							'value' => 0,
						),
					),
                );

			$usecmscontent = $applyforall; 

			if(!$usecmscontent)
			{

				if($cmsCat){
					$inputs[] = array(
	                    'type' => 'select',
	                    'label' => 'Select content page',
	                    'desc' => 'Select in which content banner will be promoted',
	                    'name' => 'cms-content',
	                    'id' => 'cms-content',
	                    'class' => 'chosen',
	                    'multiple' => true,
	                    'options' => array(
								'query' => $this->_getCmsContent($cmsCat),
								'id' => 'id_cms',
								'name' => 'title'
							),
	                );
				}
			}
		}

		// load image if 
		$oldImg = false;
		if(!$updateFlag && Tools::getValue('db_banner_img'))
		$oldImg = Tools::getValue('db_banner_img');
		else
		$oldImg = $this->updateArr['banner_img'];
		
		if($oldImg){

			$inputs[] = array(
			'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<div class="banner-img-wrap"><img src="'.$this->_path.'views/img/'.$oldImg.'"><input type="hidden" name="db_banner_img" value="'.$oldImg.'"><input type="hidden" name="db_banner_position" value="'.$position.'"></div>',
            );
		}
		// call image input
		$inputs[] = $this->_getImageInput($oldImg);
		$inputs[] = $this->_getUrlInput();
		$inputs[] = $this->_getBannerActiveFrom();
		$inputs[] = $this->_getBannerActiveTo();
		if($banner_id)
		// IF IN EDIT MODE SHOW POSITION DROP-DOWN
		$inputs[] = $this->_getPosition($banner_id,$item,$section,$category);
		$inputs[] = $this->_getBannerStatus();

		$submitClass = '';
	}

	$inputs[] = array(
                    'type' => 'hidden',
                    'name' => 'submit-flag',
                    'id' => 'submit-flag',
                );
	
		$urltoken = Tools::getAdminTokenLite('AdminModules');

		$fields_form = array(
			'form' => array(
				'legend' => array(
						'title' => (!$banner_id) ? $this->l('Create and promote banners') : $this->l('Update banner'),
						'icon' => 'icon-cogs'
						),
				'input' => $inputs,
				
				'submit' => array(
						'title' => $this->l('Save'),
						'class' => 'btn btn-default pull-right '.$submitClass
						),
				'buttons' => array(
			                    array(
			                        'href' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&action=showbanners=&token='.$urltoken,
			                        'title' => $this->l('Back to List'),
			                        'icon' => 'process-icon-back',
			                    )
		                ),
					)
				);

		foreach ($inputs as $key => $input)
		{
			//if($input['name'])
			//$empty_values[$input['name']] = (!Tools::getValue($input['name'])) ? 0 : Tools::getValue($input['name']);
			//else if($input['name']='sub-category')
			//$empty_values[$input['sub-category[]']] = ($this->_errors && Tools::getValue('sub-category')) ? Tools::getValue('sub-category') : '';
			if($input['name']=='submit-flag'){
			$empty_values[$input['name']] = 0;
		}
			else if($input['name']=='use-cms-content'){
			if(!$updateFlag || $this->_errors)
			$empty_values[$input['name']] = (Tools::getValue($input['name'])) ? Tools::getValue($input['name']) : 0;
			else
			$empty_values[$input['name']] = $applyforall;
		}
			else if($input['name']=='applyforall'){
			if(!$updateFlag || $this->_errors)
			$empty_values[$input['name']] = (Tools::getValue($input['name'])) ? Tools::getValue($input['name']) : 0;
			else
			$empty_values[$input['name']] = $applyforall;
		}
			else if($input['name']=='cms-category'){
			if(!$updateFlag || $this->_errors)
			$empty_values['cms-category[]'] = (!Tools::getValue($input['name'])) ? '' : Tools::getValue($input['name']);
			else
			$empty_values['cms-category[]'] = $category;
		}
			else if($input['name']=='cms-content'){
			if(!$updateFlag || $this->_errors)
			$empty_values['cms-content[]'] = (!Tools::getValue($input['name'])) ? '' : Tools::getValue($input['name']);
			else
				//ddd($this->updateArr[$input['name']]);
			$empty_values['cms-content[]'] = $this->updateArr[$input['name']];
		}
			else if($input['name']=='category'){
			if(!$updateFlag || $this->_errors)
			$empty_values['category'] = (!Tools::getValue($input['name'])) ? '' : Tools::getValue($input['name']);
			else
				//ddd($this->updateArr[$input['name']]);
			$empty_values['category'] = $category;
		}
			else{
			if(!$updateFlag || $this->_errors)
			$empty_values[$input['name']] = (!Tools::getValue($input['name'])) ? '' : Tools::getValue($input['name']);
			else
			$empty_values[$input['name']] = $this->updateArr[$input['name']];
			}
		}


		//ddd($empty_values);
		$action = '';
		if($banner_id)
		$action = '&id_banner='.$banner_id;

		$helper = new HelperForm();
		$helper->submit_action = 'submitManageBanner';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.$action;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $empty_values
			);
		
		//if($_POST)
		//$helper->tpl_vars['fields_value']['promote_to_page[]'] = array(0 => 'cms_1',1 => 'cms_2');

		return $helper->generateForm(array($fields_form));
	}

	private function _checkBannerPos($bannerid=false,$item=false,$section=false,$catid=false,$allflag,$cmsid=false,$position=false){

		if($bannerid)
		$whereClause = "AND b.id_banner != ".$bannerid;
		if($item)
		$whereClause .= " AND a.item = '".$item."'";
		if($section)
		$whereClause .= " AND a.section = '".$section."'";
		if($catid)
		$whereClause .= " AND b.id_cat = ".$catid;

		$whereClause .= " AND b.all_flag = ".$allflag;

		if($cmsid)
		$whereClause .= " AND b.id_cms = ".$cmsid;

		if(!$position){
		$sql = "SELECT max(b.position) FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner ".$whereClause;
		$newposition = Db::getInstance()->getValue($sql);
		}
		else{
		$whereClause .= " AND b.position = ".$position;
		$sql = "SELECT * FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner ".$whereClause; 
		$newposition = Db::getInstance()->getRow($sql);
		}

		//$newposition = Db::getInstance()->getValue($sql);
		//$position = $sql;
		//ddd($sql);
		return $newposition;

	}

	public function _getCmsContent($cat){

		$content = array();

		if(count($cat)){

		$catStr = " AND d.id_category IN ('".implode("','", $cat)."')";

		$sql = 'SELECT a.id_cms, b.meta_title ,c.name as catcms, d.id_category, d.name as catprod
			FROM '._DB_PREFIX_.'cms a, '._DB_PREFIX_.'cms_lang
 b, '._DB_PREFIX_.'cms_category_lang c, '._DB_PREFIX_.'category_lang d WHERE a.id_cms = b.id_cms and a.id_cms_category = c.id_cms_category and a.cat_assoc = d.id_category and a.id_cms_category != 1 '.$catStr; 

 		//ddd($sql);

 		$tempResult = Db::getInstance()->ExecuteS($sql);

 		foreach ($tempResult as $key => $value) {

 			$temp = array();
 			$titleSuffix = ' &#40; '.$value['catcms'].'&#44; '.$value['catprod'].' &#41; ';
 			$temp['id_cms'] = $value['id_cms'].'-'.$value['id_category']; // combining category id into this for further uses
 			$temp['title'] = $value['meta_title'].$titleSuffix;
 			$content[] = $temp;
 		}
 	}

 		return $content;

	}

	private function _getCmsCategory(){

		$sql = 'SELECT d.id_category, d.name as catprod
			FROM '._DB_PREFIX_.'cms a, '._DB_PREFIX_.'category_lang d WHERE a.cat_assoc = d.id_category GROUP BY d.id_category'; // 1 is home category
 					
 		$tempResult = Db::getInstance()->ExecuteS($sql);

 		return $tempResult;
	}

	public function generateForm1(){

			$item = Tools::getValue('item');

			$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Item',
                    'desc' => 'Select Page/Place where banner will be promoted',
                    'name' => 'item',
                    'id' => 'item',
                    'options' => array(
							'default' => array('value' => '', 'label' => $this->l('Choose one')),
							'query' => $this->getItems(),
							'id' => 'id',
							'name' => 'name'
						),
                );

			if($item && $item=='menu')
			{
				$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Category',
                    'desc' => 'Select in which category banner will be promoted',
                    'name' => 'category',
                    'options' => array(
							'default' => array('value' => 0, 'label' => $this->l('Select one')),
							'query' => $this->getCategory(),
							'id' => 'id',
							'name' => 'name'
						),
                );
			}
			else if($item){
				$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select section',
                    'desc' => 'Select in which section banner will be promoted',
                    'name' => 'sub-item',
                    'id' => 'item-child-2',
                    'options' => array(
                    		'default' => array('value' => '', 'label' => $this->l('Select one')),
							'query' => $this->getSubItems($item),
							'id' => 'id',
							'name' => 'name'
						),
                );
			}
		
		$inputs[] = array(
                    'type' => 'hidden',
                    'name' => 'submit-flag',
                    'id' => 'submit-flag',
                );

		$urltoken = Tools::getAdminTokenLite('AdminModules');

		$fields_form = array(
			'form' => array(	
				'legend' => array(
						'title' => $this->l('Get Promoted Banners'),
						'icon' => 'icon-cogs'
						),
				'input' => $inputs,

				'submit' => array(
						'title' => $this->l('Search'),
						'class' => 'btn btn-default pull-right'
						),
				'buttons' => array(
			                    array(
			                        'href' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$urltoken,
			                        'title' => $this->l('Add New Banner'),
			                        'icon' => 'process-icon-new',
			                    )
		                ), 
					)
				);

		foreach ($inputs as $input)
		{
			if($input['name']=='submit-flag')
			$empty_values[$input['name']] = '';
			else
			$empty_values[$input['name']] = (!Tools::getValue($input['name'])) ? '' : Tools::getValue($input['name']);
		}

		$helper = new HelperForm();
		$helper->submit_action = 'submitSearchBanner';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&action=showbanners';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->show_toolbar = true;
		$helper->tpl_vars = array(
			'fields_value' => $empty_values
			);
		
		//if($_POST)
		//$helper->tpl_vars['fields_value']['promote_to_page[]'] = array(0 => 'cms_1',1 => 'cms_2');

		return $helper->generateForm(array($fields_form));
	}




	public function getItems($getName=false)
	{
		$pageStr = "Menu,Home,Category,CMS,Product";
		$itemArr = explode(",",$pageStr);
		$outPut = false;

		if(!$getName){

				foreach ($itemArr as $key => $value) {
				// # code...
				$temp = array();
				$arryKey = preg_replace('/\s+/','-', preg_replace("/[^ \w]+/", " ", $value));
				$arryKey = strtolower($arryKey);
				$temp['id'] = $arryKey;
				$temp['name'] = $value;
				$itemArr[$key] = $temp;
			}

			$outPut = $itemArr;
		}
		else
		{
			foreach ($itemArr as $key => $value) {
				// # code...
				$arryKey = preg_replace('/\s+/','-', preg_replace("/[^ \w]+/", " ", $value));
				$arryKey = strtolower($arryKey);
				if($getName == $arryKey){
				$outPut = $value;
				break;
				}
			}
		}
			
		return $outPut;
	}
	public function getSubItems($item,$getName=false)
	{
		$itemArr = array();
		$subItems['home'] = array('Latest News','Video Section');
		$subItems['category'] = array('Latest News','Video Section');
		$subItems['cms'] = array('Related News');

		$arrayToProcess = $subItems[$item];

		$outPut = false;

		if(!$getName)
		{
			foreach ($arrayToProcess as $value) {
				// # code...
				$temp = array();
				$arryKey = preg_replace('/\s+/','-', preg_replace("/[^ \w]+/", " ", $value));
				$arryKey = strtolower($arryKey);
				$temp['id'] = $arryKey;
				$temp['name'] = $value;
				$itemArr[] = $temp;
			}
			$outPut = $itemArr;
		}
		else
		{
				foreach ($arrayToProcess as $value) {
					// # code...
					$arryKey = preg_replace('/\s+/','-', preg_replace("/[^ \w]+/", " ", $value));
					$arryKey = strtolower($arryKey);
					if($getName == $arryKey){
					$outPut = $value;
					break;
					}
				}
		}

		return $outPut;
	}

	private function _verifySize($item,$sizeArr){

		$validSize = array();
		$isValidSize = false;
		$validSize['menu'] = array('max-h' => 337,'max-w' => 686);
		$validSize['latest-news'] = array('max-h' => 163,'max-w' => 296);
		$validSize['video-section'] = array('max-h' => 360,'max-w' => 450);
		$validSize['related-news'] = array('max-h' => 0,'max-w' => 295);

		$imgWidth = $sizeArr[0];
		$imgHeight = $sizeArr[1];

		$validWidth = $validSize[$item]['max-w'];
		$validHeight = $validSize[$item]['max-h'];

		if(($imgWidth<=$validWidth) && ($validHeight>=0 || $imgHeight<=$validHeight))
			$isValidSize = true;


		return $isValidSize;

	}

	public function getCategory($parentID=2){

		// GET ALL CATEGORY WHERE PARENT IS HOME
		$sql = 'SELECT a.id_category as id, b.name as name FROM '._DB_PREFIX_.'category a ,'._DB_PREFIX_.'category_lang b where a.id_category = b.id_category AND (a.id_parent =  '.$parentID.') AND b.id_lang = '.(int)$this->context->language->id;

		$tempResult = Db::getInstance()->ExecuteS($sql);

		return $tempResult;
	}

	private function _getImageInput($edit=false){

		return array(
                    'type' => 'file',
                    'label' => 'Banner File',
                    'desc' => 'Note:',
                    'required' => (!$edit) ?  true : false,
                    'name' => 'banner_img'
                );
	}

	private function _getUrlInput(){

		return array(
                    'type' => 'text',
                    'label' => 'URL',
                    'desc' => 'Link of the page/promotion, leave it empty/blank if no URL',
                    'name' => 'banner_url'
                );
	}

	private function _getBannerActiveFrom(){

		return array(
                    'type' => 'date',
                    'label' => 'Active From',
                    'desc' => 'Set the start date for banner',
                    'name' => 'active_from'
                );
	}

	private function _getBannerActiveTo(){

		return array(
                    'type' => 'date',
                    'label' => 'Active To',
                    'desc' => 'Set the end date for banner',
                    'name' => 'active_to'
                );
	}

	private function _getBannerStatus(){

		return array(
                    'type' => 'switch',
                    'label' => 'Active',
                    'name' => 'status',
                    'values' => array(
                        array(
                            'id' => 'on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'off',
                            'value' => 0
                        )
                    )
                );
	}

	

	private function _generateBannerList($list,$count){


		$content = $list;

		foreach($content as $key => $value)
		{
			$value['is_sub_cat'] = '';
			$value['is_cms'] = '';
			$value['is_prod'] = '';
			$value['image'] = '<img class="banner-row-img" src="'.$this->_path."views/img/".$value['image'].'">';

			if($value['item']=='menu'){
				if($value['all_flag']==1){
					$value['is_sub_cat'] = 'Yes';
				}
				else
				{
					$value['is_sub_cat'] = 'No';
				}
			}
			else if($value['item']=='cms'){
				if($value['all_flag']==1){
					$value['is_cms'] = 'Yes';
				}
				else
				{
					$value['is_cms'] = 'No';
				}
			}

			// CHECK FOR MULTIPLE POSITIONS FOR BANNERS IF EXISTS
			/*$position_edit = $this->_checkForPosition($value);

			if($position_edit)
			{
				$opt = '<select class="hide-first-load">';
				foreach ($position_edit as $key => $value) {
					// # code...
					$opt .= '<option value="'.$value['position'].'">'.$value['position'].'</option>';
				}

				$opt .= '</select>';
			}

			$value['position_edit'] = $opt;*/

			$content[$key] = $value;
		}
		
		$fields_list = array(
		

			'id_banner' => array(
				'search' => false,
            	'orderby' => false,
            	'visible'=> false,
            	'class' => 'hide-first-load'
			),
			'image' => array(
				'title' => 'Banner',
				'type' => 'bool',
				'search' => false,
            	'orderby' => false,
            	'float' => true,
			),
			'item' => array(
				'title' => 'Item',
				'search' => false,
				'orderby' => false
			),
			'section' => array(
				'title' => $this->l('Section'),
				'filter_key' => 'section',
				'orderby' => false
			),
			'catname' => array(
				'title' => $this->l('Category'),
				'orderby' => false
			),
			'is_sub_cat' => array(
				'title' => $this->l('All Sub Category'),
				'search' => false,
				'orderby' => false
			),
			'is_cms' => array(
				'title' => $this->l('All CMS'),
				'search' => false,
				'orderby' => false
			),
			'is_prod' => array(
				'title' => $this->l('All Product'),
				'search' => false,
				'orderby' => false
			),
			'active_from' => array(
				'title' => $this->l('Active From'),
				'type' => 'date',
			),
			'active_to' => array(
				'title' => $this->l('Active To'),
				'type' => 'date',
			),
			'status' => array(
				'title' => $this->l('Status'),
				'active' => 'toggle',
                'type' => 'bool',
                'align' => 'center',
                'orderby' => false,
                'filter_type' => 'bool'
			),
			'position' => array(
				'title' => $this->l('Position'),
				'prefix' => '#',
				'orderby' => false
			),
			
		);


		$filters = '';

		// SET MESSAGE FOR UPDATING THE STATUS OF BANNER
		$statusToggle = $this->toggleStatus;
		if($statusToggle)
		$filters .= '&conf=4';

		if(count($this->bannerFilters))
		{
			foreach ($this->bannerFilters as $key => $value) {
				//# code...
				if($value)
					$filters .= '&'.$key.'='.$value;
			}
		}


		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->module = $this;
		$helper->listTotal = $count;
		$helper->table = 'banner_promotion';
		$helper->identifier = 'id_banner';
		$helper->actions = array('edit', 'delete', 'view');
		$helper->simple_header = false;
		$helper->title = $this->l('List of Banners');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name .'&module_name=' . $this->name.'&action=showbanners'.$filters;
		$helper->toolbar_scroll = false;
		//$helper->bulk = true;

		return $helper->generateList($content, $fields_list);
	}

	public function getBannerList($item,$section,$cat){

		$result = array();

		$pageNo = (int) (!Tools::getValue('submitFilterbanner_promotion')) ? 1 : Tools::getValue('submitFilterbanner_promotion');

		$end = (!Tools::getValue('banner_promotion_pagination')) ? 50 : Tools::getValue('banner_promotion_pagination');

		$start = ($pageNo-1) * $end;

		$paginateData = " LIMIT $start, $end";


		$whereClause = '';
		$orderBY = '';

		if($section)
			$whereClause .= " AND a.section = '".$section."'";
		if($cat)
			$whereClause .= " AND b.id_cat = ".$cat;

		if(Tools::isSubmit('submitFilterbanner_promotion')){

			if(Tools::getValue('banner_promotionFilter_catname')){
				$whereClause .= " AND c.name LIKE '".Tools::getValue('banner_promotionFilter_catname')."%'";
			}
			if(is_numeric(Tools::getValue('banner_promotionFilter_status'))){
				$whereClause .= " AND a.status = ".Tools::getValue('banner_promotionFilter_status');
			}
			if(Tools::getValue('banner_promotionFilter_section')){
				$whereClause .= " AND a.section LIKE '".Tools::getValue('banner_promotionFilter_section')."%'";
			}
			if(Tools::getValue('banner_promotionFilter_position')){
				$whereClause .= " AND b.position = ".Tools::getValue('banner_promotionFilter_position');
			}

			if(Tools::getValue('banner_promotionFilter_active_from')[0] && Tools::getValue('banner_promotionFilter_active_from')[1])
				$whereClause .= " AND a.active_from BETWEEN '".Tools::getValue('banner_promotionFilter_active_from')[0]."' AND '".Tools::getValue('banner_promotionFilter_active_from')[1]."'";
			else if(Tools::getValue('banner_promotionFilter_active_from')[0])
				$whereClause .= " AND a.active_from = '".Tools::getValue('banner_promotionFilter_active_from')[0]."'";

			if(Tools::getValue('banner_promotionFilter_active_to')[0] && Tools::getValue('banner_promotionFilter_active_to')[1])
				$whereClause .= " AND a.active_to BETWEEN '".Tools::getValue('banner_promotionFilter_active_to')[0]."' AND '".Tools::getValue('banner_promotionFilter_active_to')[1]."'";
			else if(Tools::getValue('banner_promotionFilter_active_to')[0])
				$whereClause .= " AND a.active_to = '".Tools::getValue('banner_promotionFilter_active_to')[0]."'";

		}

			if(Tools::getValue('banner_promotionOrderby')){

				$orderBY = " ORDER BY a.".Tools::getValue('banner_promotionOrderby').' '.Tools::getValue('banner_promotionOrderway');
			}
			

			$sql = "SELECT a.*,b.*,c.name as catname FROM "._DB_PREFIX_."banner_promotion as a INNER JOIN "._DB_PREFIX_."banner_promotion_section as b ON a.id = b.id_banner LEFT JOIN "._DB_PREFIX_."category_lang as c ON b.id_cat = c.id_category WHERE a.item = '".$item."'".$whereClause." GROUP BY a.id".$orderBY.$paginateData;

			$cntSql = "SELECT a.id FROM "._DB_PREFIX_."banner_promotion as a INNER JOIN "._DB_PREFIX_."banner_promotion_section as b ON a.id = b.id_banner LEFT JOIN "._DB_PREFIX_."category_lang as c ON b.id_cat = c.id_category WHERE a.item = '".$item."'".$whereClause." GROUP BY a.id";

			//$sql = "SELECT a.*,b.*,c.meta_title as cmstitle ,d.name as catname, e.name as subcatname,f.name as prodname FROM ps_banner_promotion as a INNER JOIN "._DB_PREFIX_."banner_promotion_section as b ON a.id = b.id_banner LEFT JOIN "._DB_PREFIX_."cms_lang as c ON b.id_cms = c.id_cms LEFT JOIN "._DB_PREFIX_."category_lang as d ON b.id_cat = d.id_category LEFT JOIN "._DB_PREFIX_."category_lang as e ON b.id_sub_cat = e.id_category LEFT JOIN "._DB_PREFIX_."product_lang as f ON b.id_product = f.id_product WHERE a.item = '".$item."'".$whereClause " GROUP BY a.id";


			$result['result'] = Db::getInstance()->ExecuteS($sql);
			$result['count'] = count(Db::getInstance()->ExecuteS($cntSql));

			return $result;

		}

	private function _setBannerView($id_banner){

		//ddd($this->bannerFilters);

		if($id_banner){
		$sql = "SELECT a.*,b.*,c.meta_title as cmstitle ,d.name as catname, e.name as subcatname,f.name as prodname FROM ps_banner_promotion as a INNER JOIN "._DB_PREFIX_."banner_promotion_section as b ON a.id = b.id_banner LEFT JOIN "._DB_PREFIX_."cms_lang as c ON b.id_cms = c.id_cms LEFT JOIN "._DB_PREFIX_."category_lang as d ON b.id_cat = d.id_category LEFT JOIN "._DB_PREFIX_."category_lang as e ON b.id_sub_cat = e.id_category LEFT JOIN "._DB_PREFIX_."product_lang as f ON b.id_product = f.id_product WHERE a.id =". $id_banner;

		$catArr = array();
		$subCatArr = array();
		$cmsPage = array();
		$prodPage  = array();
		$bannerInfo = array();


		$result = Db::getInstance()->ExecuteS($sql);

		foreach ($result as $key => $value) {
			// # code...
			if($value['catname'])
				$catArr[] = $value['catname'];

			if(!$value['all_flag'])
			{
				if($value['subcatname'])
					$subCatArr[] = $value['subcatname'];

				if($value['cmstitle'])
					$cmsPage[] = $value['cmstitle'];

				if($value['prodname'])
					$prodPage[] = $value['prodname'];
			}
		}

		$bannerInfo['image'] = $result[0]['image'];
		$bannerInfo['item'] = $this->getItems($result[0]['item']);

		$bannerInfo['section'] = $result[0]['section'];
		if($result[0]['section'])
		$bannerInfo['section'] = $this->getSubItems($result[0]['item'],$result[0]['section']);

		$bannerInfo['active_from'] = $result[0]['active_from'];
		$bannerInfo['active_to'] = $result[0]['active_to'];
		$bannerInfo['status'] = $result[0]['status'];
		$bannerInfo['position'] = $result[0]['position'];
		
		$bannerInfo['category'] = implode(", ", array_unique($catArr));
		$bannerInfo['sub-categories'] = implode(", ", array_unique($subCatArr));
		$bannerInfo['applyforall-sub-categories'] = ($result[0]['item']=='menu' && count($subCatArr)<=0 && $result[0]['all_flag']) ? 'Yes' : '';
		$bannerInfo['cms'] = implode(", ", $cmsPage);
		$bannerInfo['applyforall-cms'] = ($result[0]['item']=='cms' && count($cmsPage)<=0 && $result[0]['all_flag']) ? 'Yes' : '';
		$bannerInfo['product'] = implode(", ", $prodPage);
		$bannerInfo['applyforall-prod'] = ($result[0]['item']=='product' && count($prodPage)<=0 && $result[0]['all_flag']) ? 'Yes' : '';

		$this->context->smarty->assign(array('bannerRes' => $bannerInfo,'base' => $this->_path));
		$this->html = $this->display(__FILE__,"showbanner.tpl");

		}
		//ddd($sql);

	}

	private function _getBannerView($id_banner){

		//ddd($this->bannerFilters);

		if($id_banner){
		$sql = "SELECT a.*,b.* FROM "._DB_PREFIX_."banner_promotion as a INNER JOIN "._DB_PREFIX_."banner_promotion_section as b ON a.id = b.id_banner WHERE a.id =". $id_banner;

		$catArr = array();
		$subCatArr = array();
		$cmsPage = array();
		$prodPage  = array();
		$bannerInfo = array();


		$result = Db::getInstance()->ExecuteS($sql);

		foreach ($result as $key => $value) {
			// # code...
			if($value['id_cat'])
				$catArr[] = $value['id_cat'];

			if(!$value['all_flag'])
			{
				if($value['id_sub_cat'])
					$subCatArr[] = $value['id_sub_cat'];

				if($value['id_cms'])
					$cmsPage[] = $value['id_cms'].'-'.$value['id_cat'];

				if($value['id_product'])
					$prodPage[] = $value['id_product'];
			}
		}

		$bannerInfo['id_banner'] = $result[0]['id'];
		$bannerInfo['banner_img'] = $result[0]['image'];
		$bannerInfo['item'] = $result[0]['item'];
		$bannerInfo['sub-item'] = $result[0]['section'];
		$bannerInfo['active_from'] = $result[0]['active_from'];
		$bannerInfo['active_to'] = $result[0]['active_to'];
		$bannerInfo['status'] = $result[0]['status'];
		$bannerInfo['position'] = $result[0]['position'];
		$bannerInfo['applyforall'] = $result[0]['all_flag'];
		$bannerInfo['banner_url'] = $result[0]['url'];
		
		$bannerInfo['category'] = array_unique($catArr);
		$bannerInfo['sub-category'] = array_unique($subCatArr);
		$bannerInfo['cms-content'] = $cmsPage;
		$bannerInfo['product'] = $prodPage;
		

		//$this->context->smarty->assign(array('bannerRes' => $bannerInfo,'base' => $this->_path));
		//$this->html = $this->display(__FILE__,"showbanner.tpl");

		}
		
		return $bannerInfo;

	}

	public function _checkForPosition($arr){

		$whereClause = '';
		$item = $arr['item'];
		$banner_id = $arr['id_banner'];
		$allflag = $arr['all_flag'];
		$pos = $arr['position'];

		if($arr['section'])
		$whereClause .= " AND a.section = '".$arr['section']."'";

		if($item=='menu'){

			if($arr['id_cat'] && $allflag){
			$whereClause .= " AND b.id_cat = '".$arr['id_cat']."'";
			}
			else
			{
				// select * sub-categories of this banner
				$selSubCat = "SELECT id_sub_cat FROM "._DB_PREFIX_."banner_promotion_section WHERE id_banner = ".$banner_id;

				$subCatArr = Db::getInstance()->ExecuteS($selSubCat);
				$subCatArrTemp = array();
				foreach ($subCatArr as $key => $value) {
					// # code...
					$subCatArrTemp[] = $value['id_sub_cat'];
				}
				$whereClause .= " AND b.id_sub_cat IN ('".implode("','", array_unique(array_values($subCatArrTemp)))."')";
			}
		}
		if($item=='cms'){

				if($arr['id_cat']){
					// select * categories of this banner
					$selCat = "SELECT id_cat FROM "._DB_PREFIX_."banner_promotion_section WHERE id_banner = ".$banner_id;

					$catArr = Db::getInstance()->ExecuteS($selCat);
					$catArrTemp = array();
					foreach ($catArr as $key => $value) {
					// # code...
					$catArrTemp[] = $value['id_cat'];
					}
					$whereClause .= " AND b.id_cat IN ('".implode("','", array_unique(array_values($catArrTemp)))."')";
				}

				if(!$allflag)
				{
					// select * categories of this banner
					$selCMS = "SELECT id_cms FROM "._DB_PREFIX_."banner_promotion_section WHERE id_banner = ".$banner_id;

					$cmsArr = Db::getInstance()->ExecuteS($selCMS);
					$cmsArrTemp = array();
					foreach ($cmsArr as $key => $value) {
					// # code...
					$cmsArrTemp[] = $value['id_cms'];
					}
					$whereClause .= " AND b.id_cms IN ('".implode("','", array_unique(array_values($cmsArrTemp)))."')";
				}
		}
		

		$sql = "SELECT b.position FROM "._DB_PREFIX_."banner_promotion a, "._DB_PREFIX_."banner_promotion_section b WHERE a.id = b.id_banner AND a.id != $banner_id AND a.item = '".$item."' AND b.all_flag = ".$allflag.$whereClause. " GROUP BY a.id"; 

		$position = Db::getInstance()->ExecuteS($sql);

		return $position;
	}

	public function _getPosition($banner_id,$item,$section,$category)
	{
		if($section)
		$result = $this->getBannerList($item,$section,false);
		else
		$result = $this->getBannerList($item,false,$category);

		$default = array();
		$opt = array();

		foreach ($result as $key => $value) {
			//# code...
			if($value['id']==$banner_id){
			$default['value'] = $value['position'];
			$default['label'] = $value['position'];
			}
			else
			{
				$temp = array();
				$temp['id'] = $value['position'];
				$temp['name'] = $value['position'];
				$opt[] = $temp;
			}
		}

		return array(
                    'type' => 'select',
                    'label' => 'Position',
                    'desc' => 'Select position of banner',
                    'name' => 'position',
                    'options' => array(
							'default' => $default,
							'query' => $opt,
							'id' => 'id',
							'name' => 'name'
						),
                );
	}


} // end of class
