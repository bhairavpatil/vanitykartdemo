<div id="moduledemo" class="block">
	<p class="title_block">{l s='Whatever title' mod='moduledemo'}</p>	
	<div class="block_content">
		<p>
			<a id="mytrigger" href="javascript:void(0)" {if $switchcolor}style="background-color:{$switchcolor}"{/if}>{l s='Click to Toggle' mod='moduledemo'}</a>
		</p>
		<div id="this_to_be_opened" {if $dynamic_switch}style="display:none"{/if}>
			{if $custom_img}
				<img src="{$base_dir_ssl}/modules/{$custom_img}">
			{/if}
			{$my_special_text}
		</div>
	</div>
</div>

{addJsDef dynamic_switch=$dynamic_switch}