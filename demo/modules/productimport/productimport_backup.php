<?php

if(!defined('_PS_VERSION_'))
	exit;

class ProductImport extends Module
{

	private $_html = '';

	public function __construct()
	{
		$this->name = 'productimport';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Nemo';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

		$this->need_instance = 0;
		$this->bootstrap = true;

		$this->displayName = $this->l('Product Import');
		$this->description = $this->l('Product Import');
		parent::__construct();
	}	

	public function install()
	{
		if(
			!parent::install() OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('displayLeftColumn')
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if(!parent::uninstall() OR
			!Configuration::deleteByName('MODULEDEMO_TEST_TEXT')
			)
			return false;
		return true;
	}

	public function getContent()
	{
		$this->_postProcess();
		$this->_displayForm();
		return $this->_html;
	}


	private function _postProcess()
	{
		if(Tools::isSubmit('submitUpdate'))
		{


			move_uploaded_file($_FILES['csvfile']['tmp_name'], AdminImportController::getPath().$filename_prefix.str_replace("\0", '', $_FILES['csvfile']['name']));

			// ddd(AdminImportController::getPath()."tmp/".$filename_prefix.str_replace("\0", '', $_FILES['csvfile']['name']));

            // $_FILES['csvfile']['error'] = $this->l('An error occurred while uploading / copying the file.');


			$error = '';
			if($_FILES)
			{
				$handle = $this->openCsv();

				// d($handle);
				$helper = new HelperImageUploader('mycustomimg');
				$files = $helper->process();

				// ddd($files);

			}


		} 
	}


	public function openCsv()
    {
        $file = AdminImportController::getPath(strval(preg_replace('/\.{2,}/', '.', Tools::getValue('filename'))));
        $handle = fopen($file, 'r');

			

        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, ","); $current_line++) {

            $linenew = $line;
			if ($current_line>1)
			{
				$linenew[1] = $line[9];
				$linenew[2] = $line[6];
				$linenew[3] = $line[4].",".$line[5];
				$linenew[4] = $line[16];
				$linenew[5] = $line[56];
				$linenew[6] = $line[16];
				$linenew[7] = $line[21];
				$linenew[8] = $line[22];
				$linenew[9] = $line[23];
				$linenew[10] = $line[24];
				$linenew[11] = $line[25];
				$linenew[12] = "-";
				$linenew[13] = $line[2];
				$linenew[14] = $line[1]; // Supplier Name
				$linenew[15] = $line[3];
				$linenew[16] = $line[7];
				$linenew[17] = $line[10];
				$linenew[18] = $line[11];
				$linenew[19] = substr($line[12],0,3);//$line[12];
				$linenew[20] = substr($line[13],0,3);//$line[13];
				$linenew[21] = substr($line[14],0,3);//$line[14];
				$linenew[22] = substr($line[15],0,3);//$line[15];
				$linenew[23] = $line[26]; // Quantity

				$linenew[24] = $line[27];
				$linenew[25] = $line[38];
				$linenew[26] = 0;			//Additional shipping cost
				$linenew[27] = $line[18];
				$linenew[28] = $line[19];
				$linenew[29] = substr($line[39],0,750);
				$linenew[30] = $line[40]; // Description
				// ddd($linenew[30]);

				$linenew[31] = $line[41];   // Tags
				$linenew[32] = $line[42];
				$linenew[33] = $line[43];
				$linenew[34] = $line[44];



				$linenew[35] = $line[45];
				$linenew[36] = $line[33];
				$linenew[37] = "-";
				$linenew[38] = $line[35];

				$linenew[39] = $line[36];
				$linenew[40] = $line[37];
				$linenew[41] = $line[20];
				$linenew[42] = $line[46];
				$linenew[43] = 0;
				$linenew[44] = $line[49]; // Feature




				$linenew[45] = $line[50];
				$linenew[46] = $line[51];
				$linenew[47] = $line[52];
				$linenew[48] = $line[53];
				$linenew[49] = $line[54];
				$linenew[50] = $line[28];
				$linenew[51] = $line[29];
				$linenew[52] = $line[30];
				$linenew[53] = $line[31];
				$linenew[54] = $line[32];
				$linenew[55] = "";
				$linenew[56] = "";
				// $linenew[57] = $line[49];
            	// ddd($line);
	        	$line2[$current_line]= implode("|",$linenew);


			}
			else if ($current_line==1)
			{
	        	$line2[$current_line]= 'ID|Active (0/1)|Name *|Categories (x,y,z...)|Price tax excluded or Price tax included|Tax rules ID|Wholesale price|On sale (0/1)|Discount amount|Discount percent|Discount from (yyyy-mm-dd)|Discount to (yyyy-mm-dd)|Reference #|Supplier reference #|Supplier|Manufacturer|EAN13|UPC|Ecotax|Width|Height|Depth|Weight|Quantity|Minimal quantity|Visibility|Additional shipping cost|Unity|Unit price|Short description|Description|Tags (x,y,z...)|Meta title|Meta keywords|Meta description|URL rewritten|Text when in stock|Text when backorder allowed|Available for order (0 = No, 1 = Yes)|Product available date|Product creation date|Show price (0 = No, 1 = Yes)|Image URLs (x,y,z...)|Delete existing images (0 = No, 1 = Yes)|Feature(Name:Value:Position)|Available online only (0 = No, 1 = Yes)|Condition|Customizable (0 = No, 1 = Yes)|Uploadable files (0 = No, 1 = Yes)|Text fields (0 = No, 1 = Yes)|Out of stock|ID / Name of shop|Advanced stock management|Depends On Stock|Warehouse';
			}


        }

        $file2 = fopen("/var/www/html/vanitykart/upload/up9.csv","w");

            foreach ($line2 as $line)
			{
				fputcsv($file2,explode('|',$line), "|");
			}




fclose($file); 


        // $this->closeCsvFile($handle);

    }

	public function _displayForm()
	{

		$this->_html .= $this->_generateForm();


	}

	private function _generateForm()
	{
		$inputs = array();

		$file = '../modules/' . $this->name.'/views/img/'. Configuration::get('MODULEDEMO_CUSTOMIMG');
		$inputs[] = array(
			'type' => 'file',
			'label' => $this->l('Upload .CSV File'),
			'name' => 'csvfile',
			'hint' => $this->l('Upload .CSV file to be imported.')
			);
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
					),
				'input' => $inputs,
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submitUpdate'
					)
				)
			);

		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper = new HelperForm();
		$helper->default_form_language = $lang->id;
		// $helper->submit_action = 'submitUpdate';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		// name => value
		return array(
			'ourtext' => Configuration::getInt('MODULEDEMO_TEST_TEXT'),
			'theswitch' => Configuration::get('MODULEDEMO_SWITCH'),
			'switchcolor' => Configuration::get('MODULEDEMO_SWITCHCOLOR')
			);
	}

	public function hookDisplayHeader($params)
	{

		$this->context->controller->addCSS($this->_path . 'views/css/moduledemo.css');
		$this->context->controller->addJS($this->_path . 'views/js/moduledemo.js');
		
	}

	public function hookDisplayLeftColumn($params)
	{

		if(isset($this->context->controller->php_self) && $this->context->controller->php_self == 'category')
		{
			$allowed_categories = Configuration::get('MODULEDEMO_CATS');
			if($allowed_categories)
			{
				$allowed_categories = explode(',', $allowed_categories);
				if(!in_array(Tools::getValue('id_category'), $allowed_categories) )
					return false;
			}

			$file = Configuration::get('MODULEDEMO_CUSTOMIMG');
			$this->context->smarty->assign(array(
				'my_special_text' => Configuration::get('MODULEDEMO_TEST_TEXT', $this->context->language->id),
				'dynamic_switch' => Configuration::get('MODULEDEMO_SWITCH'),
				'switchcolor' => Configuration::get('MODULEDEMO_SWITCHCOLOR'),
				'custom_img' => file_exists( dirname(__FILE__) .'/views/img/' . $file ) ? $this->name . '/views/img/' . $file : ''
				));
			// $this->context->smarty->assign('my_special_text', Configuration::get('MODULEDEMO_TEST_TEXT'));

			return $this->display(__FILE__, 'leftColumn.tpl');	
		}

		
		// return 'Hello World!';
		// return Configuration::get('MODULEDEMO_TEST_TEXT');
	}

}