<?php

if(!defined('_PS_VERSION_'))
	exit;

class ProductImport extends Module
{

	private $_html = '';

	public function __construct()
	{
		$this->name = 'productimport';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Bhairav';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);

		$this->need_instance = 0;
		$this->bootstrap = true;

		$this->displayName = $this->l('Product Import');
		$this->description = $this->l('Product Import');
		parent::__construct();
	}	

	public function install()
	{
		if(
			!parent::install() OR
			!$this->registerHook('displayHeader') 
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if(!parent::uninstall() OR
			!Configuration::deleteByName('MODULEDEMO_TEST_TEXT')
			)
			return false;
		return true;
	}

	public function getContent()
	{
		$this->_postProcess();
		$this->_displayForm();
		return $this->_html;
	}


	private function _postProcess()
	{
		if(Tools::isSubmit('submitUpdate'))
		{


			move_uploaded_file($_FILES['csvfile']['tmp_name'], AdminImportController::getPath().$filename_prefix.str_replace("\0", '', $_FILES['csvfile']['name']));

			$error = '';
			if($_FILES)
			{
				$handle = $this->openCsv();

				// d($handle);
				$helper = new HelperImageUploader('mycustomimg');
				$files = $helper->process();

				// ddd($files);

			}

		header("Location: ?controller=AdminImport");

		} 

		// ddd(AdminImportController::getPath());

	}


	public function openCsv()
    {
        $file = AdminImportController::getPath(strval(preg_replace('/\.{2,}/', '.', Tools::getValue('filename'))));
        $handle = fopen($file, 'r');

			

        for ($current_line = 0; $line = fgetcsv($handle, MAX_LINE_SIZE, ","); $current_line++) {

            $linenew = $line;
			if ($current_line>3)
			{
				$linenew[0] = '';
				$linenew[1] = $line[13];
				$linenew[2] = $line[8];
				$linenew[3] = $line[4].",".$line[5].",".$line[6].",".$line[7];
				$linenew[4] = $line[18];
				$linenew[5] = '';
				$linenew[6] = $line[18];
				$linenew[7] = $line[21];
				$linenew[8] = $line[22];
				$linenew[9] = $line[23];
				$linenew[10] = $line[24];
				$linenew[11] = $line[25];
				$linenew[12] = "-";
				$linenew[13] = $line[2];
				$linenew[14] = $line[1]; // Supplier Name
				$linenew[15] = $line[3];
				$linenew[16] = $line[10];


				if($line[11]=='TBC')
				{
					$linenew[17] = '';	
				}
				else
				{
					$linenew[17] = $line[11];	
				}
				
				$linenew[18] = $line[12];

				$linenew[19] = substr($line[14],0,3);//$line[12];
				$linenew[20] = substr($line[15],0,3);//$line[13];
				$linenew[21] = substr($line[16],0,3);//$line[14];
				$linenew[22] = substr($line[17],0,3);//$line[15];
				$linenew[23] = $line[26]; // Quantity
				$linenew[24] = $line[27];
				$linenew[25] = $line[32];

				if($line[19]=='TBA')
				{
					$linenew[26] = '0';
				}
				else
				{
					$linenew[26] = $line[19];			//Additional shipping cost
				}
				$linenew[27] = 0;
				$linenew[28] = 0;
				$linenew[29] = substr($line[33],0,750);
				$linenew[30] = $line[34]; // Description				
				$linenew[31] = $line[35];   // Tags
				$linenew[32] = $line[36];
				$linenew[33] = $line[37];
				$linenew[34] = $line[38];



				$linenew[35] = $line[39];
				$linenew[36] = "-";
				$linenew[37] = "-";
				$linenew[38] = $line[29];
				$linenew[39] = $line[30];
				$linenew[40] = $line[31];


				$linenew[41] = $line[20];
				$linenew[42] = "tempurl";
				
				//*************************
				$linenew[43] =  $line[42];
				// $linenew[44] = substr($line[43],0,100);//$line[49]; // Feature
				// $linenew[44] = str_replace("/", "-", $line[43]); //substr($line[43],0,100);//$line[49]; // Feature

/*				$str1 = str_replace("/", " OR ", $line[43]);


				$arr1 = explode(",", $str1);

				$str2 = $arr1[0].",".$arr1[1].",".$arr1[2];*/

				// echo $str2;
				$str2 = $line[43];

				// exit;


				$linenew[44] = $str2;

				$linenew[45] = $line[45];
				$linenew[46] = $line[46];

				$linenew[47] = 0;
				$linenew[48] = $line[47];
				$linenew[49] = 0;
				$linenew[50] = $line[28];
				$linenew[51] = 0;
				$linenew[52] = 0;
				$linenew[53] = 0;
				$linenew[54] = 0;
				$linenew[55] = "";
				$linenew[56] = "";
	        	$line2[$current_line]= implode("|",$linenew);


			}
			else if ($current_line==1)
			{
	        	$line2[$current_line]= 'ID|Active (0/1)|Name *|Categories (x,y,z...)|Price tax excluded or Price tax included|Tax rules ID|Wholesale price|On sale (0/1)|Discount amount|Discount percent|Discount from (yyyy-mm-dd)|Discount to (yyyy-mm-dd)|Reference #|Supplier reference #|Supplier|Manufacturer|EAN13|UPC|Ecotax|Width|Height|Depth|Weight|Quantity|Minimal quantity|Visibility|Additional shipping cost|Unity|Unit price|Short description|Description|Tags (x,y,z...)|Meta title|Meta keywords|Meta description|URL rewritten|Text when in stock|Text when backorder allowed|Available for order (0 = No, 1 = Yes)|Product available date|Product creation date|Show price (0 = No, 1 = Yes)|Image URLs (x,y,z...)|Delete existing images (0 = No, 1 = Yes)|Feature(Name:Value:Position)|Available online only (0 = No, 1 = Yes)|Condition|Customizable (0 = No, 1 = Yes)|Uploadable files (0 = No, 1 = Yes)|Text fields (0 = No, 1 = Yes)|Out of stock|ID / Name of shop|Advanced stock management|Depends On Stock|Warehouse';
			}


        }

// ddd(AdminImportController::getPath()."up_final.csv");

        $file2 = fopen(AdminImportController::getPath()."up_final.csv","w");

            foreach ($line2 as $line)
			{
				fputcsv($file2,explode('|',$line), "|");
			}




fclose($file); 


        // $this->closeCsvFile($handle);

    }

	public function _displayForm()
	{

		$this->_html .= $this->_generateForm();


	}

	private function _generateForm()
	{
		$inputs = array();

		$file = '../modules/' . $this->name.'/views/img/'. Configuration::get('MODULEDEMO_CUSTOMIMG');
		$inputs[] = array(
			'type' => 'file',
			'label' => $this->l('Upload .CSV File'),
			'name' => 'csvfile',
			'hint' => $this->l('Upload .CSV file to be imported.')
			);
		
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
					),
				'input' => $inputs,
				'submit' => array(
					'title' => $this->l('Save'),
					'class' => 'btn btn-default pull-right',
					'name' => 'submitUpdate'
					)
				)
			);

		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper = new HelperForm();
		$helper->default_form_language = $lang->id;
		// $helper->submit_action = 'submitUpdate';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);
		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		// name => value
		return array(
			'ourtext' => Configuration::getInt('MODULEDEMO_TEST_TEXT'),
			'theswitch' => Configuration::get('MODULEDEMO_SWITCH'),
			'switchcolor' => Configuration::get('MODULEDEMO_SWITCHCOLOR')
			);
	}

	public function hookDisplayHeader($params)
	{

		$this->context->controller->addCSS($this->_path . 'views/css/moduledemo.css');
		$this->context->controller->addJS($this->_path . 'views/js/moduledemo.js');
		
	}

	

}