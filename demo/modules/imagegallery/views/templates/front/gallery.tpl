{if isset($gallery_list) && !empty($gallery_list)}
<ul>        
    {foreach name=outer from=$gallery_list key=k1 item=list}
    <li>
        <div>
            {$gallery.$k1.name}
        </div>
        <ul>
            {foreach from=$list key=k2 item=listtem}
            <div>
                <ul>{$k2} 
                	<div>
                	{if isset($listtem.title) && !empty($listtem.title)}
	               		 <li>{$listtem.title}</li>
	                {/if}
	                {if isset($listtem.description) && !empty($listtem.description)}
	               		 <li>{$listtem.description}</li>
	                {/if}
	                {if isset($listtem.target_url) && !empty($listtem.target_url)}
	               		 <li>{$listtem.target_url}</li>
	                {/if}
	                {if isset($listtem.image) && !empty($listtem.image)}
	               		 <li>{$img_ps_dir}gallery/{$listtem.image}</li>
	                {/if}	               
	              
	                </div> </br></br>
                </ul>
            </div>
            {/foreach}
        </ul>

    </li>
    {/foreach}
</ul>
{/if}