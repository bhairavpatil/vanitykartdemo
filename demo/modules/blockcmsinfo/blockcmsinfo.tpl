{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $infos|@count > 0}
<!-- MODULE Block cmsinfo -->
<style type="text/css">
	#cmsinfo_block{
		padding: 0px;
	}
	#cmsinfo_block img{
		width: 100%;
		height: 100%;
	}
	#cmsinfo_block video{
		width: 100%;
		height: 100%;
	}
	.video iframe{
		width: 100%;
	}
	#cmsinfo_block div{
		padding:0px;
	}
	#cmsinfo_block > div + div{
		border: 0px solid #000;
		padding-left: 10px;
	}
	#facebook_block, #cmsinfo_block{
		background: #fff !important;
	}
	.custom-home-feature-block1
	{
		text-align: center;
	}
	.custom-home-feature-blocktitle{
		display: inline-block;
		float: none;
		text-transform: uppercase;
		font-size: 21px;
		font-weight: 200;
	}
	.custom-home-feature-title{
		text-align: center;
		text-transform: uppercase;
		padding: 10px;
		color: black;
	}
	.custom-home-feature-subtitle{
		text-align: center;
		text-transform: uppercase;
		padding: 5px;
		font-size: 22px;
	}
	.custom-home-feature-imageicon{
		margin: 20px;
	}
</style>
<div id="cmsinfo_block">
	<div class="container">
		<div class="col-xs-7 video">{$infos[0].text}</div>
		<div class="col-xs-3 full_image">{$infos[1].text}</div>
		<div class="col-xs-2 half_image">{$infos[2].text}</div>
	</div> <!--/container-->
</div> <!--/cmsinfo_block-->
<!-- /MODULE Block cmsinfo -->

</div> 
</div> <!--/center_column-->
</div> <!--/row-->
</div> <!--/container-->

<!-- Static Section Start -->
<section class="sectionbg1">
	<div class="flower-icon1"><img src="{$img_dir}flower3.png" align="img" class="img-responsive"></div>
	<div class="flower-icon2"><img src="{$img_dir}flower2.png" align="img" class="img-responsive"></div>
	<div class="flower-icon3"><img src="{$img_dir}flower1.png" align="img" class="img-responsive"></div>
    <div class="content-section">
        <div class="container">
        	<div class="row">
                <div class="col-md-12 section-title">
                    <h2>Vanity kart is every women's new online home<br> <span>of luxury fashion, beauty and lifestyle</span></h2>
                </div> <!--/section-title-->
            </div> <!--/row-->

            <div class="row">
                <div class="col-md-12">
					<div class="col-md-4 col-sm-4">
						<div align="center"><img src="{$img_dir}delivery.png" alt="icons"></div>
						<h3 class="h3-title">Free 2 hour delivery in Dubai</h3>
						<div align="center" class="small">Orders may currently only be<br> delivered to addresses within..</div>
					</div> <!--/col-md-4-->
					<div class="col-md-4 col-sm-4">                   
						<div align="center"><img src="{$img_dir}gift.png" alt="icons"></div>
						<h3 class="h3-title">CUSTOM SALE ALERTS</h3>
						<div align="center" class="small">At VanityKart we are committed to <br>offering you the best price possible.</div>
					</div> <!--/col-md-4-->
					<div class="col-md-4 col-sm-4">                    
						<div align="center"><img src="{$img_dir}e-rewards.png" alt="icons"></div>
						<h3 class="h3-title">EXCLUSIVE REWARDS </h3>
						<div align="center" class="small">Simply visit any Participating Store <br>and our staff will be happy to..</div>
					</div> <!--/col-md-4-->
                </div> <!--/col-md-12-->
            </div> <!--/row-->
          </div> <!--/container-->
      </div> <!--/content-section-->
      	<div class="row">
	        <div class="col-md-12 text-center">
	        	<button type="button" class="btn btn-lg black btn-member">become an exclusive member today</button>
	        </div>
	    </div> <!--/row-->
 </section>
<!-- Static Section End -->

{/if}



<!-- <div class="custom-home-feature-block">
	<div class="col-md-12">
		<div class="custom-home-feature-title"><h1> VANITY KART IS THE MIDDLE EAST'S NEW ONLINE HOME</div>
		<div class="custom-home-feature-subtitle">OF LUXURY FASHION , BEAUTY AND LIFESTYLE</div>
		<div class="col-md-4 custom-home-feature-block1"> 
			<div class="custom-home-feature-imageicon"><img src="{$img_dir}home-feature-block1-icon.png"></div>
			<div class="custom-home-feature-blocktitle">free 2 hout delivery in dubai</div>
			<div class="custom-home-feature-blockdesc">this is dummy</div>
		 </div>
		<div class="col-md-4 custom-home-feature-block1">
			<div class="custom-home-feature-imageicon"><img src="{$img_dir}gift.png"></div>
			<div class="custom-home-feature-blocktitle">price match with vanitykart</div>
			<div class="custom-home-feature-blockdesc">this is dummy</div> 
		</div>
		<div class="col-md-4 custom-home-feature-block1">
			<div class="custom-home-feature-imageicon"><img src="{$img_dir}reward.png"></div>
			<div class="custom-home-feature-blocktitle">exclusive rewards</div>
			<div class="custom-home-feature-blockdesc">this is dummy</div>
		</div>
	</div>
</div> -->