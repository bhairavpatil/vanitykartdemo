<section class="related_product_sec">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 cust-review-section">


				<div class="slider_title">
					<span class="slide_text">
						<img src="{$img_dir}flower5.png" alt="flowers">
							related products
						<img src="{$img_dir}flower5.png" alt="flowers">
					</span>
				</div> <!--/slider_title-->

				<div id="recommended-products" class="jcarousel-wrapper product_slider">
				    <div class="jcarousel">
				        <ul>

				        	<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/1.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/2.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/3.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/4.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/5.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/6.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/7.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/8.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/9.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/3.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/2.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>

							<li>
				        		<div class="slider_item_sec">
									<a href="product-detail-hair-straightner.php">
										<div class="img-container img-container-related-imgs">
										<img src="{$img_dir}showcase/1.jpg" alt="thumbnails" class="img-card">
										</div> <!--/img-container-->
										<div class="card-content">
											<h4>Braun Satin Hair 7 ST750 Hair Straightener
											with color saver and IONTEC technology</h4>
											<!-- <h5>2 regular brushes, epilation</h5> -->
											<p>429 AED</p>                
										</div>
									</a>
									<div class="overlay-icon">
										<span class="show-icon show-icon1">
										<a href="javascript:void(0)"></a>
										</span>
										<span class="show-icon show-icon2">
										<a href="javascript:void(0)"></a>
										</span>
									</div>
								</div> <!--/slider_item_sec-->
							</li>
				            
				        </ul>
				    </div> <!--/jcarousel-->

				    <a href="#" class="jcarousel-control-prev"><i class="icon-chevron-left"></i></a>
				    <a href="#" class="jcarousel-control-next"><i class="icon-chevron-right"></i></a>

				    <p class="jcarousel-pagination"></p>
				</div> <!--/jcarousel-wrapper-->
			</div> <!--/col-md-12-->
		</div> <!--/row-->
	</div> <!--/container-->
</section> <!--/related_product_sec-->