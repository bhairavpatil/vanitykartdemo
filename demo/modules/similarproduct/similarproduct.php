<?php

if(!defined('_PS_VERSION_'))
	exit;

class SimilarProduct extends Module
{
	public function __construct()
	{
		$this->name = 'similarproduct';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.5' , 'max' => _PS_VERSION_);

		$this->displayName = $this->l('Similar Product');
		$this->description = $this->l('Display similar product on the basis of same category level');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('productFooter') OR
			!$this->registerHook('similarProducts')
			)
			return false;
		return true;
	}

	public function unisntall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}

	public function hookProductFooter($params)
	{	
		//ddd($params);
		$current_product_id = $params['product']->id;
		//$current_category_id = $params['category']->id;
		//$parent_category_id = $params['category']->id_parent;				
		
		$current_category_id = $params['product']->id_category_default;	
		$result = Db::getInstance()->executeS('SELECT id_parent FROM '._DB_PREFIX_.'category WHERE id_category = ' . $current_category_id);		
		$parent_category_id = $result[0]['id_parent'];

			//ddd($parent_category_id);	
		$current_product_price = Product::getPriceStatic((int)$current_product_id, true, null, 2);
		//ddd($current_product_price);
		//Calculate (+/-) 10% price using current price
		$product_price_plus =  $current_product_price  +  (($current_product_price * 10) / 100) ;
		$product_price_minus =  $current_product_price  -  (($current_product_price * 10) / 100) ;

		if(isset($parent_category_id))
			$category = new Category((int)$parent_category_id);

		$similar_product_arr = array();	

		$start = 1;
		$end = 100;
		$counter = 0;
		
		for ($i = $start; $i < $end; $i++)
		{ 					
			//Fetch Products with the same parent category level (parent of last category only)
			$category_products = $category->getProducts($this->context->language->id, $i, 100);	
			//ddd($category_products);
			if(is_array($category_products) && count($category_products))
			{
				foreach ($category_products as $key => $value) 
				{				
					//Remove current product from list
					if($value['id_product'] == $current_product_id)
					{
						unset($category_products[$key]);	
						continue;
					}

					//Remove current category products from list
					if($value['id_category_default'] == $current_category_id)
					{
						unset($category_products[$key]);	
						continue;
					}					

					//Include (+/-) 10% price range products
					//$product_price = Product::getPriceStatic((int)$value['id_product'], true, null, 2);

					//commented now for demo purpose				
					/*$product_price = $value['price'];				
					if($product_price >= $product_price_minus && $product_price <= $product_price_plus)
					{							
						$similar_product_arr[] = $category_products[$key];	
						$counter++;	

						// if($counter == 6)													
							//break 2; 						
					}*/

					$similar_product_arr[] = $category_products[$key];	
				}							
				
			}	
			else
			{
				break;
			}


		}

		//ddd($similar_product_arr);
		if(is_array($similar_product_arr) && count($similar_product_arr))
		{
			$this->context->smarty->assign(array(
					'similar_product_arr' => $similar_product_arr
				));
		}
		
		//return $this->display(__FILE__,'similarproduct.tpl');
	}

	public function hookSimilarProducts($params)
	{	
		return $this->display(__FILE__,'similarproduct.tpl');
		//return $this->display(__FILE__,'slidercode.tpl');
	} 


}