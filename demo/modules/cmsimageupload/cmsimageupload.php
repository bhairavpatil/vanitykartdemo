<?php

if(!defined('_PS_VERSION_'))
	exit;

class CmsImageUpload extends Module
{
	function __construct()
	{
		$this->name = 'cmsimageupload';
		$this->tab = 'content_management';
		$this->version = '1.0';
		$this->author = 'Sumit Jain';
		$this->need_instance = 0;

		$this->bootstrap = true;

	 	parent::__construct();

		$this->displayName = $this->l('CMS Image Upload');
		$this->description = $this->l('Upload thumbnail and banner image using default cms');
	}

	public function install()
	{
		if (!parent::install() OR
			!$this->registerHook('cmsImageDisplay') OR
			!$this->registerHook('displayHeader') OR
			!$this->alterCmsTable()
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if (!parent::uninstall() OR
			!$this->removeCmsImage() OR
			!$this->alterCmsTable('remove') OR
			!$this->removeOverrideFiles()
			)
			return false;
		return true;
	}

	public function alterCmsTable($method = 'add')
	{
		if($method == 'add'){	

			$sql = Db::getInstance()->Execute('ALTER TABLE ' . _DB_PREFIX_ . 'cms_lang ADD `thumb_image` VARCHAR(256) NOT NULL AFTER `link_rewrite`, ADD `banner_image` VARCHAR(256) NOT NULL AFTER `thumb_image`');
			$sql &= Db::getInstance()->Execute('ALTER TABLE ' . _DB_PREFIX_ . 'cms ADD `home_active` TINYINT(1) NOT NULL DEFAULT \'0\' AFTER `indexation`, ADD `cat_active` TINYINT(1) NOT NULL DEFAULT \'0\' AFTER `home_active` , ADD `cat_assoc` VARCHAR(256) NOT NULL  AFTER `cat_active`, ADD `author_name` VARCHAR(256) NOT NULL  AFTER `cat_assoc`, ADD `added_date` DATE NOT NULL  AFTER `author_name` ');

		} else {		

			$sql = Db::getInstance()->Execute('ALTER TABLE ' . _DB_PREFIX_ . 'cms_lang DROP `thumb_image`, DROP `banner_image`');
			$sql &= Db::getInstance()->Execute('ALTER TABLE ' . _DB_PREFIX_ . 'cms DROP `home_active`, DROP `cat_active` , DROP `cat_assoc`  , DROP `author_name`  , DROP `added_date`');
		}	
		
		return $sql;
	}

	public function removeCmsImage()
	{
		$db = Db::getInstance();
        $sql = 'SELECT thumb_image,banner_image FROM '._DB_PREFIX_.'cms_lang';
        $db_result = $db->executeS($sql);
       	foreach ($db_result as $key) {       		
	       	if( isset($key['thumb_image']) && !empty($key['thumb_image']) ) {		        	
	        	unlink(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$key['thumb_image']);
	        }
	        if( isset($key['banner_image']) && !empty($key['thumb_image']) ) {
	        	unlink(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$key['banner_image']);
	        }
       	}

       return true;	
	}

	public function hookDisplayHeader()
	{
	  $this->context->controller->addCSS($this->_path.'/views/css/cmspagecontent.css', 'all');
	}	
	
	public function removeOverrideFiles()
	{		  
		  unlink(_PS_ROOT_DIR_.'/override/controllers/admin/AdminCmsController.php');
		  unlink(_PS_ROOT_DIR_.'/override/classes/CMS.php');
		  unlink(_PS_ROOT_DIR_.'/cache/class_index.php');
	}

	//display image in front end cms
	public function hookCmsImageDisplay($params)
	{		
		$image_result = Db::getInstance()->getRow('SELECT thumb_image, banner_image FROM '._DB_PREFIX_.'cms_lang WHERE id_cms = ' . (int)Tools::getValue('id_cms'));

		$category_name = Db::getInstance()->getRow('SELECT pcl.name FROM '._DB_PREFIX_.'cms pc, '._DB_PREFIX_.'category_lang pcl where pc.cat_assoc = pcl.id_category and pc.id_cms = ' . (int)Tools::getValue('id_cms'));
		
		$this->context->smarty->assign(
									array(
										'thumb_image' => $image_result['thumb_image'], 
										'banner_image' => $image_result['banner_image'], 
										'cat_name' => $category_name['name'], 
									));
		//ddd($this->context->smarty);
		return $this->display(__FILE__, 'cmsimagedisplay.tpl'); 
	}

}