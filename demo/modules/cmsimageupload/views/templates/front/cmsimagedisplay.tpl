<div class="container">
	<div class="row">
		<div class="col-lg-8 col-md-8 col-sm-8">
			<div class="article-name">
				{if isset($cat_name) && $cat_name}
					{$cat_name}
				{/if}
			</div> <!--/article-name-->
			
			<h2 class="article-title">
				{if isset($cms->meta_title) && $cms->meta_title}
					{$cms->meta_title}
				{/if}
			</h2> <!--/article-title-->
			
			<p class="short-desp">
				{if isset($cms->meta_description) && $cms->meta_description}
					{$cms->meta_description}
				{/if}
			</p> <!--/short-desp-->
			
			<div class="social-icons-wrap clearfix">
				<div class="row">
					<div class="col-md-4">
					  <div class="small-title">
						{if isset($cms->author_name) && $cms->author_name}
							{$cms->author_name}
						{/if}
					  </div> <!--/small-title-->
					  
					  <div class="date-title">
						{if isset($cms->added_date) && $cms->added_date}
							{$cms->added_date|date_format:"%B %e %Y"}
						{/if}
					  </div> <!--/date-title-->
					  
					</div> <!--/col-md-3-->
					
					<div class="col-md-8 social-links text-right">					
						{hook h="displayRightColumnProduct"}
					</div> <!--/col-md-8-->
				</div> <!--/row-->
				
			</div> <!--/social-icons-wrap-->
			
			{$cms->content}
			
		</div> <!--/col-md-8-->
		
		<div class="col-md-1">
		</div> <!--/col-md-1-->
		
		<div class="col-md-3">
			{hook h="reletedarticleright"}
		</div> <!--/col-md-3-->

	</div> <!--/row-->


<!-- <div class="row">
	<div class="col-lg-12 col-md-12 article-btm-data">
		
	</div> 
</div> -->


<!-- User Comment Section Start -->

{hook h="reviewsOnCmsPage"}

<!-- User Comment Section End -->
</div> <!--/container-->

<!-- Hot Product Section Start -->
{hook h="hotProducts"}
<!-- Hot Product Section End -->



<!-- {if isset($cms->thumb_image) && $cms->thumb_image}
    <img src="{$img_ps_dir}/cms/{$cms->thumb_image}">
{/if}
{if isset($cms->banner_image) && $cms->banner_image}
    <img src="{$img_ps_dir}/cms/{$cms->banner_image}">
{/if} -->


<!-- {if isset($thumb_image) && $thumb_image}
    <img src="{$img_ps_dir}/cms/{$thumb_image}">
{/if}
{if isset($banner_image) && $banner_image}
    <img src="{$img_ps_dir}/cms/{$banner_image}">
{/if} -->


