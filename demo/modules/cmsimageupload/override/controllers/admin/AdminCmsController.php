<?php
class AdminCmsController extends AdminCmsControllerCore
{
	public function renderForm()
	{		
        $this->display = 'edit';
        $this->toolbar_btn['save-and-preview'] = array(
            'href' => '#',
            'desc' => $this->l('Save and preview')
        );
        $this->initToolbar();
        if (!$this->loadObject(true))
        return;

        $categories = CMSCategory::getCategories($this->context->language->id, false);
        $html_categories = CMSCategory::recurseCMSCategory($categories, $categories[0][1], 1, $this->getFieldValue($this->object, 'id_cms_category'), 1);
	  
        $thumb_url = '';
        $banner_url = '';
        $imgName = '';
        if(isset($this->object->id))
        $imgName = $this->getImageValue($this->object); 
        if(isset($imgName['thumb_image'])) {
            $thumb_url = '../img/cms/' . $imgName['thumb_image'];             
        }
        if(isset($imgName['banner_image'])) {
            $banner_url =  '../img/cms/' . $imgName['banner_image'];            
        }

        $selected_assoc_cat = explode(',',$this->object->cat_assoc);

        $this->fields_form = array(
            'tinymce' => true,
            'legend' => array(
                'title' => $this->l('CMS Page overrides'),
                'image' => '../img/admin/tab-categories.gif'
            ),
            'input' => array(
                array(
                    'type' => 'select_category',
                    'label' => $this->l('CMS Category'),
                    'name' => 'id_cms_category',
                    'options' => array(
                         'html' => $html_categories,
                    ),
                ),
                array(
                     'type' => 'categories',
                     'label' => $this->l('Associated Category'),
                     'name' => 'cat_assoc',
                     'tree' => array(
                          'root_category' => $this->context->shop->getCategory(),
                          'id' => 'id_category',
                          'use_radio' => true,
                          'use_search' => true,
                          'selected_categories' => $selected_assoc_cat,
                     ),
                ),
        	    array(
            		 'type' => 'text',
            		 'label' => $this->l('Meta title:'),
            		 'name' => 'meta_title',
            		 'id' => 'name', // for copy2friendlyUrl compatibility
            		 'lang' => true,
            		 'required' => true,
            		 'class' => 'copyMeta2friendlyURL',
            		 'hint' => $this->l('Invalid characters:').' <>;=#{}',
            		 'size' => 50
        	    ),
        	    array(
            		 'type' => 'text',
            		 'label' => $this->l('Meta description'),
            		 'name' => 'meta_description',
            		 'lang' => true,
            		 'hint' => $this->l('Invalid characters:').' <>;=#{}',
            		 'size' => 70
        	    ),
        	    array(
            		 'type' => 'tags',
            		 'label' => $this->l('Meta keywords'),
            		 'name' => 'meta_keywords',
            		 'lang' => true,
            		 'hint' => $this->l('Invalid characters:').' <>;=#{}',
            		 'size' => 70,
            		 'desc' => $this->l('To add "tags" click in the field, write something, then press "Enter"')
        	    ),
        	    array(
            		 'type' => 'text',
            		 'label' => $this->l('Friendly URL'),
            		 'name' => 'link_rewrite',
            		 'required' => true,
            		 'lang' => true,
            		 'hint' => $this->l('Only letters and the minus (-) character are allowed')
        	    ),
        	    array(
            		 'type' => 'textarea',
            		 'label' => $this->l('Page content'),
            		 'name' => 'content',
            		 'autoload_rte' => true,
            		 'lang' => true,
            		 'rows' => 5,
            		 'cols' => 40,
            		 'hint' => $this->l('Invalid characters:').' <>;=#{}'
        	    ),
            	array(
            		 'type' => 'file',
            		 'label' => $this->l('Upload Thumbnail Image'),
            		 'name' => 'thumb_image', 
            		 //'required' => true,
            		 'desc' => $this->l('Upload an image to show thumbnail on homepage'),
            		 'lang' => true,
            		 'display_image' => true,
            		 'thumb' => $thumb_url ? $thumb_url : '',
            		 'hint' => $this->l('small size images are allowed')
            	    ),
            	array(
            		 'type' => 'file',
            		 'label' => $this->l('Upload Poster Image'),
            		 'name' => 'banner_image', 
            		// 'required' => true,
            		 'desc' => $this->l('Upload an image to show vertical banner image on homepage'),
            		 'lang' => true,
            		 'display_image' => true,
            		 'thumb' => $banner_url ? $banner_url : '',
            		 'hint' => $this->l('big size images are allowed')
            	    ),
                array(
                     'type' => 'text',
                     'label' => $this->l('Author Name'),
                     'name' => 'author_name',         
                     'hint' => $this->l('Invalid characters:').' <>;=#{}',
                     'size' => 70
                    ),
                array(
                     'type' => 'date',
                     'label' => $this->l('Date'),
                     'name' => 'added_date',                           
                    ),
            	array(
                    'type' => 'switch',
                    'label' => $this->l('Indexation by search engines'),
                    'name' => 'indexation',
                    'required' => false,
                    'class' => 't',
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'indexation_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'indexation_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Displayed'),
                    'name' => 'active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Promote to Home Page'),
                    'name' => 'home_active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'home_active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'home_active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Promote to Category Page'),
                    'name' => 'cat_active',
                    'required' => false,
                    'is_bool' => true,
                    'values' => array(
                        array(
                            'id' => 'cat_active_on',
                            'value' => 1,
                            'label' => $this->l('Enabled')
                        ),
                        array(
                            'id' => 'cat_active_off',
                            'value' => 0,
                            'label' => $this->l('Disabled')
                        )
                    ),
                ),
	   ),
	   'submit' => array(
	    'title' => $this->l('   Save   '),
	    'class' => 'button'
	   )
	  );

	  $this->fields_form['buttons'] = array(
				'save_and_preview' => array(
					'name' => 'viewcms',
					'type' => 'submit',
					'title' => $this->l('Save and preview'),
					'class' => 'btn btn-default pull-right',
					'icon' => 'process-icon-preview'
				),
			);
	  $this->fields_form['submit'] = array(
			'title' => $this->l('Save'),
			'class' => 'button btn btn-default pull-right'
		);

        if (Shop::isFeatureActive())
        {
            $this->fields_form['input'][] = array(
                'type' => 'shop',
                'label' => $this->l('Shop association:'),
                'name' => 'checkBoxShopAsso',
            );
        }

	   if (Validate::isLoadedObject($this->object)) {
            $this->context->smarty->assign('url_prev', $this->getPreviewUrl($this->object));
        }

        $this->tpl_form_vars = array(
            'active' => $this->object->active
        );	  

	  return AdminController::renderForm();//<----------------------------- Use this... not use parent
	}
	
    public function postProcess()
    {
            //save associated category while creating any cms type
            if (Tools::isSubmit('submitAddcms') || Tools::isSubmit('viewcms')){
                 $cat_assoc = Tools::getValue('cat_assoc');
                if(!$cat_assoc)
                    $cat_assoc = array();

                if(is_array($cat_assoc))
                    $cat_comma_list = implode(',', $cat_assoc);
                else
                    $cat_comma_list = $cat_assoc;

                $_POST['cat_assoc'] = $cat_comma_list;
                //$this->errors[] = $this->l('author name is not there');
                $_POST['author_name'] = Tools::getValue('author_name');
                $_POST['added_date'] = Tools::getValue('added_date');
            }      


           //if thumbnail image is exist in submission form 
           if ((Tools::isSubmit('submitAddcms') || Tools::isSubmit('viewcms'))
		        && isset($_FILES['thumb_image'])
		        && isset($_FILES['thumb_image']['tmp_name'])
		        && !empty($_FILES['thumb_image']['tmp_name']))
            {           	
                if ($error = ImageManager::validateUpload($_FILES['thumb_image'], 4000000))
                    return $error;
                else
                {
                    $ext = substr($_FILES['thumb_image']['name'], strrpos($_FILES['thumb_image']['name'], '.') + 1);
                    $file_name = 'cms_'.  md5($_FILES['thumb_image']['name']).'.'.$ext;
                    if (!move_uploaded_file($_FILES['thumb_image']['tmp_name'],
                        _PS_IMG_DIR_ .'cms'.DIRECTORY_SEPARATOR.$file_name))
                        return Tools::displayError($this->l('An error occurred while attempting to upload the file.'));
                    else
                    {	
                    	$_POST['thumb_image'] = $file_name;
                        @chmod(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$file_name, 0666);
                    }
                }
               
            }  
            //if banner (poster) image is exist in submission form 
            if ((Tools::isSubmit('submitAddcms') || Tools::isSubmit('viewcms'))
		        && isset($_FILES['banner_image'])
		        && isset($_FILES['banner_image']['tmp_name'])
		        && !empty($_FILES['banner_image']['tmp_name']))
            {           	
                if ($error = ImageManager::validateUpload($_FILES['banner_image'], 4000000))
                    return $error;
                else
                {
                    $ext = substr($_FILES['banner_image']['name'], strrpos($_FILES['banner_image']['name'], '.') + 1);
                    $file_name = 'cms_'.  md5($_FILES['banner_image']['name']).'.'.$ext;
                    if (!move_uploaded_file($_FILES['banner_image']['tmp_name'],
                        _PS_IMG_DIR_ .'cms'.DIRECTORY_SEPARATOR.$file_name))
                        return Tools::displayError($this->l('An error occurred while attempting to upload the file.'));
                    else
                    {	
                    	$_POST['banner_image'] = $file_name;
                        @chmod(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$file_name, 0666);
                    }
                }
               
            }  
            //when particular row is deleted
            if (Tools::isSubmit('deletecms')) {
            	$id_cms = (int)Tools::getValue('id_cms');
            	$this->deleteCmsRow($id_cms);		        
            }  
            //when bulk deletion process is taken place
            if (Tools::isSubmit('submitBulkdeletecms')) {
            	$del_array_id = Tools::getValue($this->table.'Box');
            	foreach($del_array_id as $id_cms){
            		$this->deleteCmsRow($id_cms);
            	}
            }
        parent::postProcess();
    }
   
    public function getImageValue()
    {
        $db = Db::getInstance();
        $sql = 'SELECT thumb_image,banner_image FROM '._DB_PREFIX_.'cms_lang WHERE id_cms = ' . $this->object->id;
        return $db->getRow($sql);
    }

    public function deleteCmsRow($id)
    {
    	$db = Db::getInstance();
        $sql = 'SELECT thumb_image,banner_image FROM '._DB_PREFIX_.'cms_lang WHERE id_cms = ' . $id;
        $db_result = $db->getRow($sql);
        if( isset($db_result['thumb_image']) && !empty($db_result['thumb_image']) ) {		        	
        	unlink(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$db_result['thumb_image']);
        }
        if( isset($db_result['banner_image']) && !empty($db_result['thumb_image']) ) {
        	unlink(_PS_IMG_DIR_.'cms'.DIRECTORY_SEPARATOR.$db_result['banner_image']);
        }
        return true;
    }
}