<?php

class CMS extends CMSCore
{
public $thumb_image; 
public $banner_image; 
public $home_active; 
public $cat_active; 
public $cat_assoc; 
public $author_name; 
public $added_date;
 
/**
  * @see ObjectModel::$definition
  */
public static $definition = array(
        'table' => 'cms',
        'primary' => 'id_cms',
        'multilang' => true,
        'multilang_shop' => true,
        'fields' => array(
            'id_cms_category' =>    array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'position' =>            array('type' => self::TYPE_INT),
            'indexation' =>         array('type' => self::TYPE_BOOL),
            'active' =>            array('type' => self::TYPE_BOOL),

            /* Lang fields */
            'meta_description' =>    array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_keywords' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 255),
            'meta_title' =>            array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'required' => true, 'size' => 128),
            'link_rewrite' =>        array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isLinkRewrite', 'required' => true, 'size' => 128),
            'content' =>            array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isCleanHtml', 'size' => 3999999999999),
            'thumb_image' =>   array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999),
            'banner_image' =>   array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isString', 'size' => 3999999999999),
            'home_active' =>            array('type' => self::TYPE_INT),
            'cat_active' =>            array('type' => self::TYPE_INT),
            'cat_assoc' =>            array('type' => self::TYPE_STRING),
            'author_name' =>            array('type' => self::TYPE_STRING),
            'added_date' =>            array('type' => self::TYPE_STRING),
      ),// <-- and your desired properties
);
}