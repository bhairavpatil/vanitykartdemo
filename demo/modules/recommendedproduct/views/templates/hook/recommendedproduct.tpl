<!--Recommended Product Start -->
{if isset($recommended_product_arr)}

<section id="recommendedProducts" class="related_product_sec">
		<div class="container">
			<div class="row">
				<div class="col-lg-12 col-md-12 cust-review-section">


<div class="slider_title">
<span class="slide_text">
	<img src="{$img_dir}flower5.png" alt="flowers">
		{l s="Recommended Products" mod="similarproduct"}
	<img src="{$img_dir}flower5.png" alt="flowers">
</span>
</div> <!--/slider_title-->

<div id="recommended-products" class="jcarousel-wrapper product_slider">
<div class="jcarousel">
    <ul>

    	{foreach from=$recommended_product_arr item=res1}
    	<li>
    		<div class="slider_item_sec">
				<a href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="{$res1.name|htmlspecialchars}">
					<div class="img-container img-container-related-imgs">
						<img class="img-card" src="{$link->getImageLink($res1.link_rewrite, $res1.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$res1.name|htmlspecialchars}" />
					</div> <!--/img-container-->
					<div class="card-content">
						<h4>{$res1.name|truncate:14:'...'|escape:'html':'UTF-8'}</h4>
						<!-- <h5>2 regular brushes, epilation</h5> -->
						<p>{convertPrice price=$res1.price}</p>                
					</div>
				</a>
				<div class="overlay-icon">
						<!-- start wishlist status icon -->
						{hook h='displayWishlistStatus' id_prod="{$res1.id_product}"}
						<!-- end wishlist status icon -->
					<span class="show-icon show-icon2">
						<!-- <a href="javascript:void(0)"></a> -->
						<a class="quick-view" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="Quick view"></a>
					</span>
				</div>
			</div> <!--/slider_item_sec-->
		</li>
		{/foreach}
    </ul>
</div> <!--/jcarousel-->

<a href="#" class="jcarousel-control-prev"><i class="icon-chevron-left"></i></a>
<a href="#" class="jcarousel-control-next"><i class="icon-chevron-right"></i></a>

<p class="jcarousel-pagination"></p>
</div> <!--/jcarousel-wrapper-->


</div> <!--/col-md-12-->
</div> <!--/row-->
</div> <!--/container-->
</section> <!--/related_product_sec-->
<!-- Similar Product End -->
{/if}






<!-- Original Code Start -->
<!-- {if isset($recommended_product_arr)}
{l s="Recommended Products" mod="similarproduct"}
{foreach from=$recommended_product_arr item=res1}	
	<div>
		<ul>
			<li>{$res1.name|truncate:14:'...'|escape:'html':'UTF-8'}</li>
			<li>
				<a href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" title="{$res1.name|htmlspecialchars}">
				<img src="{$link->getImageLink($res1.link_rewrite, $res1.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$res1.name|htmlspecialchars}" />
				</a>
			</li>
			<li>
			<p class="price_display">
				<span class="price">{convertPrice price=$res1.price}</span>
			</p>
				
			</li>

			<div class="quick-view-wrapper-mobile">
                <a class="quick-view-mobile" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}">
                    <i class="icon-eye-open"></i>
                </a>
            </div>
            <a class="quick-view" href="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}" rel="{$link->getProductLink($res1.id_product)|escape:'html':'UTF-8'}">
                <span>Quick view</span>
            </a>
		</ul>	
	</div>	
{/foreach}
{/if} -->
<!-- Original Code End