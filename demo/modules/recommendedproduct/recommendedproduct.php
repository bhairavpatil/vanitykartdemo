<?php

if(!defined('_PS_VERSION_'))
	exit;

class RecommendedProduct extends Module
{
	public function __construct()
	{
		$this->name = 'recommendedproduct';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.5' , 'max' => _PS_VERSION_);

		$this->displayName = $this->l('Recommended Product');
		$this->description = $this->l('Display recommended product on the basis of same parent category level');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('productFooter') OR
			!$this->registerHook('recommendedProducts')
			)
			return false;
		return true;
	}

	public function unisntall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}

	

	public function hookProductFooter($params)
	{	
		//ddd($params);
		$current_product_id = $params['product']->id;
		//$current_category_id = $params['category']->id;		
		$current_category_id = $params['product']->id_category_default;		
		$current_product_price = Product::getPriceStatic((int)$current_product_id, true, null, 2);

		//Calculate (+/-) 10% price using current price
		$product_price_plus =  $current_product_price  +  (($current_product_price * 10) / 100) ;
		$product_price_minus =  $current_product_price  -  (($current_product_price * 10) / 100) ;

		if(isset($current_category_id))
			$category = new Category((int)$current_category_id);

		$recommended_product_arr = array();	

		$start = 1;
		$end = 100;
		$counter = 0;
		
		for ($i = $start; $i < $end; $i++)
		{ 					
			//Fetch Products with the same category (Last category only)
			$category_products = $category->getProducts($this->context->language->id, $i, 100);	

			if(is_array($category_products) && count($category_products))
			{
				foreach ($category_products as $key => $value) 
				{				
					//Remove current product from list
					if($value['id_product'] == $current_product_id)
					{
						unset($category_products[$key]);	
						continue;
					}

					//Include (+/-) 10% price range products
					//$product_price = Product::getPriceStatic((int)$value['id_product'], true, null, 2);

					//commented now for demo purpose			
					/*$product_price = $value['price'];				
					if($product_price >= $product_price_minus && $product_price <= $product_price_plus)
					{							
						$recommended_product_arr[] = $category_products[$key];	
						$counter++;	

						 //if($counter == 6)													
						//	break 2;	 					
					}*/

					$recommended_product_arr[] = $category_products[$key];	
				}							
				
			}	
			else
			{
				break;
			}


		}

		//ddd($recommended_product_arr);
		if(is_array($recommended_product_arr) && count($recommended_product_arr))
		{
			$this->context->smarty->assign(array(
					'recommended_product_arr' => $recommended_product_arr
				));
		}
		
		//return $this->display(__FILE__,'recommendedproduct.tpl');
	}
	
	public function hookRecommendedProducts($params)
	{
		return $this->display(__FILE__,'recommendedproduct.tpl');
	} 


}