<?php

if(!defined('_PS_VERSION_'))
	exit;

class VideoPromotion extends Module
{

	private $html = '';
	private $errors = '';

	public function __construct()
	{
		$this->name = 'videopromotion';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->ps_versions_compliancy = array('min' => '1.6' , 'max' => _PS_VERSION_);
		
		$this->bootstrap = true;
		$this->need_instance = 0;

		$this->displayName = $this->l('Video Promotion');
		$this->description = $this->l('Assign video to category landing pages and homepage');

		$this->table_name = 'video_promotion';

		parent::__construct();

	}

	public function install()
	{
		if(!parent::install() OR
			!$this->createTabs() OR
			!$this->installMyTable() OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('displayHome') OR
			!$this->registerHook('displayVideoPromotion') OR
			!$this->registerHook('videoPromotionOnCategory')
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if(!parent::uninstall() OR
			!$this->eraseTabs() OR
			!$this->removeMyTable()
			)
			return false;
		return true;	
	}

	private function createTabs()
	{
		$tab = new Tab();
		$tab->active = 1;
		$languages = Language::getLanguages(false);
		if(is_array($languages))
		{
			foreach ($languages as $language) {
				$tab->name[$language['id_lang']] = $this->l('Video Promotion');
			}
		}
		$tab->class_name = 'Admin'.ucfirst($this->name);
		$tab->module = $this->name;
		$tab->id_parent = 142;

		return (bool)$tab->add();
	}

	private function eraseTabs()
	{
		$id_tab = (int)Tab::getIdFromClassName('Admin'.ucfirst($this->name));
		if($id_tab){
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return true;
	}

	private function installMyTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.$this->table_name.'` (
				`id_video_promotion` INT(12) NOT NULL AUTO_INCREMENT,				
				`title` VARCHAR(64) NOT NULL,
				`video_url` VARCHAR(512) NOT NULL,				
				`category` VARCHAR(512) NOT NULL,				
				`category_name` text NOT NULL,				
				`active` TINYINT NOT NULL,				
				PRIMARY KEY (`id_video_promotion`)
				) ENGINE = ' . _MYSQL_ENGINE_;

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;		
	}

	private function removeMyTable()
	{
		$sql = 'DROP TABLE '._DB_PREFIX_.$this->table_name;

		if(!Db::getInstance()->Execute($sql))
			return false;
		return true;
	}

	public function getContent()
	{
		$this->postProcess();
		$this->displayForm();
		return $this->html;
	}

	public function postProcess()
	{		

	//ddd(Tools::getAllValues());	
		if(Tools::isSubmit('submitNewVideos'))
		{
			//$reviews['id_page'] = Tools::getValue('id_page');			
			$video_promotion['title'] = Tools::getValue('title');
			$video_promotion['video_url'] = Tools::getValue('video_url');
			$video_promotion['active'] = Tools::getValue('active');			
			$video_promotion['category'] = implode(",",Tools::getValue('category_list'));

			$category_list_arr = Tools::getValue('category_list');			
			$category_name_list = '';
			foreach ($category_list_arr as $key => $value) { 
				 $category_name_list .= $this->getCategoryName($value)['name'] . ", ";
			}
			
			$category_name_list = rtrim($category_name_list,", ");
			$video_promotion['category_name'] = $category_name_list;

			//ddd($category_list);

			if(empty($video_promotion['video_url']) || !isset($video_promotion['video_url']))
					$this->errors[] = Tools::displayError('Please Enter Video URL');

			if(empty($video_promotion['category']) || !isset($video_promotion['category']))
					$this->errors[] = Tools::displayError('Please Select Category');	
		

			if(!$this->errors)
			{
				if(Tools::getValue('id_video_promotion'))
				{
					if(!Db::getInstance()->update($this->table_name,$video_promotion, 'id_video_promotion = '. (int)Tools::getValue('id_video_promotion')))			
						$this->errors[] = Tools::displayError('Error while updating the database'). ': ' . $mysql_error();		
				} else {
					if(!Db::getInstance()->insert($this->table_name,$video_promotion))
						$this->errors[] = Tools::displayError('Error while adding to the database'). ': ' . $mysql_error();
				}
				
			}

			$confirmation = Tools::getValue('id_video_promotion') ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');
				
			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else{
				$this->html .= $this->displayConfirmation($confirmation);	
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name);
			}
		}
		else if (Tools::isSubmit('deletevideo_promotion'))
		{			
			if(!Db::getInstance()->delete($this->table_name,'id_video_promotion = '. (int)Tools::getValue('id_video_promotion')))
				$this->errors[] = Tools::displayError('Error while deleting from the database'). ': ' . $mysql_error();

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($this->l('Entry Successfully Removed'));	
		}
		else if (Tools::isSubmit('togglevideo_promotion'))
		{
			$id_video_promotion = Tools::getValue('id_video_promotion');
			if($this->toggleVideosActiveState($id_video_promotion))
				$this->html .= $this->displayConfirmation($this->l('Entry Status Changed'));
			else
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));		
			
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&conf=4');
		}
		
	}

	public function toggleVideosActiveState($id_video_promotion)
	{		
		if(Db::getInstance()->getValue('SELECT active FROM '._DB_PREFIX_.$this->table_name.' where id_video_promotion = '.(int)$id_video_promotion))
		{ 
			//Disable it
			if(!Db::getInstance()->update($this->table_name, array('active' => 0) , 'id_video_promotion = '.(int)$id_video_promotion))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;

		}
		else //Enable it
		{
			if(!Db::getInstance()->update($this->table_name, array('active' => 1) , 'id_video_promotion = '.(int)$id_video_promotion))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;
		}
	}

	//Display add, edit and listview on the basis of condition
	public function displayForm()
	{
		//ddd(Tools::getAllValues());
		if(Tools::isSubmit('updatevideo_promotion') || (Tools::getValue('id_video_promotion') && !Tools::isSubmit('deletevideo_promotion')))
			$this->html .= $this->generateForm(true);
		else if (Tools::isSubmit('addNewVideos') || Tools::isSubmit('submitNewVideos')) 		
			$this->html .= $this->generateForm();
		else {
			$this->html .= $this->generateVideosList();			
		}
				
	}

	//Create Form for add and edit mode
	public function generateForm($editing = false)
	{
		if($editing)
		{
			$inputs[] =  array(
					'type' => 'hidden',
					'name' => 'id_video_promotion'
				);
		}


		//Fetch Main Category List under the home category
		$category_list = $this->getCategoryList(2);

		//Added home category explicitly in main category array
		$category_list[] = array('id_category' => 2, 'name' => 'Home');
	
		//sort multidimensional array
		usort($category_list, function($a, $b) {
   			return $a['id_category'] - $b['id_category'];
		});


		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Promotion Title'),
				'name' => 'title',
			);

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Embed Video URL'),
				'name' => 'video_url',
			);

		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select Category',
                    'name' => 'category_list[]',
                    'class' => 'chosen',
                    'multiple' => true,
                    'options' => array(                       
                        'query' => $category_list,
                        'id' => 'id_category',
                        'name' => 'name'
                    ),
                );

		$inputs[] =  array(
                    'type' => 'switch',
                    'label' => 'Active',
                    'name' => 'active',
                    'values' => array(
                        array(
                            'id' => 'active_on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'active_off',
                            'value' => 0
                        )
                    )
                );
		

	$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $editing ?  $this->l('Edit Video') : $this->l('Add New Video'),
						'icon' => 'icon-cogs'
						),
					'input' => $inputs,
					'submit' => array(
						'title' => $editing ?  $this->l('Update') : $this->l('Add'),
						'class' => 'btn btn-default pull-right'
						),
					'buttons' => array(
					        array(
					            'href' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name,
					            'title' => $this->l('Cancel'),
					            'icon' => 'process-icon-cancel'
					        )
					    )
					)			
			);

		if(!$editing)
		{
			foreach($inputs as $input)
				$values[$input['name']] = '';
		}
				
		//if video promotion id is present then only editing is true
		$id_video_promotion = Tools::getValue('id_video_promotion');
		if(!empty($id_video_promotion))
			$editing = true;
		else
			$editing = false; 


		$helper = new HelperForm();
		$helper->submit_action = 'submitNewVideos';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues($editing,$id_video_promotion)
			);

		return $helper->generateForm(array($fields_form));

	} 

	public function getConfigFieldsValues($editing,$id_video_promotion)
	{	
				
		$values = $this->getSingle($id_video_promotion);
		return array(
			'id_video_promotion' => $values['id_video_promotion'],
			'title' => $values['title'],
			'video_url' => $values['video_url'],
			'active' => $values['active'], 			 
			'category_list[]' => explode(',', $values['category'])			 
		);

		
	}

	public function generateVideosList()
	{
		$content = $this->getAll();

		foreach ($content as $key => $value) 
		{ 
			/*$category_name = explode(",", $value['category']);
			$category_name_list = '';
			foreach ($category_name as $key => $value) { 
				 $category_name_list = $this->getCategoryName($value);
			}

			//ddd($category_name_list);

			$category_name_list = rtrim($category_name_list['name'],", ");
			//ddd($category_name_list);

			$value['category'] = $category_name_list;*/

			$value['video_url'] = '<iframe width="150" height="100" src="'.$value['video_url'].'"></iframe>';

			

			$content[$key] = $value;
		}

		$fields_list = array(
			'id_video_promotion' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs',
				'orderby' => false,
				'search' => false
				),
			'title' => array(
				'title' => $this->l('Title'),
				'orderby' => false,
				'search' => false				
				),
			'video_url' => array(
				'title' =>  $this->l('Video'),
				'orderby' => false,
				'search' => false,
				'float' => true 			
				),			
			'category_name' => array(
				'title' =>  $this->l('Category'),
				'orderby' => false,
				'search' => false			
				),
			'active' => array(
				'title' =>  $this->l('Active'),
				'active' => 'toggle',
				'orderby' => false,
				'search' => false,
				'type' => 'bool'			
				)
			);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->actions = array('edit','delete');
		//$helper->show_toolbar = true;
		//$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar();		
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'id_video_promotion';
		$helper->title = $this->l('List of Videos Promotion');
		$helper->table = $this->table_name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;

		return $helper->generateList($content,$fields_list);
	}

	public function initToolbar()
	{
		$getToken = Tools::getAdminTokenLite('AdminModules');

		$this->toolbar_btn['new'] = array(
				'href' => $this->context->link->getAdminLink('AdminModules',false).'&token='.$getToken.'&configure='.$this->name.'&addNewVideos',
				'desc' => $this->l('Add New')
			);

		return $this->toolbar_btn;
	}

	//Fetch All videos promotion from the database
	public function getAll()
	{
		return Db::getInstance()->ExecuteS('
				SELECT * 
				FROM '._DB_PREFIX_.$this->table_name);
	}

	public function getCategoryList($cat_id)
	{
		$sql = new DbQuery();
		$sql->select('cl.id_category,cl.name');
		$sql->from('category','c');
		$sql->innerJoin('category_lang','cl','c.id_category = cl.id_category');
		$sql->where('c.id_parent = '.$cat_id);
		$sql->where('c.active = 1');
		$sql->where('cl.id_lang = '.$this->context->language->id);
		$sql->orderBy('c.id_category');
		return Db::getInstance()->executeS($sql);
	}

	public function getCategoryName($cat_id)
	{		
		return Db::getInstance()->getRow('
				SELECT name 
				FROM '._DB_PREFIX_.'category_lang where id_category= ' .$cat_id );
	}

	public function getSingle($id)
	{
		return Db::getInstance()->getRow('
			SELECT *
			FROM '._DB_PREFIX_.$this->table_name.
			' where id_video_promotion ='.(int)$id
			 );
	}

/*	public function hookDisplayHome($params)
	{
		//if($this->context->controller->php_self == 'index');

		//$pageid = $this->context->controller->cms->id;
		$category_id = 2;
		$gallery_result = $this->getVideoPromotionList($category_id);	

		if(isset($gallery_result) && !empty($gallery_result))
		{			
			$this->context->smarty->assign(array(						
						'videogallery_home' => $gallery_result
					));
		}
		return $this->display(__FILE__,'videopromotion_home.tpl');		
	}*/

	public function hookDisplayVideoPromotion($params)
	{
		//if($this->context->controller->php_self == 'index');

		//$pageid = $this->context->controller->cms->id;
		$category_id = 2;
		$gallery_result = $this->getVideoPromotionList($category_id);	

		if(isset($gallery_result) && !empty($gallery_result))
		{			
			$this->context->smarty->assign(array(						
						'videogallery_home' => $gallery_result
					));
		}
		return $this->display(__FILE__,'videopromotion_home.tpl');		
	}

	public function hookVideoPromotionOnCategory($params)
	{
		//if($this->context->controller->php_self == 'index');

		//$pageid = $this->context->controller->cms->id;
		$category_id = 3;
		$gallery_result = $this->getVideoPromotionList($category_id);		
		if(isset($gallery_result) && !empty($gallery_result))
		{			
			$this->context->smarty->assign(array(						
						'videogallery_category' => $gallery_result
					));
		}
		return $this->display(__FILE__,'videopromotion_category.tpl');		
	}


	public function getVideoPromotionList($category_id)
	{
		$gallery_list = Db::getInstance()->ExecuteS('
					SELECT * 
					FROM '._DB_PREFIX_.$this->table_name.
					' where active = 1');

		//ddd($gallery_list);

		foreach ($gallery_list as $key => $value)
		{				
				
			if(!empty($value['category']))
				$category_ids = explode(',', $value['category']);
			else
				$category_ids = $value['category'];
			
			if(is_array($category_ids) && count($category_ids))
			{
				$cat_flag = in_array($category_id, $category_ids);
				
				if($cat_flag)
				{
					$gallery_list_ids[] = $value['id_video_promotion'];						
					$gallery_result[] = $value;						
				}
			}
			else
			{
				if($category_id == $category_ids)
				{
					$gallery_list_ids[] = $value['id_video_promotion'];					
					$gallery_result[] = $value;
				}
			}
				
		}

		//return latest video promotion
		if(isset($gallery_list_ids))
		{
			$key_result = array_keys($gallery_list_ids, max($gallery_list_ids));
			return $gallery_result[$key_result[0]];
		}
		else
			return false;
	}


}

?>