<?php

if(!defined('_PS_VERSION_'))
	exit;

class MenuList extends Module
{
	public function __construct()
	{
		$this->name = 'menulist';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.5' , 'max' => _PS_VERSION_);

		$this->displayName = $this->l('Menu List');
		$this->description = $this->l('Display Menu List on homepage');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('displayTop')
			)
			return false;
		return true;
	}

	public function unisntall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}

	public function hookDisplayTop($params)
	{	
		
		//fetch main category list under home category
		$main_category_list = Db::getInstance()->ExecuteS('SELECT pcl.id_category,pcl.name FROM `ps_category_lang` pcl, `ps_category` pc WHERE pc.id_category = pcl.id_category and pc.id_parent = 2 and active = 1 and `id_lang` = '.$this->context->language->id);

		foreach ($main_category_list as $key => $value)
		{
			$category_array[$value['id_category']]['id_category'] = $value['id_category'];
			$category_array[$value['id_category']]['name'] = $value['name'];

			//fetch sub category list
			$sub_category_list = Db::getInstance()->ExecuteS('SELECT pcl.id_category,pcl.name FROM `ps_category_lang` pcl, `ps_category` pc WHERE pc.id_category = pcl.id_category and pc.id_parent = '.$value['id_category'].' and active = 1 and `id_lang` = '.$this->context->language->id);
			foreach ($sub_category_list as $key1 => $value1)
			{
				$category_array[$value['id_category']]['children'][$value1['id_category']]['id_category'] = $value1['id_category'];
				$category_array[$value['id_category']]['children'][$value1['id_category']]['name'] = $value1['name'];

				//fetch sub sub category list
				$sub_sub_category_list = Db::getInstance()->ExecuteS('SELECT pcl.id_category,pcl.name FROM `ps_category_lang` pcl, `ps_category` pc WHERE pc.id_category = pcl.id_category and pc.id_parent = '.$value1['id_category'].' and active = 1 and `id_lang` = '.$this->context->language->id);
				foreach ($sub_sub_category_list as $key2 => $value2)
				{
					$category_array[$value['id_category']]['children'][$value1['id_category']]['children'][$value2['id_category']]['id_category'] = $value2['id_category'];
					$category_array[$value['id_category']]['children'][$value1['id_category']]['children'][$value2['id_category']]['name'] = $value2['name'];
				}
			}

		}
		
		//$cats = Category::getNestedCategories( 2, (int)($this->context->language->id)  ) ;
		//ddd($category_array);
		//ddd($cats);
	
		if(is_array($category_array) && count($category_array))
		{
			$this->context->smarty->assign(array(
					'category_array' => $category_array
				));
		}
		
		return $this->display(__FILE__,'menulist.tpl');
	}


}