<!-- Main Menu Start -->
    <nav class="navbar main-navbar-default">
      <div class="">
      	<div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{$base_dir}"></a>

          <div class="wishlist_cart_sec"></div> <!--/wishlist_cart_sec-->

          <div class="mobile_search_sec"></div> <!--/mobile_search_sec-->
          
        </div> <!--/navbar-header-->
        </div> <!--/container-->


        

        <div id="navbar" class="navbar-collapse collapse main-navbar">

          {if isset($category_array)}
          <ul class="nav navbar-nav">
            {foreach from=$category_array item=res1}
			<li class="dropdown">
				<span class="menu-plus"><i class="icon-plus"></i> <i class="icon-minus"></i></span>
				<a href="{$base_path}{$res1.id_category}-{$res1.urlname}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{$res1.name} <span class="caret" style="display:none;"></span></a>

				{if isset($res1['children']) }
				<div class="dropdown-menu menu-show">

						<div class="cust_tab_sec">

						  {foreach from=$res1['children'] item=res2}
						  <h4 class="">
						  	<a href="{$base_path}{$res2.id_category}-{$res2.urlname}">{$res2.name}</a>
						  	<span class="menu-plus"><i class="icon-plus"></i> <i class="icon-minus"></i></span>
						  </h4>
						  

						  {if isset($res2['children']) }
						  <ul class="third_menu row">
						    <li class="col-md-2">
						    	<ul class="third_level_menu">
						    		{foreach from=$res2['children'] item=res3}
						    			<li><a href="{$base_path}{$res3.id_category}-{$res3.urlname}">{$res3.name}</a></li>
						    		{/foreach}
						    	</ul>
						    </li>
						    <!-- Menu Banner Start -->
						    {hook h="bannerDisplayRight" section='menu' cat_id="{$res1.id_category}" sub_cat_id="{$res2.id_category}"}
						    <!-- Menu Banner End -->

						    <!-- Top Brands Start -->
						    {hook h="displayTopBrands" category_id="{$res1.id_category}"}
						    <!-- Top Brands End -->
						  </ul>
						  {/if}
						  {/foreach}
						  <div class="clearfix"></div>
						</div> <!--/cust_tab_sec-->

				<div class="clearfix"></div>
				</div> <!--/dropdown-menu-->

				{/if}
            </li>
            {/foreach}


            <li class="mob_profile">
          		<!-- Block user information module NAV  -->
				<div class="header_user_info">
				    {if $is_logged}
				        <div class="dropdown">
				            <a class="account" data-toggle="dropdown" href="#">
				            	<!-- <i class="icon-user"></i> -->
				            	<img src="{$img_dir}login/user-active.png" alt="{l s='My Account'}"/>
				            	<span> {$cookie->customer_firstname} {$cookie->customer_lastname}</span>
				            	<img class="dwn_arrw" src="{$img_dir}down-arrow.png"/>
				            </a>
				             <ul class="dropdown-menu" role="menu">
				             	<!-- <li><a href="{$link->getPageLink('history', true)|escape:'html':'UTF-8'}" title="{l s='Orders' mod='blockuserinfo'}"><i class="icon-sort-numeric-asc"></i> <span>{l s='My Orders' mod='blockuserinfo'}</span></a></li> -->
				             	<li><a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" title="{l s='View my account' mod='blockuserinfo'}"><i class="icon-suitcase"></i> <span>{l s='My Account' mod='blockuserinfo'}</span></a></li>             	
				             	<!-- <li><a href="{$link->getPageLink('identity', true)|escape:'html':'UTF-8'}" title="{l s='Information' mod='blockuserinfo'}"><i class="icon-info"></i> <span>{l s='My Profile' mod='blockuserinfo'}</span></a></li> -->
				                
				                
				               <!--  <li><a href="{$link->getPageLink('addresses', true)|escape:'html':'UTF-8'}" title="{l s='Addresses' mod='blockuserinfo'}"><i class="icon-map-marker"></i> <span>{l s='Addresses' mod='blockuserinfo'}</span></a></li> -->
				                
				                
				                {capture assign="HOOK_USER_INFO"}{hook h="displayUserInfo"}{/capture}
				                {if !empty($HOOK_USER_INFO)}{$HOOK_USER_INFO}{/if}
				                
				                <li><a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}"><i class="icon-sign-out"></i> <span>{l s='LOGOUT' mod='blockuserinfo'}</span></a></li>
				            </ul>
				        </div>
				    {else}
				        <a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='LOGIN' mod='blockuserinfo'}">	        	
						<img src="{$img_dir}login/user.png" alt="{l s='LOGIN'}"/>
				            {l s='LOGIN' mod='blockuserinfo'}
				        </a>       
				    {/if}
				</div>
				<!-- /Block user information module NAV -->
          	</li>
           
          </ul>
          {/if}
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>
<!-- Main Menu End -->







<!-- {if isset($category_array)}

{foreach from=$category_array item=res1}	
	
	<div >
		<ul>
			<li ><a style="color: blue;" href="{$res1.id_category}">{$res1.name}</a></li>			
		</ul>			
	</div>	

		{if isset($res1['children']) }
			{foreach from=$res1['children'] item=res2}	
		
		<div>
			<ul>
				<li><a style="color: red;" href="{$res2.id_category}">{$res2.name}</a></li>			
			</ul>			
		</div>		
		
						{if isset($res2['children']) }
						{foreach from=$res2['children'] item=res3}	
					
					<div>
						<ul>
							<li><a style="color: green;" href="{$res3.id_category}">{$res3.name}</a></li>			
						</ul>			
					</div>		
					

						{/foreach}
					{/if}



			{/foreach}
		{/if}

{/foreach}
{/if}















