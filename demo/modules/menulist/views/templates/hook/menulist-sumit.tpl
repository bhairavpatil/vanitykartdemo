<!-- Main Menu Start -->
    <nav class="navbar main-navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="display:none;">Vanity kart</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse main-navbar">

        {if isset($category_array)}
        	
          <ul class="nav navbar-nav">
			
			{foreach from=$category_array item=res1}
			<li class="dropdown">
				<a href="{$res1.id_category}" class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{$res1.name} <span class="caret" style="display:none;"></span></a>
				
				{if isset($res1['children']) }
				<div class="dropdown-menu menu-show">
						
										
					<div class="col-lg-2 col-md-2 col-sm-2">

						<div class="second_level_menu">
						  {foreach from=$res1['children']  item=res2 name="foo"}
                          	<a href="{$res2.id_category}" class="showSingle" target="{$smarty.foreach.foo.index + 1}">{$res2.name}</a>
                          {/foreach}
                      	</div> <!--/second_level_menu-->

					</div> <!--/col-md-2-->
					


					<div class="col-lg-10 col-md-10 col-sm-10">

						
						<div class="third_level_menu_sec">
							<!-- {if isset($res2['children']) } -->
							<div id="div1" class="targetDiv">
								<div class="col-md-2">
									<ul class="third_level_menu">
									  {foreach from=$res1['children'] item=res3}
									  	<!-- <li class=""><a href="{$res3.id_category}">{$res3['children'].name}</a></li> -->
									  	 {foreach from=$res3['children'] item=res4}
				                          	<a href="{$res4.id_category}" class="showSingle" target="1">{$res4.name}</a>
				                          {/foreach}

									  {/foreach}
									</ul>
								</div> <!--/col-md-2-->								
							</div> <!--/targetDiv-->
							<!-- {/if} -->							
						</div> <!--/third_level_menu_sec-->
						

					</div> <!--/col-md-10-->

				</div> <!--/dropdown-menu-->
				{/if}
            </li>
             {/foreach}
           
           
          </ul>         
		{/if}
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>





{if isset($category_array)}

{foreach from=$category_array item=res1}	
	
	<div >
		<ul>
			<li ><a style="color: blue;" href="{$res1.id_category}">{$res1.name}</a></li>			
		</ul>			
	</div>	

		{if isset($res1['children']) }
			{foreach from=$res1['children'] item=res2}	
		
		<div>
			<ul>
				<li><a style="color: red;" href="{$res2.id_category}">{$res2.name}</a></li>			
			</ul>			
		</div>		
		
						{if isset($res2['children']) }
						{foreach from=$res2['children'] item=res3}	
					
					<div>
						<ul>
							<li><a style="color: green;" href="{$res3.id_category}">{$res3.name}</a></li>			
						</ul>			
					</div>		
					

						{/foreach}
					{/if}



			{/foreach}
		{/if}

{/foreach}
{/if}