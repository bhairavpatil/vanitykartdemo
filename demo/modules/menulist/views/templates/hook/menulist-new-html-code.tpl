<!-- Main Menu Start -->
    <nav class="navbar main-navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="display:none;">Vanity kart</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse main-navbar">
          <ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Beauty <span class="caret" style="display:none;"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Makeup</a></li>
					<li><a href="#">Skin Care</a></li>
					<li><a href="#">Hair Care</a></li>
					<li role="separator" class="divider"></li>
					<li class="dropdown-header">Nav header</li>
					<li><a href="#">Separated link</a></li>
					<li><a href="#">One more separated link</a></li>
				</ul>
            </li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fashion <span class="caret" style="display:none;"></span></a>
				<div class="dropdown-menu menu-show">
						
					<div class="container">
					<div class="cust_tab_sec">

					  <h4 class="active">clothing</h4>
					  <ul class="third_menu row">
					    <li class="col-md-2">

					    	<ul class="third_level_menu">
					    		<li><a href="#">Arabic Wear</a></li>
					    		<li><a href="#">Western Wear</a></li>
					    		<li><a href="#">Indian Wear</a></li>
					    		<li><a href="#">Sports/Active Wear</a></li>
					    	</ul>

					    </li>

					    <li class="col-md-6">Mood Board</li>
					    
					    <li class="col-md-4">Top brands</li>
					  </ul>

					  <h4>Shop by</h4>
					  <ul class="third_menu row">
					    <li>Potius inflammat, ut coercendi magis quam dedocendi esse videantur.</li>
					    <li>Atqui reperies, inquit, in hoc quidem pertinacem;</li>
					    <li>Verba tu fingas et ea dicas, quae non sentias?</li>
					  </ul>

					  <h4>accessories</h4>
					  <ul class="third_menu row">
					    <li>Qui autem de summo bono dissentit de tota philosophiae ratione dissentit.</li>
					    <li>Sed quanta sit alias, nunc tantum possitne esse tanta.</li>
					    <li>An est aliquid per se ipsum flagitiosum, etiamsi nulla comitetur infamia?</li>
					  </ul>

					  <h4>Designers</h4>
					  <ul class="third_menu row">
					    <li>Omnes enim iucundum motum, quo sensus hilaretur.</li>
					    <li>Oratio me istius philosophi non offendit;</li>
					    <li>Ut pulsi recurrant?</li>
					  </ul>


					  <h4>sale</h4>
					  <ul class="third_menu row">
					    <li>Et ille ridens: Video, inquit, quid agas;</li>
					    <li>Sed tu istuc dixti bene Latine, parum plane.</li>
					    <li>Eorum enim omnium multa praetermittentium, dum eligant aliquid, quod sequantur, quasi curta sententia;</li>
					  </ul>

					</div>
				</div> <!--/container-->

				</div> <!--/dropdown-menu-->
            </li>
            <li><a href="#" class="hvr-underline-from-center">Fragrance</a></li>
			<li><a href="#" class="hvr-underline-from-center">Fitness</a></li>
			<li><a href="#" class="hvr-underline-from-center" >Health & Wellness</a></li>
			<li><a href="#" class="hvr-underline-from-center">Accessories</a></li>
			<li><a href="#" class="hvr-underline-from-center">Services</a></li>
			<li><a href="#" class="hvr-underline-from-center">Value</a></li>
			<li><a href="#" class="hvr-underline-from-center">Pre-Owned</a></li>
           
          </ul>
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>







<!-- {if isset($category_array)}

{foreach from=$category_array item=res1}	
	
	<div >
		<ul>
			<li ><a style="color: blue;" href="{$res1.id_category}">{$res1.name}</a></li>			
		</ul>			
	</div>	

		{if isset($res1['children']) }
			{foreach from=$res1['children'] item=res2}	
		
		<div>
			<ul>
				<li><a style="color: red;" href="{$res2.id_category}">{$res2.name}</a></li>			
			</ul>			
		</div>		
		
						{if isset($res2['children']) }
						{foreach from=$res2['children'] item=res3}	
					
					<div>
						<ul>
							<li><a style="color: green;" href="{$res3.id_category}">{$res3.name}</a></li>			
						</ul>			
					</div>		
					

						{/foreach}
					{/if}



			{/foreach}
		{/if}

{/foreach}
{/if}















