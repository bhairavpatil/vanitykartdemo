<!-- Main Menu Start -->
    <nav class="navbar main-navbar-default">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="display:none;">Vanity kart</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse main-navbar">
          <ul class="nav navbar-nav">
			<li class="dropdown">
				<a href="#" class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Beauty <span class="caret" style="display:none;"></span></a>
				<ul class="dropdown-menu">
					<li><a href="#">Makeup</a></li>
					<li><a href="#">Skin Care</a></li>
					<li><a href="#">Hair Care</a></li>
					<li role="separator" class="divider"></li>
					<li class="dropdown-header">Nav header</li>
					<li><a href="#">Separated link</a></li>
					<li><a href="#">One more separated link</a></li>
				</ul>
            </li>
			<li class="dropdown">
				<a href="#" class="dropdown-toggle hvr-underline-from-center" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Fashion <span class="caret" style="display:none;"></span></a>
				<div class="dropdown-menu menu-show">
						
					<div class="col-lg-2 col-md-2 col-sm-2">
						<div class="second_level_menu_sec">
							 <!-- Nav tabs -->
							<ul class="nav nav-tabs" role="tablist">
							   <li class="active"><a href="#men" role="tab" data-toggle="tab">Men</a></li>
							   <li><a href="#women" role="tab" data-toggle="tab">Women</a></li>
							   <li><a href="#kids" role="tab" data-toggle="tab">Kids</a></li>
							   <li><a href="#sports" role="tab" data-toggle="tab">Sports</a></li>
							</ul> 
						</div> <!--/second_level_menu_sec-->						
					</div> <!--/col-md-2-->

					<div class="col-lg-10 col-md-10 col-sm-10">
						<div class="third_level_menu_sec">


							<!-- Tab panes -->
                        <div class="tab-content responsive">
                          <div class="tab-pane active" id="men">
                            <ul class="nav-list list-inline">
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Running.png"><span>Running</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Basketball.png"><span>Basketball</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Football.png"><span>Football</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Soccer.png"><span>Soccer</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_MensTraining.png"><span>Men's Training</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_WomensTraining.png"><span>Women's Training</span></a></li>
                            </ul>
                          </div>
                          <div class="tab-pane" id="women">
                            <ul class="nav-list list-inline">
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Running.png"><span>Running</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Basketball.png"><span>Basketball</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Football.png"><span>Football</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Soccer.png"><span>Soccer</span></a></li>                                
                            </ul>
                          </div>
                          <div class="tab-pane" id="kids">
                            <ul class="nav-list list-inline">
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Running.png"><span>Running</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Basketball.png"><span>Basketball</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Football.png"><span>Football</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Soccer.png"><span>Soccer</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_MensTraining.png"><span>Men's Training</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_WomensTraining.png"><span>Women's Training</span></a></li>
                            </ul>
                          </div>
                          <div class="tab-pane" id="sports">
                            <ul class="nav-list list-inline">                                
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Basketball.png"><span>Basketball</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Football.png"><span>Football</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_Soccer.png"><span>Soccer</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_MensTraining.png"><span>Men's Training</span></a></li>
                                <li><a href="#"><img src="http://content.nike.com/content/dam/one-nike/globalAssets/menu_header_images/OneNike_Global_Nav_Icons_WomensTraining.png"><span>Women's Training</span></a></li>
                            </ul>
                          </div>
                        </div> <!--/tab-content-->
							
						
						</div> <!--/third_level_menu-->
					</div> <!--/col-md-10-->

				</div> <!--/dropdown-menu-->
            </li>
            <li><a href="#" class="hvr-underline-from-center">Fragrance</a></li>
			<li><a href="#" class="hvr-underline-from-center">Fitness</a></li>
			<li><a href="#" class="hvr-underline-from-center" >Health & Wellness</a></li>
			<li><a href="#" class="hvr-underline-from-center">Accessories</a></li>
			<li><a href="#" class="hvr-underline-from-center">Services</a></li>
			<li><a href="#" class="hvr-underline-from-center">Value</a></li>
			<li><a href="#" class="hvr-underline-from-center">Pre-Owned</a></li>
           
          </ul>
          
        </div><!--/.nav-collapse -->
      </div>
    </nav>




























<!-- {if isset($category_array)}

{foreach from=$category_array item=res1}	
	
	<div >
		<ul>
			<li ><a style="color: blue;" href="{$res1.id_category}">{$res1.name}</a></li>			
		</ul>			
	</div>	

		{if isset($res1['children']) }
			{foreach from=$res1['children'] item=res2}	
		
		<div>
			<ul>
				<li><a style="color: red;" href="{$res2.id_category}">{$res2.name}</a></li>			
			</ul>			
		</div>		
		
						{if isset($res2['children']) }
						{foreach from=$res2['children'] item=res3}	
					
					<div>
						<ul>
							<li><a style="color: green;" href="{$res3.id_category}">{$res3.name}</a></li>			
						</ul>			
					</div>		
					

						{/foreach}
					{/if}



			{/foreach}
		{/if}

{/foreach}
{/if}















