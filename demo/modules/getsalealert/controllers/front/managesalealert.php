<?php

class GetSaleAlertManageSaleAlertModuleFrontController extends ModuleFrontController
{
	public $ssl = true;
	public $auth = true;
	
	
	public function __construct()
	{
		parent::__construct();
		$this->context = Context::getContext();
		require_once($this->module->getLocalPath().'getsalealert.php');
	}

	/**
	 * @see FrontController::initContent()*/
	 
	public function initContent()
	{
		//parent::initContent();
		//$action = Tools::getValue('action');

		parent::initContent();

		/**
	 	* Assign emplate
		*/
		$this->_getSaleAlertList();
		//$this->setTemplate(_PS_MODULE_DIR_."getsalealert/views/templates/front/managesalealert.tpl");
	 	
		

	}

	private function _getSaleAlertList(){


		$this->context = Context::getContext();
		//ddd($this->module);
		$list = array();
		if($this->context->customer->isLogged())
		{
			$custID = $this->context->customer->id;
			$param['id_customer'] = $custID;
			$sql = "SELECT * FROM "._DB_PREFIX_."getsalealert_user WHERE user_id = ".$custID;

			$list = $this->module->_getAlertList($param); // tHIS FUNCTION ACCEPT ARRAY OF PARAMEMTERS
		}

		$this->context->smarty->assign(array(
				'alertlist' => $list,
			));
		$this->setTemplate("managesalealert.tpl");
		//ddd($list);

	}

	

}
