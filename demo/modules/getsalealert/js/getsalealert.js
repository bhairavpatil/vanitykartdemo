$(document).ready(function(){


	$(document).on('click','#get-sale-alert',function(){
		getsalealert('add');
	})

})

$(document).on('change','.wishlist-pick-prod',function(){

	var actSaleAlert = false;
	var actAddBag = false;

	if($('.wishlist-pick-prod:checked').length)
	{
		actAddBag = true;
		
		$('.wishlist-pick-prod:checked').each(function(){

			var myVal = $(this).val();
			var temp = $('.alert-on-'+myVal);
			if(!$(temp).is('.active')){
			actSaleAlert = $(this).val();
			//itr++;
			}
		})

	}

	toggleSaleAlert(actSaleAlert);

	if(actAddBag)
		$('#wishlist-add-to-bag').fadeIn();
	else
		$('#wishlist-add-to-bag').fadeOut();
})

function toggleSaleAlert(action=false,actionrm=false)
{
	if(action)
		$('#get-sale-alert').fadeIn();
	else
		$('#get-sale-alert').fadeOut();

	if(actionrm)
		$('#get-sale-alert').remove();
}

function getsalealert(action,myProdID,refresh=false){

	var param = {};
	var alert_action = action;
	if(typeof myProdID!= 'undefined' && action=='add')
		param = {id_prod:myProdID,action:action};
	else if(typeof myProdID!= 'undefined' && action=='remove')
		param = {id_prod:myProdID,action:action};

	var prodArr = [];
	var totItems = $('.wishlist-pick-prod').length;
	var totOffSales = $('.on-alert').length;
	var itr = 0;
	// GET ALL PICKED PRODUCT
	$('.wishlist-pick-prod:checked').each(function(){

		var myVal = $(this).val();
		var temp = $('.alert-on-'+myVal);
		if(!$(temp).is('.active')){
		prodArr[itr] = $(this).val();
		itr++;
		}
	})

	if(alert_action=='add')
	{
		param.id_prod = prodArr;
	}

	//console.log(prodArr);
	//return false;

	$.ajax({
			type:'POST',
			url:baseDir+'modules/getsalealert/subscribe.php',
			data:param,
			dataType:'json',
			headers: { "cache-control": "no-cache" },
			cache:false,
			success:function(data){

				var message = 'You have subscribed to Sale Alert';

				
				if(alert_action=='add')
				{
					$.each(data,function(index,value){

							if(parseInt(value['status'])>=0)
							{
								$('.alert-on-'+value['id']).fadeIn(function(){
										// APPEND REMOVAL OF SALEALERT
var removeAlert = '<span id="rm-alert-'+value['id']+'" onclick="javascript:getsalealert(\'remove\','+value['status']+');" style="visibility: hidden;"></span>';
										$(this).toggleClass('active on-alert');
										$(this).after(removeAlert);

								});

								totOffSales += 1;

							}
					});
					
				}
				

				if(alert_action=='add')
				{
					if((totItems-totOffSales)<=0)
					toggleSaleAlert(false,true); // hide and remove button 
					else
					toggleSaleAlert(false); // hide button
				}
				else if(alert_action=='remove' && !refresh){
					totItems -= 1; //$('.wishlist-pick-prod').length;

					if((totItems-totOffSales)<=0 || totItems<=0)
					toggleSaleAlert(false,true); // hide and remove button 
				}
				else if(alert_action=='remove' && refresh){
					window.location.reload();
				}

				console.log('tot:'+totItems+' active:'+totOffSales);

				
				
			},
			error:function(){
				console.log("Error-GSA: Request Could'nt not completed");
			}


		});
}
