<?php


require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/getsalealert.php');
//require_once(dirname(__FILE__).'/classes/GetSaleAlert.php');

require_once(dirname(__FILE__).'/../blockwishlist/blockwishlist.php');
require_once(dirname(__FILE__).'/../blockwishlist/WishList.php');



$context = Context::getContext();
$custInfo = array();



if(Tools::getValue('checkAlert')){
	
	checkPriceDropAlert();
}
else if(Tools::getValue('sendAlert')){
	
	sendDropPriceAlert($context);
}
else if($context->customer->isLogged())
{
	$custInfo['id'] = (int)$context->customer->id;
	$custInfo['email'] = $context->customer->email;

	$temp1 = array();
	
	/*if(Tools::getValue('id_prod') && Tools::getValue('action')=='add')
	{
		$idProd = (int) Tools::getValue('id_prod');
		$temp['id'] = $idProd;
		$temp['status'] = checkSaleAlertProd($idProd);
		$temp1[] = $temp;
	}*/
	if(Tools::getValue('id_prod') && Tools::getValue('action')=='remove')
	{
		$idAlert = (int) Tools::getValue('id_prod');
		$temp['id'] = $idAlert;
		$temp['status'] = removeSaleAlertProd($idAlert);
		$temp1[] = $temp;
	}
	// IF INDIVIDUAL PRODUCT IS REQUESTED
	else
	{
			//die(json_encode(Tools::getvalue('id_prod')));

			//$wishlists = Wishlist::getByIdCustomer((int)$context->customer->id);
			//die(json_encode($wishlists));
			$prodArr = Tools::getvalue('id_prod');

			if(count($prodArr)){
			
				//$products = WishList::getProductByIdCustomer($wishlists[0]['id_wishlist'], $context->customer->id, $context->language->id, null, true);

				//if(count($products)){
				
					foreach ($prodArr as $value) {
						// # code...
						// check existance of product in getsalealert
						//$temp['prod_name'] = $value['name'];
						$temp['id'] = $value;
						$temp['status'] = checkSaleAlertProd($value);
						$temp1[] = $temp;

					}
				//}
	  		}
	}

	die(json_encode($temp1));
}
else
{
	return false;
}

	function checkSaleAlertProd($prodID){

			global $custInfo;
			$custID = $custInfo['id'];
			$prodExist = false;
			$sql = 'SELECT * FROM '._DB_PREFIX_.'getsalealert_user WHERE id_prod = '.$prodID. ' AND user_id = '.$custID;
			$prodExist = Db::getInstance()->getRow($sql);
			if(!$prodExist)
			{
				return createSaleAlertSubscription($prodID);
			}
			else
			{
				return $prodExist['alert_id'];
				
			}

		}

	/*function createSaleAlertProd($prodID){

		$current_price = Product::getPriceStatic($prodID, true, null, 2);
		$sql = "INSERT INTO "._DB_PREFIX_."getsalealert_prod (id_prod,previous_price,current_price) VALUES (".$prodID.",".$current_price.",".$current_price.")";
		if(Db::getInstance()->ExecuteS($sql))
		{
			$lastID = Db::getInstance()->Insert_ID();
			createSaleAlertSubscription($lastID);
		}


	}*/

	function createSaleAlertSubscription($prodID){

		global $custInfo;
		$custID = $custInfo['id'];
		$custEmail = $custInfo['email'];
		//die($custEmail);
		// check previous existance
		$checkDuplicate = "SELECT * FROM "._DB_PREFIX_."getsalealert_user WHERE user_id = ".$custID." AND prod_id = ".$prodID;
		if(!Db::getInstance()->getRow($checkDuplicate)){

			$current_price = Product::getPriceStatic($prodID, $usetax = true, $id_product_attribute = null, $decimals = 2, $divisor = null,
        $only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = $custID);

			$status = 0; // Not subscribed
		
			$sql = "INSERT INTO "._DB_PREFIX_."getsalealert_user (user_id,email,id_prod,previous_price,current_price,flag) VALUES (".$custID.",'".$custEmail."',".$prodID.",".$current_price.",".$current_price.",0)";

			if(Db::getInstance()->ExecuteS($sql))
			$status = Db::getInstance()->Insert_ID();

		}

		return $status;

	}

	function removeSaleAlertProd($idAlert)
	{
		$status = false;
		$sql = "SELECT * FROM "._DB_PREFIX_."getsalealert_user WHERE alert_id = ".$idAlert;

		$getAlert = Db::getInstance()->getRow($sql);
		if(count($getAlert))
		{
			$status = Db::getInstance()->delete('getsalealert_user','alert_id = '.$idAlert);

			$sql1 = "SELECT * FROM "._DB_PREFIX_."getsalealert_email_pool WHERE user_id = ".$getAlert['user_id']. " AND id_prod = ".$getAlert['id_prod'];

			$getPool = Db::getInstance()->getRow($sql1);

			if(count($getPool))
			Db::getInstance()->delete('getsalealert_email_pool', 'user_id = '.$getPool['user_id'].' AND id_prod = '.$getPool['id_prod']);
		}
		return $status;
	}

	// FUNCTION TO CHECK CURRENT PRICE OF PRODUCT 

	function checkPriceDropAlert(){

	$arrayToProcess = array();

	$priceDropArr = array();

	$priceDropEmail = array();

	// GET ALL PRODUCT FOR WHICH SUBSCRIPSIONS HAS APPLIED

	$sql = "SELECT id_prod FROM "._DB_PREFIX_."getsalealert_user GROUP BY id_prod";

	$prodArr = Db::getInstance()->ExecuteS($sql);



	if(count($prodArr))
	{
		//die(ddd($prodArr));
		// GET CUSTOMER SUBSCRIBED FOR THE PRODUCT
		foreach ($prodArr as $key => $value) {

			// # code...
			$prodID = $value['id_prod'];
			$sql1 = "SELECT user_id FROM "._DB_PREFIX_."getsalealert_user WHERE id_prod = ".$prodID;
			$custArr = Db::getInstance()->ExecuteS($sql1);
			if($custArr)
			{
				$arrayToProcess[$prodID] = $custArr;
			}
		}
	}
	

	if(count($arrayToProcess))
	{

		// get price 
		foreach ($arrayToProcess as $id_product => $arrCust) {
			// # code...
			// GET CURRENT PRICE FROM LOGS AND CHECK CURRENT PRICE OF PRODUCT FOR EACH CUSTOMER
			foreach ($arrCust as $key => $value) {
				// # code...
				$custID = $value['user_id'];
				$sql3 = "SELECT * FROM "._DB_PREFIX_."getsalealert_user WHERE id_prod = ".$id_product ." AND user_id = ".$custID;

				$alertDetails = Db::getInstance()->getRow($sql3);
				//die(ddd($sql3));
				$loggedPrice = $alertDetails['current_price'];
				$custEmail = $alertDetails['email'];

				$current_price = Product::getPriceStatic($id_product, $usetax = true, $id_product_attribute = null, $decimals = 2, $divisor = null,
        $only_reduc = false, $usereduc = true, $quantity = 1, $force_associated_tax = false, $id_customer = $custID);

				if(($current_price-$loggedPrice)<0) {
				// IF CURRENT PRICE OF PRODUCT IS LESS THAN PREVIOUS LOGGED PRICE BY CUSTOMER.
					//$tempArr['id_prod'] = $id_product;
					$tempArr['previous_price'] = $loggedPrice;
					$tempArr['current_price'] = $current_price;

					$priceDropArr[$custID][$id_product] = $tempArr;

					if(!array_key_exists($custID,$priceDropEmail))
						$priceDropEmail[$custID] = $custEmail;

					// CHANGE THE LOGGED AND CURRENT PRICE OF PRODUCT IN LIST
					$dbArr = $tempArr;
					$dbArr['previous_price'] = $current_price;
					Db::getInstance()->update('getsalealert_user',$dbArr, 'id_prod = '.$id_product .' AND user_id = '.$custID);

				}

			}
		}
	}

	// lOG DETAILS IN GETSALEALERT_EMAIL_POOL TO SEND EMAIL
	// die(ddd($priceDropArr));


  	if(count($priceDropArr))
  	{
  		foreach ($priceDropArr as $id_cust => $value) {
  			// # code...
  			$custEmail = $priceDropEmail[$id_cust];

  			foreach ($value as $id_prod => $pricevalue) {
  				// # code...
  			
  			$sql = "INSERT INTO "._DB_PREFIX_."getsalealert_email_pool (user_id,email,id_prod,previous_price,current_price) VALUES (".$id_cust.",'".$custEmail."',".$id_prod.",".$pricevalue['previous_price'].",".$pricevalue['current_price'].")";

  				Db::getInstance()->ExecuteS($sql);
  			}
  		}
  	}
  	
  	die(ddd($priceDropArr));
}

function sendDropPriceAlert($context,$maxLimit = 50){

	$pool_exist = false;
	$sendEmailArr = array();
	// CHECK EMAIL POOL IF EXISTS
	$sql = "SELECT user_id, email FROM "._DB_PREFIX_."getsalealert_email_pool GROUP BY user_id LIMIT 0, ".$maxLimit;
	

	if(Db::getInstance()->ExecuteS($sql)){

		$emailPool = Db::getInstance()->ExecuteS($sql);

		$pool_exist = true;

		$link = new Link();

		foreach ($emailPool as $key => $value) {
			// SELECT PRODUCT FOR EACH
			$custID = $value['user_id'];
			$custEmail = $value['email'];
			$custName = Db::getInstance()->getValue("SELECT firstname FROM "._DB_PREFIX_
				. "customer WHERE id_customer = ".$custID);
			

			$sql1 = "SELECT * FROM "._DB_PREFIX_."getsalealert_email_pool WHERE user_id = ".$custID;
			$prodArr = Db::getInstance()->ExecuteS($sql1);



			foreach ($prodArr as $key => $value) {
				$tempProd = new Product((int)$value['id_prod']);
				$image = Product::getCover($value['id_prod']);
				$imagePath = $context->link->getImageLink($tempProd->link_rewrite[1], $image['id_image'], ImageType::getFormatedName('home'));
				$prodArr[$key]['prod_name'] = $tempProd->name[1];
				$prodArr[$key]['prod_img'] = $imagePath;
				$prodArr[$key]['link_rewrite'] = $tempProd->link_rewrite[1];
			}

			$context->smarty->assign(array('products' => $prodArr));
			$t = $context->smarty->fetch(_PS_MODULE_DIR_.'getsalealert/views/templates/front/getsalealertemail.tpl');

			//die(ddd($prodArr));

			$sendEmailArr[$custEmail] = array('customer' => $custName,'template' => $t);
		}

	}
	
	// START SENDING EMAIL IF EMAIL POOL EXISTS
	/*public static function Send($id_lang, $template, $subject, $template_vars, $to,
$to_name = null, $from = null, $from_name = null, $file_attachment = null, $mode_smtp = null,
$template_path = _PS_MAIL_DIR_, $die = false, $id_shop = null, $bcc = null)*/
	
	if($pool_exist){

		//die(ddd($sendEmailArr));

		/*Mail::Send($context->language->id, 'salealertemail', 'Get Sale Alert',
						array(
							'{customer_name}' => 'test',
							), 'geetepunit@gmail.com', 'test', Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), null, true, dirname(__FILE__).'/mails/', true);*/

		foreach ($sendEmailArr as $to => $emailParam) {
			// # code...
			Mail::Send($context->language->id, 'salealertemail', 'Get Sale Alert',
						array(
							'{customer_name}' => $emailParam['customer'],
							'{email_body}' => $emailParam['template'],
							), 'geetepunit@gmail.com', $emailParam['customer'], Configuration::get('PS_SHOP_EMAIL'), Configuration::get('PS_SHOP_NAME'), null, true, dirname(__FILE__).'/mails/', true);
		}
		//ddd($t);
	}
	else
	die('No email pool');
	
}
