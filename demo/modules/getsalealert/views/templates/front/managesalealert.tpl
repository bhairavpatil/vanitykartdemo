<div class="panel col-lg-12">
		<h3>Manage Sale Alerts</h3>
{if $alertlist}

	<div class="table-responsive-row clearfix">
		<table class="table configuration">
			<thead>
				<tr class="nodrag nodrop">
															<th class="">
						<span class="title_box">
															ID
																				</span>
					</th>
										
										<th class="">
						<span class="title_box">
															Image
																				</span>
					</th>
										<th class="">
						<span class="title_box">
															Product Name
																				</span>
					</th>
										<th class="">
						<span class="title_box">
															Alert Price
																				</span>
					</th>

					<th class="">
						<span class="title_box">
															Action
																				</span>
					</th>
																					<th></th>
									</tr>
						</thead>

<tbody>

	{foreach from=$alertlist key=i item=product}
	<tr class="">
					<td class="pointer">
						{math equation="$i+1"}
					</td>		
					<!-- <td class="pointer">
					{$product.alert_id}
					</td> -->
					<td class="pointer">
						<a href="{$link->getProductLink($product.id_prod, $product.link_rewrite)|escape:'html'}" title="{$product.name}">{$product.name}</a>
					</td>
					<td class="pointer">
						<a href="{$link->getProductLink($product.id_prod, $product.link_rewrite)|escape:'html'}" title="{$product.name}">{$product.image}</a>
					</td>
					<td class="pointer">
						{$product.current_price}
					</td>
					<td class="pointer">
						<span class="btn btn-default" onClick="getsalealert('remove',{$product.alert_id},true);">Remove</span>
					</td>
					
			
		
			</tr>
	{/foreach}
</tbody>

	</table>
</div>
<div class="row">
	<div class="col-lg-6">
			</div>
	</div>


	
{else}
<div class="row">
	<div class="col-lg-6">
			No susbscriptions found. For sale alert subscriptions, first create a wishlist and then select items to get sale alerts.
			</div>
	</div>
{/if}
</div>