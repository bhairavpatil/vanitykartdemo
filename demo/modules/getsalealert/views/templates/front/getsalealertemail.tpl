{if $products}

<table>
		{foreach from=$products item=product name=i}
		<tr>
				<td class="prod-image"><a href="{$link->getProductLink($product.id_prod, $product.link_rewrite)|escape:'html'}"><img src="{$product.prod_img}" title="{$product.prod_name}" alt="{$product.prod_name}" style="width: 100px; border:thin solid #dedede; border-radius: 5px;"></a></td><td class="prod-info" style="padding: 5px 10px; vertical-align: top !important;"><a href="{$link->getProductLink($product.id_prod, $product.link_rewrite)|escape:'html'}" style="color:#555454;"><span class="prod-name" style="font-weight:bold; font-size: 16px; float: left; margin: 0 0 5px 0;">{$product.prod_name|escape:'html':'UTF-8'}</span></a><br><span style="float: left;width: 100%"><strong>Was: {$product.previous_price}</strong></span><br><span style="float: left;width: 100%"><strong>Now: {$product.current_price} </strong></span><p></p><span class="drop-perc" style="background: red; font-weight: bold; padding: 5px; border-radius: 3px; margin-top: 5px; color: #fff; float: left;">{math equation="round(((x - y)/x)*100)" x={$product.previous_price} y={$product.current_price}}% OFF<span></td>
		</tr>
		{/foreach}
</table>

	
{/if}