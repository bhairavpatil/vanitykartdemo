<?php
/*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.6.0
 */
//ddd(dirname(__FILE__));

require_once(dirname(__FILE__).'/../../classes/manageboardclass.php');

class AdminManageBoardController extends ModuleAdminController
{
    private $_html = '';
    public $confirmation = '';
    public $errors = array();
	public function __construct()
    {
        $this->table = 'moodboard_content';
        $this->module = 'manageboard';
        $this->class = 'manageboardclass';
        $this->lang = false;
        $this->identifier = 'id';
        $this->bootstrap = true;
        $this->context = Context::getContext();

        $this->fields_list = array(
            'id' => array(
                'title' => 'ID',
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'title' => array(
                'title' => $this->l('Title'),
                'type' => 'text',
            ),
            'temp_id' => array(
                'title' => $this->l('Moodboard')
            ),
            'promoted_pages' => array(
            'title' => $this->l('Promoted Pages'),
            ),
            'status' => array(
                'title' => $this->l('Approved'),
                'active' => 'toggle',
                'type' => 'bool',
                'align' => 'center',
                'orderby' => false
            )
            
            
        );

        parent::__construct();
    } 

    public function renderList()
    {
        if(isset($this->_filter) && trim($this->_filter) == '')
            $this->_filter = $this->original_filter;

        $this->addRowAction('edit');
        $this->addRowAction('delete');

        //ddd($this);

        return parent::renderList();

    }
    public function initContent()
    {
        parent::initContent();
        //$this->context->smarty->assign(array(
           // 'confirmation' => $this->confirmation
        //));
        //$this->setTemplate('display.tpl');

        //$this->context->smarty->assign(array(
//'errors' => $errors,
//));
        
    }


    public function renderForm(){

        $myRedirect = $this->context->link->getAdminLink('AdminModules', true).'&configure='.$this->module->name.'&tab_module='.$this->module->tab.'&module_name='.$this->module->name.'&action=addmoodboard';

        if(Tools::isSubmit('addmoodboard_content')){
         
         Tools::redirectAdmin($myRedirect);
        }
        else if(Tools::isSubmit('updatemoodboard_content') && Tools::getValue('id'))
        {
            $id = Tools::getValue('id');
            Tools::redirectAdmin($myRedirect.'&updatemoodboard_content&id='.$id);
        }


    }

    public function postProcess()
    {
        if(Tools::isSubmit('deletemoodboard_content'))
        {
            $this->_deletePromotions();

        }
        if(Tools::isSubmit('togglemoodboard_content'))
        {
            $this->_updatePromotions();

        }
        return parent::postProcess();
    }

    private function _deletePromotions()
    {
        //ddd(2);
        $err = 0;
        $mid = Tools::getValue('id');
        if(!Db::getInstance()->delete($this->table, 'id = '. (int)$mid))
            $err = 1;
        if(!Db::getInstance()->delete($this->table.'_promotion', 'mid = '. (int)$mid))
            $err = 1;


        if($err)
        $this->errors[] = Tools::displayError('Error while deleting from the database') . ': ' . mysql_error();
        else
            $this->redirect_after = self::$currentIndex.'&conf=2&token='.$this->token;

    }

    private function _updatePromotions(){

        $id  = Tools::getValue('id');

        //if($id)
            $getStatus = Db::getInstance()->getValue('SELECT status FROM '._DB_PREFIX_.$this->table.' WHERE id = '. (int)$id);

        //ddd($getStatus);

        if($getStatus){
            // if(!Db::getInstance()->update($this->table, array('status' => 0 ), ' WHERE id = '. (int)$id))
            if(!Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.$this->table.' SET status = 0  WHERE id = '. (int)$id))
                $this->errors[] = Tools::displayError('Error while changing status 0');
            else
            $this->redirect_after = self::$currentIndex.'&conf=5&token='.$this->token;
        }
        else{
            if(!Db::getInstance()->ExecuteS('UPDATE '._DB_PREFIX_.$this->table.' SET status = 1  WHERE id = '. (int)$id))
                $this->errors[] = Tools::displayError('Error while changing status 1');
            else
            $this->redirect_after = self::$currentIndex.'&conf=5&token='.$this->token;
        }
    }

    /*public function ajaxProcessgetsearchcontent(){}

   public function getSelectBox($id_content,$id_ele){}

    public function getColorOptions($id_prod){}


public function _getTemplatePreview($id_temp,$data){}*/

public function ajaxProcessgetsearchcontent(){


        $id_prod = Tools::getValue('id_prod');
        $id_temp = Tools::getValue('id_temp');
        $type = Tools::getValue('type');
        //$id_ele = Tools::getValue('id_ele');
        //$html = $this->getSelectBox($id_content,$id_ele);
        if($id_prod){
        $html = $this->getColorOptions($id_prod,$type);
        die(Tools::jsonEncode(array('thumbnail' => $html)));
        }

        if(is_numeric($id_temp) && $id_temp>=0)
        {
            $id_data = Tools::getValue('data');
            $html = $this->_getTemplatePreview($id_temp,$id_data);
            //die(Tools::jsonEncode(array('thumbnail' => $id_data)));
            die($html);
        }
        //$label = 'Position '.$id_ele;
        //$html = '<span>'.$label.'</span>';
        //echo(0);
        //die(Tools::jsonEncode(array('thumbnail' => $html)));
        //die($html);
    }

    public function getColorOptions($id_prod,$type){

        $id_prod = $id_prod;
        $type = $type;
        //die($id_prod);

        $colorImageArr = array();
        $colors = array();
        $colorImgUrl = array();

        if($type=='product')
        {
        $product = new Product($id_prod, true, $this->context->language->id, $this->context->shop->id);
        //$combination_images = $product->getCombinationImages($this->context->language->id);
        //$attributes_groups = $product->getAttributesGroups($this->context->language->id);
        
        $combination_images = false;

        
        if(count($combination_images)>0 && $combination_images)
        {
            foreach ($attributes_groups as $k => $row) {
                if (isset($row['is_color_group']) && $row['is_color_group'] && (isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_.$row['id_attribute'].'.jpg'))){
                        $colors[$row['id_attribute']]['value'] = $row['attribute_color'];
                        $colors[$row['id_attribute']]['name'] = $row['attribute_name'];
                        if (!isset($colors[$row['id_attribute']]['attributes_quantity'])) {
                            $colors[$row['id_attribute']]['attributes_quantity'] = 0;
                        }
                        $colors[$row['id_attribute']]['attributes_quantity'] += (int)$row['quantity'];
                    }

            $colorImageArr[$row['id_attribute']] = $combination_images[$row['id_product_attribute']][0]['id_image'];
                    
            }

            //ppp($colorImageArr);
            if(count($colorImageArr)>0)
            {
            
            $temp = array_unique($colorImageArr);
            

                foreach ($temp as $key => $value) {
                    // # code...
                    //$colorImgUrl[$value] = basename($this->context->link->getImageLink($product->link_rewrite, $value, ImageType::getFormatedName('home')));
                    $colorImgUrl[$value] = $this->context->link->getImageLink($product->link_rewrite, $value, ImageType::getFormatedName('home'));
                  
                }
           }
        }
        else {
            // GET DEFAULT IMAGE OF PRODUCT IF NO COMBINATION FOUND
            // $id_image = Product::getCover($product->id);
            // $value = $id_image['id_image'];
            // $colorImgUrl[$value] = $this->context->link->getImageLink($product->link_rewrite, $value, ImageType::getFormatedName('home'));
            $images = Image::getImages($this->context->language->id,$product->id);
            // GET DEFAULT IMAGE OF PRODUCT IF NO COMBINATION FOUND
            //$id_image = Product::getCover($product->id);
            //$value = $id_image['id_image'];

            foreach ($images as $key => $value) {
                // # code...
                $colorImgUrl[$value['id_image']] = $this->context->link->getImageLink($product->link_rewrite, $value['id_image'], ImageType::getFormatedName('home'));
            }
        }

        //die(Tools::jsonEncode(array('thumbnail' => $colorImgUrl)));
           //die(json_encode(array('test' => $colorImgUrl)));

           /*$html = '';

           foreach($colorImgUrl as $key => $img){
            $this->$_html .= $img;
           }*/

           //return $colorImgUrl;
        
        $tempArr = array();
        $tempArr['name'] = $product->name;
        $tempArr['image'] =  $colorImgUrl;

        }
        else
        {
            // SELECT * CMS UPLOADED IMAGES
            $sql = "SELECT * FROM "._DB_PREFIX_."cms_lang WHERE id_cms = ".$id_prod;

            $cmsArr = Db::getInstance()->getRow($sql);

            $tempArr = array();
            $tempArr['name'] = $cmsArr['meta_title'];
            $path = 'img/cms/';
            $temp = array();
            if($cmsArr['thumb_image'])
                $temp[] = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.$path.$cmsArr['thumb_image'];
            if($cmsArr['banner_image'])
                $temp[] = 'http://'.$_SERVER['HTTP_HOST'].__PS_BASE_URI__.$path.$cmsArr['banner_image'];
            $tempArr['image'] =  $temp;

        }

        return $tempArr;
        
    
    }

    public function _getTemplatePreview($id_temp,$data){

    /*foreach ($data as $key => $value) {
        // # code...
    }*/
    $tempVar = array();
    $link = new Link();

    if($data){
        // GET DETAILS FOR DATA
        foreach ($data as $key => $value) {

            $tempArr = array();
            $tempArr['type'] = $value['type'];
            // # code...
            if($value['type']=='cms')
            {
                $newCMS =  new CMS($value['id'],$this->context->language->id);
                $cmstitle = $newCMS->meta_title;
                $cmsdesc = $newCMS->meta_description;

                // CHECK IF MOODBOARD TITLE TO BE DIPLAYED
                if($value['cms-data-flag'])
                {
                    if($value['cms-title'])
                        $cmstitle = $value['cms-title'];
                    if($value['cms-desc'])
                        $cmsdesc = $value['cms-desc'];
                }

                $tempArr['title'] = $cmstitle;
                $tempArr['description'] = $cmsdesc;
                $tempArr['link'] = $link->getCMSLink($newCMS->id, $newCMS->link_rewrite);

                // GET ASSOCIATED CATEGORY NAME
                $categoryName = '';
                if($newCMS->cat_assoc){
                $category = new Category((int)$newCMS->cat_assoc, (int)$this->context->language->id);
                $categoryName = $category->name;
                }
                $tempArr['img'] = $value['img'];
                $tempArr['category'] = $categoryName;
                $tempVar[] = $tempArr;

            }
            if($value['type']=='product')
            {
                $newProd =  new Product($value['id'],$this->context->language->id);
                $category = new Category((int)$newProd->id_category_default, (int)$this->context->language->id);
                $tempArr['title'] = $newProd->name[1];
                $tempArr['description'] = $newProd->description[1];
                $tempArr['link'] = $link->getProductLink($newProd);
                $tempArr['img'] = $value['img'];
                $tempArr['category'] = $category->name;
                $tempVar[] = $tempArr;
            }

            

        }
    }

    /*$tempVar = new Object();
    $tempVar->title = 'test';*/

    $templateName = 'template_'.$id_temp.'.tpl';
    $this->context->smarty->assign(
      array(
          'css_path' => $_SERVER['HTTP_HOST'].__PS_BASE_URI__.'modules/manageboard/css/manageboard_front.css',
          'my_module_link' => 'Hello World!', 'myproducts' => $tempVar)
  );
    //die($this->display(_PS_MODULE_DIR_.'manageboard/views/templates/admin/', $templateName));
    return $this->context->smarty->fetch(_PS_MODULE_DIR_.'manageboard/views/templates/admin/'.$templateName);

    //die(Tools::jsonEncode(array('thumbnail' => $tempVar)));

}

public function getSelectBox($id_content,$id_ele){

        //return $html;
        $label = 'Position '.$id_ele;

        $html = array(
                    'type' => 'select',
                    'label' => $label,
                    'name' => 'pos_1',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->module->_getCMSProduct($id_content),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

        return $html;

    }

	
}
