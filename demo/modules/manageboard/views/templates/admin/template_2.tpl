{if $myproducts}
<link href="//{$css_path}" rel="stylesheet" type="text/css" media="all">
<div class="clearfix">
<div class="content-section margin-btm0">
         <div class="container">
            <div class="row">
                <div class="col-md-4 showcase-row-first">
                    <div class="product-item-1 parent">
                        <div class="product-thumb child">
                            <img src="{$myproducts['0']['img']}" title="{$myproducts['0']['title']}" alt="{$myproducts['0']['title']}">
                        </div> <!-- /.product-thumb -->
                            {if $myproducts['0']['type']=='product'}
                            <div class="overlay-icon">
                                <span class="show-icon1">
                                  <a href="javascript:void(0)"></a>
                                </span>
                                <span class="show-icon2">
                                  <a href="javascript:void(0)"></a>
                                </span>
                            </div>
                            {/if}
                    </div> <!-- /.product-item -->

                    <div class="product-item-1 parent">
                        <div class="product-thumb child">
                            <img src="{$myproducts['1']['img']}" title="{$myproducts['1']['title']}" alt="{$myproducts['1']['title']}">
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['1']['type']=='product'}
                       <div class="overlay-icon">
                                <span class="show-icon1">
                                   <a href="javascript:void(0)"></a>
                                </span>
                                <span class="show-icon2">
                                    <a href="javascript:void(0)"></a>
                                </span>
                            </div>
                        {/if}
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-3 -->
                <div class="col-md-4 showcase-row-second">
                    <div class="product-holder">
                        <div class="product-item-2">
                            <div class="product-thumb">
                                <img src="{$myproducts['2']['img']}" title="{$myproducts['2']['title']}" alt="{$myproducts['2']['title']}">
                            </div> <!-- /.product-thumb -->
                        </div> <!-- /.product-item-2 -->
                        <div class="product-item-2">
                            <div class="product-content white nonebdr nonepad">
                                <h2>{$myproducts['2']['title']}</h2>
                                <p class="tagline text-lowercase" align="center">{$myproducts['2']['description']}
                                </p>
                                <div align="center"><button type="button" class="btn btn-cust hvr-shutter-out-horizontal discoverBtn">DISCOVER MORE</button></div>                                
                            </div>
                        </div> <!-- /.product-item-2 -->
                        <div class="clearfix"></div>
                    </div> <!-- /.product-holder -->
                </div> <!-- /.col-md-5 -->
                <div class="col-md-4 showcase-row-third">
                    <div class="product-item-3 parent">
                        <div class="product-thumb child">
                            <img src="{$myproducts['3']['img']}" title="{$myproducts['3']['title']}" alt="{$myproducts['3']['title']}">
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['3']['type']=='product'}
                        <div class="overlay-icon">
                                <span class="show-icon1">
                                   <a href="javascript:void(0)"></a>
                                </span>
                                <span class="show-icon2">
                                  <a href="javascript:void(0)"></a>
                                </span>
                            </div>
                            {/if}
                    </div>
                    <div class="product-item-3 parent">
                        <div class="product-thumb child">
                            <img src="{$myproducts['4']['img']}" title="{$myproducts['4']['title']}" alt="{$myproducts['4']['title']}">
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['4']['type']=='product'}
                        <div class="overlay-icon">
                                <span class="show-icon1">
                                   <a href="javascript:void(0)"></a>
                                </span>
                                <span class="show-icon2">
                                  <a href="javascript:void(0)"></a>
                                </span>
                            </div>
                            {/if}
                    </div>
                </div>
            </div> <!-- /.row -->
 

 </div> 
 </div>
 </div>
 {/if}