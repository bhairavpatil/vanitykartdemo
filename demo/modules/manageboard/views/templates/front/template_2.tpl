{if $myproducts}
         <div class="container">
            <div class="row">

                <div class="col-md-4 showcase-row-second col-md-push-4">
                    <div class="product-holder">
                        <div class="product-item-2">
                            <div class="product-thumb">
                                <img src="{$myproducts['2']['img']}" title="{$myproducts['2']['title']}" alt="{$myproducts['2']['title']}">
                            </div> <!-- /.product-thumb -->
                        </div> <!-- /.product-item-2 -->
                        <div class="product-item-2">
                            <div class="product-content white nonebdr nonepad">
                                <h2>{$myproducts['2']['title']|truncate:30}</h2>
                                <p class="tagline text-lowercase" align="center">{$myproducts['2']['description']|truncate:250:""}
                                </p>
                                <div align="center"><a href="{$myproducts['2']['link']}" title="{$myproducts['2']['title']}"><button type="button" class="btn btn-cust hvr-shutter-out-horizontal discoverBtn">DISCOVER MORE</button></a></div>                                
                            </div>
                        </div> <!-- /.product-item-2 -->
                        <div class="clearfix"></div>
                    </div> <!-- /.product-holder -->
                </div> <!-- /.col-md-4 -->

                <div class="col-md-4 col-sm-6 showcase-row-first col-md-pull-4">
                    <div class="product-item-1 parent">
                        <div class="product-thumb child">
                            <a href="{$myproducts['0']['link']}" title="{$myproducts['0']['title']}"><img src="{$myproducts['0']['img']}" title="{$myproducts['0']['title']}" alt="{$myproducts['0']['title']}"></a>
                        </div> <!-- /.product-thumb -->
                            {if $myproducts['0']['type']=='product'}
                            <div class="overlay-icon">
                                <!-- start wishlist status icon -->
                                            {hook h='displayWishlistStatus' id_prod="{$myproducts['0']['id']}"}
                                        <!-- end wishlist status icon -->
                                <span class="show-icon show-icon2">
                                  <a class="quick-view" href="{$myproducts['0']['link']}" rel="{$myproducts['0']['link']}"></a>
                                </span>
                            </div>
                            {/if}
                    </div> <!-- /.product-item -->

                    <div class="product-item-1 parent">
                        <div class="product-thumb child">
                            <a href="{$myproducts['1']['link']}" title="{$myproducts['1']['title']}"><img src="{$myproducts['1']['img']}" title="{$myproducts['1']['title']}" alt="{$myproducts['1']['title']}"></a>
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['1']['type']=='product'}
                       <div class="overlay-icon">
                                <!-- start wishlist status icon -->
                                            {hook h='displayWishlistStatus' id_prod="{$myproducts['1']['id']}"}
                                        <!-- end wishlist status icon -->
                                <span class="show-icon show-icon2">
                                    <a class="quick-view" href="{$myproducts['1']['link']}" rel="{$myproducts['1']['link']}"></a>
                                </span>
                            </div>
                        {/if}
                    </div> <!-- /.product-item -->
                </div> <!-- /.col-md-4-->

                <div class="col-md-4 col-sm-6 showcase-row-third">
                    <div class="product-item-3 parent">
                        <div class="product-thumb child">
                            <a href="{$myproducts['3']['link']}" title="{$myproducts['3']['title']}"><img src="{$myproducts['3']['img']}" title="{$myproducts['3']['title']}" alt="{$myproducts['3']['title']}"></a>
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['3']['type']=='product'}
                        <div class="overlay-icon">
                                <!-- start wishlist status icon -->
                                            {hook h='displayWishlistStatus' id_prod="{$myproducts['3']['id']}"}
                                        <!-- end wishlist status icon -->
                                <span class="show-icon show-icon2">
                                  <a class="quick-view" href="{$myproducts['3']['link']}" rel="{$myproducts['3']['link']}"></a>
                                </span>
                            </div>
                            {/if}
                    </div>
                    <div class="product-item-3 parent">
                        <div class="product-thumb child">
                            <a href="{$myproducts['4']['link']}" title="{$myproducts['4']['title']}"><img src="{$myproducts['4']['img']}" title="{$myproducts['4']['title']}" alt="{$myproducts['4']['title']}"></a>
                        </div> <!-- /.product-thumb -->
                        {if $myproducts['4']['type']=='product'}
                        <div class="overlay-icon">
                                <!-- start wishlist status icon -->
                                            {hook h='displayWishlistStatus' id_prod="{$myproducts['4']['id']}"}
                                        <!-- end wishlist status icon -->
                                <span class="show-icon show-icon2">
                                  <a class="quick-view" href="{$myproducts['4']['link']}" rel="{$myproducts['4']['link']}"></a>
                                </span>
                            </div>
                            {/if}
                    </div>
                </div> <!--/col-md-4-->
            </div> <!-- /.row -->
 

 {/if}