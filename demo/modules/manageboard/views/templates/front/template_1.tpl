	{if $myproducts}

			<div class="row moodfirst-first">
				<div class="col-md-4 col-sm-6 mood-block-first">
					<div class="product-item-vote">
						<div class="product-thumb prod-first-img">
							<img src="{$myproducts['0']['img']}" title="{$myproducts['0']['title']}" alt="{$myproducts['0']['title']}">
						</div> <!-- /.product-thum -->
						<div class="product-content">
							<span class="tagline">{$myproducts['0']['category']}</span>
							<div class="clearfix"></div>
							<h5><a href="{$myproducts['0']['link']}" title="{$myproducts['0']['title']}">
							  {$myproducts['0']['title']}</a>
							</h5>                    
						</div>
						<div class="clearfix"></div>
					</div> <!-- /.product-item-vote -->
				</div> <!-- /.col-md-4 -->
				<div class="col-md-4 col-sm-6 mood-block-second">
					<div class="product-item-vote">
						<div class="product-thumb prod-others-img">
							<img src="{$myproducts['1']['img']}" title="{$myproducts['1']['title']}" alt="{$myproducts['1']['title']}">
						</div> <!-- /.product-thum -->
						<div class="product-content">
							<span class="tagline">{$myproducts['1']['category']}</span> 
							<div class="clearfix"></div>
							<h5><a href="{$myproducts['1']['link']}" title="{$myproducts['1']['title']}">{$myproducts['1']['title']}</a></h5>                                               
						</div>
						<div class="clearfix"></div>
					</div> <!-- /.product-item-vote -->
					<div class="product-item-vote">
						<div class="product-thumb prod-others-img">
							<img src="{$myproducts['2']['img']}" title="{$myproducts['2']['title']}" alt="{$myproducts['2']['title']}">
						</div> <!-- /.product-thum -->
						<div class="product-content">
							<span class="tagline">{$myproducts['2']['category']}</span>
							<div class="clearfix"></div>
							<h5><a href="{$myproducts['2']['link']}" title="{$myproducts['2']['title']}">{$myproducts['2']['title']}</a></h5>														
						</div>
						<div class="clearfix"></div>
					</div> <!-- /.product-item-vote -->
				</div> <!-- /.col-md-4 -->
				<div class="col-md-4 col-sm-6 mood-block-third">
					<div class="product-item-vote">
						<div class="product-thumb prod-others-img">
							<img src="{$myproducts['3']['img']}" title="{$myproducts['3']['title']}" alt="{$myproducts['3']['title']}">
						</div> <!-- /.product-thum -->
						<div class="product-content">
							<span class="tagline">{$myproducts['3']['category']}</span>
							<div class="clearfix"></div>
							<h5><a href="{$myproducts['3']['link']}" title="{$myproducts['3']['title']}">{$myproducts['3']['title']}</a></h5>
						</div>
						<div class="clearfix"></div>
					</div> <!-- /.product-item-vote -->
					<div class="product-item-vote">
						<div class="product-thumb prod-others-img">
							<img src="{$myproducts['4']['img']}" title="{$myproducts['4']['title']}" alt="{$myproducts['4']['title']}">
						</div> <!-- /.product-thum -->
						<div class="product-content">
							<span class="tagline">{$myproducts['4']['category']}</span>
							<div class="clearfix"></div>
							<h5><a href="{$myproducts['4']['link']}" title="{$myproducts['4']['title']}">{$myproducts['4']['title']}</a></h5>
						</div>
						<div class="clearfix"></div>
					</div> <!-- /.product-item-vote -->
				</div> <!-- /.col-md-4 -->
			</div> <!--/row-->
		{/if}
		