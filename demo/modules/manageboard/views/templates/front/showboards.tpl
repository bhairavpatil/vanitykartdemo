
<div class="mood-board-sec">
	<div class="container">
		<div class="row">	
			<div class="col-lg-9 col-md-9 col-sm-8">
				<!-- Moodboard Section Start-->
					{hook h='displayBoardOne'}
				<!-- Moodboard Section End-->
			</div> <!--/col-md-9-->			
			<div class="col-lg-3 col-md-3 col-sm-4">
				<!-- Article Section Start-->
					{hook h='homeDisplayRight'}
				<!-- Article Section End-->
			</div> <!--/col-md-3-->		
		</div> <!--/row-->
	</div> <!--/container-->
</div> <!--/mood-board-sec-->

<!-- Showcase Section Start-->
<div class="showcase-sec">
	<div class="row">
		<div class="col-lg-12 col-md-12">
			{hook h='displayBoardTwo'}
		</div> <!--/col-lg-12-->
	</div> <!--/row-->
</div> <!--/showcase-sec-->
<!-- Showcase Section End-->
