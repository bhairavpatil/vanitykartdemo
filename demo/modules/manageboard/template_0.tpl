<div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="product-item-vote">
                <div class="product-thumb">
                    <img src="images/grid-img1.jpg" alt="img">
                </div> <!-- /.product-thum -->
                <div class="product-content">
                    <span class="tagline">Fitness</span>                            
                    <h5><a href="#">
                      ROONEY MARA'S ROAD <br>TO THE 2016 OSCARS</a>
                    </h5>                    
                </div>
            </div> <!-- /.product-item-vote -->
        </div> <!-- /.col-md-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="product-item-vote">
                <div class="product-thumb">
                    <img src="images/grid-img2.jpg" alt="img">
                </div> <!-- /.product-thum -->
                <div class="product-content">
                    <span class="tagline">Health &amp; wellness</span> 
                    <h5><a href="#">10 Ideas For A Sugar<br>Detox Diet Plan</a></h5>
                                               
                </div>
            </div> <!-- /.product-item-vote -->
            <div class="product-item-vote">
                <div class="product-thumb">
                    <img src="images/grid-img3.jpg" alt="img">
                </div> <!-- /.product-thum -->
                <div class="product-content">
                    <span class="tagline">Beauty</span>
                    <h5><a href="#">contouring magic in<br>90 seconds</a></h5>
                                                
                </div>
            </div> <!-- /.product-item-vote -->
        </div> <!-- /.col-md-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="product-item-vote">
                <div class="product-thumb">
                    <img src="images/grid-img4.jpg" alt="img">
                </div> <!-- /.product-thum -->
                <div class="product-content">
                    <span class="tagline">fragrances</span>
                    <h5><a href="#">New Rose-Infused<br>Beauty Products to Try</a></h5>
                </div>
            </div> <!-- /.product-item-vote -->
            <div class="product-item-vote">
                <div class="product-thumb">
                    <img src="images/grid-img5.jpg" alt="img">
                </div> <!-- /.product-thum -->
                <div class="product-content">
                    <span class="tagline">Fitness</span>
                    <h5><a href="#">can Jogging Help You<br>Lose Weight?</a></h5>
                </div>
            </div> <!-- /.product-item-vote -->
        </div> <!-- /.col-md-3 -->
        <div class="col-md-3 col-sm-6">
            <div class="product-item-vote news-section">
                <div class="product-content news-title">
                    <span class="news-title"><img src="images/flower5.png" alt="flowers"> Latest for you</span>                           
                </div>
                
                 <div class="product-content">
                    <span class="tagline">Fashion</span>
                    <h5><a href="#">3 expert ways to wear denim shorts at a festival</a></h5>                            
                </div>
                <hr>
                <div class="product-content">
                    <span class="tagline">News</span>
                    <h5><a href="#">Revisiting Reality TV's Biggest Body Transformations</a></h5>                            
                </div>
                <hr>
                <div class="product-content">
                    <span class="tagline">Fitness</span>
                    <h5><a href="#">7 Things You May Not Know About Richard Simmons</a></h5>                            
                </div>
                <hr>
                <div class="product-content">
                    <span class="tagline">Health &amp; wellness</span>
                    <h5><a href="news-article.html">Top 5 Eco-friendly Women's roll-on deos</a></h5>                            
                </div>
               
                <hr>
                 <div class="product-thumb">
                    <img src="images/grid-img6.jpg" alt="img">
                </div> <!-- /.product-thum -->
            </div> <!-- /.product-item-vote -->
        </div> <!-- /.col-md-3 -->
    </div>