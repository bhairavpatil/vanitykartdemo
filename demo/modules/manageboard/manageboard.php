<?php
if (!defined('_PS_VERSION_'))
  exit;

class ManageBoard extends Module 
{
	private $_html = '';

	private $imgValidatorArr = array();

	private $dbValues = array();

	function __construct()
	{
		$this->name = 'manageboard';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
    	$this->author = 'uwtech';
    	$this->need_instance = 0;
    	$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_); 
    	$this->bootstrap = true;
 
    	parent::__construct();
 
    	$this->displayName = $this->l('Manage Boards');
    	$this->description = $this->l('Module to create different boards (Mood, Story) and manage locations to display those boards.');

	}

	public function install(){

		/* CREATE TABLE IF NOT EXISTS `moodboard_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL,
  `template_file_name` varchar(256) DEFAULT NULL,
  `data` mediumtext,
  `created` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ; */

/*
CREATE TABLE IF NOT EXISTS `moodboard_content_promotion` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mid` int(11) DEFAULT NULL COMMENT 'Id of moodboard',
  `page_name` varchar(256) DEFAULT NULL,
  `position` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
*/

		// Prepare tab
		$tab = new Tab();
		$tab->active = 1;
		//$tab->class_name = 'AdminSearchContent';
		$tab->class_name = 'AdminManageBoard';
		$tab->name = array();
		foreach (Language::getLanguages(true) as $lang)
			//$tab->name[$lang['id_lang']] = 'SearchContent';
			$tab->name[$lang['id_lang']] = 'ManageBoard';
		$tab->id_parent = -1;
		$tab->module = $this->name;

		if(!$tab->add() || !parent::install() || !$this->registerHook('actionAdminManageBoardListingResultsModifier') || !$this->registerHook('displayHome') || !$this->registerHook('displayBoardOne') || !$this->registerHook('displayBoardTwo') || !$this->registerHook('Header') || !$this->registerHook('actionAdminControllerSetMedia'))
			return false;
		 return true;
	}

	public function uninstall(){

		//$id_tab = (int)Tab::getIdFromClassName('AdminSearchContent');
		$id_tab = (int)Tab::getIdFromClassName('AdmimManageBoard');
		if ($id_tab)
		{
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return parent::uninstall();
	}

	public function hookActionAdminControllerSetMedia($param){

			if(Tools::getValue('configure')=='manageboard'){
			$this->context->controller->addCSS($this->_path.'/css/'.$this->name.'.css');
			$this->context->controller->addJS($this->_path.'/js/'.$this->name.'.js');
			}
	}
	
	public function hookHeader(){
		$this->context->controller->addCSS($this->_path.'/css/'.$this->name.'_front.css');
		//$this->context->controller->addCSS($this->_path.'/css/custom.css');
	}

	public function hookDisplayHome($params){
	 return $this->display(__FILE__, 'showboards.tpl');
	}

	public function hookDisplayBoardOne($param){



		$moodboard = $this->getMoodBoard(1);

		if(count($moodboard)) // IF ANY MOODBOARD ASSIGNED
		{
		$boardData = unserialize(str_replace("'", "\'", $moodboard['data']));
		$moodboardContent = $this->_getBoardTemplate($moodboard['temp_id'],$boardData);

		//$boardArr['template'] = $templateName;
    //$boardArr['template_vars'] = $tempVar;

		$this->context->smarty->assign(array(
				'myproducts'=> $moodboardContent['template_vars']
			));
	
		
		}
	
		return $this->display(__FILE__, 'template_1.tpl');
	}

	public function hookDisplayBoardTwo($param){



		$moodboard = $this->getMoodBoard(2);

		if(count($moodboard)) // IF ANY MOODBOARD ASSIGNED
		{
		$boardData = unserialize(str_replace("'", "\'", $moodboard['data']));
		$moodboardContent = $this->_getBoardTemplate($moodboard['temp_id'],$boardData);

		//$boardArr['template'] = $templateName;
    //$boardArr['template_vars'] = $tempVar;

		$this->context->smarty->assign(array(
				'myproducts'=> $moodboardContent['template_vars']
			));

		
		}
	
		return $this->display(__FILE__, 'template_2.tpl');
	}


	public function getMoodBoard($id){

		$context = Context::getContext();
		$myController = $context->controller->php_self;
		$boardID = $id;
		/*switch($context->controller->php_self)
		{

			$pageCat = '';
			case 'index':
			$pageCat = $context->shop->id_category;
			break;
			case 'product':
			break;

		}*/

		if($myController=='index')
			$pageCat = $context->shop->id_category;
		else if($myController=='product')
			$pageCat = $context->shop->id_category;

		$sql = "SELECT * FROM "._DB_PREFIX_."moodboard_content_promotion a, "._DB_PREFIX_."moodboard_content b WHERE a.mid = b.id and b.temp_id = $boardID and b.status = 1 and a.page_id = ".$pageCat." GROUP BY a.mid";

		$promotedBoard = Db::getInstance()->getRow($sql);

		return $promotedBoard;

		
	}

	private function _getBoardTemplate($id_temp,$data = array()){

    /*foreach ($data as $key => $value) {
        // # code...
    }*/
    $boardArr = array();
    $tempVar = array();
    $link = new Link();
    //$data = unserialize($data);

    if($data){
        // GET DETAILS FOR DATA
        foreach ($data as $key => $value) {

        	foreach ($value as $key1 => $value1) {
        	
            	$tempArr = array();
            	$tempArr['type'] = $key1;
            	// # code...
	            if($key1=='cms')
	            {
	                $newCMS =  new CMS($value1['id'],$this->context->language->id);
	                $cmstitle = $newCMS->meta_title;
	                $cmsdesc = $newCMS->meta_description;

	                // CHECK IF MOODBOARD TITLE TO BE DIPLAYED
	                if($value1['cms-data-flag'])
	                {
	                	if($value1['cms-title'])
	                		$cmstitle = stripslashes($value1['cms-title']);
	                	if($value1['cms-desc'])
	                		$cmsdesc = stripslashes($value1['cms-desc']);
	                }

	                $tempArr['id'] = $value1['id'];
	                $tempArr['title'] = $cmstitle;
	                $tempArr['description'] = $cmsdesc;
	                $tempArr['link'] = $link->getCMSLink($newCMS->id, $newCMS->link_rewrite);

	                // GET ASSOCIATED CATEGORY NAME
	                $categoryName = '';
	                if($newCMS->cat_assoc){
	                $category = new Category((int)$newCMS->cat_assoc, (int)$this->context->language->id);
	                $categoryName = $category->name;
	            	}
	                $tempArr['img'] = $value1['img'];
	                $tempArr['category'] = $categoryName;

	                $tempVar[] = $tempArr;
	            }
	            if($key1=='product')
	            {
	                $newProd =  new Product($value1['id'],$this->context->language->id);
	                $category = new Category((int)$newProd->id_category_default, (int)$this->context->language->id);
	                $tempArr['id'] = $value1['id'];
	                $tempArr['title'] = $newProd->name[1];
	                $tempArr['description'] = $newProd->description[1];
	                $tempArr['link'] = $link->getProductLink($newProd);
	                $tempArr['img'] = $value1['img'];
	                $tempArr['category'] = $category->name;
	                $tempVar[] = $tempArr;
	            }

            }

        }
    }

    $templateName = 'template_'.$id_temp.'.tpl';
    $boardArr['template'] = $templateName;
    $boardArr['template_vars'] = $tempVar;
    
    //die($this->display(_PS_MODULE_DIR_.'manageboard/views/templates/admin/', $templateName));
    return $boardArr;

    //die(Tools::jsonEncode(array('thumbnail' => $tempVar)));

}

	public function getContent(){


		//$getPagesByType = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."moodboard_content_promotion WHERE mid = 13");

		//ddd($getPagesByType);

		/* default code */
		$this->boardImgValidator();
		$this->_postProcess();
		$this->displayForm();
		return $this->_html;

		/* testing code to get different color images */
		/* $product = new Product(5, true, $this->context->language->id, $this->context->shop->id);
		$images = $product->getImages((int)$this->context->cookie->id_lang);
		//ppp($images);
		ppp('<pre>');
		$combination_images = $product->getCombinationImages($this->context->language->id);
		//ppp($combination_images);

		$attributes_groups = $product->getAttributesGroups($this->context->language->id);

		$colorImageArr = array();
		//ppp($attributes_groups);
		//ppp(_PS_COL_IMG_DIR_);
		$colors = array();
		foreach ($attributes_groups as $k => $row) {
			if (isset($row['is_color_group']) && $row['is_color_group'] && (isset($row['attribute_color']) && $row['attribute_color']) || (file_exists(_PS_COL_IMG_DIR_.$row['id_attribute'].'.jpg'))){
                    $colors[$row['id_attribute']]['value'] = $row['attribute_color'];
                    $colors[$row['id_attribute']]['name'] = $row['attribute_name'];
                    if (!isset($colors[$row['id_attribute']]['attributes_quantity'])) {
                        $colors[$row['id_attribute']]['attributes_quantity'] = 0;
                    }
                    $colors[$row['id_attribute']]['attributes_quantity'] += (int)$row['quantity'];
                }

        $colorImageArr[] = $combination_images[$row['id_product_attribute']][0]['id_image'];
                
		}

		$temp = array_unique($colorImageArr);
		$colorImgUrl = array();

		foreach ($temp as $key => $value) {
			// # code...
			$colorImgUrl[] = basename($this->context->link->getImageLink($product->link_rewrite, $value, ImageType::getFormatedName('home')));
		}
		//$images = Product::getCover(5);
		//$image_url = $this->context->link->getImageLink($product->link_rewrite, $images['id_image'], ImageType::getFormatedName('home'));
		ppp($colorImgUrl);
		//$product_images = $product->context->smarty->tpl_vars['images']->value;
		//ppp($product);
		//ppp(array_unique($colorImageArr));
		//ppp($colors);

		// end color images code 
		
		//$this->displayForm();
		//return $this->_html;

		//$content = $this->_getCMSProduct();
		/*$content = Country::getCountries((int)Context::getContext()->cookie->id_lang);
		ppp('<pre>');
		ppp($content);

		$content1 = $this->_getCMSProduct();
		ppp($content1);*/

/*		$newCMS = new CMS(13);
		$category = new Category((int)$newCMS->cat_assoc, (int)$this->context->language->id);
	    $categoryName = $category->name;
		ddd($categoryName);*/


	}

	private function _postProcess(){

		//ddd($_POST);

		if(Tools::isSubmit('submitNewBoard'))
		{
			//ppp($_POST);

			$tempName = $_POST['title'];
			$tempID = $_POST['template'];
			$status = $_POST['moodboard_status'];

			// CHECK FOR EACH POSITION CORRECT IMAGE SIZE HAS UPLOADED
			$allPost = Tools::getAllValues();


			$imgPosArr = array();

			foreach ($allPost as $key => $value) {
				//# code...
				if(strstr($key,'opt_')){	
					$postedFor = $value;
					$myIndex = preg_replace('/[^\d.]/','',$key);
					$imgToLook = 'color_pos_'.$postedFor.'_'.$myIndex;

					$imgPosArr[$myIndex] = Tools::getValue($imgToLook);
					// CREATE ARRAY OF IMAGES POSTED FOR AND VALID ONE BY ONE
				}
			}


			$temp = array();

			foreach ($imgPosArr as $imageIndex => $imgToCheck) {
				// # code...
				//$imageIndex = preg_replace('/[^\d.]/','',$imgToCheck);
				if(!$imgToCheck){

					$this->_errors[] = 'Invalid image, please select image for Position '.$imageIndex;
				}
				/*else
				{
					// CHECK IF IMAGE IS FOR VALID SIZE (WxH)
					$sizeToArr = getimagesize($imgToCheck);
					$imgWidth = (int) $sizeToArr[0];
					$imgHeight = (int) $sizeToArr[1];


					if($this->imgValidatorArr[$tempID][$imageIndex]['w']!=$imgWidth || $this->imgValidatorArr[$tempID][$imageIndex]['h']!=$imgHeight)
						$this->_errors[] = 'Invalid image size, please choose correct image dimensions ('.$this->imgValidatorArr[$tempID][$imageIndex]['w'].' x '.$this->imgValidatorArr[$tempID][$imageIndex]['h'].') for Position '.$imageIndex;
				}*/
			}



			$update = ($_POST['id_moodboard']) ? $_POST['id_moodboard'] : false;

			if(empty($tempName) || !Validate::isGenericName($tempName))
				$this->_errors[] = 'Invalid Title';
			else{

				// GET DATABASE VALUES TO CHECK WHETHER TEMPLATE ALREADY EXISTS
				$tempMachineName = preg_replace('/\s+/','-', preg_replace("/[^ \w]+/", " ", $tempName)).'-'.$tempID;
				$tempMachineName = strtolower($tempMachineName);

				// CHECK EXISTING TEMPLATE WHILE ADDING NEW
				if(!$update)
				$getTemplate = "SELECT * FROM "._DB_PREFIX_."moodboard_content WHERE template_file_name = '".$tempMachineName."'";
				// CHECK EXISTING TEMPLATE EXCEPT UPDATING ONE
				else
				$getTemplate = "SELECT * FROM "._DB_PREFIX_."moodboard_content WHERE template_file_name = '".$tempMachineName."' AND id != ".$update;

				if(Db::getInstance()->getRow($getTemplate))
					$this->_errors[] = 'Template already exists, try with a different title';

			}

			if(!$this->_errors)
			{
			$data = array();
			$data[]['template'] = $_POST['template'];

			foreach ($_POST as $key => $value) {

				$tempArr = array();
				// # code...
				if(strstr($key,'opt_'))
				{
					$tempIndex = preg_match_all('/\d+/',$key,$match);
					$myIndex = $match[0][0];

					if($value=='cms')
					{
						$myValue = 'pos_'.$value.'_'.$myIndex;
						$myImg = 'color_pos_'.$value.'_'.$myIndex;
						$myImgChoice = 'img_pos_'.$value.'_'.$myIndex;
						$cmsDataFlag = 'flag_pos_'.$value.'_'.$myIndex;
						$cmsTitle = 'title_pos_'.$value.'_'.$myIndex;
						$cmsDesc = 'desc_pos_'.$value.'_'.$myIndex;

						$tempArr['cms']['id'] = $_POST[$myValue];
						$tempArr['cms']['img'] = $_POST[$myImg];
						$tempArr['cms']['img-choice'] = $_POST[$myImgChoice];
						$tempArr['cms']['cms-data-flag'] = $_POST[$cmsDataFlag];
						$tempArr['cms']['cms-title'] = pSQL($_POST[$cmsTitle]);
						$tempArr['cms']['cms-desc'] = pSQL($_POST[$cmsDesc]);

						$data[] = $tempArr;
					}
					if($value=='product')
					{
						$myValue = 'pos_'.$value.'_'.$myIndex;
						$myImg = 'color_pos_'.$value.'_'.$myIndex;
						$myImgChoice = 'img_pos_'.$value.'_'.$myIndex;
						$tempArr['product']['id'] = $_POST[$myValue];
						$tempArr['product']['img'] = $_POST[$myImg];
						$tempArr['product']['img-choice'] = $_POST[$myImgChoice];
						$data[] = $tempArr;
					}
				}
			}

			
			// INSERT INTO DB
			if(!$update)
			{
			$sql = "INSERT INTO "._DB_PREFIX_."moodboard_content (`title`,`temp_id`,`template_file_name`,`data`,`promoted_pages`,`created`,`changed`,`status`) VALUES ('".pSQL($tempName)."','".$tempID."','".$tempMachineName."','".serialize($data)."',NULL,'".time()."','".time()."',".$status.")";
			
				if(!Db::getInstance()->execute($sql))
				{
					$this->_errors[] = Tools::displayError('Error while inserting Moodboard') . ': ' . mysql_error();	
				}
				else
				{
					$lastID = Db::getInstance()->Insert_ID(); 
					
					if($lastID)
					{
						if(count($_POST['promote_to_page'])>0)
						{
						foreach ($_POST['promote_to_page'] as $key => $value) {
							//# code...
							$pageArr = explode("_",$value);
							$page_name = $pageArr[0];
							$page_id = $pageArr[1];

							// INSERT INTO MOODBOARD_CONTENT_PROMOTION
							$sql = "INSERT INTO "._DB_PREFIX_."moodboard_content_promotion (`mid`,`page_id`,`page_type`,`position`) VALUES ('".$lastID."','".$page_id."','".$page_name."',NULL)";

							if(!Db::getInstance()->execute($sql))
								$this->_errors[] = Tools::displayError('Error while inserting Moodboard Promote') . ': ' . mysql_error();

							}

						// update promoted_pages in moodboard_content table
						$pages = $this->_getPromotedPages($lastID,true);

						$sql = "UPDATE "._DB_PREFIX_."moodboard_content SET promoted_pages = '".pSQL($pages)."' WHERE id  = ".$lastID;

						Db::getInstance()->ExecuteS($sql);
						
						}

						
					}

				}
			}
			else
			{
			$sql = "UPDATE "._DB_PREFIX_."moodboard_content SET title = '".pSQL($tempName)."',temp_id = '".$tempID."',template_file_name = '".$tempMachineName."',data = '".serialize($data)."', promoted_pages = NULL, changed = '".time()."', status = ".$status." WHERE id = ".$update."";
			
				if(!Db::getInstance()->execute($sql))
				{
					$this->_errors[] = Tools::displayError('Error while updating Moodboard') . ': ' . mysql_error();	
				}
				else
				{
					//$lastID = Db::getInstance()->Insert_ID(); 
					if($update)
					{
						// CHECK IF ENTRY EXISTS
						if(Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."moodboard_content_promotion WHERE mid = $update"))
						{
						// REMOVE ALL EXISTING PROMOTIONS AND OVVERRIDE NEW ONE
						$sql = "DELETE FROM "._DB_PREFIX_."moodboard_content_promotion WHERE mid = $update";
						if(!Db::getInstance()->ExecuteS($sql))
							$this->_errors[] = Tools::displayError('Error while removing Moodboard Promote') . ': ' . mysql_error();
						}
						if(count($_POST['promote_to_page'])>0)
						{
								foreach ($_POST['promote_to_page'] as $key => $value) {
								//# code...
								$pageArr = explode("_",$value);
								$page_name = $pageArr[0];
								$page_id = $pageArr[1];

								// INSERT INTO MOODBOARD_CONTENT_PROMOTION
								$sql = "INSERT INTO "._DB_PREFIX_."moodboard_content_promotion (`mid`,`page_id`,`page_type`,`position`) VALUES ('".$update."','".$page_id."','".$page_name."',NULL)";

								if(!Db::getInstance()->execute($sql))
									$this->_errors[] = Tools::displayError('Error while inserting Moodboard Promote') . ': ' . mysql_error();

							}

							// update promoted_pages in moodboard_content table
						$pages = $this->_getPromotedPages($update,true);

						$sql = "UPDATE "._DB_PREFIX_."moodboard_content SET promoted_pages = '".pSQL($pages)."' WHERE id  = ".$update;

						Db::getInstance()->ExecuteS($sql);
						}

						
					}
					
				}
			}

				if($this->_errors)
					$this->_html .= $this->displayError(implode($this->_errors, '<br />'));
				else
				{
					if(!$update){
						$successMsg = $this->l('Moodboard created successfully.');
						$conf = 3;
					}
					else{
						$successMsg = $this->l('Moodboard updated successfully.');
						$conf = 4;
					}


					//$curToken = Tools::getAdminTokenLite('AdminManageBoard');

					$mylink = $this->context->link->getAdminLink('AdminManageBoard').'&conf='.$conf;

					$this->_html .= $this->displayConfirmation($successMsg.'<a href="'.$this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.$curToken.'">View List</a>');

					//ddd($mylink.'token:'.$curToken);

					Tools::redirectAdmin($mylink); 
				}
			}
			else
			{
				$this->_html .= $this->displayError(implode($this->_errors, '<br />'));

			}
		
		}
		
	}

	public function _getCMSProduct($id_content=false,$string=false){

		
			$content = array();

			if(!$id_content || $id_content=='product')
			{
				$sql = 'SELECT a.id_product as id_cms, b.name as meta_title , c.name as catprod
			FROM '._DB_PREFIX_.'product a, '._DB_PREFIX_.'product_lang
 b, '._DB_PREFIX_.'category_lang c WHERE a.id_product = b.id_product and a.id_category_default = c.id_category';
					if($string)
					$strType = 'product';
			}
 			else if($id_content=='cms')
 			{
 				$sql = 'SELECT a.id_cms, b.meta_title ,c.name as catcms, d.name as catprod
			FROM '._DB_PREFIX_.'cms a, '._DB_PREFIX_.'cms_lang
 b, '._DB_PREFIX_.'cms_category_lang c, '._DB_PREFIX_.'category_lang d WHERE a.id_cms = b.id_cms and a.id_cms_category = c.id_cms_category and a.cat_assoc = d.id_category and a.id_cms_category != 1'; // 1 is home category
 					if($string)
 					$strType = 'cms';
 			}
 			else if($id_content=='category')
 			{
 				$sql = 'SELECT a.id_category as id_cms, b.name as meta_title FROM '._DB_PREFIX_.'category a ,'._DB_PREFIX_.'category_lang b where a.id_category = b.id_category AND (a.id_parent =  2 OR a.id_category = 2)';
 					if($string)
 					$strType = 'category';
 			}
 			

			$tempResult = Db::getInstance()->ExecuteS($sql);
			//return Db::getInstance()->ExecuteS($sql);
			
			foreach ($tempResult as $key => $value) {
				// # code...
				$temp = array();
				$titleSuffix = '';
				if(!$string)
				{
				$tempID = (int) $value['id_cms'];
				$temp['id_cms'] = (int) $value['id_cms'];

				if($id_content=='cms')
					$titleSuffix = ' &#40; '.$value['catcms'].'&#44; '.$value['catprod'].' &#41; ';
				if(!$id_content || $id_content=='product')
					$titleSuffix = ' &#40; '.$value['catprod'].' &#41; ';

				$temp['title'] = $value['meta_title'].$titleSuffix;
				$content[$tempID] = $temp;
				}
				else
				{
					$tempID = $strType.'_'.$value['id_cms'];
					$temp['id_cms'] = $strType.'_'.$value['id_cms'];

					if($id_content=='cms')
					$titleSuffix = ' &#40; '.$value['catcms'].'&#44; '.$value['catprod'].' &#41; ';
					if(!$id_content || $id_content=='product')
					$titleSuffix = ' &#40; '.$value['catprod'].' &#41; ';


					$temp['title'] = $value['meta_title'].$titleSuffix;
					$content[$tempID] = $temp;
				}
			}
			
			return $content;
	}

	public function displayForm()
	{
		//$this->_html .= '<div class="manageboard-wrapper">';
		if(!Tools::getValue('action') && !Tools::getValue('id')){
			//AdminSearchContentController::test();
		$this->_html .= $this->_generateMoodboardList();
		}
		else
		{
			 if((Tools::getValue('action')=='addmoodboard' && !Tools::getValue('id'))){
			 	//ddd('new form');
			 $this->_html .= $this->_generateForm();
			}
			else if(Tools::getValue('id')){
				//ddd('update exiting');
			$this->_html .= $this->_generateForm(true);
			}

		}
		
		//$this->_html .= '</div>';
	}

	private function _generateForm($edit)
	{
		
			//ddd('test');

		$update = 0;
		if(Tools::isSubmit('updatemoodboard_content'))
		$update = 1;

		$doEdit = Tools::getValue('id');
		//ddd($doEdit);
		if($doEdit){
			$content = $this->_getMoodBoard($doEdit);
			//$dbValues = array();

			// IF EDIT MODE MAINTAIN MOODBOARD ID TO BE EDITED
			$inputs[] = array(
				'type' => 'hidden',
				'name' => 'id_moodboard',
			);

			$contentPromoted = $this->_getPromotedPages($content['id']);

			$this->dbValues['id_moodboard'] = $content['id'];
			$this->dbValues['title'] = $content['title'];	
			$this->dbValues['template'] = $content['temp_id'];
			$this->dbValues['moodboard_status'] = $content['status'];
			$tempAttr = unserialize(str_replace("'", "\'", $content['data']));


			// GET VALUES AND SET INPUT ACCORDINGLY
			foreach ($tempAttr as $key => $value) {
				// # code...
				foreach ($value as $key1 => $value1) {

					if($key1!='template')
					$this->dbValues['opt_'.$key] = $key1;
					// # code...
					if($key1=='product')
					{
						$this->dbValues['pos_'.$key1.'_'.$key] = $value1['id'];
						$this->dbValues['img_pos_'.$key1.'_'.$key] = $value1['img-choice'];
					}
					else if($key1=='cms')
					{
						$this->dbValues['pos_'.$key1.'_'.$key] = $value1['id'];
						// CMS TITLE , DESCRIPTION
						$this->dbValues['flag_pos_'.$key1.'_'.$key] = $value1['cms-data-flag'];
						$this->dbValues['title_pos_'.$key1.'_'.$key] = stripslashes($value1['cms-title']);
						$this->dbValues['desc_pos_'.$key1.'_'.$key] = stripslashes($value1['cms-desc']);

					}
				}
			}

			// GET VALUES AND SET PROMOTED PAGES ACCORDINGLY
			if(count($contentPromoted)>0)
			{
			foreach ($contentPromoted as $key => $value) {
				// # code...
				$this->dbValues['promote_to_page'][$key] = $value['page_type'].'_'.$value['page_id'];
			}

		 }

		}

		
		/*$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Select Moodboard'),
				'name' => 'template',
				'required' => true,
				'class' => 'templateBG',
				'values'  => array(
				array(
						'id' => 'temp1',
						'value' => 1,
						'label' => $this->l('Moodboard 1'),
					),
				array(
						'id' => 'temp2',
						'value' => 2,
						'label' => $this->l('Moodboard 2'),
					)
				)

			);*/

		$inputs[] = array(
			'type' => 'html',
			'name' => 'html_templates',
			'label' => $this->l('Select Moodboard'),
			'html_content' => '<div class="col-lg-9"><div class="radio templateBG"><label class="mb-temp1"><input type="radio" name="template" id="temp1" value="1" '.$this->activeTemplate(1,$update).'>Moodboard 1</label></div>
<div class="radio templateBG"><label class="mb-temp2"><input type="radio" name="template" id="temp2" value="2" '.$this->activeTemplate(2,$update).'>Moodboard 2</label></div></div>
',
			);

		
		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Title'),
				'name' => 'title',
				'required' => true
			);

		// group for position 1
		$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Choose position 1'),
				'name' => 'opt_1',
				'required' => true,
				'class' => 'templateLeftAlign moodContent',
				'values'  => array(
				array(
						'id' => 'opt_1_1',
						'value' => 'cms',
						'label' => $this->l('Article'),
					),
				array(
						'id' => 'opt_1_2',
						'value' => 'product',
						'label' => $this->l('Product'),
					)
				)

			);

		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_product_1',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct(),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		
		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_cms_1',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct('cms'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		// INPUTS TO SHOW SELECTED IMAGE ONCE FORM POSTED OR IN EDIT VIEW
		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_product_1',
                    'name' => 'img_pos_product_1',
                );

		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_cms_1',
                    'name' => 'img_pos_cms_1',
                );

		// INPUTS TO GIVE OPTIONAL TEXT AND DESCRIPTION FOR ARTICLES/CMS
		$contenType = ($doEdit) ? $this->dbValues['opt_1'] : Tools::getValue('opt_1');
		$inputs[] = $this->getCmsDataFields(1,$contenType,$update);


		// group for position 2
		$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Choose position 2'),
				'name' => 'opt_2',
				'required' => true,
				'class' => 'templateLeftAlign moodContent',
				'values'  => array(
				array(
						'id' => 'opt_2_1',
						'value' => 'cms',
						'label' => $this->l('Article'),
					),
				array(
						'id' => 'opt_2_2',
						'value' => 'product',
						'label' => $this->l('Product'),
					)
				)

			);

		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_product_2',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct(),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		
		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_cms_2',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct('cms'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		// INPUTS TO SHOW SELECTED IMAGE ONCE FORM POSTED OR IN EDIT VIEW
		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_product_2',
                    'name' => 'img_pos_product_2',
                );

		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_cms_2',
                    'name' => 'img_pos_cms_2',
                );

		// INPUTS TO GIVE OPTIONAL TEXT AND DESCRIPTION FOR ARTICLES/CMS
		$contenType = ($doEdit) ? $this->dbValues['opt_2'] : Tools::getValue('opt_2');
		$inputs[] = $this->getCmsDataFields(2,$contenType,$update);

		// group for position 3
		$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Choose position 3'),
				'name' => 'opt_3',
				'required' => true,
				'class' => 'templateLeftAlign moodContent',
				'values'  => array(
				array(
						'id' => 'opt_3_1',
						'value' => 'cms',
						'label' => $this->l('Article'),
					),
				array(
						'id' => 'opt_3_2',
						'value' => 'product',
						'label' => $this->l('Product'),
					)
				)

			);

		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_product_3',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct(),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		
		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_cms_3',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct('cms'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		// INPUTS TO SHOW SELECTED IMAGE ONCE FORM POSTED OR IN EDIT VIEW
		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_product_3',
                    'name' => 'img_pos_product_3',
                );

		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_cms_3',
                    'name' => 'img_pos_cms_3',
                );

		// INPUTS TO GIVE OPTIONAL TEXT AND DESCRIPTION FOR ARTICLES/CMS
		$contenType = ($doEdit) ? $this->dbValues['opt_3'] : Tools::getValue('opt_3');
		$inputs[] = $this->getCmsDataFields(3,$contenType,$update);

		// group for position 2
		$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Choose position 4'),
				'name' => 'opt_4',
				'required' => true,
				'class' => 'templateLeftAlign moodContent',
				'values'  => array(
				array(
						'id' => 'opt_4_1',
						'value' => 'cms',
						'label' => $this->l('Article'),
					),
				array(
						'id' => 'opt_4_2',
						'value' => 'product',
						'label' => $this->l('Product'),
					)
				)

			);

		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_product_4',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct(),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		
		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_cms_4',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct('cms'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		// INPUTS TO SHOW SELECTED IMAGE ONCE FORM POSTED OR IN EDIT VIEW
		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_product_4',
                    'name' => 'img_pos_product_4',
                );

		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_cms_4',
                    'name' => 'img_pos_cms_4',
                );

		// INPUTS TO GIVE OPTIONAL TEXT AND DESCRIPTION FOR ARTICLES/CMS
		$contenType = ($doEdit) ? $this->dbValues['opt_4'] : Tools::getValue('opt_4');
		$inputs[] = $this->getCmsDataFields(4,$contenType,$update);

		// group for position 5
		$inputs[] = array(
				'type' => 'radio',
				'label' => $this->l('Choose position 5'),
				'name' => 'opt_5',
				'required' => true,
				'class' => 'templateLeftAlign moodContent',
				'values'  => array(
				array(
						'id' => 'opt_5_1',
						'value' => 'cms',
						'label' => $this->l('Article'),
						'checked' => 'checked'
					),
				array(
						'id' => 'opt_5_2',
						'value' => 'product',
						'label' => $this->l('Product'),
					)
				)

			);

		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_product_5',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct(),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		
		$inputs[] = array(
                    'type' => 'select',
                    //'label' => 'Position 1',
                    'name' => 'pos_cms_5',
                    'class' => 'chosen',
                    'options' => array(
                        'query' => $this->_getCMSProduct('cms'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		// INPUTS TO SHOW SELECTED IMAGE ONCE FORM POSTED OR IN EDIT VIEW
		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_product_5',
                    'name' => 'img_pos_product_5',
                );

		$inputs[] = array(
                    'type' => 'hidden',
                    'id' => 'img_pos_cms_5',
                    'name' => 'img_pos_cms_5',
                );

		// INPUTS TO GIVE OPTIONAL TEXT AND DESCRIPTION FOR ARTICLES/CMS
		$contenType = ($doEdit) ? $this->dbValues['opt_5'] : Tools::getValue('opt_5');
		$inputs[] = $this->getCmsDataFields(5,$contenType,$update);

		$inputs[] = array(
                    'type' => 'select',
                    'label' => 'Select page to promote moodboard ()',
                    'name' => 'promote_to_page',
                    'class' => 'chosen promote_to_page',
                    'multiple' => true,
                    'options' => array(
                        //'query' => array_merge($this->_getCMSProduct('cms','string'),$this->_getCMSProduct('category','string')),
                        'query' => $this->_getCMSProduct('category','string'),
                        'id' => 'id_cms',
                        'name' => 'title'
                    ),
                );

		$inputs[] = array(
                    'type' => 'switch',
                    'label' => 'Active',
                    'name' => 'moodboard_status',
                    'values' => array(
                        array(
                            'id' => 'moodboard_status_on',
                            'value' => 1
                        ),
                        array(
                            'id' => 'moodboard_status_off',
                            'value' => 0
                        )
                    )
                );

		$inputs[] = array(
				'type' => 'hidden',
				'name' => 'mytoken',
				//'value' => Context::getContext()->link->getAdminLink('AdminSearchContent').'&ajax=1&action=getsearchcontent'
				'value' => Context::getContext()->link->getAdminLink('AdminManageBoard').'&ajax=1&action=getsearchcontent'
			);

		$inputs[] = array(
			'type' => 'html',
                    'name' => 'html_data',
                    'html_content' => '<!-- Modal -->
									  <div class="modal fade" id="myModal" role="dialog">
									    <div class="modal-dialog">
									    
									      <!-- Modal content-->
									      <div class="modal-content">
									        <div class="modal-header">
        										<button type="button" class="close" data-dismiss="modal">&times;</button>
      										</div>
									        <div class="modal-body">
									          <p>Some text in the modal.</p>
									        </div>
									        
									      </div>
									      
									    </div>
									  </div>'
                    );

		$fields_form = array(
			'form' => array(
				'legend' => array(
						'title' => (!$doEdit) ? $this->l('Create new board') : $this->l('Update board: '.$this->dbValues['title']),
						'icon' => 'icon-cogs'
						),
				'input' => $inputs,
				
				'submit' => array(
						'title' => (!$doEdit) ? $this->l('Save') : $this->l('Update'),
						'class' => 'btn btn-default pull-right'
						),
				'buttons' => array(
	                'preview' => array(
	                    'name' => 'previewboard',
	                    'title' => $this->l('Preview'),
	                    'class' => 'btn btn-default pull-right getPreview',
	                    'icon' => 'process-icon-preview'
	                ),
	                'viewlist' => array(
	                    'name' => 'listboard',
	                    'title' => $this->l('Cancel'),
	                    'class' => 'btn btn-default pull-left viewList',
	                    'icon' => 'process-icon-cancel',
	                    'attributes' => array(
                            'onclick' => 'alert(\'something done\');'
                        )
	                )
            	)
					)
				);

		//ppp($inputs);
	
		foreach ($inputs as $input)
		{
			if($input['name']=='mytoken')
			$empty_values[$input['name']] = Context::getContext()->link->getAdminLink('AdminManageBoard').'&ajax=1&action=getsearchcontent';
			else if($input['name']=='template'){

				if(!$update)
				$empty_values[$input['name']] = ($this->_errors && Tools::getValue($input['name'])) ? Tools::getValue($input['name']) : 1;
				else
					$empty_values[$input['name']] = $this->dbValues[$input['name']];
			}

			else if(strstr($input['name'], 'opt_'))
			{	
				if(!$update)
				$empty_values[$input['name']] = ($this->_errors && Tools::getValue($input['name'])) ? Tools::getValue($input['name']) : 'product';
				else
				$empty_values[$input['name']] = $this->dbValues[$input['name']];
			}

			else if($input['name']=='promote_to_page')
			{
				if(!$update)
				$empty_values['promote_to_page[]'] = ($this->_errors && Tools::getValue('promote_to_page')) ? Tools::getValue('promote_to_page') : '';
				else
				$empty_values['promote_to_page[]'] = $this->dbValues[$input['name']]; 
			}

			else{
				if(!$update)
				$empty_values[$input['name']] = ($this->_errors && Tools::getValue($input['name'])) ? Tools::getValue($input['name']) : '';
				else
				$empty_values[$input['name']] = $this->dbValues[$input['name']]; 
			}
			

		}
		
		$action = 'addmoodboard';
		if($doEdit)
		$action .= '&id='.$this->dbValues['id_moodboard'];

		$helper = new HelperForm();
		$helper->submit_action = 'submitNewBoard';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&action='.$action;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $empty_values
			);
		
		//if($_POST)
		//$helper->tpl_vars['fields_value']['promote_to_page[]'] = array(0 => 'cms_1',1 => 'cms_2');

		return $helper->generateForm(array($fields_form));

	}

	public function activeTemplate($tempID,$update){

		$tempCheck = '';
		if(!$update && !Tools::isSubmit('submitNewBoard'))
		{
				if($tempID==1)
					$tempCheck = 'checked="checked"';
		}
		else
		{
			if($tempID==Tools::getValue('template'))
			$tempCheck = 'checked="checked"';
			else if($tempID==$this->dbValues['template'] && !Tools::getValue('template'))
			$tempCheck = 'checked="checked"';
				
		}
		return $tempCheck;
	}

	public function getCmsDataFields($pos,$cmsID=false,$update=false){

		$showMe = $showMyData = 'display:none';
		$cmsTitle = '';
		$cmsDesc = '';
		$cmsChcked = 0;
		$checkMe = '';

		if($cmsID=='cms'){
			$showMe = '';

			if(!$update)
			$cmsChcked = (Tools::getValue('flag_pos_cms_'.$pos)) ? Tools::getValue('flag_pos_cms_'.$pos) : 0;
			else
			$cmsChcked = ($this->dbValues['flag_pos_cms_'.$pos]) ? $this->dbValues['flag_pos_cms_'.$pos] : 0;


			if($cmsChcked){
				$cmsTitle = (!$update) ? Tools::getValue('title_pos_cms_'.$pos) : $this->dbValues['title_pos_cms_'.$pos];
				$cmsDesc = (!$update) ? Tools::getValue('desc_pos_cms_'.$pos) : $this->dbValues['desc_pos_cms_'.$pos];
				$showMyData = '';
				$checkMe = 'checked="checked"';
			}
		}

		return array(
			'type' => 'html',
                    'name' => 'cms_data_'.$pos,
                    'html_content' => '<div class="cms_data_'.$pos.' cms-data-wrap" style="'.$showMe.'">
                    					
										Use below details for article<br>
										<input type="checkbox" name="flag_pos_cms_'.$pos.'" '.$checkMe.'  value="1" class="use_cms_data flag_pos_cms_'.$pos.'"><em>If checked, given title and description is used instead Article title.</em><br>
										<div class="flag_use_pos flag_use_pos_cms_'.$pos.'" style="'.$showMyData.'" >
                    					Title:&nbsp;<div class="input-group"><span id="title_pos_cms_'.$pos.'_counter" class="input-group-addon">30</span>
<input type="text" name="title_pos_cms_'.$pos.'" id="title_pos_cms_'.$pos.'" value="'.$cmsTitle.'" class="char-cnt" data-maxchar="30" maxlength="30">
</div><br>
                    					Description:&nbsp;<div class="input-group"><span id="desc_pos_cms_'.$pos.'_counter" class="input-group-addon">250</span><textarea name="desc_pos_cms_'.$pos.'" id="desc_pos_cms_'.$pos.'" cols="5" rows="3" resize="no" maxlength="250" data-maxchar="250" class="char-cnt">'.$cmsDesc.'</textarea></div>
                    					</div>
                    					</div>
                    '
                    );
	}

	public function _getMoodBoard($mid){

		//$mid = Tools::getValue('id');

		if(!$mid){
			return false;
		}
		else{

			$sql = "SELECT * from "._DB_PREFIX_."moodboard_content WHERE id = $mid";
			$getMid = Db::getInstance()->getRow($sql);
			if($getMid)
				return $getMid;
		}
			
	}

	public function _getPromotedPages($mid,$name=false){

		if(!$name)
		{
			$promotedPages = array();
			$getPromoted = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."moodboard_content_promotion WHERE mid = $mid");
			if($getPromoted){
				$promotedPages = $getPromoted;
			}

		}
		else
		{
			$temp = array();
			$promotedPages = '';

			$getPagesByType = Db::getInstance()->ExecuteS("SELECT * FROM "._DB_PREFIX_."moodboard_content_promotion WHERE mid = $mid");

			if($getPagesByType)
			{

				foreach($getPagesByType as $key => $val){

					foreach ($val as $key1 => $val1) {

						if($key1=='page_type')
						{
							$querKey = _DB_PREFIX_.$val1.'_lang';
							$joinId = 'id_'.$val1;
							$id = $val['page_id'];

							if($val1=='cms')
							$getPromoted = Db::getInstance()->getRow("SELECT meta_title as page_name FROM $querKey WHERE $joinId  = $id");
							else if($val1=='category')
							$getPromoted = Db::getInstance()->getRow("SELECT name as page_name FROM $querKey WHERE $joinId  = $id");

							if($getPromoted)
							$temp[] = $getPromoted['page_name'];

						}
					}
				}

				$promotedPages = implode(", ", $temp);
			}

			
		}



		return $promotedPages;

	}



	public function getAll()
	{
		$sql = "SELECT *
			FROM "._DB_PREFIX_."moodboard_content";

		return Db::getInstance()->ExecuteS($sql);
	}

	private function _generateMoodboardList()
	{


		$content = $this->getAll();
		
		$fields_list = array(
			'id' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs'
			),
			'name' => array(
				'title' => $this->l('Title'),
				'filter' => '!name',
				'type' => 'text',
				'search' => true 
			),
			'temp_id' => array(
				'title' => $this->l('Template')
			)
			

			
		);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->actions = array('edit','delete');
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'id';
		$helper->title = $this->l('List of Moodboard');
		$helper->table = 'moodboard_content';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		//$helper->addRowAction('view');
		//$helper->addRowAction('add');
		//$helper->addRowAction('edit');
		//$helper->addRowAction('delete');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name .'&module_name=' . $this->name;
		$helper->toolbar_scroll = true;
		$helper->bulk = true;
		$helper->toolbar_btn = $this->initToolbar();

		return $helper->generateList($content, $fields_list);
	}

	public function initToolbar(){

		$getToken = Tools::getAdminTokenLite('AdminModules');

		$this->toolbar_btn['new'] = array(
					'href' => $this->context->link->getAdminLink('AdminModules', false).'&token='.$getToken.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&action=addmoodboard',
					'desc' => $this->l('Add new')
				);

		return $this->toolbar_btn;
	}


	public function hookActionAdminManageBoardListingResultsModifier($params)
    {
        //If you want to customize the renderlist by adding some of your desired elements in the list
        //if(isset($params['select']))
        //ppp($params['select']);

        foreach ($params['list'] as $key => $list_row) {
         //add your first element in the row of the renderlist here
        //$params['list'][$key]['my_element_2'] = 'value_element_2'; //add your second element in the row of the renderlist here

        	// add Template to temp_id value
        $params['list'][$key]['temp_id'] = 'Moodboard '.$list_row['temp_id'];

        //$params['list'][$key]['page_id'] = $this->_getPromotedPages($list_row['id'],true); // _getPromotedPages(ID,Name) Name is flag default is false.

          
        }

        //ppp($params);
       //ddd($params);
    }

    public function boardImgValidator(){

		$boardTemplate = array(1,2);
		$tempPosition = array(1,2,3,4,5);
    	//$imgValidatorArr = array();


    	foreach ($boardTemplate as $tempKey => $tempID) {
    		// # code...
    		 foreach ($tempPosition as $poskey => $pos) {
    		 	//# code...
    		 	$this->imgValidatorArr[$tempID][$pos] = $this->getBoradImgSize($tempID,$pos);
    		 }
    		
    	}

    }

   public function getBoradImgSize($tempID,$pos){

   			$sizeArr = array();

			switch ($tempID) 
			{
			
				case '1':
					// # code...
					switch ($pos) {
						case '1':
							// # code...
							$sizeArr = array('w' => 296,'h' => 613);
							break;
						
						default:
							// # code...
							$sizeArr = array('w' => 300,'h' => 236);
							break;
					}
					break;
				
				default:
					switch ($pos) {
						case '1':
							// # code...
							$sizeArr = array('w' => 400,'h' => 317);
							break;

						case '2':
							// # code...
							$sizeArr = array('w' => 400,'h' => 407);
							break;

						case '3':
							// # code...
							$sizeArr = array('w' => 398,'h' => 507);
							break;

						case '4':
							// # code...
							$sizeArr = array('w' => 400,'h' => 445);
							break;
						
						default:
							// # code...
							$sizeArr = array('w' => 400,'h' => 317);
							break;
					}
					break;
			
			}

			return $sizeArr;

   }


} // END OF CLASS

