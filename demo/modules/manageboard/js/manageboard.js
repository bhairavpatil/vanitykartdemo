$(window).load(function(){

    setTimeout(function(){
        $('.moodContent input:checked').change();
    },2000)
    
    //alert('load');
});

$(document).ready(function(){

$('.char-cnt').keydown(function(){
    var myId = $(this).attr('id');
    countDown($('#'+myId), $('#'+myId+'_counter'));
    var maxLimit = parseInt($('#'+myId+'_counter').text());
    if(maxLimit<=0)
        alert('Max limit exceeded');
})

$(document).on('keydown','.chosen-container-single .chosen-search input',function(e){

    var myName = $(this).parents('.chosen-container-single').attr('id');



    if(myName.search('pos_product')>=0){

        getMyImage(myName,e.type);
    }

});

$(document).on('click','.chosen-container-single .chosen-single',function(e){

    var myName = $(this).parent('.chosen-container-single').attr('id');

    if(myName.search('pos_product')>=0){
    getMyImage(myName,e.type);
    }
    
    
})

// START GET PRODUCT THUMBNAIL IN LIST AND SEARCH
var getMyImage = function(myName,type){

var mySelID = myName.replace('_chosen','');


var baseDir = window.module_dir.replace('modules/','');


if(type=='click')
{
$('#'+mySelID+' option').each(function(){

    var mIndex = $(this).index();
    var myval = $(this).attr('value');
    //var myText = $.trim($(this).text().substr(0,$(this).text().indexOf('('))).replace(/\s\s+/g, ' ');
    //myText = myText.replace(/\s+/g, '-').toLowerCase();
    //var myImg = '<img height="50px;" src="'+baseDir+myval+'-small_default/'+myText+'.jpg"> ';
    var myImg = '<img class="imgm img-thumbnail" src="'+baseDir+'img/tmp/product_mini_'+myval+'_1.jpg"> ';
    $('#'+mySelID).siblings('#'+myName).find('.chosen-results li[data-option-array-index="'+mIndex+'"]').not('.no-results').prepend(myImg);
    })
}
else if(type=='keydown')
{
    setTimeout(function(){

            $('#'+myName+' .chosen-results li').not('.no-results').each(function(){

            $(this).find('img').remove();

            var mIndex = $(this).attr('data-option-array-index');
            var myOptProd = $('#'+mySelID+' option:eq('+mIndex+')');
            var myval = $(myOptProd).attr('value');
            //var myText = $.trim($(myOptProd).text().substr(0,$(myOptProd).text().indexOf('('))).replace(/\s\s+/g, ' ');
            //myText = myText.replace(/\s+/g, '-').toLowerCase();
            //var myImg = '<img height="50px;" src="'+baseDir+myval+'-small_default/'+myText+'.jpg"> ';
            var myImg = '<img class="imgm img-thumbnail" src="'+baseDir+'img/tmp/product_mini_'+myval+'_1.jpg"> ';;

            $(this).prepend(myImg);
        })

        },500);
}

}
// END GET PRODUCT THUMBNAIL IN LIST AND SEARCH

$(document).on('click','.viewList',function(){

    var toJump = 'controller=AdminManageBoard';
    var toLocate = window.location.href;
    var tempUrl = toLocate.split('?');
    tempUrl[1] = toJump;
    window.location.href = tempUrl.join("?");
})
////$('.chosen-search input').removeAttr('readonly');
//alert('ready');
/*$('body form').submit(function(){

    alert('submit');
});*/

setTimeout(function(){
    $('div.chosen-container').removeClass('chosen-container-single-nosearch');

    /*$('div.chosen-container').each(function(){

            //if($(this).attr('id').search('pos_product')>-1)
                //console.log($(this).attr('id'));
                //$(this).parent('div:first').append('<input type="button" class="getAllColor" value="Get Color Options" title="Get all available color for products">');
    });*/

    $('.chosen-search input').removeAttr('readonly');

    $('select.chosen').each(function(event){
        if($(this).attr('name').search('cms')>=0){
            $(this).parents('.form-group').addClass('hide');

             //if($(this).is(':checked'))
             getAllColor($(this),event.type);
        }
        else if($(this).attr('name').search('product')>=0){

            //if($(this).is(':checked'))
            getAllColor($(this),event.type);
        }
    });

    

},1000);

$(document).on('change','.use_cms_data',function(){

        if($(this).is(':checked'))
            $(this).siblings('.flag_use_pos').slideDown();
        else
            $(this).siblings('.flag_use_pos').slideUp();
})

$(document).on('change','.moodContent input',function(){

	var id_ele = $(this).attr('name').replace ( /[^\d.]/g, '' );
    var id_content  = $(this).val();
    var ref_link = $('input[name="mytoken"]').val();

    var showMe = 'pos_product_'+id_ele;
    var hideMe = 'pos_cms_'+id_ele;
    var cmsData = '.cms_data_'+id_ele;

    if(id_content=='cms')
    {
    showMe = 'pos_cms_'+id_ele;
    hideMe = 'pos_product_'+id_ele;

        // SHOW ADDITIONAL TITLE DESCRIPTION BOX
        $(cmsData).slideDown(function(){
            /*if($('.flag_pos_cms_'+id_ele).is(':checked'))
                $('.flag_use_pos_cms_'+id_ele).show();
                    $('.flag_use_pos_cms_'+id_ele).hide();*/
                    $('.flag_pos_cms_'+id_ele).trigger('change');
        })

    }
    else
    {
        // HIDE ADDITIONAL TITLE DESCRIPTION BOX
        $(cmsData).slideUp();
    }
    
    $('#'+showMe).parents('.form-group').removeClass('hide');
    $('#'+hideMe).parents('.form-group').addClass('hide');

    // remove color elements if exists
    //console.log(ref_link+id_ele+''+id_content);
	//console.log(parseInt(id_ele));
	
	/*$.ajax({
    type: 'GET', 
    url : ref_link,
    data:{'id_content':id_content,'id_ele':id_ele},
    type:'json',
    cache:false,
    success : function(response){
        //console.log(response);
        var t = JSON.parse(response);
        
        var myObj = t.thumbnail.options.query;
        console.log(myObj);
        var myHtml = '<ul class="chosen-results">';
        var myOptions = '';
        $.each(myObj,function(index,value){

            var letMeActive = '';

            if(index==1)
            {
                letMeActive = 'result-selected';
                myHtml += '<li class="active-result '+letMeActive+'" style="" data-option-array-index="'+(index-1)+'">'+value.title+'</li>';
                myOptions += '<option value="'+index+'">'+value.title+'</option>';
                $('.chosen-single span').text(value.title);
            }
            else
            {
                myHtml += '<li class="active-result '+letMeActive+'" style="" data-option-array-index="'+(index-1)+'">'+value.title+'</li>';
                myOptions += '<option value="'+index+'">'+value.title+'</option>';
            }

            
        
        });

        myHtml += '</ul>';
        //console.log(index);
        //$('.chosen-drop ul').hide().remove();
        $('select#pos_1').html(myOptions).trigger('change');
        $('.chosen-drop').html(myHtml);

    },
    error:function(){
    	console.log('Could not make it');
    }
});*/
 //console.log('moodContent:'+$(this).attr('name')+' value '+$(this).val());
});

$(document).on('click','.getAllColor',function(){
        var meObj = $(this);
        var prodID = parseInt($(this).siblings('select').val());
        var ref_link = $('input[name="mytoken"]').val();
        var tempImgWrap = $('<div class="prodColorOpt"></div>');
        var colorExists = false;
        //$('.prodColorOpt').empty().remove();
        if($(this).next('.prodColorOpt').length)
            $(this).next('.prodColorOpt').remove();
        

        var htmlOut = '';
        //console.log(prodID);
        $.ajax({
            type: 'POST', 
            url : ref_link,
            data:{'id_prod':prodID},
            dataType:'json',
            cache:false,
            success:function(data){

                htmlOut += '<span class="prodName">'+data.thumbnail.name+'</span><br>';

                $.each(data.thumbnail.image,function(index,value){

                    htmlOut += '<img src="'+value+'">&nbsp;';
                });


                $(tempImgWrap).empty().html(htmlOut);
                $(meObj).after(tempImgWrap);
            },
            error:function(){

            }
            
        });

    });

// TRIGGER ON CHANING OF SELECT
$('select.chosen').change(function(event){

    // EXCLUDE ALL CMS BASED DROP-DOWN
    //if($(this).attr('name').search('cms')<0)
   // {
    getAllColor($(this),event.type);
   // }

});

var getAllColor = function(me,action){

            //console.log('event'+action);

        var checkMultiple = $(me).attr('multiple');
        var meObj = $(me);
        // TO AVOID LOAD FOR MULTIPLE CHOICE
        if(typeof checkMultiple=='undefined' || checkMultiple=='' )
        {
        //var meObj = $(me);
        var prodID = meObj.val();
        var objType = 'product';
        var ref_link = $('input[name="mytoken"]').val();
        var colorBoxExists = false;
        var myName = $(meObj).attr('name');
        var myIndex = myName.replace ( /[^\d.]/g, '' );

        var hide_first = 'display:none;';
        //$('input[name=opt_'+myIndex+']')
        if($('input[name=opt_'+myIndex+']').val()==objType) // CHECK IF RADIO OF OPTION IS SELECTED
            hide_first = '';

         // indentify whether it's CMS or PRODUCT
        if(myName.search('cms')>=0)
            objType = 'cms';

        var tempImgWrap = $('<div class="prodColorOpt box-img-'+objType+'"></div>');

        //console.log('myName'+$('input[name=opt_'+myIndex+']').val()+' '+objType);
        //return false;

       


        if($(meObj).siblings('.box-img-'+objType).length)
        {
            colorBoxExists = true;
            tempImgWrap = $(meObj).siblings('.box-img-'+objType);
        }
           

        var htmlOut = '';
        //console.log(prodID);
        $.ajax({
            type: 'POST', 
            url : ref_link,
            data:{'id_prod':prodID,type:objType},
            dataType:'json',
            cache:false,
            success:function(data){

                htmlOut += '<span class="prodName">'+data.thumbnail.name+'</span><br>';
                var cnt = 1;
                var checkMe = '';
                var preSetColor = false;
                var itr = 0;
                // TO SET PRE-SELECTED COLOR
                var eleToLook = $('#img_'+myName).val();
                if(action=='change')
                    eleToLook = '';

                if(typeof eleToLook != 'undefined' && eleToLook!='')
                    preSetColor = true;


                $.each(data.thumbnail.image,function(index,value){

                    var choosenID = 'img-'+myIndex+'-'+prodID+'-'+index;

                    checkMe = '';

                    if(!preSetColor)
                    {
                        if(cnt==1)
                        checkMe = 'checked';
                    }
                    else
                    {
                        if(eleToLook==choosenID)
                            checkMe = 'checked';
                    }
                    


                    htmlOut += '<span class="img-wrap"><label for="img-'+myIndex+'-'+prodID+'-'+index+'"><img src="'+value+'"></label><input type="radio" id="img-'+myIndex+'-'+prodID+'-'+index+'" name="color_'+myName+'" value="'+value+'" '+checkMe+'></span>';

                    if(!preSetColor && checkMe){
                        //var choosenID = 'img-'+myIndex+'-'+prodID+'-'+index;
                        chooseProductColor('img_'+myName,choosenID);
                    }

                    cnt++;
                    itr++;
                });

        //}
                if(itr<=0){
                htmlOut += 'No image found for moodboard';
                chooseProductColor('img_'+myName,'');
                }

                $(tempImgWrap).empty().html(htmlOut);
                if(!colorBoxExists)
                $(meObj).next('.chosen-container').after(tempImgWrap);
            },
            error:function(){
                console.log('Could not completed.');
            }
            
        });
    
        }
        else{
            // add button at the end of select box to process further
            /*if(!$(meObj).siblings('.pageToPromote').length)
            $(meObj).next('.chosen-container').after('<input type="button" class="pageToPromote" value="Continue & select position" title="Add selected page to promote moodboard then proceed to choose template and position">');
            //$(meObj).next('.chosen-container').css('border','thin solid red');
            console.log('test message');*/

            // TO SHOW THE POSITIONS FOR SELECTED PAGE TO DECIDE ON WHICH POSITION PROMOTED MOODBOARD CONTENT WILL BE DISPLAYED. NOT IN USE FOR NOW, ENABLED IF REQUIED.
            //pageToPromote(meObj);
        }
    }

/*$('select.promote_to_page').change(function(){

        //if($(this).siblings('.pageToPromote').length)
        //console.log($(this).val());
        pageToPromote($(this));
})*/

$(document).on('change','.prodColorOpt input[type=radio]',function(){

    var me = $(this);
    var myValue = me.attr('id');
    var mySiblingName = me.attr('name').replace('color','');
    mySiblingName = 'img'+mySiblingName;
    //console.log(mySiblingName);
    
    chooseProductColor(mySiblingName,myValue);
    //console.log('test');
})

var chooseProductColor = function(mySiblingName,value){

    $('#'+mySiblingName).val(value);
}

var pageToPromote = function(meSel){
    var meAsObj = $(meSel);
    var myValues = meAsObj.val();
    var myHtmlPos = '';
    var t = meAsObj.find('option:selected');
    var afterMe = $(meAsObj).siblings('.chosen-container');
    // IF OPTIONS NOT EXISTS
    if(!(meAsObj.siblings('.layout').length))
    {
        var myHtmlObj = $('<div class="layout"></div>');
        $(t).each(function(){

            var myLable = $(this).text();
            var myVal = $(this).val();
            //var myLable = $(this).find('option:selected').text();
            myHtmlPos += '<div class="'+myVal+'"><label for="'+myVal+'">'+myLable+':</label>&nbsp;<select id="'+myVal+'" class="fixed-width-xl"><option>TopContent</option><option>MiddelContent</option></select></div>';
        });
        
        $(myHtmlObj).html(myHtmlPos);
        $(afterMe).after(myHtmlObj);
    }
    else
    {
        if(!myValues)
        {
            $('.layout').hide().remove();
        }
        else
        {
            var myHtmlObj = $('.layout');
            $(t).each(function(){

                var myLable = $(this).text();
                var myVal = $(this).val();
                if(!$('.layout div.'+myVal).length)
                myHtmlPos += '<div class="'+myVal+'"><label for="'+myVal+'">'+myLable+':</label>&nbsp;<select id="'+myVal+'" class="fixed-width-xl"><option>TopContent</option><option>MiddelContent</option></select></div>';
            });
        
            $(myHtmlObj).append(myHtmlPos);
            // FOR REMOVAL OF EXISTING ELEMENT IF IT WAS EXISTS AND NOW REMOVED.
            var meAsExists = $(myHtmlObj).find('div');

            $(meAsExists).each(function(){
                var tempClass = $(this).attr('class');
                //if(!$.inArray(tempClass,meAsObj.val()))
                if(myValues.indexOf(tempClass)<0)
                    $(this).slideUp().remove();
            });
        
        }
    }
}

$(document).on('click','.pageToPromote',function(){

        var meAsObj = $(this);
        var mySelect = $(this).siblings('select');
        var mySelectVal = $(this).siblings('select').val();
        var myHtmlObj = $('<div class="layout"></div>');
        var myHtmlPos = '';

        var t = $(mySelect).find('option:selected');

        $(t).each(function(){

            var myLable = $(this).text();
            var myVal = $(this).val();
            //var myLable = $(this).find('option:selected').text();
            myHtmlPos += '<div class="'+myVal+'"><label for="'+myVal+'">'+myLable+':</label>&nbsp;<select id="'+myVal+'" class="fixed-width-xl"><option>TopContent</option><option>MiddelContent</option></select></div>';
        });
        
        $(myHtmlObj).html(myHtmlPos);
        $(meAsObj).after(myHtmlObj);

});

$(document).on('click','.getPreview',function(){
    var myForm = $('form#configuration_form :input');
    var selectedInput = new Object;
    var selectedItem = new Object;
    var ref_link = $('#mytoken').val();

    $.each(myForm,function(){
        //console.log($(this).attr('name'));
        if($(this).attr('type')=='radio')
            if($(this).is(':checked'))
            {
                var myName = $(this).attr('name');
                selectedInput[myName] = $(this).val();
                //console.log($(this).val()+' name:'+myName);

                if(myName=='template'){
                   selectedItem[myName] = new Object;
                   selectedItem[myName]['id'] = $(this).val();
                }
            }
    })


    $.each(selectedInput,function(index,value){

        if(index.search('opt_')>=0)
        {
            var tempName = 'pos_'+value+'_';
            var mySelVar = index.replace('opt_',tempName);
            selectedItem[index] = new Object;
            selectedItem[index]['type'] = value;
            selectedItem[index]['id'] = $('select#'+mySelVar).val();

            var tempColor = 'color_'+mySelVar;

            // GET CHECKED IMAGE
            var imgWrapID = $('input[name="'+tempColor+'"]:checked').attr('id'); 
            var tempImg = $('label[for="'+imgWrapID+'"] img').attr('src');
            selectedItem[index]['img'] = tempImg;

            if(value=='cms')
            {
                var useCmsData = $('input[name="flag_'+mySelVar+'"]:checked').val();
                var cmsTitle = '';
                var cmsDesc = '';

                if(typeof useCmsData != 'undefined')
                {
                    cmsTitle = $('#title_'+mySelVar).val();
                    cmsDesc = $('#desc_'+mySelVar).val();
                }

                selectedItem[index]['cms-data-flag'] = useCmsData;
                selectedItem[index]['cms-title'] = cmsTitle;
                selectedItem[index]['cms-desc'] = cmsDesc;
            }
        }
    })

    //console.log(selectedItem);

    // LOAD TEMPLATE AND SHOW PREVIEW
    $.ajax({
            type: 'POST', 
            url : ref_link,
            data:{'id_temp':selectedItem.template.id,'data':selectedItem},
            cache:false,
            dataType:'html',
            success:function(data){
                $('#myModal').find('.modal-body').html(data);
                $('#myModal').modal('show');
            },
            error:function(){}
    });

})

});