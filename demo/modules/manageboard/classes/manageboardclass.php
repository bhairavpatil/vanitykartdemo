<?php
//ddd('id');
class ManageboardClass extends ObjectModel
{
	public $id;
	public $title;
	public $temp_id;
	public $template_file_name;
	public $data;
	public $created;
	public $changed;
	public $status;
	//public $approved;

	public static $definition = array(
		'table' => 'moodboard_content',
		'primary' => 'id',
		'fields' => array(
			'title' => 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
			'temp_id' => array('type' => self::TYPE_INT),
			'template_file_name' => array('type' => self::TYPE_STRING),
			'data' => array('type' => self::TYPE_STRING),
			'created' => array('type' => self::TYPE_INT),
			'changed' => array('type' => self::TYPE_INT),
			'status' => array('type' => self::TYPE_BOOL),
			)
		);
}