
<div class="event-sec">
	<div class="event-sec-title">
		<img src="{$img_dir}flower.png" alt="{l s='Latest for you'}"> Latest for you
	</div> <!--/event-sec-title-->
	
	{if isset($article_result)}
		<ul class="event-block" id="test">
			{foreach from=$article_result key=k item=results}
				<li>
					<p class="category_name">{$k}</p>
					{foreach from=$results key=k1 item=result}
						<p><a href="content/{$result.id_cms}-{$result.link_url}">{$result.title}</a> </p> 
					{/foreach}
				</li>
			{/foreach}
		</ul>
	{/if}
</div> <!--/event-sec-->

<!-- get all banners except first -->
{hook h='bannerDisplayRight' section='latest-news' showbanner='1'}