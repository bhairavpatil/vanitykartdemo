<!--Start From Mood-board Section (template_1.tpl & template_2.tpl ) -->

		<div class="col-lg-3 col-md-3">
			<div class="event-sec">
				<div class="event-sec-title">
					<img src="{$img_dir}flower.png" alt="{l s='Latest for you'}"> Latest for you
				</div> <!--/event-sec-title-->
				
				{if isset($article_result)}
					
					<ul class="event-block" id="nt-example1">
						{foreach from=$article_result key=k item=results}
							<li>
								<p class="category_name">{$k}</p>
								{foreach from=$results key=k1 item=result}
									<p><a href="content/{$result.id_cms}-{$result.link_url}">{$result.title}</a> </p> 
								{/foreach}
							</li>
						{/foreach}
					</ul>
					
				{/if}
			</div> <!--/event-sec-->
		</div> <!--/col-lg-3-->
	</div> <!--/row-->
</div> <!--/mood-board-sec-->

