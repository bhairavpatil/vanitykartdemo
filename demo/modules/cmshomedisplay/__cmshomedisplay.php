<?php

if(!defined('_PS_VERSION_'))
	exit;

class CmsHomeDisplay extends Module
{
	function __construct()
	{
		$this->name  = 'cmshomedisplay';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->need_instance = 0;
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('cms display on homepage');
		$this->description = $this->l('by enabling this module allow us to display the cms content on homepage');

	}

	public function install()
	{
		if(!parent::install() OR
			!$this->registerHook('homeDisplayRight') OR
			!$this->registerHook('categoryDisplayRight') OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('displayFooter') OR
			!$this->registerHook('displayHome')
			)
			return false;
		return true;
	}

	//Display news and articles on homepage right side
	public function hookHomeDisplayRight($params)
	{	
		$default_lang = $this->context->language->id;
		$sql = Db::getInstance()->executeS('SELECT pcl.id_cms, pcl.meta_title, pcl.link_rewrite, pc.cat_assoc FROM `ps_cms` as pc , `ps_cms_lang` as pcl where pc.id_cms = pcl.id_cms and pc.active = 1 and pc.home_active = 1 and pcl.id_lang = '.$default_lang.' and pc.cat_assoc != \'\' order by pcl.id_cms desc');


		//Fetch all Category
		$sql1 = Db::getInstance()->executeS('SELECT id_category, name FROM `ps_category_lang` where id_lang = '.$default_lang.'');	
		$cat_array = array();
		foreach ($sql1 as $key => $value) {				
			$cat_array[$value['id_category']] = $value['name'];				
		}

		foreach ($sql as $key => $value) {
			
			$article_result[$cat_array[$value['cat_assoc']]][$value['id_cms']] = array('id_cms' => $value['id_cms'], 'category' => $cat_array[$value['cat_assoc']], 'title' => $value['meta_title'], 'link_url' => $value['link_rewrite']);
		}
	
		if(isset($article_result)){
			$this->context->smarty->assign(
			      array(
			          'article_result' => $article_result		          
			      )
			  );
		}
		return $this->display(__FILE__, 'homedisplayright.tpl');
	}

	//Display news and articles on category page right side
	public function hookCategoryDisplayRight($params)
	{	
		$default_lang = $this->context->language->id;
		$sql = Db::getInstance()->executeS('SELECT pcl.id_cms, pcl.meta_title, pcl.link_rewrite, pc.cat_assoc FROM `ps_cms` as pc , `ps_cms_lang` as pcl where pc.id_cms = pcl.id_cms and pc.active = 1 and pc.cat_active = 1 and pcl.id_lang = '.$default_lang.'');


		//Fetch all Category
		$sql1 = Db::getInstance()->executeS('SELECT id_category, name FROM `ps_category_lang` where id_lang = '.$default_lang.'');	
		$cat_array = array();
		foreach ($sql1 as $key => $value) {				
			$cat_array[$value['id_category']] = $value['name'];				
		}

		foreach ($sql as $key => $value) {
			
			$article_result[$cat_array[$value['cat_assoc']]][$value['id_cms']] = array('id_cms' => $value['id_cms'], 'category' => $cat_array[$value['cat_assoc']], 'title' => $value['meta_title'], 'link_url' => $value['link_rewrite']);
		}
	
		if(isset($article_result)){
			$this->context->smarty->assign(
			      array(
			          'article_cat_result' => $article_result		          
			      )
			  );
		}
		return $this->display(__FILE__, 'categorydisplayright.tpl');
	}

	public function hookDisplayHeader()
	{
	  $this->context->controller->addCSS($this->_path.'/views/css/cmshomedisplay.css', 'all');
	  //$this->context->controller->addJS($this->_path .'/views/js/newsbox.js');
	}
	
	/* public function hookDisplayFooter()
	{
	  $this->context->controller->addJS($this->_path.'/views/js/newsbox.js', 'all');
	} */
	
	public function hookDisplayHome()
	{
	  return $this->display(__FILE__, 'articledisplay.tpl');
	}

}


