<?php

if(!defined('_PS_VERSION_'))
	exit;

class HotProducts extends Module
{
	public function __construct()
	{
		$this->name = 'hotproducts';
		$this->tab = 'front_office_features';
		$this->author = 'sumit jain';
		$this->version = '1.0';

		$this->bootstrap = true;
		$this->ps_versions_compliancy = array('min' => '1.5' , 'max' => _PS_VERSION_);

		$this->displayName = $this->l('Hot Product');
		$this->description = $this->l('Display hot product');

		parent::__construct();
	}

	public function install()
	{
		if(!parent::install() ||
			!$this->registerHook('displayHome') ||
			!$this->registerHook('hotProducts')
			)
			return false;
		return true;
	}

	public function unisntall()
	{
		if(!parent::uninstall())
			return false;
		return true;
	}
	

	public function hookHotProducts($params){
		
		//ddd($params);
		
		$current_category_id = 3;

		if(isset($current_category_id))
			$category = new Category((int)$current_category_id);
		
		for ($i=0; $i <3 ; $i++)
		{ 
			$hot_product_arr['beauty'][$i] = $category->getProducts((int)Context::getContext()->language->id, $i+1, 4, 'id_product', 'asc');
		}
		


		$current_category_id = 15;

		if(isset($current_category_id))
			$category = new Category((int)$current_category_id);
		
		for ($i=0; $i <1 ; $i++)
		{
			$hot_product_arr['fashion'][$i] = $category->getProducts((int)Context::getContext()->language->id, $i+1, 4, 'id_product', 'asc');
		}

		//ddd($hot_product_arr);
		if(is_array($hot_product_arr) && count($hot_product_arr))
		{
			$this->context->smarty->assign(array(
					'hot_product_arr' => $hot_product_arr
				));
		}
		
		return $this->display(__FILE__,'hotproducts.tpl');
		
	}
	
	public function hookDisplayHome($params)
	{	
		
		return $this->display(__FILE__,'homehotproducts.tpl');
	}


}