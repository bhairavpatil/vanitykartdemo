<?php
if(!defined('_PS_VERSION_'))
	exit;
class RelatedArticle extends Module
{
	public function __construct()
	{
		$this->name = 'relatedarticle';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Bhairav';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		$this->need_instance = 0;
		$this->bootstrap = true;
		$this->displayName = $this->l('Related Article');
		$this->description = $this->l('Related Article');
		parent::__construct();
	}

	public function install()
	{
		if(
			!parent::install() OR
			!$this->registerHook('displayHeader') OR
			!$this->registerHook('reletedarticleright')
			)
			return false;
		return true;
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'/css/article.css');
	}

	public function hookReletedArticleRight()
	{
		$arrarticles = $this->getarticles();
		$this->context->smarty->assign(array('myarticles'=>$arrarticles));


		return $this->display(__FILE__,"relatedarticle.tpl");
	}

	public function getarticles($params)
	{

		$context = Context::getContext();		
		$id = $context->controller->cms->id; //9;
		$dataarticles = Db::getInstance()->getRow('SELECT * FROM ps_cms_lang where id_cms = '.$id);
		// ddd($dataarticles['meta_keywords']);

		$arrtags = explode(",", $dataarticles['meta_keywords']);

		foreach ($arrtags as $tag) {

				$tag = trim($tag);

				$a[$tag] = 0;

				// $x=array();
				$y=array();
				$dataotherarticles = Db::getInstance()->ExecuteS('SELECT * FROM ps_cms_lang a , ps_cms b where a.id_cms <> '.$id." and a.id_cms = b.id_cms and b.active = 1 order by a.id_cms desc");  
				foreach ($dataotherarticles as $article) {

					$arrotherarticles = explode(",", $article['meta_keywords']);

					foreach ($arrotherarticles as $otherarticlestag) {

						$otherarticlestag = trim($otherarticlestag);

						if($tag == $otherarticlestag)
						{							
							$resultarr[$article['id_cms']] = $resultarr[$article['id_cms']]+1;

							// $valarr = $resultarr;
						}
					}
					
				}


		}



		$sortedarray = $resultarr;
		arsort($sortedarray);
		// ddd($sortedarray);
		$n = 0;
		foreach ($sortedarray as $outoutarticle=>$val) {
				$n++;
				if($n < 5)
				{

					$sql = 'SELECT * FROM ps_cms_lang where id_cms = '.$outoutarticle;
					$dataoutoutarticle = Db::getInstance()->getRow($sql); 

					$outoutarticlearr[$n]['name'] = $dataoutoutarticle['meta_title'];
					$outoutarticlearr[$n]['url'] = $dataoutoutarticle['id_cms']."-".$dataoutoutarticle['link_rewrite'];
					$outoutarticlearr[$n]['id'] = $dataoutoutarticle['id_cms'];
				}

		}
		// ddd($outoutarticlearr);
		return $outoutarticlearr;
	}
}