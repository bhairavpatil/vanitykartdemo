<div class="related-articles">
    <div class="news-title-static">                  
    	<span class="news-title-inside"><img src="{$img_dir}flower5.png" alt="{l s='Flower'}"/>Related Articles</span>    
    </div>      
    {foreach from=$myarticles item=article}   
        <div class="news-section">
            <div class="articleContent nonebdr">                          
                <h5><a href="{$article.url}">{$article.name}</a></h5>                                                
            </div>
        </div>
        <hr>
    {/foreach}
</div> <!--/related-articles-->

<!-- get all banners except first -->
{hook h='bannerDisplayRight' section='related-news' showbanner='2'}