{if isset($videogallery_list) && !empty($videogallery_list)}
<ul>        
    {foreach name=outer from=$videogallery_list key=k1 item=list}
    <li>
        <div>
            {$videogallery.$k1.name}
        </div>
        <ul>
            {foreach from=$list key=k2 item=listtem}
            <div>
                <ul>{$k2} 
                	<div>
                	{if isset($listtem.title) && !empty($listtem.title)}
	               		 <li>{$listtem.title}</li>
	                {/if}
	                {if isset($listtem.description) && !empty($listtem.description)}
	               		 <li>{$listtem.description}</li>
	                {/if}
	                {if isset($listtem.target_url) && !empty($listtem.target_url)}
	               		 <li>{$listtem.target_url}</li>
	                {/if}
	                {if isset($listtem.videourl) && !empty($listtem.videourl)}
	               		 <li><iframe width="150" height="100" src="{$listtem.videourl}"></iframe></li>
	                {/if}	               
	              
	                </div> </br></br>
                </ul>
            </div>
            {/foreach}
        </ul>

    </li>
    {/foreach}
</ul>
{/if}