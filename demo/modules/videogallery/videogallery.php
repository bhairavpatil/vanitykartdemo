<?php

if(!defined('_PS_VERSION_'))
	exit;


class VideoGallery extends Module
{	
	private $html = '';
	private $errors = '';

	public function __construct()
	{
		$this->name = 'videogallery';
		$this->tab = 'front_office_features';	
		$this->author = 'sumit jain';
		$this->version = '1.0';
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);

		$this->need_instance = 0;
		$this->bootstrap = true;

		$this->displayName = $this->l('Video Gallery');
		$this->description = $this->l('Assign Video gallery to product and other pages');

		$this->table_name1 = 'video_gallery_list';
		$this->table_name2 = 'video_gallery_list_item';

		parent::__construct();	
	}

	public function install()
	{
		if(!parent::install()  ||
			!$this->createTabs() ||
			//!$this->installMyTable() ||
			!$this->registerHook('displayHeader') ||
			!$this->registerHook('videoGalleryOnCms') || 
			!$this->registerHook('VideoGalleryOnCategory') ||
			!$this->registerHook('backOfficeHeader')
			)
			return false;
		return true;
	}

	public function uninstall()
	{
		if(!parent::uninstall() ||
			//!$this->removeMyTable() ||
			!$this->eraseTabs()
			)
			return false;
		return true;
	}

/*	private function installMyTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.'video_gallery_list_item` (
				  `vg_list_item_id` int(11) NOT NULL AUTO_INCREMENT,
				  `vg_list_id` int(11) NOT NULL,
				  `title` varchar(256) NOT NULL,
				  `description` text NOT NULL,
				  `videourl` varchar(256) NOT NULL,				  
					PRIMARY KEY (`vg_list_item_id`)
				) ENGINE = ' . _MYSQL_ENGINE_;

		$sql1 = 'CREATE TABLE `'._DB_PREFIX_.'video_gallery_list` (
				  `vg_list_id` int(11) NOT NULL AUTO_INCREMENT,
				  `name` varchar(256) NOT NULL,
				  `cat_flag` tinyint(4) NOT NULL,
				  `category` varchar(256) NOT NULL,
				  `cms_flag` tinyint(4) NOT NULL,
				  `cms_page` varchar(256) NOT NULL,
				  `publish` tinyint(4) NOT NULL,
					PRIMARY KEY (`vg_list_id`)
				) ENGINE = ' . _MYSQL_ENGINE_;		

		if(!Db::getInstance()->Execute($sql))
			return false;
		if(!Db::getInstance()->Execute($sql1))
			return false;
		return true;		
	}

	private function removeMyTable()
	{
		$sql = 'DROP TABLE '._DB_PREFIX_.$this->table_name1;
		$sql1 = 'DROP TABLE '._DB_PREFIX_.$this->table_name2;

		if(!Db::getInstance()->Execute($sql))
			return false;
		if(!Db::getInstance()->Execute($sql1))
			return false;
		return true;
	}*/


	private function createTabs()
	{
		$tab = new Tab();
		$tab->active = 1;
		$languages = language::getLanguages(false);
		if(is_array($languages))
		{
			foreach ($languages as $language) {
				$tab->name[$language['id_lang']] = $this->l('Video Gallery');
			}
		}
		$tab->class_name = 'Admin'.ucfirst($this->name);
		$tab->module = $this->name;
		$tab->id_parent = 142;

		return (bool)$tab->add();
	}

	private function eraseTabs()
	{
		$id_tab = (int)Tab::getIdFromClassName('Admin'.ucfirst($this->name));
		if($id_tab){
			$tab = new Tab($id_tab);
			$tab->delete();
		}
		return true;
	}

	public function getContent()
	{
		$this->postProcess();
		$this->displayForm();
		return $this->html;
	}

	public function postProcess()
	{
		//ddd(Tools::getAllValues());
		//Add, update Gallery List
		if(Tools::isSubmit('addVideoGalleryList'))
		{
			$gallery_list['name'] = Tools::getValue('name');
			$gallery_list['cat_flag'] = Tools::getValue('cat_flag');
			$gallery_list['cms_flag'] = Tools::getValue('cms_flag');

			//if category flag is true then then value should be selected from category list
			if(Tools::getValue('cat_flag') == 1)
			{
				$main_category_list = Tools::getValue('main_category');
				if(empty($main_category_list))
					$this->errors[] = Tools::displayError('Please Include Category');
				else{
					$gallery_list['category'] = implode(',',$main_category_list);
				}

			}

			//if cms flag is true then then value should be selected from cms list
			if(Tools::getValue('cms_flag') == 1)
			{
				$cms_page_list = Tools::getValue('cms_page');
				if(empty($cms_page_list))
					$this->errors[] = Tools::displayError('Please Include CMS Page');
				else{
					$gallery_list['cms_page'] = implode(',',$cms_page_list);
				}

			}		
		

			if(empty($gallery_list['name']) || !Validate::isGenericName($gallery_list['name']))
					$this->errors[] = Tools::displayError('Please Enter Title');

			if(!$this->errors)
			{
				if(Tools::getValue('vg_list_id'))
				{ 
					if(!Db::getInstance()->update($this->table_name1,$gallery_list, 'vg_list_id = '. (int)Tools::getValue('vg_list_id')))			
						$this->errors[] = Tools::displayError('Error while updating the database'). ': ' . $mysql_error();		
				} else { 
					if(!Db::getInstance()->insert($this->table_name1,$gallery_list))
						$this->errors[] = Tools::displayError('Error while adding to the database'). ': ' . $mysql_error();
				}
				
			}	

			$confirmation = Tools::getValue('vg_list_id') ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');
				
			$conf_value = Tools::getValue('vg_list_id') ? '4' : '3' ;

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else{
				$this->html .= $this->displayConfirmation($confirmation);
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&conf='.$conf_value);	
			}		

		}

		//Add, update Gallery List Item
		if(Tools::isSubmit('addVideoGalleryListItem'))
		{
			$gallery_list_item['title'] = Tools::getValue('title');
			$gallery_list_item['description'] = Tools::getValue('description');
			$gallery_list_item['videourl'] = Tools::getValue('videourl');
			$gallery_list_item['vg_list_id'] = Tools::getValue('vg_list_id');
			$list_item_id = Tools::getValue('vg_list_item_id');

			//If Image is present then add or update
			if(empty($gallery_list_item['videourl']))
					$this->errors[] = Tools::displayError('Please Enter URL');


            if(!$this->errors)
			{ 
				if($list_item_id)
				{ 
					if(!Db::getInstance()->update($this->table_name2,$gallery_list_item, 'vg_list_item_id = '. (int)Tools::getValue('vg_list_item_id')))			
						$this->errors[] = Tools::displayError('Error while updating the database2'). ': ' . $mysql_error();		
				} else { 
					if(!Db::getInstance()->insert($this->table_name2,$gallery_list_item))
						$this->errors[] = Tools::displayError('Error while adding to the database2'). ': ' . $mysql_error();
				}
				
			}

			$confirmation = Tools::getValue('vg_list_item_id') ? $this->l('Entry Successfully updated') : $this->l('Entry Successfully added');
				
			$conf_value = Tools::getValue('vg_list_item_id') ? '4' : '3' ;

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else{
				$this->html .= $this->displayConfirmation($confirmation);
				Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&vg_list_id='.Tools::getValue('vg_list_id').'&viewvideo_gallery_list&conf='.$conf_value);	
			}		


		}

		//Delete Gallery List Item
		if(Tools::isSubmit('deletevideo_gallery_list_item'))
		{			
			$list_item_id = Tools::getValue('vg_list_item_id');
			$result = $this->getSingleGalleryListItem($list_item_id);			
   			
			if(!Db::getInstance()->delete($this->table_name2,'vg_list_item_id = '. (int)$list_item_id))
				$this->errors[] = Tools::displayError('Error while deleting from the database'). ': ' . $mysql_error();

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($this->l('Entry Successfully Removed'));	

			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&vg_list_id='.$result['vg_list_id'].'&viewvideo_gallery_list&conf=2');
		}

		//Delete Gallery List and its associated item
		if(Tools::isSubmit('deletevideo_gallery_list'))
		{	
				
			$list_id = Tools::getValue('vg_list_id');			
   			
			if(!Db::getInstance()->delete($this->table_name2,'vg_list_id = '. (int)$list_id))
				$this->errors[] = Tools::displayError('Error while deleting from the database'). ': ' . $mysql_error();
			if(!Db::getInstance()->delete($this->table_name1,'vg_list_id = '. (int)$list_id))
				$this->errors[] = Tools::displayError('Error while deleting from the database'). ': ' . $mysql_error();

			if($this->errors)
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));
			else
				$this->html .= $this->displayConfirmation($this->l('Entry Successfully Removed'));	
			
		}

		//Publish unpublish gallery list
		if(Tools::isSubmit('togglevideo_gallery_list'))
		{						
			$list_id = Tools::getValue('vg_list_id');			
   			
			if($this->toggleGalleryStatus($list_id))
				$this->html .= $this->displayConfirmation($this->l('Entry Status Changed'));
			else
				$this->html .= $this->displayError(implode($this->errors,'<br/>'));		
			
			Tools::redirectAdmin($this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&conf=6');
			
		}


	}

	public function toggleGalleryStatus($list_id)
	{		
		if(Db::getInstance()->getValue('SELECT publish FROM '._DB_PREFIX_.$this->table_name1.' where vg_list_id = '.(int)$list_id))
		{ 
			//Disable it
			if(!Db::getInstance()->update($this->table_name1, array('publish' => 0) , 'vg_list_id = '.(int)$list_id))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;

		}
		else //Enable it
		{
			if(!Db::getInstance()->update($this->table_name1, array('publish' => 1) , 'vg_list_id = '.(int)$list_id))
				$this->errors[] = Tools::displayError('Error '). ': ' . $mysql_error();
			else
				return true;
		}
	}

	public function displayForm()
	{ 
		if(Tools::isSubmit('createVideoGalleryList')) 
			$this->html .= $this->generateGalleryListForm();
		elseif (Tools::isSubmit('updatevideo_gallery_list') || Tools::isSubmit('addVideoGalleryList')) 
			$this->html .= $this->generateGalleryListForm(true);
		elseif (Tools::isSubmit('viewvideo_gallery_list')) 
			$this->html .= $this->generateGalleryListItem();
		elseif (Tools::isSubmit('createVideoGalleryListItem') ) 
			$this->html .= $this->generateGalleryListItemForm();	
		elseif (Tools::isSubmit('updatevideo_gallery_list_item') || Tools::isSubmit('addVideoGalleryListItem')) {
			//$this->html .= '<link rel="stylesheet" type="text/css" media="screen, projection" href="../modules/Videogallery/views/css/Videogallery.css" /> ';				
			$this->html .= $this->generateGalleryListItemForm(true);				
		}
		else
			$this->html .= $this->generateGalleryList();			
				
	}

	public function generateGalleryListForm($editing = false)
	{

		$inputs = array();

		//if list id is present then only editing is true
		$list_id = Tools::getValue('vg_list_id');
		if(!empty($list_id))
			$editing = true;
		else
			$editing = false; 
			
		if($editing)
		{
			$inputs[] =  array(
					'type' => 'hidden',
					'name' => 'vg_list_id'
				);
		}


		//Fetch Main Category List under the home category
		$catlist = $this->getMainCategory();

		//Added home category explicitly in main category array
		$catlist[] = array('id_category' => 2, 'name' => 'Home');
	
		//sort multidimensional array
		usort($catlist, function($a, $b) {
   			return $a['id_category'] - $b['id_category'];
		});
	
		//Fetch CMS List
		$cms_list = $this->getCMSList();
		
		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Gallery Title'),
				'name' => 'name',
			);

		$inputs[] = array(
	                    'type' => 'switch',
	                    'label' => $this->l('Include Category'),
	                    'name' => 'cat_flag',
	                    'required' => false,
	                    'is_bool' => true,
	                    'values' => array(
	                        array(
	                            'id' => 'active_on',
	                            'value' => 1,
	                            'label' => $this->l('Enabled')
	                        ),
	                        array(
	                            'id' => 'active_off',
	                            'value' => 0,
	                            'label' => $this->l('Disabled')
	                        )
	                    ),
                	);

		//Select Main Category		
		$inputs[] = array(
                    'type' => 'select',
                    'label' => '',
                    'class' => 'chosen',
                    'multiple' => true,
                    'name' => 'main_category',                                
                    'options' => array(                       
                        'query' => $catlist,
                        'id' => 'id_category',
                        'name' => 'name'
                    ),
                );

		$inputs[] = array(
	                    'type' => 'switch',
	                    'label' => $this->l('Include CMS Page'),
	                    'name' => 'cms_flag',
	                    'required' => false,
	                    'is_bool' => true,
	                    'values' => array(
	                        array(
	                            'id' => 'active_on',
	                            'value' => 1,
	                            'label' => $this->l('Enabled')
	                        ),
	                        array(
	                            'id' => 'active_off',
	                            'value' => 0,
	                            'label' => $this->l('Disabled')
	                        )
	                    ),
                	);

		//Select CMS Article		
		$inputs[] = array(
                    'type' => 'select',
                    'label' => '',
                    'class' => 'chosen',
                    'multiple' => true,
                    'name' => 'cms_page',                                
                    'options' => array(                       
                        'query' => $cms_list,
                        'id' => 'id_cms',
                        'name' => 'meta_title'
                    ),
                );

		
		$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $editing ? $this->l('Update Gallery') : $this->l('Create New Gallery'),
						'icon' => 'icon-cogs'
						),
					'input' => $inputs,
					'submit' => array(
						'title' => $editing ? $this->l('Update') : $this->l('Add'),
						'class' => 'btn btn-default pull-right'
						),
					'buttons' => array(
					        array(
					            'href' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&token='.Tools::getAdminTokenLite('AdminModules'),
					            'title' => $this->l('Cancel'),
					            'icon' => 'process-icon-cancel'
					        )
					    )
					)			
			);

		
		if(!$editing)
		{
			foreach($inputs as $input)
				$values[$input['name']] = '';
		}
		

		$helper = new HelperForm();
		$helper->submit_action = 'addVideoGalleryList';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues($editing,$list_id)
			);


		return $helper->generateForm(array($fields_form));		
	} 

	public function getConfigFieldsValues($editing,$list_id)
	{	
				
		$values = $this->getSingleGalleryList($list_id);
		return array(
			'vg_list_id' => $values['vg_list_id'],
			'name' => $values['name'],
			'cat_flag' => $values['cat_flag'], 
			'cms_flag' => $values['cms_flag'], 
			'main_category[]' => explode(',', $values['category']), 
			'cms_page[]' => explode(',',$values['cms_page']) 
		);

		
	}


	public function generateGalleryListItemForm($editing = false)
	{
			
		//if list item id is present then only editing is true
		$list_item_id = Tools::getValue('vg_list_item_id');
		if(!empty($list_item_id))
			$editing = true;
		else
			$editing = false; 

		if($editing)
		{
			$inputs[] =  array(
					'type' => 'hidden',
					'name' => 'vg_list_item_id'
				);	

			$inputs[] =  array(
					'type' => 'hidden',
					'name' => 'vg_list_id'
				);			
		}

	/*	$list_item_row = $this->getSingleGalleryListItem(Tools::getValue('vg_list_item_id'));
		$thumb_url = _PS_BASE_URL_.__PS_BASE_URI__.'img/gallery'.'/'.$list_item_row['image'];
		$thumb_dir_url = _PS_IMG_DIR_.'gallery'.'/'.$list_item_row['image'];
		$image_size = file_exists($thumb_dir_url) ? (filesize($thumb_dir_url) / 1000) : false;*/
				
	 	$result_item = $this->getSingleGalleryListItem(Tools::getValue('vg_list_item_id'));
		
		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Video Title'),
				'name' => 'title',
			);

		$inputs[] = array(
				'type' => 'textarea',
				'label' => $this->l('Video Description'),
				'name' => 'description',
			);

		$inputs[] = array(
				'type' => 'text',
				'label' => $this->l('Video URL'),
				'name' => 'videourl',
			);

		/*$inputs[] = array(
				'type' => 'file',
				'label' => $this->l('Choose Image'),
				'desc' => $this->l('Upload an image to show in gallery'),
				'name' => 'image',
				'display_image' => true,
	    		'thumb' => $thumb_url ? $thumb_url : false,
	    		'size' => $image_size,
	    		'hint' => $this->l('small size images are not allowed')
			);*/

		
		$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $editing ? $this->l('Update Gallery List Item') : $this->l('Create New Gallery List Item'),
						'icon' => 'icon-cogs'
						),
					'input' => $inputs,
					'submit' => array(
						'title' => $editing ? $this->l('Update') : $this->l('Add'),
						'class' => 'btn btn-default pull-right'
						),
					'buttons' => array(
					        array(
					            'href' => $this->context->link->getAdminLink('AdminModules').'&configure='.$this->name.'&tab_module='.$this->tab.'&vg_list_id='.$result_item['vg_list_id'].'&viewvideo_gallery_list&token='.Tools::getAdminTokenLite('AdminModules'),
					            'title' => $this->l('Cancel'),
					            'icon' => 'process-icon-cancel'
					        )
					    )
					)			
			);

			
		if(!$editing)
		{
			foreach($inputs as $input)
				$values[$input['name']] = '';
		}
		else
		{			
			$values = $result_item;
		}

		$helper = new HelperForm();
		$helper->submit_action = 'addVideoGalleryListItem';

		if(!$editing)
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name.'&vg_list_id='.Tools::getValue('vg_list_id');
		else
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name.'&updatevideo_gallery_list_item&vg_list_item_id='.Tools::getValue('vg_list_item_id');

		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $values
			);

	

		return $helper->generateForm(array($fields_form));		
	}

	//Form for gallery list level1
	public function generateGalleryList()
	{
		$content = $this->getAllGalleryList();

		$fields_list = array(
			'vg_list_id' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs',
				'orderby' => false,
				'search' => false
				),
			'name' => array(
				'title' => $this->l('Gallery Title'),
				'orderby' => false,
				'search' => false				
				),
			'publish' => array(
				'title' => $this->l('Publish'),
				'active' => 'toggle',
				'type' => 'bool',	
				'orderby' => false,
				'search' => false				
				)
			);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->actions = array('view','edit','delete');
		//$helper->show_toolbar = true;
		//$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar('gallerylist');		
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'vg_list_id';
		$helper->title = $this->l('List of Video Gallery');
		$helper->table = 'video_gallery_list';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;

		return $helper->generateList($content,$fields_list);
	}

	
	//Form for gallery list item level2
	public function generateGalleryListItem()
	{
		$vg_list_id = Tools::getValue('vg_list_id');
		$content = $this->getAllGalleryListItem($vg_list_id);

		//To Display Image in helper list section
		foreach ($content as $key => $value) {
			$value['videourl'] = '<iframe width="150" height="100" src="'.$value['videourl'].'"></iframe>';
			$content[$key] = $value;
		}

		$fields_list = array(
			'vg_list_item_id' => array(
				'title' => 'ID',
				'align' => 'center',
				'class' => 'fixed-width-xs',
				'orderby' => false,
				'search' => false
				),
			'title' => array(
				'title' => $this->l('Video Title'),
				'orderby' => false,
				'search' => false			
				),
			'videourl' => array(
				'title' => $this->l('Video'),
				'align' => 'center',
				'orderby' => false,
				'search' => false,
				'float' => true // a trick - prevents from html escaping							
				)
			);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->actions = array('edit','delete');
		//$helper->show_toolbar = true;
		//$helper->toolbar_scroll = true;
		$helper->toolbar_btn = $this->initToolbar('gallerylistitem');		
		$helper->module = $this;
		$helper->listTotal = count($content);
		$helper->identifier = 'vg_list_item_id';
		$helper->title = $this->l('List Item of Video Gallery');
		$helper->table = 'video_gallery_list_item';
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules',false).'&configure='.$this->name.'&module_name='.$this->name;

		return $helper->generateList($content,$fields_list);
	}


	public function initToolbar($type)
	{
		$getToken = Tools::getAdminTokenLite('AdminModules');

		if($type == 'gallerylist'){
			$this->toolbar_btn['new'] = array(
					'href' => $this->context->link->getAdminLink('AdminModules',false).'&token='.$getToken.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&createVideoGalleryList',
					'desc' => $this->l('Add New')
				);
		}

		if($type == 'gallerylistitem'){
			$this->toolbar_btn['new'] = array(
					'href' => $this->context->link->getAdminLink('AdminModules',false).'&token='.$getToken.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&createVideoGalleryListItem&vg_list_id='.Tools::getValue('vg_list_id'),
					'desc' => $this->l('Add New')
				);

			$this->toolbar_btn['back'] = array(
					'href' => $this->context->link->getAdminLink('AdminModules',false).'&token='.$getToken.'&configure='.$this->name.'&tab_module='.$this->tab,
					'desc' => $this->l('back to the gallery list')
				);
		}

		return $this->toolbar_btn;
	}

	public function getAllGalleryListItem($id)
	{
		return Db::getInstance()->ExecuteS('
				SELECT * 
				FROM '._DB_PREFIX_.$this->table_name2.
				' where vg_list_id ='.(int)$id
			);
	}

	public function getSingleGalleryListItem($id)
	{
		return Db::getInstance()->getRow('
			SELECT *
			FROM '._DB_PREFIX_.$this->table_name2.
			' where vg_list_item_id ='.(int)$id
			 );
	}

	public function getAllGalleryList()
	{
		return Db::getInstance()->ExecuteS('
				SELECT * 
				FROM '._DB_PREFIX_.$this->table_name1);
	}

	public function getSingleGalleryList($id)
	{
		return Db::getInstance()->getRow('
			SELECT *
			FROM '._DB_PREFIX_.$this->table_name1.
			' where vg_list_id ='.(int)$id
			 );
	}

	public function getMainCategory()
	{
		$sql = new DbQuery();
		$sql->select('cl.id_category,cl.name');
		$sql->from('category','c');
		$sql->innerJoin('category_lang','cl','c.id_category = cl.id_category');
		$sql->where('c.id_parent = 2');
		$sql->orderBy('c.id_category');
		return Db::getInstance()->executeS($sql);
	}

	public function getCMSList()
	{
		$sql = new DbQuery();
		$sql->select('cl.id_cms,cl.meta_title');
		$sql->from('cms','c');
		$sql->innerJoin('cms_lang','cl','c.id_cms = cl.id_cms and cl.id_lang = '.$this->context->language->id);		
		$sql->where('c.active = 1');
		$sql->orderBy('c.id_cms');
		return Db::getInstance()->executeS($sql);
	}

	public function hookBackOfficeHeader($params)
	{
		$this->context->controller->addCSS(_THEME_CSS_DIR_.'custom_admin.css');
		$this->context->controller->addJS(_THEME_JS_DIR_.'custom_admin.js');
	}

	public function hookVideoGalleryOnCms($params)
	{
		if($this->context->controller->php_self == 'cms')
		{
			$pageid = $this->context->controller->cms->id;
			$gallery_result = $this->getVideoGalleryList($pageid,'cms');
			if(isset($gallery_result) && !empty($gallery_result))
			{	

				foreach ($gallery_result as $key => $value) {					
					$gallery_list_ids[] = $value['id'];
				}
				
				$gallery_list_item_arr = $this->getVideoGalleryListItem($gallery_list_ids);	
				//ddd($gallery_result);
				if(isset($gallery_list_item_arr) && !empty($gallery_list_item_arr))
				{
					$this->context->smarty->assign(array(
							'videogallery_list' => $gallery_list_item_arr,
							'videogallery' => $gallery_result
						));
				}
			}

			return $this->display(__FILE__,'videogallery.tpl');

		}
	}

	public function hookVideoGalleryOnCategory($params)
	{
		//if($this->context->controller->php_self == 'index');

		//$pageid = $this->context->controller->cms->id;
		$pageid = 2;
		$gallery_result = $this->getVideoGalleryList($pageid,'category');
		if(isset($gallery_result) && !empty($gallery_result))
		{
			foreach ($gallery_result as $key => $value) {					
				$gallery_list_ids[] = $value['id'];
			}
			
			$gallery_list_item_arr = $this->getVideoGalleryListItem($gallery_list_ids);	
			//ddd($gallery_result);
			if(isset($gallery_list_item_arr) && !empty($gallery_list_item_arr))
			{
				$this->context->smarty->assign(array(
						'videogallery_list' => $gallery_list_item_arr,
						'videogallery' => $gallery_result
					));
			}
		}

		return $this->display(__FILE__,'videogallery.tpl');
		
	}

	public function getVideoGalleryList($pageid,$type)
	{
		if($type == 'cms')
		{
			$gallery_list = Db::getInstance()->ExecuteS('
					SELECT * 
					FROM '._DB_PREFIX_.$this->table_name1.
					' where cms_flag = 1 and publish = 1');
		} 
		else 
		{

			$gallery_list = Db::getInstance()->ExecuteS('
					SELECT * 
					FROM '._DB_PREFIX_.$this->table_name1.
					' where cat_flag = 1 and publish = 1');
		}

		foreach ($gallery_list as $key => $value) {
			if($type == 'cms')
				$pageArr = explode(',', $value['cms_page']);
			else
				$pageArr = explode(',', $value['category']);
			if(in_array($pageid, $pageArr))
			{
				$gallery_list_id_arr[$key]['id'] =  $value['vg_list_id'];
				$gallery_list_id_arr[$key]['name'] =  $value['name'];
			}
		}

		if(isset($gallery_list_id_arr)){
			$gallery_list_id_arr = array_values($gallery_list_id_arr);
			return $gallery_list_id_arr;
		}
		else
			return false;
	}

	public function getVideoGalleryListItem($gallery_list_ids)
	{
		

		foreach ($gallery_list_ids as $key => $value) 
		{
			$gallery_list_item_arr[$key] = Db::getInstance()->ExecuteS('
					SELECT * 
					FROM '._DB_PREFIX_.$this->table_name2.
					' where vg_list_id = '.$value);
		}

		if(isset($gallery_list_item_arr))
			return $gallery_list_item_arr;
		else
			return false;
	}

	

}
