<?php
if(!defined('_PS_VERSION_'))
	exit;
class CategoryLanding extends Module
{
	public function __construct()
	{
		$this->name = 'categorylanding';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Bhairav';
		$this->ps_versions_compliancy = array('min' => '1.5', 'max' => _PS_VERSION_);
		$this->need_instance = 0;
		$this->bootstrap = true;
		$this->displayName = $this->l('Category Landing Page');
		$this->description = $this->l('Category Landing Page');
		parent::__construct();
	}

	public function install()
	{
		if(
			!parent::install() OR
			!$this->registerHook('displayHeader') OR			
			!$this->registerHook('displayHome') 
			)
			return false;
		return true;
	}

	public function hookDisplayHome($params)
	{
		return $this->display(__FILE__, 'categorylanding.tpl');
	}

	public function hookHeader($params)
	{
		$this->context->controller->addJS($this->_path . '/views/js/categorylanding.js');
		$this->context->controller->addCSS($this->_path.'/css/custom.css');
	}

	/*public function hookReletedArticleRight($params,$id)
	{
		ddd($id);
		$arrarticles = $this->getarticles();
		$this->context->smarty->assign(array('myarticles'=>$arrarticles));

		return $this->display(__FILE__,"relatedarticle.tpl");
	}
*/

	public function getarticlesforajax($myvar)
	{


		$arrarticles = $this->getarticles('');

		// ddd($arrarticles);
		$this->context->smarty->assign(array('myarticles'=>$arrarticles));

		return $this->display(__FILE__,"relatedarticle.tpl");
	}
	public function getarticles($params)
	{

		$context = Context::getContext();		
		$id = $context->controller->cms->id; //9;
		$id = 9;
		$dataarticles = Db::getInstance()->getRow('SELECT * FROM ps_cms_lang where id_cms = '.$id);
		// ddd($dataarticles['meta_keywords']);

		$arrtags = explode(",", $dataarticles['meta_keywords']); // girls,conditions,solutions,informations,notice

		foreach ($arrtags as $tag) {


				$a[$tag] = 0;

				// $x=array();
				$y=array();

				$dataotherarticles = Db::getInstance()->ExecuteS('SELECT * FROM ps_cms_lang a , ps_cms b where a.id_cms <> '.$id." and a.id_cms = b.id_cms and b.active = 1 order by a.id_cms desc"); 


				// ddd('SELECT * FROM ps_cms_lang a , ps_cms b where a.id_cms <> '.$id." and a.id_cms = b.id_cms and b.active = 1 order by a.id_cms desc");
				foreach ($dataotherarticles as $article) {

					$arrotherarticles = explode(",", $article['meta_keywords']); //girls,celebration,vendor,Academy,Talent,hospitality,industries,Hotel,government,Partner


					// ddd("sfgasdfsdfg");

					foreach ($arrotherarticles as $otherarticlestag) {

						if($tag == $otherarticlestag)
						{							
							$resultarr[$article['id_cms']] = $resultarr[$article['id_cms']]+1;

							// $valarr = $resultarr;
						}
					}
					
				}


		}



		$sortedarray = $resultarr;
		arsort($sortedarray);
		// ddd($sortedarray);
		$n = 0;
		foreach ($sortedarray as $outoutarticle=>$val) {
				$n++;
				if($n <= 4)
				{
					$sql = 'SELECT * FROM ps_cms_lang where id_cms = '.$outoutarticle;
					$dataoutoutarticle = Db::getInstance()->getRow($sql); 

					$outoutarticlearr[$n]['name'] = $dataoutoutarticle['meta_title'];
					$outoutarticlearr[$n]['url'] = $dataoutoutarticle['id_cms']."-".$dataoutoutarticle['link_rewrite'];
					$outoutarticlearr[$n]['id'] = $dataoutoutarticle['id_cms'];
				}


		}
		// ddd($outoutarticlearr);
		return $outoutarticlearr;
	}
}